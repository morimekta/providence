package net.morimekta.providence.config.impl;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.config.ConfigListener;
import net.morimekta.providence.config.ConfigSupplier;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.test.providence.config.Database;
import net.morimekta.testing.time.FakeClock;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.time.Clock;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FixedConfigSupplierTest {
    @Test
    @SuppressWarnings("unchecked")
    public void testCreate() {
        FakeClock clock = FakeClock.forCurrentTimeMillis(System.currentTimeMillis());
        Database value = Database.builder()
                                 .build();

        ConfigSupplier<Database> supplier = new ConfigSupplier<Database>() {
            final long millis = clock.millis();

            @Override
            public Database get() {
                return value;
            }

            @Override
            public <RM extends PMessage<RM>> ConfigSupplier<RM> reference(PField... fields) throws ConfigException {
                return null;
            }

            @Override
            public void addListener(@Nonnull ConfigListener<Database> listener) {}

            @Override
            public void removeListener(@Nonnull ConfigListener<Database> listener) {}

            @Override
            public String getName() {
                return "test";
            }

            @Override
            public long configTimestamp() {
                return millis;
            }

            @Override
            public Clock getClock() {
                return clock;
            }
        };
        FixedConfigSupplier<Database> fixed = new FixedConfigSupplier<>(supplier);

        clock.tick(1000);

        ConfigSupplier<Database> snapshot = fixed.snapshot();

        assertThat(fixed.get(), is(sameInstance(value)));
        assertThat(snapshot.get(), is(sameInstance(value)));

        assertThat(fixed.configTimestamp(), is(snapshot.configTimestamp()));
        assertThat(fixed.snapshot(), is(sameInstance(fixed)));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testListeners() {
        FixedConfigSupplier<Database> fixed = new FixedConfigSupplier<>(
                Database.builder().build());
        ConfigListener<Database> listener = mock(ConfigListener.class);
        fixed.addListener(listener);
        fixed.removeListener(listener);
        fixed.removeListener(listener);

        // since the object is immutable there is no way to "trigger a change" and verify
        // no calls to the listener...
    }
}
