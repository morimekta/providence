package net.morimekta.providence.config.testing;

import net.morimekta.test.providence.config.Database;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestConfigSupplierTest {
    @Test
    public void testToString() {
        TestConfigSupplier<Database> supplier =
                new TestConfigSupplier<>(Database.builder().build());

        assertThat(supplier.toString(), is("TestConfig{config.Database}"));
        assertThat(supplier.getName(), is("TestConfig"));
    }
}
