package net.morimekta.providence.config.util;

import javax.annotation.Nonnull;
import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 * Untility for resolving file paths and opening them.
 */
public interface ContentResolver {
    @Nonnull
    Path canonical(Path file) throws IOException;

    @Nonnull
    Path reference(Path referenceLocation, Path from, String reference);

    @Nonnull
    Path referenceLocationPath(Path referenceLocation, Path from, String reference);

    @Nonnull
    @WillNotClose
    InputStream open(Path file) throws IOException;
}
