package net.morimekta.providence.config.parser;

import net.morimekta.util.lexer.TokenType;

import java.util.Locale;

public enum ConfigTokenType implements TokenType {
    IDENTIFIER,
    NUMBER,
    STRING,
    SYMBOL,
    BINARY,
    ;

    @Override
    public String toString() {
        return "<" + name().toLowerCase(Locale.US) + ">";
    }
}
