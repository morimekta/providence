package net.morimekta.providence.server;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.PApplicationException;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.PServiceCallInstrumentation;
import net.morimekta.providence.client.google.ProvidenceHttpContent;
import net.morimekta.providence.serializer.BinarySerializer;
import net.morimekta.providence.serializer.DefaultSerializerProvider;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.server.internal.NoLogging;
import net.morimekta.test.providence.service.Failure;
import net.morimekta.test.providence.service.Request;
import net.morimekta.test.providence.service.Response;
import net.morimekta.test.providence.service.TestService;
import net.morimekta.testing.DataProviderUtil;
import net.morimekta.util.io.IOUtils;
import org.awaitility.Awaitility;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;
import static net.morimekta.providence.server.internal.TestNetUtil.factory;
import static net.morimekta.providence.server.internal.TestNetUtil.getExposedPort;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(DataProviderRunner.class)
public class ProvidenceHttpServletWrapperTest {
    private PServiceCallInstrumentation instrumentation;

    private static class TestServlet implements TestService.Iface {
        private TestServlet() { }

        @Override
        public void voidMethod(int pParam) { }

        @Nonnull
        @Override
        public Response test(Request request) throws Failure {
            if (request.getText().startsWith("fail ")) {
                throw Failure.builder()
                             .setText(request.getText().substring(5))
                             .build();
            }
            if (request.getText().startsWith("other ")) {
                throw new RuntimeException(request.getText());
            }
            return Response.builder()
                           .setText(request.getText())
                           .build();
        }

        @Override
        public double otherTest(double pP1, double pP2) {
            return 0;
        }

        @Override
        public void ping() { }

        @Override
        public int inBaseService() {
            return 0;
        }
    }

    private static class TestExceptionHandler extends ExceptionHandler {
        @Override
        protected int statusCodeForException(@Nonnull Throwable exception) {
            if (exception instanceof Failure) {
                return HttpStatus.EXPECTATION_FAILED_417;
            }
            return super.statusCodeForException(exception);
        }
    }

    private int    port;
    private Server server;

    private static final String ENDPOINT = "foo";

    private GenericUrl endpoint(String method) {
        return new GenericUrl("http://localhost:" + port + "/" + ENDPOINT + "/" + method);
    }

    @Before
    public void setUpServer() throws Exception {
        Awaitility.setDefaultPollDelay(2, TimeUnit.MILLISECONDS);
        Log.setLog(new NoLogging());

        instrumentation = mock(PServiceCallInstrumentation.class);

        server = new Server(0);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new ProvidenceHttpServletWrapper(
                                   TestService.kDescriptor,
                                   (http) -> new TestService.Processor(new TestServlet()),
                                   new TestExceptionHandler(),
                                   new DefaultSerializerProvider(),
                                   instrumentation)),
                           "/" + ENDPOINT + "/*");

        server.setHandler(handler);
        server.start();
        port = getExposedPort(server);
    }

    @After
    public void tearDownServer() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"request\":{\"text\": \"" + text + "\"}}");
        assertThat(response.getStatusCode(), is(HttpStatus.OK_200));

        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\"text\":\"" + text + "\"}"));

        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    @DataProvider
    public static Object[][] overrideAccept() {
        return DataProviderUtil.buildDataDimensions(
                asList(new BinarySerializer(), new JsonSerializer(), new JsonSerializer().named() ),
                asList(new BinarySerializer(), new JsonSerializer(), new JsonSerializer().named() ));
    }

    @Test
    @UseDataProvider("overrideAccept")
    public void testSimpleRequest_OverrideAccept(Serializer toSend, Serializer toReceive) throws IOException, InterruptedException {
        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        TestService.Test_Request request = TestService.Test_Request.builder()
                                 .setRequest(Request.builder()
                                                    .setText(text)
                                                    .build())
                                 .build();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        toSend.serialize(out, request);

        HttpResponse httpResponse = factory(rq -> rq.getHeaders().setAccept(toReceive.mediaType()))
                .buildPostRequest(endpoint("test"), new ProvidenceHttpContent(request, toSend))
                .execute();

        assertThat(httpResponse.getStatusCode(), is(HttpStatus.OK_200));
        assertThat(httpResponse.getContentType(), is(toReceive.mediaType()));

        Response response = toReceive.deserialize(httpResponse.getContent(), Response.kDescriptor);
        assertThat(response, is(Response.builder()
                                        .setText(text)
                                        .build()));

        Thread.sleep(10);
        verify(instrumentation).onComplete(
                anyDouble(),
                any(PServiceCall.class),
                any(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    @Test
    public void testSimpleRequest_badAccept() throws IOException {
        Serializer toSend = new JsonSerializer();

        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        TestService.Test_Request request = TestService.Test_Request
                .builder()
                .setRequest(Request.builder()
                                   .setText(text)
                                   .build())
                .build();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        toSend.serialize(out, request);

        HttpResponse httpResponse = factory(rq -> rq.getHeaders().setAccept("not-a media type, ,"))
                .buildPostRequest(endpoint("test"), new ByteArrayContent(toSend.mediaType(), out.toByteArray()))
                .setThrowExceptionOnExecuteError(false)
                .execute();

        assertThat(httpResponse.getStatusCode(), is(HttpStatus.OK_200));
        Response response = toSend.deserialize(httpResponse.getContent(), Response.kDescriptor);
        assertThat(response, is(Response.builder()
                                        .setText(text)
                                        .build()));

        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    @Test
    public void testFailedRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "fail " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"request\":{\"text\": \"" + text + "\"}}");
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\"text\":\"" + num + "\"}"));
        assertThat(response.getStatusCode(), is(HttpStatus.EXPECTATION_FAILED_417));

        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    @Test
    public void testOtherFailedRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "other " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"request\":{\"text\": \"" + text + "\"}}");
        String content = IOUtils.readString(response.getContent());
        assertThat(content, containsString(text));
        assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR_500));

        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    @Test
    public void testBadRequest() throws IOException, InterruptedException {
        HttpResponse response;

        response = post("text/url-encoded", "method=test");
        // assertThat(response.getStatusMessage(), is("Unknown content-type: text/url-encoded"));
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST_400));

        response = post("application/json", "[\"foo\", 1, 1, {}]");
        assertThat(response.getStatusMessage(), is("Bad Request"));
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST_400));

        Thread.sleep(100);
        verify(instrumentation, times(2)).onTransportException(
                any(PApplicationException.class),
                anyDouble(),
                nullable(PServiceCall.class),
                nullable(PServiceCall.class));
        verifyNoMoreInteractions(instrumentation);
    }

    private HttpResponse post(String contentType, String content) throws IOException {
        HttpRequest request = factory()
                .buildPostRequest(
                        endpoint("test"), new ByteArrayContent(contentType, content.getBytes(StandardCharsets.UTF_8)))
                .setThrowExceptionOnExecuteError(false);
        request.getHeaders().setAccept("*/*");
        return request.execute();
    }

}
