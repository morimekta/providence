package net.morimekta.providence.model;

/**
 * (oneway)? &lt;return_type&gt; &lt;name&gt;&#39;(&#39;&lt;param&gt;*&#39;)&#39; (throws &#39;(&#39; &lt;exception&gt;+ &#39;)&#39;)?
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface FunctionType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<FunctionType> {
    /**
     * @return The one_way value.
     */
    boolean isOneWay();

    /**
     * @return Optional one_way value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Boolean> optionalOneWay();

    /**
     * @return If one_way is present.
     */
    boolean hasOneWay();

    /**
     * @return The return_type value.
     */
    String getReturnType();

    /**
     * @return Optional return_type value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalReturnType();

    /**
     * @return If return_type is present.
     */
    boolean hasReturnType();

    /**
     * @return The params value.
     */
    @javax.annotation.Nonnull
    java.util.List<net.morimekta.providence.model.FieldType> getParams();

    /**
     * @return If params is present.
     */
    boolean hasParams();

    /**
     * @return Number of entries in params.
     */
    int numParams();

    /**
     * @return The exceptions value.
     */
    java.util.List<net.morimekta.providence.model.FieldType> getExceptions();

    /**
     * @return Optional exceptions value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.providence.model.FieldType>> optionalExceptions();

    /**
     * @return If exceptions is present.
     */
    boolean hasExceptions();

    /**
     * @return Number of entries in exceptions.
     */
    int numExceptions();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

}
