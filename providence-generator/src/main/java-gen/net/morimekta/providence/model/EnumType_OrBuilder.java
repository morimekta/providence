package net.morimekta.providence.model;

/**
 * enum {
 *   (&lt;value&gt; ([;,])?)*
 * }
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface EnumType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<EnumType> {
    /**
     * @return The values value.
     */
    @javax.annotation.Nonnull
    java.util.List<net.morimekta.providence.model.EnumValue> getValues();

    /**
     * @return If values is present.
     */
    boolean hasValues();

    /**
     * @return Number of entries in values.
     */
    int numValues();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

}
