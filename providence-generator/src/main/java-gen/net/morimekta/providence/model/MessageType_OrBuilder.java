package net.morimekta.providence.model;

/**
 * &lt;variant&gt; {
 *   (&lt;field&gt; ([,;])?)*
 * }
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface MessageType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<MessageType> {
    /**
     * @return The variant value.
     */
    net.morimekta.providence.model.MessageVariant getVariant();

    /**
     * @return Optional variant value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.MessageVariant> optionalVariant();

    /**
     * @return If variant is present.
     */
    boolean hasVariant();

    /**
     * @return The fields value.
     */
    @javax.annotation.Nonnull
    java.util.List<net.morimekta.providence.model.FieldType> getFields();

    /**
     * @return If fields is present.
     */
    boolean hasFields();

    /**
     * @return Number of entries in fields.
     */
    int numFields();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

    /**
     * Interface this message is implementing.
     *
     * @return The implementing value.
     */
    String getImplementing();

    /**
     * Interface this message is implementing.
     *
     * @return Optional implementing value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalImplementing();

    /**
     * @return If implementing is present.
     */
    boolean hasImplementing();

}
