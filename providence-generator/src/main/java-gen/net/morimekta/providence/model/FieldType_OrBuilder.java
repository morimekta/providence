package net.morimekta.providence.model;

/**
 * For fields:
 *   (&lt;key&gt;:)? (required|optional)? &lt;type&gt; &lt;name&gt; (= &lt;default_value&gt;)?
 * For const:
 *   const &lt;type&gt; &lt;name&gt; = &lt;default_value&gt;
 * <p>
 * Fields without key is assigned values ranging from 65335 and down (2^16-1)
 * in order of appearance. Because of the &quot;in order of appearance&quot; the field
 * *must* be filled by the IDL parser.
 * <p>
 * Consts are always given the key &#39;0&#39;.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface FieldType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<FieldType> {
    /**
     * @return The id value.
     */
    int getId();

    /**
     * @return If id is present.
     */
    boolean hasId();

    /**
     * @return The requirement value.
     */
    net.morimekta.providence.model.FieldRequirement getRequirement();

    /**
     * @return Optional requirement value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.FieldRequirement> optionalRequirement();

    /**
     * @return If requirement is present.
     */
    boolean hasRequirement();

    /**
     * @return The type value.
     */
    @javax.annotation.Nonnull
    String getType();

    /**
     * @return If type is present.
     */
    boolean hasType();

    /**
     * @return The default_value value.
     */
    String getDefaultValue();

    /**
     * @return Optional default_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalDefaultValue();

    /**
     * @return If default_value is present.
     */
    boolean hasDefaultValue();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

}
