package net.morimekta.providence.model;

/**
 * The requirement of the field.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum FieldRequirement
        implements net.morimekta.providence.PEnumValue<FieldRequirement> {
    /**
     * Will be serialized, will be made present from builder,
     * but not required for validity.
     */
    DEFAULT(0, "DEFAULT", "DEFAULT"),
    /**
     * Presence is optional.
     */
    OPTIONAL(1, "OPTIONAL", "OPTIONAL"),
    /**
     * Must be set for message to be &#39;valid&#39;.
     */
    REQUIRED(2, "REQUIRED", "REQUIRED"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    FieldRequirement(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static FieldRequirement findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 0: return FieldRequirement.DEFAULT;
            case 1: return FieldRequirement.OPTIONAL;
            case 2: return FieldRequirement.REQUIRED;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static FieldRequirement findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "DEFAULT": return FieldRequirement.DEFAULT;
            case "OPTIONAL": return FieldRequirement.OPTIONAL;
            case "REQUIRED": return FieldRequirement.REQUIRED;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static FieldRequirement findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "DEFAULT": return FieldRequirement.DEFAULT;
            case "OPTIONAL": return FieldRequirement.OPTIONAL;
            case "REQUIRED": return FieldRequirement.REQUIRED;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static FieldRequirement valueForId(int id) {
        FieldRequirement value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.FieldRequirement for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static FieldRequirement valueForName(@javax.annotation.Nonnull String name) {
        FieldRequirement value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.FieldRequirement for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static FieldRequirement valueForPojoName(@javax.annotation.Nonnull String name) {
        FieldRequirement value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.FieldRequirement for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<FieldRequirement> {
        private FieldRequirement mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = FieldRequirement.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = FieldRequirement.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public FieldRequirement build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<FieldRequirement> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<FieldRequirement> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<FieldRequirement> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<FieldRequirement>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<FieldRequirement> {
        public _Descriptor() {
            super("p_model", "FieldRequirement", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public FieldRequirement[] getValues() {
            return FieldRequirement.values();
        }

        @Override
        @javax.annotation.Nullable
        public FieldRequirement findById(int id) {
            return FieldRequirement.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public FieldRequirement findByName(String name) {
            return FieldRequirement.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public FieldRequirement findByPojoName(String name) {
            return FieldRequirement.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
