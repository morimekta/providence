package net.morimekta.providence.generator.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.util.io.IOUtils.readString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by morimekta on 23.04.17.
 */
public class FileManagerTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testFileManager() throws IOException {
        FileManager fm = new FileManager(tmp.getRoot().toPath());
        Path programFile = tmp.newFile().toPath();

        fm.createIfMissingOrOutdated(
                programFile,
                "tmp/boo", "foo.bar", out -> {
                    out.write(("Test\n" +
                               "More\n").getBytes(UTF_8));
                });

        Path file = tmp.getRoot().toPath().resolve("tmp/boo/foo.bar");
        try (InputStream in = Files.newInputStream(file)) {
            assertThat(readString(in),
                       is("Test\n" +
                          "More\n"));
        }
    }
}
