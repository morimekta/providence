package net.morimekta.providence.example.server.core;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.console.util.Parser;
import net.morimekta.providence.server.ProvidenceHttpServletWrapper;
import net.morimekta.providence.server.ProvidenceServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class CoreApplication {
    public static void main(String... args) {
        AtomicBoolean help = new AtomicBoolean();
        AtomicInteger port = new AtomicInteger(8080);
        AtomicReference<String> context = new AtomicReference<>("/example/core");

        ArgumentParser parser = new ArgumentParser("example-server-core", "", "");
        parser.add(new Option("--port", "p", "PORT", "Port number to open",
                              Parser.i32(port::set), String.valueOf(port.get())));
        parser.add(new Option("--context", "c", "PATH", "Context path",
                              context::set, context.get()));
        parser.add(new Flag("--help", "h?", "Show this help info", help::set));

        try {
            parser.parse(args);
            if (help.get()) {
                System.out.println(parser.getSingleLineUsage());
                System.out.println();
                parser.printUsage(System.out);
                return;
            }

            Server server = new Server(port.get());
            ServletContextHandler handler = new ServletContextHandler();
            handler.setContextPath(context.get());

            // --------------------------------
            // ---- Setup of servlets here ----
            // --------------------------------

            CoreService.Iface impl = new CoreServiceImpl();
            CoreService.Processor processor = new CoreService.Processor(impl);

            handler.addServlet(
                    new ServletHolder(new CoreProcessImpl(impl)),
                    "/post");
            handler.addServlet(
                    new ServletHolder(new ProvidenceServlet(processor)),
                    "/thrift");
            handler.addServlet(
                    new ServletHolder(new ProvidenceHttpServletWrapper(
                            CoreService.kDescriptor, request -> processor, null, null, null)),
                    "/wrapper/*");

            // ---- DONE ----

            server.setHandler(handler);
            server.start();
            server.join();
            return;
        } catch (ArgumentException e) {
            System.err.println(parser.getSingleLineUsage());
            System.err.println();
            parser.printUsage(System.err);
            System.err.println();
            e.printStackTrace();
        } catch (Exception e) {
            System.err.println(parser.getSingleLineUsage());
            System.err.println();
            e.printStackTrace();
        }
        System.exit(1);
    }
}
