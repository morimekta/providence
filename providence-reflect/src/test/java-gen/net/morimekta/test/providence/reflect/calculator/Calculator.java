package net.morimekta.test.providence.reflect.calculator;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public class Calculator {
    public interface Iface {
        /**
         * @param pOp The op value.
         * @return The calculate result.
         * @throws net.morimekta.test.providence.reflect.calculator.CalculateException The ce exception.
         * @throws java.io.IOException On providence or non-declared exceptions.
         */
        @javax.annotation.Nonnull
        net.morimekta.test.providence.reflect.calculator.Operand calculate(
                net.morimekta.test.providence.reflect.calculator.Operation pOp)
                throws java.io.IOException,
                       net.morimekta.test.providence.reflect.calculator.CalculateException;

        /**
         * @throws java.io.IOException On providence or non-declared exceptions.
         */
        void iamalive()
                throws java.io.IOException;

        /**
         * @throws java.io.IOException On providence or non-declared exceptions.
         */
        void ping()
                throws java.io.IOException;
    }

    /**
     * Client implementation for calculator.Calculator
     */
    public static final class Client
            extends net.morimekta.providence.PClient
            implements Iface {
        private final net.morimekta.providence.PServiceCallHandler handler;

        /**
         * Create calculator.Calculator service client.
         *
         * @param handler The client handler.
         */
        public Client(net.morimekta.providence.PServiceCallHandler handler) {
            this.handler = handler;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.reflect.calculator.Operand calculate(
                net.morimekta.test.providence.reflect.calculator.Operation pOp)
                throws java.io.IOException,
                       net.morimekta.test.providence.reflect.calculator.CalculateException {
            Calculate_Request._Builder rq = Calculate_Request.builder();
            if (pOp != null) {
                rq.setOp(pOp);
            }

            net.morimekta.providence.PServiceCall<Calculate_Request> call = new net.morimekta.providence.PServiceCall<>("calculate", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, Calculator.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Calculate_Response msg = (Calculate_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case CE:
                        throw msg.getCe();
                    case SUCCESS:
                        return msg.getSuccess();
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for calculator.Calculator.calculate() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }

        @Override
        public void iamalive()
                throws java.io.IOException {
            Iamalive_Request._Builder rq = Iamalive_Request.builder();

            net.morimekta.providence.PServiceCall<Iamalive_Request> call = new net.morimekta.providence.PServiceCall<>("iamalive", net.morimekta.providence.PServiceCallType.ONEWAY, getNextSequenceId(), rq.build());
            handler.handleCall(call, Calculator.kDescriptor);
        }

        @Override
        public void ping()
                throws java.io.IOException {
            Ping_Request._Builder rq = Ping_Request.builder();

            net.morimekta.providence.PServiceCall<Ping_Request> call = new net.morimekta.providence.PServiceCall<>("ping", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, Calculator.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Ping_Response msg = (Ping_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case SUCCESS:
                        return;
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for calculator.Calculator.ping() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }
    }

    public static final class Processor implements net.morimekta.providence.PProcessor {
        private final Iface impl;
        private final java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer;
        public Processor(@javax.annotation.Nonnull Iface impl) {
            this(impl, (m, e) -> e);
        }

        public Processor(@javax.annotation.Nonnull Iface impl,
                         @javax.annotation.Nonnull java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer) {
            this.impl = impl;
            this.exceptionTransformer = exceptionTransformer;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getDescriptor() {
            return kDescriptor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <Request extends net.morimekta.providence.PMessage<Request>,
                Response extends net.morimekta.providence.PMessage<Response>>
        net.morimekta.providence.PServiceCall<Response> handleCall(
                net.morimekta.providence.PServiceCall<Request> call,
                net.morimekta.providence.descriptor.PService service)
                throws net.morimekta.providence.PApplicationException {
            net.morimekta.providence.PApplicationException ex = null;
            switch(call.getMethod()) {
                case "calculate": {
                    Calculate_Response._Builder rsp = Calculate_Response.builder();
                    try {
                        Calculate_Request req = (Calculate_Request) call.getMessage();
                        try {
                            net.morimekta.test.providence.reflect.calculator.Operand result =
                                    impl.calculate(req.getOp());
                            if (result == null) {
                                ex = new net.morimekta.providence.PApplicationException(
                                        "Returning null value from calculate in calculator.Calculator",
                                        net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
                                break;
                            }
                            rsp.setSuccess(result);
                        } catch (net.morimekta.test.providence.reflect.calculator.CalculateException e) {
                            throw e;
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.CALCULATE, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method calculate in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (net.morimekta.test.providence.reflect.calculator.CalculateException e) {
                        rsp.setCe(e);
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method calculate in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                case "iamalive": {
                    try {
                        Iamalive_Request req = (Iamalive_Request) call.getMessage();
                        try {
                            impl.iamalive();
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.IAMALIVE, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        throw new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method iamalive in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                    } catch (Exception e) {
                        throw new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method iamalive in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                    }
                    return null;
                }
                case "ping": {
                    Ping_Response._Builder rsp = Ping_Response.builder();
                    try {
                        Ping_Request req = (Ping_Request) call.getMessage();
                        try {
                            impl.ping();
                            rsp.setSuccess();
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.PING, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method ping in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method ping in calculator.Calculator: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                default: {
                    ex = new net.morimekta.providence.PApplicationException(
                            "Unknown method \"" + call.getMethod() + "\" on calculator.Calculator.",
                            net.morimekta.providence.PApplicationExceptionType.UNKNOWN_METHOD);
                    break;
                }
            }
            net.morimekta.providence.PServiceCall reply =
                    new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                              net.morimekta.providence.PServiceCallType.EXCEPTION,
                                                              call.getSequence(),
                                                              ex);
            return reply;
        }
    }

    public enum Method implements net.morimekta.providence.descriptor.PServiceMethod {
        CALCULATE("calculate", false, false, Calculate_Request.kDescriptor, Calculate_Response.kDescriptor),
        IAMALIVE("iamalive", true, false, Iamalive_Request.kDescriptor, null),
        PING("ping", false, false, Ping_Request.kDescriptor, Ping_Response.kDescriptor),
        ;

        private final String name;
        private final boolean oneway;
        private final boolean protoStub;
        private final net.morimekta.providence.descriptor.PStructDescriptor request;
        private final net.morimekta.providence.descriptor.PUnionDescriptor response;

        private Method(String name, boolean oneway, boolean protoStub, net.morimekta.providence.descriptor.PStructDescriptor request, net.morimekta.providence.descriptor.PUnionDescriptor response) {
            this.name = name;
            this.oneway = oneway;
            this.protoStub = protoStub;
            this.request = request;
            this.response = response;
        }

        @javax.annotation.Nonnull
        @Override
        public String getName() {
            return name;
        }

        public boolean isOneway() {
            return oneway;
        }

        public boolean isProtoStub() {
            return protoStub;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor getRequestType() {
            return request;
        }

        @javax.annotation.Nullable
        @Override
        public net.morimekta.providence.descriptor.PUnionDescriptor getResponseType() {
            return response;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }

        @javax.annotation.Nullable
        public static Method findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "calculate": return CALCULATE;
                case "iamalive": return IAMALIVE;
                case "ping": return PING;
            }
            return null;
        }
        @javax.annotation.Nonnull
        public static Method methodForName(@javax.annotation.Nonnull String name) {
            Method method = findByName(name);
            if (method == null) {
                throw new IllegalArgumentException("No such method \"" + name + "\" in service calculator.Calculator");
            }
            return method;
        }
    }

    private static final class _Descriptor extends net.morimekta.providence.descriptor.PService {
        private _Descriptor() {
            super("calculator", "Calculator", null, Method.values());
        }

        @Override
        public Method getMethod(String name) {
            return Method.findByName(name);
        }
    }

    private static final class _Provider implements net.morimekta.providence.descriptor.PServiceProvider {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }
    }

    public static final net.morimekta.providence.descriptor.PService kDescriptor = new _Descriptor();

    public static net.morimekta.providence.descriptor.PServiceProvider provider() {
        return new _Provider();
    }

    // type --> Calculator.calculate.request
    @SuppressWarnings("unused")
    public interface Calculate_Request_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Calculate_Request> {
        /**
         * @return The op value.
         */
        net.morimekta.test.providence.reflect.calculator.Operation getOp();

        /**
         * @return Optional op value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operation> optionalOp();

        /**
         * @return If op is present.
         */
        boolean hasOp();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Calculate_Request
            implements Calculate_Request_OrBuilder,
                       net.morimekta.providence.PMessage<Calculate_Request>,
                       Comparable<Calculate_Request>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = 5385883517742336295L;

        private final transient net.morimekta.test.providence.reflect.calculator.Operation mOp;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Calculate_Request tSerializeInstance;

        private Calculate_Request(_Builder builder) {
            mOp = builder.mOp_builder != null ? builder.mOp_builder.build() : builder.mOp;
        }

        public boolean hasOp() {
            return mOp != null;
        }

        /**
         * @return The <code>op</code> value
         */
        public net.morimekta.test.providence.reflect.calculator.Operation getOp() {
            return mOp;
        }

        /**
         * @return Optional of the <code>op</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operation> optionalOp() {
            return java.util.Optional.ofNullable(mOp);
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return mOp != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) mOp;
                default: return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Calculate_Request other = (Calculate_Request) o;
            return java.util.Objects.equals(mOp, other.mOp);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Calculate_Request.class,
                        _Field.OP, mOp);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator.calculate.request" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            if (hasOp()) {
                out.append("op:")
                   .append(mOp.asString());
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Calculate_Request other) {
            int c;

            c = Boolean.compare(mOp != null, other.mOp != null);
            if (c != 0) return c;
            if (mOp != null) {
                c = mOp.compareTo(other.mOp);
                if (c != 0) return c;
            }

            return 0;
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (hasOp()) {
                length += writer.writeByte((byte) 12);
                length += writer.writeShort((short) 1);
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mOp);
            }

            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Calculate_Request> {
            OP(1, net.morimekta.providence.descriptor.PRequirement.DEFAULT, "op", "op", net.morimekta.test.providence.reflect.calculator.Operation.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Calculate_Request> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 1: return _Field.OP;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "op": return _Field.OP;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "op": return _Field.OP;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator.calculate.request");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.calculate.request");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.calculate.request");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Calculate_Request> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Calculate_Request> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PStructDescriptor<Calculate_Request> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PStructDescriptor<Calculate_Request> {
            public _Descriptor() {
                super("calculator", "Calculator.calculate.request", _Builder::new, false);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Calculate_Request> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PStructDescriptor<Calculate_Request> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator.calculate.request</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Calculate_Request>
                implements Calculate_Request_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private java.util.BitSet optionals;
            private java.util.BitSet modified;

            private net.morimekta.test.providence.reflect.calculator.Operation mOp;
            private net.morimekta.test.providence.reflect.calculator.Operation._Builder mOp_builder;

            /**
             * Make a calculator.Calculator.calculate.request builder instance.
             */
            public _Builder() {
                optionals = new java.util.BitSet(1);
                modified = new java.util.BitSet(1);
            }

            /**
             * Make a mutating builder off a base calculator.Calculator.calculate.request.
             *
             * @param base The base Calculator.calculate.request
             */
            public _Builder(Calculate_Request base) {
                this();

                if (base.hasOp()) {
                    optionals.set(0);
                    mOp = base.mOp;
                }
            }

            @javax.annotation.Nonnull
            @Override
            public Calculate_Request._Builder merge(Calculate_Request from) {
                if (from.hasOp()) {
                    optionals.set(0);
                    modified.set(0);
                    if (mOp_builder != null) {
                        mOp_builder.merge(from.getOp());
                    } else if (mOp != null) {
                        mOp_builder = mOp.mutate().merge(from.getOp());
                        mOp = null;
                    } else {
                        mOp = from.getOp();
                    }
                }
                return this;
            }

            /**
             * Set the <code>op</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Request._Builder setOp(net.morimekta.test.providence.reflect.calculator.Operation_OrBuilder value) {
                if (value == null) {
                    return clearOp();
                }

                optionals.set(0);
                modified.set(0);
                if (value instanceof net.morimekta.test.providence.reflect.calculator.Operation._Builder) {
                    value = ((net.morimekta.test.providence.reflect.calculator.Operation._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.calculator.Operation)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for calculator.Operation: " + value.getClass().getName());
                }
                mOp = (net.morimekta.test.providence.reflect.calculator.Operation) value;
                mOp_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>op</code> field.
             *
             * @return True if op has been set.
             */
            public boolean isSetOp() {
                return optionals.get(0);
            }

            /**
             * Checks for presence of the <code>op</code> field.
             *
             * @return True if op is present.
             */
            public boolean hasOp() {
                return optionals.get(0);
            }

            /**
             * Checks if the <code>op</code> field has been modified since the
             * builder was created.
             *
             * @return True if op has been modified.
             */
            public boolean isModifiedOp() {
                return modified.get(0);
            }

            /**
             * Clear the <code>op</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Request._Builder clearOp() {
                optionals.clear(0);
                modified.set(0);
                mOp = null;
                mOp_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>op</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.calculator.Operation._Builder mutableOp() {
                optionals.set(0);
                modified.set(0);

                if (mOp != null) {
                    mOp_builder = mOp.mutate();
                    mOp = null;
                } else if (mOp_builder == null) {
                    mOp_builder = net.morimekta.test.providence.reflect.calculator.Operation.builder();
                }
                return mOp_builder;
            }

            /**
             * @return The <code>op</code> field value
             */
            public net.morimekta.test.providence.reflect.calculator.Operation getOp() {
                return mOp_builder != null ? mOp_builder.build() : mOp;
            }

            /**
             * @return Optional <code>op</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operation> optionalOp() {
                return java.util.Optional.ofNullable(mOp_builder != null ? mOp_builder.build() : mOp);
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Calculate_Request._Builder other = (Calculate_Request._Builder) o;
                return java.util.Objects.equals(optionals, other.optionals) &&
                       java.util.Objects.equals(getOp(), other.getOp());
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Calculate_Request.class, optionals,
                        Calculate_Request._Field.OP, getOp());
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    case 1: return mutableOp();
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Calculate_Request._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 1: setOp((net.morimekta.test.providence.reflect.calculator.Operation) value); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 1: return optionals.get(0);
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                switch (key) {
                    case 1: return modified.get(0);
                    default: break;
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 1: return (T) getOp();
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 1: return mOp != null || mOp_builder != null;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Calculate_Request._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Calculate_Request._Builder clear(int key) {
                switch (key) {
                    case 1: clearOp(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                return true;
            }

            @Override
            public Calculate_Request._Builder validate() {
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PStructDescriptor<Calculate_Request> descriptor() {
                return Calculate_Request.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 1: {
                            if (type == 12) {
                                mOp = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.calculator.Operation.kDescriptor, strict);
                                optionals.set(0);
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for calculator.Calculator.calculate.request.op, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Calculate_Request build() {
                return new Calculate_Request(this);
            }
        }
    }

    // type <-- Calculator.calculate.response
    @SuppressWarnings("unused")
    public interface Calculate_Response_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Calculate_Response> {
        /**
         * @return The success value.
         */
        net.morimekta.test.providence.reflect.calculator.Operand getSuccess();

        /**
         * @return Optional success value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operand> optionalSuccess();

        /**
         * @return If success is present.
         */
        boolean hasSuccess();

        /**
         * @return The ce value.
         */
        net.morimekta.test.providence.reflect.calculator.CalculateException getCe();

        /**
         * @return Optional ce value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.calculator.CalculateException> optionalCe();

        /**
         * @return If ce is present.
         */
        boolean hasCe();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Calculate_Response
            implements Calculate_Response_OrBuilder,
                       net.morimekta.providence.PUnion<Calculate_Response>,
                       Comparable<Calculate_Response>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = -1787619653444046051L;

        private final transient net.morimekta.test.providence.reflect.calculator.Operand mSuccess;
        private final transient net.morimekta.test.providence.reflect.calculator.CalculateException mCe;

        private transient final _Field tUnionField;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Calculate_Response tSerializeInstance;

        /**
         * @param value The union value
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Calculate_Response withSuccess(@javax.annotation.Nonnull net.morimekta.test.providence.reflect.calculator.Operand_OrBuilder value) {
            return new _Builder().setSuccess(value).build();
        }

        /**
         * @param value The union value
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Calculate_Response withCe(@javax.annotation.Nonnull net.morimekta.test.providence.reflect.calculator.CalculateException_OrBuilder value) {
            return new _Builder().setCe(value).build();
        }

        private Calculate_Response(_Builder builder) {
            tUnionField = builder.tUnionField;

            mSuccess = tUnionField != _Field.SUCCESS
                    ? null
                    : builder.mSuccess_builder != null ? builder.mSuccess_builder.build() : builder.mSuccess;
            mCe = tUnionField != _Field.CE
                    ? null
                    : builder.mCe_builder != null ? builder.mCe_builder.build() : builder.mCe;
        }

        public boolean hasSuccess() {
            return tUnionField == _Field.SUCCESS && mSuccess != null;
        }

        /**
         * @return The <code>success</code> value
         */
        public net.morimekta.test.providence.reflect.calculator.Operand getSuccess() {
            return mSuccess;
        }

        /**
         * @return Optional of the <code>success</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operand> optionalSuccess() {
            return java.util.Optional.ofNullable(mSuccess);
        }

        public boolean hasCe() {
            return tUnionField == _Field.CE && mCe != null;
        }

        /**
         * @return The <code>ce</code> value
         */
        public net.morimekta.test.providence.reflect.calculator.CalculateException getCe() {
            return mCe;
        }

        /**
         * @return Optional of the <code>ce</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.calculator.CalculateException> optionalCe() {
            return java.util.Optional.ofNullable(mCe);
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 0: return tUnionField == _Field.SUCCESS;
                case 1: return tUnionField == _Field.CE;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 0: return (T) mSuccess;
                case 1: return (T) mCe;
                default: return null;
            }
        }

        @Override
        public boolean unionFieldIsSet() {
            return tUnionField != null;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field unionField() {
            if (tUnionField == null) throw new IllegalStateException("No union field set in calculator.Calculator.calculate.response");
            return tUnionField;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Calculate_Response other = (Calculate_Response) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                   java.util.Objects.equals(mSuccess, other.mSuccess) &&
                   java.util.Objects.equals(mCe, other.mCe);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Calculate_Response.class,
                        _Field.SUCCESS, mSuccess,
                        _Field.CE, mCe);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator.calculate.response" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            switch (tUnionField) {
                case SUCCESS: {
                    out.append("success:")
                       .append(mSuccess.asString());
                    break;
                }
                case CE: {
                    out.append("ce:")
                       .append(mCe.asString());
                    break;
                }
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Calculate_Response other) {
            if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
            int c = tUnionField.compareTo(other.tUnionField);
            if (c != 0) return c;

            switch (tUnionField) {
                case SUCCESS:
                    return mSuccess.compareTo(other.mSuccess);
                case CE:
                    return mCe.compareTo(other.mCe);
                default: return 0;
            }
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (tUnionField != null) {
                switch (tUnionField) {
                    case SUCCESS: {
                        length += writer.writeByte((byte) 12);
                        length += writer.writeShort((short) 0);
                        length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mSuccess);
                        break;
                    }
                    case CE: {
                        length += writer.writeByte((byte) 12);
                        length += writer.writeShort((short) 1);
                        length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mCe);
                        break;
                    }
                    default: break;
                }
            }
            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Calculate_Response> {
            SUCCESS(0, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "success", "success", net.morimekta.test.providence.reflect.calculator.Operand.provider(), null, null),
            CE(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "ce", "ce", net.morimekta.test.providence.reflect.calculator.CalculateException.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Calculate_Response> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 0: return _Field.SUCCESS;
                    case 1: return _Field.CE;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                    case "ce": return _Field.CE;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                    case "ce": return _Field.CE;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator.calculate.response");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.calculate.response");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.calculate.response");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<Calculate_Response> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<Calculate_Response> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PUnionDescriptor<Calculate_Response> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PUnionDescriptor<Calculate_Response> {
            public _Descriptor() {
                super("calculator", "Calculator.calculate.response", _Builder::new, false);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<Calculate_Response> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PUnionDescriptor<Calculate_Response> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator.calculate.response</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Calculate_Response>
                implements Calculate_Response_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private Calculate_Response._Field tUnionField;

            private boolean modified;

            private net.morimekta.test.providence.reflect.calculator.Operand mSuccess;
            private net.morimekta.test.providence.reflect.calculator.Operand._Builder mSuccess_builder;
            private net.morimekta.test.providence.reflect.calculator.CalculateException mCe;
            private net.morimekta.test.providence.reflect.calculator.CalculateException._Builder mCe_builder;

            /**
             * Make a calculator.Calculator.calculate.response builder instance.
             */
            public _Builder() {
                modified = false;
            }

            /**
             * Make a mutating builder off a base calculator.Calculator.calculate.response.
             *
             * @param base The base Calculator.calculate.response
             */
            public _Builder(Calculate_Response base) {
                this();

                tUnionField = base.tUnionField;

                mSuccess = base.mSuccess;
                mCe = base.mCe;
            }

            @javax.annotation.Nonnull
            @Override
            public Calculate_Response._Builder merge(Calculate_Response from) {
                if (!from.unionFieldIsSet()) {
                    return this;
                }

                switch (from.unionField()) {
                    case SUCCESS: {
                        if (tUnionField == Calculate_Response._Field.SUCCESS && mSuccess != null) {
                            mSuccess = mSuccess.mutate().merge(from.getSuccess()).build();
                        } else {
                            setSuccess(from.getSuccess());
                        }
                        break;
                    }
                    case CE: {
                        if (tUnionField == Calculate_Response._Field.CE && mCe != null) {
                            mCe = mCe.mutate().merge(from.getCe()).build();
                        } else {
                            setCe(from.getCe());
                        }
                        break;
                    }
                }
                return this;
            }

            /**
             * Set the <code>success</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Response._Builder setSuccess(net.morimekta.test.providence.reflect.calculator.Operand_OrBuilder value) {
                if (value == null) {
                    return clearSuccess();
                }

                tUnionField = Calculate_Response._Field.SUCCESS;
                modified = true;
                if (value instanceof net.morimekta.test.providence.reflect.calculator.Operand._Builder) {
                    value = ((net.morimekta.test.providence.reflect.calculator.Operand._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.calculator.Operand)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for calculator.Operand: " + value.getClass().getName());
                }
                mSuccess = (net.morimekta.test.providence.reflect.calculator.Operand) value;
                mSuccess_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>success</code> field.
             *
             * @return True if success has been set.
             */
            public boolean isSetSuccess() {
                return tUnionField == Calculate_Response._Field.SUCCESS;
            }

            /**
             * Checks for presence of the <code>success</code> field.
             *
             * @return True if success is present.
             */
            public boolean hasSuccess() {
                return tUnionField == Calculate_Response._Field.SUCCESS;
            }

            /**
             * Clear the <code>success</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Response._Builder clearSuccess() {
                if (tUnionField == Calculate_Response._Field.SUCCESS) tUnionField = null;
                modified = true;
                mSuccess = null;
                mSuccess_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>success</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.calculator.Operand._Builder mutableSuccess() {
                if (tUnionField != Calculate_Response._Field.SUCCESS) {
                    clearSuccess();
                }
                tUnionField = Calculate_Response._Field.SUCCESS;
                modified = true;

                if (mSuccess != null) {
                    mSuccess_builder = mSuccess.mutate();
                    mSuccess = null;
                } else if (mSuccess_builder == null) {
                    mSuccess_builder = net.morimekta.test.providence.reflect.calculator.Operand.builder();
                }
                return mSuccess_builder;
            }

            /**
             * @return The <code>success</code> field value
             */
            public net.morimekta.test.providence.reflect.calculator.Operand getSuccess() {
                return mSuccess_builder != null ? mSuccess_builder.build() : mSuccess;
            }

            /**
             * @return Optional <code>success</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.calculator.Operand> optionalSuccess() {
                return java.util.Optional.ofNullable(mSuccess_builder != null ? mSuccess_builder.build() : mSuccess);
            }

            /**
             * Set the <code>ce</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Response._Builder setCe(net.morimekta.test.providence.reflect.calculator.CalculateException_OrBuilder value) {
                if (value == null) {
                    return clearCe();
                }

                tUnionField = Calculate_Response._Field.CE;
                modified = true;
                if (value instanceof net.morimekta.test.providence.reflect.calculator.CalculateException._Builder) {
                    value = ((net.morimekta.test.providence.reflect.calculator.CalculateException._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.calculator.CalculateException)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for calculator.CalculateException: " + value.getClass().getName());
                }
                mCe = (net.morimekta.test.providence.reflect.calculator.CalculateException) value;
                mCe_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>ce</code> field.
             *
             * @return True if ce has been set.
             */
            public boolean isSetCe() {
                return tUnionField == Calculate_Response._Field.CE;
            }

            /**
             * Checks for presence of the <code>ce</code> field.
             *
             * @return True if ce is present.
             */
            public boolean hasCe() {
                return tUnionField == Calculate_Response._Field.CE;
            }

            /**
             * Clear the <code>ce</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Calculate_Response._Builder clearCe() {
                if (tUnionField == Calculate_Response._Field.CE) tUnionField = null;
                modified = true;
                mCe = null;
                mCe_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>ce</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.calculator.CalculateException._Builder mutableCe() {
                if (tUnionField != Calculate_Response._Field.CE) {
                    clearCe();
                }
                tUnionField = Calculate_Response._Field.CE;
                modified = true;

                if (mCe != null) {
                    mCe_builder = mCe.mutate();
                    mCe = null;
                } else if (mCe_builder == null) {
                    mCe_builder = net.morimekta.test.providence.reflect.calculator.CalculateException.builder();
                }
                return mCe_builder;
            }

            /**
             * @return The <code>ce</code> field value
             */
            public net.morimekta.test.providence.reflect.calculator.CalculateException getCe() {
                return mCe_builder != null ? mCe_builder.build() : mCe;
            }

            /**
             * @return Optional <code>ce</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.calculator.CalculateException> optionalCe() {
                return java.util.Optional.ofNullable(mCe_builder != null ? mCe_builder.build() : mCe);
            }

            /**
             * Checks if the <code>Calculator.calculate.response</code> union has been modified since the
             * builder was created.
             *
             * @return True if Calculator.calculate.response has been modified.
             */
            public boolean isUnionModified() {
                return modified;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Calculate_Response._Builder other = (Calculate_Response._Builder) o;
                return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                       java.util.Objects.equals(getSuccess(), other.getSuccess()) &&
                       java.util.Objects.equals(getCe(), other.getCe());
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Calculate_Response.class,
                        Calculate_Response._Field.SUCCESS, getSuccess(),
                        Calculate_Response._Field.CE, getCe());
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    case 0: return mutableSuccess();
                    case 1: return mutableCe();
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Calculate_Response._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 0: setSuccess((net.morimekta.test.providence.reflect.calculator.Operand) value); break;
                    case 1: setCe((net.morimekta.test.providence.reflect.calculator.CalculateException) value); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 0: return tUnionField == Calculate_Response._Field.SUCCESS;
                    case 1: return tUnionField == Calculate_Response._Field.CE;
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                return modified;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 0: return (T) getSuccess();
                    case 1: return (T) getCe();
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 0: return tUnionField == _Field.SUCCESS;
                    case 1: return tUnionField == _Field.CE;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Calculate_Response._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Calculate_Response._Builder clear(int key) {
                switch (key) {
                    case 0: clearSuccess(); break;
                    case 1: clearCe(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                if (tUnionField == null) {
                    return false;
                }

                switch (tUnionField) {
                    case SUCCESS: return mSuccess != null || mSuccess_builder != null;
                    case CE: return mCe != null || mCe_builder != null;
                    default: return true;
                }
            }

            @Override
            public Calculate_Response._Builder validate() {
                if (!valid()) {
                    throw new java.lang.IllegalStateException("No union field set in calculator.Calculator.calculate.response");
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PUnionDescriptor<Calculate_Response> descriptor() {
                return Calculate_Response.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 0: {
                            if (type == 12) {
                                mSuccess = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.calculator.Operand.kDescriptor, strict);
                                tUnionField = Calculate_Response._Field.SUCCESS;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for calculator.Calculator.calculate.response.success, should be struct(12)");
                            }
                            break;
                        }
                        case 1: {
                            if (type == 12) {
                                mCe = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.calculator.CalculateException.kDescriptor, strict);
                                tUnionField = Calculate_Response._Field.CE;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for calculator.Calculator.calculate.response.ce, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Calculate_Response build() {
                return new Calculate_Response(this);
            }
        }
    }

    // type --> Calculator.iamalive.request
    @SuppressWarnings("unused")
    public interface Iamalive_Request_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Iamalive_Request> {
    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Iamalive_Request
            implements Iamalive_Request_OrBuilder,
                       net.morimekta.providence.PMessage<Iamalive_Request>,
                       Comparable<Iamalive_Request>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = -4737575730674403867L;


        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Iamalive_Request tSerializeInstance;

        private Iamalive_Request(_Builder builder) {
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                default: return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            return true;
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Iamalive_Request.class);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator.iamalive.request" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Iamalive_Request other) {
            int c;

            return 0;
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Iamalive_Request> {
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Iamalive_Request> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator.iamalive.request");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.iamalive.request");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.iamalive.request");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Iamalive_Request> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Iamalive_Request> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PStructDescriptor<Iamalive_Request> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PStructDescriptor<Iamalive_Request> {
            public _Descriptor() {
                super("calculator", "Calculator.iamalive.request", _Builder::new, true);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Iamalive_Request> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PStructDescriptor<Iamalive_Request> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator.iamalive.request</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Iamalive_Request>
                implements Iamalive_Request_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private java.util.BitSet optionals;
            private java.util.BitSet modified;

            /**
             * Make a calculator.Calculator.iamalive.request builder instance.
             */
            public _Builder() {
                optionals = new java.util.BitSet(0);
                modified = new java.util.BitSet(0);
            }

            /**
             * Make a mutating builder off a base calculator.Calculator.iamalive.request.
             *
             * @param base The base Calculator.iamalive.request
             */
            public _Builder(Iamalive_Request base) {
                this();

            }

            @javax.annotation.Nonnull
            @Override
            public Iamalive_Request._Builder merge(Iamalive_Request from) {
                return this;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                return true;
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Iamalive_Request.class, optionals);
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Iamalive_Request._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Iamalive_Request._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Iamalive_Request._Builder clear(int key) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                return true;
            }

            @Override
            public Iamalive_Request._Builder validate() {
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PStructDescriptor<Iamalive_Request> descriptor() {
                return Iamalive_Request.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Iamalive_Request build() {
                return new Iamalive_Request(this);
            }
        }
    }

    // type --> Calculator.ping.request
    @SuppressWarnings("unused")
    public interface Ping_Request_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Ping_Request> {
    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Ping_Request
            implements Ping_Request_OrBuilder,
                       net.morimekta.providence.PMessage<Ping_Request>,
                       Comparable<Ping_Request>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = -4600906282490783449L;


        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Ping_Request tSerializeInstance;

        private Ping_Request(_Builder builder) {
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                default: return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            return true;
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Ping_Request.class);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator.ping.request" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Ping_Request other) {
            int c;

            return 0;
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Ping_Request> {
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Ping_Request> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator.ping.request");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.ping.request");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.ping.request");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Ping_Request> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Ping_Request> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PStructDescriptor<Ping_Request> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PStructDescriptor<Ping_Request> {
            public _Descriptor() {
                super("calculator", "Calculator.ping.request", _Builder::new, true);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Ping_Request> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PStructDescriptor<Ping_Request> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator.ping.request</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Ping_Request>
                implements Ping_Request_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private java.util.BitSet optionals;
            private java.util.BitSet modified;

            /**
             * Make a calculator.Calculator.ping.request builder instance.
             */
            public _Builder() {
                optionals = new java.util.BitSet(0);
                modified = new java.util.BitSet(0);
            }

            /**
             * Make a mutating builder off a base calculator.Calculator.ping.request.
             *
             * @param base The base Calculator.ping.request
             */
            public _Builder(Ping_Request base) {
                this();

            }

            @javax.annotation.Nonnull
            @Override
            public Ping_Request._Builder merge(Ping_Request from) {
                return this;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                return true;
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Ping_Request.class, optionals);
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Ping_Request._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Ping_Request._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Ping_Request._Builder clear(int key) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                return true;
            }

            @Override
            public Ping_Request._Builder validate() {
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PStructDescriptor<Ping_Request> descriptor() {
                return Ping_Request.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Ping_Request build() {
                return new Ping_Request(this);
            }
        }
    }

    // type <-- Calculator.ping.response
    @SuppressWarnings("unused")
    public interface Ping_Response_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Ping_Response> {
        /**
         * @return If success is present.
         */
        boolean hasSuccess();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Ping_Response
            implements Ping_Response_OrBuilder,
                       net.morimekta.providence.PUnion<Ping_Response>,
                       Comparable<Ping_Response>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = -1098386840489696915L;


        private transient final _Field tUnionField;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Ping_Response tSerializeInstance;

        /**
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Ping_Response withSuccess() {
            return new _Builder().setSuccess().build();
        }

        private Ping_Response(_Builder builder) {
            tUnionField = builder.tUnionField;

        }

        public boolean hasSuccess() {
            return tUnionField == _Field.SUCCESS;
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 0: return tUnionField == _Field.SUCCESS;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 0: return hasSuccess() ? (T) Boolean.TRUE : null;
                default: return null;
            }
        }

        @Override
        public boolean unionFieldIsSet() {
            return tUnionField != null;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field unionField() {
            if (tUnionField == null) throw new IllegalStateException("No union field set in calculator.Calculator.ping.response");
            return tUnionField;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Ping_Response other = (Ping_Response) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Ping_Response.class);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator.ping.response" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            switch (tUnionField) {
                case SUCCESS: {
                    out.append("success:")
                       .append("true");
                    break;
                }
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Ping_Response other) {
            if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
            int c = tUnionField.compareTo(other.tUnionField);
            if (c != 0) return c;

            switch (tUnionField) {
                case SUCCESS:
                default: return 0;
            }
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (tUnionField != null) {
                switch (tUnionField) {
                    case SUCCESS: {
                        length += writer.writeByte((byte) 1);
                        length += writer.writeShort((short) 0);
                        break;
                    }
                    default: break;
                }
            }
            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Ping_Response> {
            SUCCESS(0, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "success", "success", net.morimekta.providence.descriptor.PPrimitive.VOID.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Ping_Response> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 0: return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator.ping.response");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.ping.response");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator.ping.response");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<Ping_Response> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<Ping_Response> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PUnionDescriptor<Ping_Response> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PUnionDescriptor<Ping_Response> {
            public _Descriptor() {
                super("calculator", "Calculator.ping.response", _Builder::new, true);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<Ping_Response> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PUnionDescriptor<Ping_Response> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator.ping.response</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Ping_Response>
                implements Ping_Response_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private Ping_Response._Field tUnionField;

            private boolean modified;


            /**
             * Make a calculator.Calculator.ping.response builder instance.
             */
            public _Builder() {
                modified = false;
            }

            /**
             * Make a mutating builder off a base calculator.Calculator.ping.response.
             *
             * @param base The base Calculator.ping.response
             */
            public _Builder(Ping_Response base) {
                this();

                tUnionField = base.tUnionField;

            }

            @javax.annotation.Nonnull
            @Override
            public Ping_Response._Builder merge(Ping_Response from) {
                if (!from.unionFieldIsSet()) {
                    return this;
                }

                switch (from.unionField()) {
                    case SUCCESS: {
                        tUnionField = Ping_Response._Field.SUCCESS;
                        break;
                    }
                }
                return this;
            }

            /**
             * Set the <code>success</code> field value.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Ping_Response._Builder setSuccess() {
                tUnionField = Ping_Response._Field.SUCCESS;
                modified = true;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>success</code> field.
             *
             * @return True if success has been set.
             */
            public boolean isSetSuccess() {
                return tUnionField == Ping_Response._Field.SUCCESS;
            }

            /**
             * Checks for presence of the <code>success</code> field.
             *
             * @return True if success is present.
             */
            public boolean hasSuccess() {
                return tUnionField == Ping_Response._Field.SUCCESS;
            }

            /**
             * Clear the <code>success</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Ping_Response._Builder clearSuccess() {
                if (tUnionField == Ping_Response._Field.SUCCESS) tUnionField = null;
                modified = true;
                return this;
            }

            /**
             * Checks if the <code>Calculator.ping.response</code> union has been modified since the
             * builder was created.
             *
             * @return True if Calculator.ping.response has been modified.
             */
            public boolean isUnionModified() {
                return modified;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Ping_Response._Builder other = (Ping_Response._Builder) o;
                return java.util.Objects.equals(tUnionField, other.tUnionField);
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Ping_Response.class);
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Ping_Response._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 0: setSuccess(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 0: return tUnionField == Ping_Response._Field.SUCCESS;
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                return modified;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 0: return hasSuccess() ? (T) Boolean.TRUE : null;
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 0: return tUnionField == _Field.SUCCESS;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Ping_Response._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Ping_Response._Builder clear(int key) {
                switch (key) {
                    case 0: clearSuccess(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                if (tUnionField == null) {
                    return false;
                }

                switch (tUnionField) {
                    case SUCCESS: return true;
                    default: return true;
                }
            }

            @Override
            public Ping_Response._Builder validate() {
                if (!valid()) {
                    throw new java.lang.IllegalStateException("No union field set in calculator.Calculator.ping.response");
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PUnionDescriptor<Ping_Response> descriptor() {
                return Ping_Response.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 0: {
                            if (type == 1) {
                                tUnionField = Ping_Response._Field.SUCCESS;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for calculator.Calculator.ping.response.success, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Ping_Response build() {
                return new Ping_Response(this);
            }
        }
    }

    protected Calculator() {}
}
