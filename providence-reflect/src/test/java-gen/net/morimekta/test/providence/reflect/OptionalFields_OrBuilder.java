package net.morimekta.test.providence.reflect;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface OptionalFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<OptionalFields> {
    /**
     * @return The booleanValue value.
     */
    boolean isBooleanValue();

    /**
     * @return Optional booleanValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Boolean> optionalBooleanValue();

    /**
     * @return If booleanValue is present.
     */
    boolean hasBooleanValue();

    /**
     * @return The byteValue value.
     */
    byte getByteValue();

    /**
     * @return Optional byteValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Byte> optionalByteValue();

    /**
     * @return If byteValue is present.
     */
    boolean hasByteValue();

    /**
     * @return The shortValue value.
     */
    short getShortValue();

    /**
     * @return Optional shortValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Short> optionalShortValue();

    /**
     * @return If shortValue is present.
     */
    boolean hasShortValue();

    /**
     * @return The integerValue value.
     */
    int getIntegerValue();

    /**
     * @return Optional integerValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Integer> optionalIntegerValue();

    /**
     * @return If integerValue is present.
     */
    boolean hasIntegerValue();

    /**
     * @return The longValue value.
     */
    long getLongValue();

    /**
     * @return Optional longValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Long> optionalLongValue();

    /**
     * @return If longValue is present.
     */
    boolean hasLongValue();

    /**
     * @return The doubleValue value.
     */
    double getDoubleValue();

    /**
     * @return Optional doubleValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Double> optionalDoubleValue();

    /**
     * @return If doubleValue is present.
     */
    boolean hasDoubleValue();

    /**
     * @return The stringValue value.
     */
    String getStringValue();

    /**
     * @return Optional stringValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalStringValue();

    /**
     * @return If stringValue is present.
     */
    boolean hasStringValue();

    /**
     * @return The binaryValue value.
     */
    net.morimekta.util.Binary getBinaryValue();

    /**
     * @return Optional binaryValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.util.Binary> optionalBinaryValue();

    /**
     * @return If binaryValue is present.
     */
    boolean hasBinaryValue();

    /**
     * @return The enumValue value.
     */
    net.morimekta.test.providence.reflect.Value getEnumValue();

    /**
     * @return Optional enumValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.reflect.Value> optionalEnumValue();

    /**
     * @return If enumValue is present.
     */
    boolean hasEnumValue();

    /**
     * @return The compactValue value.
     */
    net.morimekta.test.providence.reflect.CompactFields getCompactValue();

    /**
     * @return Optional compactValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.reflect.CompactFields> optionalCompactValue();

    /**
     * @return If compactValue is present.
     */
    boolean hasCompactValue();

}
