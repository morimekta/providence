package net.morimekta.test.providence.reflect;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public class ContainerService {
    public interface Iface {
        /**
         * @param pC The c value.
         * @return The load result.
         * @throws net.morimekta.test.providence.reflect.ExceptionFields The ef exception.
         * @throws java.io.IOException On providence or non-declared exceptions.
         */
        @javax.annotation.Nonnull
        net.morimekta.test.providence.reflect.CompactFields load(
                net.morimekta.test.providence.reflect.Containers pC)
                throws java.io.IOException,
                       net.morimekta.test.providence.reflect.ExceptionFields;
    }

    /**
     * Client implementation for providence.ContainerService
     */
    public static final class Client
            extends net.morimekta.providence.PClient
            implements Iface {
        private final net.morimekta.providence.PServiceCallHandler handler;

        /**
         * Create providence.ContainerService service client.
         *
         * @param handler The client handler.
         */
        public Client(net.morimekta.providence.PServiceCallHandler handler) {
            this.handler = handler;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.reflect.CompactFields load(
                net.morimekta.test.providence.reflect.Containers pC)
                throws java.io.IOException,
                       net.morimekta.test.providence.reflect.ExceptionFields {
            Load_Request._Builder rq = Load_Request.builder();
            if (pC != null) {
                rq.setC(pC);
            }

            net.morimekta.providence.PServiceCall<Load_Request> call = new net.morimekta.providence.PServiceCall<>("load", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, ContainerService.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Load_Response msg = (Load_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case EF:
                        throw msg.getEf();
                    case SUCCESS:
                        return msg.getSuccess();
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for providence.ContainerService.load() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }
    }

    public static final class Processor implements net.morimekta.providence.PProcessor {
        private final Iface impl;
        private final java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer;
        public Processor(@javax.annotation.Nonnull Iface impl) {
            this(impl, (m, e) -> e);
        }

        public Processor(@javax.annotation.Nonnull Iface impl,
                         @javax.annotation.Nonnull java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer) {
            this.impl = impl;
            this.exceptionTransformer = exceptionTransformer;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getDescriptor() {
            return kDescriptor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <Request extends net.morimekta.providence.PMessage<Request>,
                Response extends net.morimekta.providence.PMessage<Response>>
        net.morimekta.providence.PServiceCall<Response> handleCall(
                net.morimekta.providence.PServiceCall<Request> call,
                net.morimekta.providence.descriptor.PService service)
                throws net.morimekta.providence.PApplicationException {
            net.morimekta.providence.PApplicationException ex = null;
            switch(call.getMethod()) {
                case "load": {
                    Load_Response._Builder rsp = Load_Response.builder();
                    try {
                        Load_Request req = (Load_Request) call.getMessage();
                        try {
                            net.morimekta.test.providence.reflect.CompactFields result =
                                    impl.load(req.getC());
                            if (result == null) {
                                ex = new net.morimekta.providence.PApplicationException(
                                        "Returning null value from load in providence.ContainerService",
                                        net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
                                break;
                            }
                            rsp.setSuccess(result);
                        } catch (net.morimekta.test.providence.reflect.ExceptionFields e) {
                            throw e;
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.LOAD, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method load in providence.ContainerService: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (net.morimekta.test.providence.reflect.ExceptionFields e) {
                        rsp.setEf(e);
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method load in providence.ContainerService: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                default: {
                    ex = new net.morimekta.providence.PApplicationException(
                            "Unknown method \"" + call.getMethod() + "\" on providence.ContainerService.",
                            net.morimekta.providence.PApplicationExceptionType.UNKNOWN_METHOD);
                    break;
                }
            }
            net.morimekta.providence.PServiceCall reply =
                    new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                              net.morimekta.providence.PServiceCallType.EXCEPTION,
                                                              call.getSequence(),
                                                              ex);
            return reply;
        }
    }

    public enum Method implements net.morimekta.providence.descriptor.PServiceMethod {
        LOAD("load", false, false, Load_Request.kDescriptor, Load_Response.kDescriptor),
        ;

        private final String name;
        private final boolean oneway;
        private final boolean protoStub;
        private final net.morimekta.providence.descriptor.PStructDescriptor request;
        private final net.morimekta.providence.descriptor.PUnionDescriptor response;

        private Method(String name, boolean oneway, boolean protoStub, net.morimekta.providence.descriptor.PStructDescriptor request, net.morimekta.providence.descriptor.PUnionDescriptor response) {
            this.name = name;
            this.oneway = oneway;
            this.protoStub = protoStub;
            this.request = request;
            this.response = response;
        }

        @javax.annotation.Nonnull
        @Override
        public String getName() {
            return name;
        }

        public boolean isOneway() {
            return oneway;
        }

        public boolean isProtoStub() {
            return protoStub;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor getRequestType() {
            return request;
        }

        @javax.annotation.Nullable
        @Override
        public net.morimekta.providence.descriptor.PUnionDescriptor getResponseType() {
            return response;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }

        @javax.annotation.Nullable
        public static Method findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "load": return LOAD;
            }
            return null;
        }
        @javax.annotation.Nonnull
        public static Method methodForName(@javax.annotation.Nonnull String name) {
            Method method = findByName(name);
            if (method == null) {
                throw new IllegalArgumentException("No such method \"" + name + "\" in service providence.ContainerService");
            }
            return method;
        }
    }

    private static final class _Descriptor extends net.morimekta.providence.descriptor.PService {
        private _Descriptor() {
            super("providence", "ContainerService", null, Method.values());
        }

        @Override
        public Method getMethod(String name) {
            return Method.findByName(name);
        }
    }

    private static final class _Provider implements net.morimekta.providence.descriptor.PServiceProvider {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }
    }

    public static final net.morimekta.providence.descriptor.PService kDescriptor = new _Descriptor();

    public static net.morimekta.providence.descriptor.PServiceProvider provider() {
        return new _Provider();
    }

    // type --> ContainerService.load.request
    @SuppressWarnings("unused")
    public interface Load_Request_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Load_Request> {
        /**
         * @return The c value.
         */
        net.morimekta.test.providence.reflect.Containers getC();

        /**
         * @return Optional c value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.Containers> optionalC();

        /**
         * @return If c is present.
         */
        boolean hasC();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Load_Request
            implements Load_Request_OrBuilder,
                       net.morimekta.providence.PMessage<Load_Request>,
                       Comparable<Load_Request>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = 642175186578463330L;

        private final transient net.morimekta.test.providence.reflect.Containers mC;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Load_Request tSerializeInstance;

        private Load_Request(_Builder builder) {
            mC = builder.mC_builder != null ? builder.mC_builder.build() : builder.mC;
        }

        public boolean hasC() {
            return mC != null;
        }

        /**
         * @return The <code>c</code> value
         */
        public net.morimekta.test.providence.reflect.Containers getC() {
            return mC;
        }

        /**
         * @return Optional of the <code>c</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.Containers> optionalC() {
            return java.util.Optional.ofNullable(mC);
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return mC != null;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) mC;
                default: return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Load_Request other = (Load_Request) o;
            return java.util.Objects.equals(mC, other.mC);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Load_Request.class,
                        _Field.C, mC);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "providence.ContainerService.load.request" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            if (hasC()) {
                out.append("c:")
                   .append(mC.asString());
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Load_Request other) {
            int c;

            c = Boolean.compare(mC != null, other.mC != null);
            if (c != 0) return c;
            if (mC != null) {
                c = mC.compareTo(other.mC);
                if (c != 0) return c;
            }

            return 0;
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (hasC()) {
                length += writer.writeByte((byte) 12);
                length += writer.writeShort((short) 1);
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mC);
            }

            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Load_Request> {
            C(1, net.morimekta.providence.descriptor.PRequirement.DEFAULT, "c", "c", net.morimekta.test.providence.reflect.Containers.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Load_Request> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 1: return _Field.C;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "c": return _Field.C;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "c": return _Field.C;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in providence.ContainerService.load.request");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ContainerService.load.request");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ContainerService.load.request");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Load_Request> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Load_Request> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PStructDescriptor<Load_Request> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PStructDescriptor<Load_Request> {
            public _Descriptor() {
                super("providence", "ContainerService.load.request", _Builder::new, false);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Load_Request> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PStructDescriptor<Load_Request> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>providence.ContainerService.load.request</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Load_Request>
                implements Load_Request_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private java.util.BitSet optionals;
            private java.util.BitSet modified;

            private net.morimekta.test.providence.reflect.Containers mC;
            private net.morimekta.test.providence.reflect.Containers._Builder mC_builder;

            /**
             * Make a providence.ContainerService.load.request builder instance.
             */
            public _Builder() {
                optionals = new java.util.BitSet(1);
                modified = new java.util.BitSet(1);
            }

            /**
             * Make a mutating builder off a base providence.ContainerService.load.request.
             *
             * @param base The base ContainerService.load.request
             */
            public _Builder(Load_Request base) {
                this();

                if (base.hasC()) {
                    optionals.set(0);
                    mC = base.mC;
                }
            }

            @javax.annotation.Nonnull
            @Override
            public Load_Request._Builder merge(Load_Request from) {
                if (from.hasC()) {
                    optionals.set(0);
                    modified.set(0);
                    if (mC_builder != null) {
                        mC_builder.merge(from.getC());
                    } else if (mC != null) {
                        mC_builder = mC.mutate().merge(from.getC());
                        mC = null;
                    } else {
                        mC = from.getC();
                    }
                }
                return this;
            }

            /**
             * Set the <code>c</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Request._Builder setC(net.morimekta.test.providence.reflect.Containers_OrBuilder value) {
                if (value == null) {
                    return clearC();
                }

                optionals.set(0);
                modified.set(0);
                if (value instanceof net.morimekta.test.providence.reflect.Containers._Builder) {
                    value = ((net.morimekta.test.providence.reflect.Containers._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.Containers)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for providence.Containers: " + value.getClass().getName());
                }
                mC = (net.morimekta.test.providence.reflect.Containers) value;
                mC_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>c</code> field.
             *
             * @return True if c has been set.
             */
            public boolean isSetC() {
                return optionals.get(0);
            }

            /**
             * Checks for presence of the <code>c</code> field.
             *
             * @return True if c is present.
             */
            public boolean hasC() {
                return optionals.get(0);
            }

            /**
             * Checks if the <code>c</code> field has been modified since the
             * builder was created.
             *
             * @return True if c has been modified.
             */
            public boolean isModifiedC() {
                return modified.get(0);
            }

            /**
             * Clear the <code>c</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Request._Builder clearC() {
                optionals.clear(0);
                modified.set(0);
                mC = null;
                mC_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>c</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.Containers._Builder mutableC() {
                optionals.set(0);
                modified.set(0);

                if (mC != null) {
                    mC_builder = mC.mutate();
                    mC = null;
                } else if (mC_builder == null) {
                    mC_builder = net.morimekta.test.providence.reflect.Containers.builder();
                }
                return mC_builder;
            }

            /**
             * @return The <code>c</code> field value
             */
            public net.morimekta.test.providence.reflect.Containers getC() {
                return mC_builder != null ? mC_builder.build() : mC;
            }

            /**
             * @return Optional <code>c</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.Containers> optionalC() {
                return java.util.Optional.ofNullable(mC_builder != null ? mC_builder.build() : mC);
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Load_Request._Builder other = (Load_Request._Builder) o;
                return java.util.Objects.equals(optionals, other.optionals) &&
                       java.util.Objects.equals(getC(), other.getC());
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Load_Request.class, optionals,
                        Load_Request._Field.C, getC());
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    case 1: return mutableC();
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Load_Request._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 1: setC((net.morimekta.test.providence.reflect.Containers) value); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 1: return optionals.get(0);
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                switch (key) {
                    case 1: return modified.get(0);
                    default: break;
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 1: return (T) getC();
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 1: return mC != null || mC_builder != null;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Load_Request._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Load_Request._Builder clear(int key) {
                switch (key) {
                    case 1: clearC(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                return true;
            }

            @Override
            public Load_Request._Builder validate() {
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PStructDescriptor<Load_Request> descriptor() {
                return Load_Request.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 1: {
                            if (type == 12) {
                                mC = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.Containers.kDescriptor, strict);
                                optionals.set(0);
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.ContainerService.load.request.c, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Load_Request build() {
                return new Load_Request(this);
            }
        }
    }

    // type <-- ContainerService.load.response
    @SuppressWarnings("unused")
    public interface Load_Response_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Load_Response> {
        /**
         * @return The success value.
         */
        net.morimekta.test.providence.reflect.CompactFields getSuccess();

        /**
         * @return Optional success value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.CompactFields> optionalSuccess();

        /**
         * @return If success is present.
         */
        boolean hasSuccess();

        /**
         * @return The ef value.
         */
        net.morimekta.test.providence.reflect.ExceptionFields getEf();

        /**
         * @return Optional ef value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<net.morimekta.test.providence.reflect.ExceptionFields> optionalEf();

        /**
         * @return If ef is present.
         */
        boolean hasEf();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Load_Response
            implements Load_Response_OrBuilder,
                       net.morimekta.providence.PUnion<Load_Response>,
                       Comparable<Load_Response>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = 4669041823902453548L;

        private final transient net.morimekta.test.providence.reflect.CompactFields mSuccess;
        private final transient net.morimekta.test.providence.reflect.ExceptionFields mEf;

        private transient final _Field tUnionField;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Load_Response tSerializeInstance;

        /**
         * @param value The union value
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Load_Response withSuccess(@javax.annotation.Nonnull net.morimekta.test.providence.reflect.CompactFields_OrBuilder value) {
            return new _Builder().setSuccess(value).build();
        }

        /**
         * @param value The union value
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Load_Response withEf(@javax.annotation.Nonnull net.morimekta.test.providence.reflect.ExceptionFields_OrBuilder value) {
            return new _Builder().setEf(value).build();
        }

        private Load_Response(_Builder builder) {
            tUnionField = builder.tUnionField;

            mSuccess = tUnionField != _Field.SUCCESS
                    ? null
                    : builder.mSuccess_builder != null ? builder.mSuccess_builder.build() : builder.mSuccess;
            mEf = tUnionField != _Field.EF
                    ? null
                    : builder.mEf_builder != null ? builder.mEf_builder.build() : builder.mEf;
        }

        public boolean hasSuccess() {
            return tUnionField == _Field.SUCCESS && mSuccess != null;
        }

        /**
         * @return The <code>success</code> value
         */
        public net.morimekta.test.providence.reflect.CompactFields getSuccess() {
            return mSuccess;
        }

        /**
         * @return Optional of the <code>success</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.CompactFields> optionalSuccess() {
            return java.util.Optional.ofNullable(mSuccess);
        }

        public boolean hasEf() {
            return tUnionField == _Field.EF && mEf != null;
        }

        /**
         * @return The <code>ef</code> value
         */
        public net.morimekta.test.providence.reflect.ExceptionFields getEf() {
            return mEf;
        }

        /**
         * @return Optional of the <code>ef</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.reflect.ExceptionFields> optionalEf() {
            return java.util.Optional.ofNullable(mEf);
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 0: return tUnionField == _Field.SUCCESS;
                case 1: return tUnionField == _Field.EF;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 0: return (T) mSuccess;
                case 1: return (T) mEf;
                default: return null;
            }
        }

        @Override
        public boolean unionFieldIsSet() {
            return tUnionField != null;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field unionField() {
            if (tUnionField == null) throw new IllegalStateException("No union field set in providence.ContainerService.load.response");
            return tUnionField;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Load_Response other = (Load_Response) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                   java.util.Objects.equals(mSuccess, other.mSuccess) &&
                   java.util.Objects.equals(mEf, other.mEf);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Load_Response.class,
                        _Field.SUCCESS, mSuccess,
                        _Field.EF, mEf);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "providence.ContainerService.load.response" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            switch (tUnionField) {
                case SUCCESS: {
                    out.append("success:")
                       .append(mSuccess.asString());
                    break;
                }
                case EF: {
                    out.append("ef:")
                       .append(mEf.asString());
                    break;
                }
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Load_Response other) {
            if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
            int c = tUnionField.compareTo(other.tUnionField);
            if (c != 0) return c;

            switch (tUnionField) {
                case SUCCESS:
                    return mSuccess.compareTo(other.mSuccess);
                case EF:
                    return mEf.compareTo(other.mEf);
                default: return 0;
            }
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (tUnionField != null) {
                switch (tUnionField) {
                    case SUCCESS: {
                        length += writer.writeByte((byte) 12);
                        length += writer.writeShort((short) 0);
                        length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mSuccess);
                        break;
                    }
                    case EF: {
                        length += writer.writeByte((byte) 12);
                        length += writer.writeShort((short) 1);
                        length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mEf);
                        break;
                    }
                    default: break;
                }
            }
            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Load_Response> {
            SUCCESS(0, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "success", "success", net.morimekta.test.providence.reflect.CompactFields.provider(), null, null),
            EF(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "ef", "ef", net.morimekta.test.providence.reflect.ExceptionFields.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Load_Response> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 0: return _Field.SUCCESS;
                    case 1: return _Field.EF;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                    case "ef": return _Field.EF;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                    case "ef": return _Field.EF;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in providence.ContainerService.load.response");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ContainerService.load.response");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ContainerService.load.response");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<Load_Response> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<Load_Response> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PUnionDescriptor<Load_Response> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PUnionDescriptor<Load_Response> {
            public _Descriptor() {
                super("providence", "ContainerService.load.response", _Builder::new, false);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<Load_Response> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PUnionDescriptor<Load_Response> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>providence.ContainerService.load.response</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Load_Response>
                implements Load_Response_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private Load_Response._Field tUnionField;

            private boolean modified;

            private net.morimekta.test.providence.reflect.CompactFields mSuccess;
            private net.morimekta.test.providence.reflect.CompactFields._Builder mSuccess_builder;
            private net.morimekta.test.providence.reflect.ExceptionFields mEf;
            private net.morimekta.test.providence.reflect.ExceptionFields._Builder mEf_builder;

            /**
             * Make a providence.ContainerService.load.response builder instance.
             */
            public _Builder() {
                modified = false;
            }

            /**
             * Make a mutating builder off a base providence.ContainerService.load.response.
             *
             * @param base The base ContainerService.load.response
             */
            public _Builder(Load_Response base) {
                this();

                tUnionField = base.tUnionField;

                mSuccess = base.mSuccess;
                mEf = base.mEf;
            }

            @javax.annotation.Nonnull
            @Override
            public Load_Response._Builder merge(Load_Response from) {
                if (!from.unionFieldIsSet()) {
                    return this;
                }

                switch (from.unionField()) {
                    case SUCCESS: {
                        if (tUnionField == Load_Response._Field.SUCCESS && mSuccess != null) {
                            mSuccess = mSuccess.mutate().merge(from.getSuccess()).build();
                        } else {
                            setSuccess(from.getSuccess());
                        }
                        break;
                    }
                    case EF: {
                        if (tUnionField == Load_Response._Field.EF && mEf != null) {
                            mEf = mEf.mutate().merge(from.getEf()).build();
                        } else {
                            setEf(from.getEf());
                        }
                        break;
                    }
                }
                return this;
            }

            /**
             * Set the <code>success</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Response._Builder setSuccess(net.morimekta.test.providence.reflect.CompactFields_OrBuilder value) {
                if (value == null) {
                    return clearSuccess();
                }

                tUnionField = Load_Response._Field.SUCCESS;
                modified = true;
                if (value instanceof net.morimekta.test.providence.reflect.CompactFields._Builder) {
                    value = ((net.morimekta.test.providence.reflect.CompactFields._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.CompactFields)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for providence.CompactFields: " + value.getClass().getName());
                }
                mSuccess = (net.morimekta.test.providence.reflect.CompactFields) value;
                mSuccess_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>success</code> field.
             *
             * @return True if success has been set.
             */
            public boolean isSetSuccess() {
                return tUnionField == Load_Response._Field.SUCCESS;
            }

            /**
             * Checks for presence of the <code>success</code> field.
             *
             * @return True if success is present.
             */
            public boolean hasSuccess() {
                return tUnionField == Load_Response._Field.SUCCESS;
            }

            /**
             * Clear the <code>success</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Response._Builder clearSuccess() {
                if (tUnionField == Load_Response._Field.SUCCESS) tUnionField = null;
                modified = true;
                mSuccess = null;
                mSuccess_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>success</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.CompactFields._Builder mutableSuccess() {
                if (tUnionField != Load_Response._Field.SUCCESS) {
                    clearSuccess();
                }
                tUnionField = Load_Response._Field.SUCCESS;
                modified = true;

                if (mSuccess != null) {
                    mSuccess_builder = mSuccess.mutate();
                    mSuccess = null;
                } else if (mSuccess_builder == null) {
                    mSuccess_builder = net.morimekta.test.providence.reflect.CompactFields.builder();
                }
                return mSuccess_builder;
            }

            /**
             * @return The <code>success</code> field value
             */
            public net.morimekta.test.providence.reflect.CompactFields getSuccess() {
                return mSuccess_builder != null ? mSuccess_builder.build() : mSuccess;
            }

            /**
             * @return Optional <code>success</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.CompactFields> optionalSuccess() {
                return java.util.Optional.ofNullable(mSuccess_builder != null ? mSuccess_builder.build() : mSuccess);
            }

            /**
             * Set the <code>ef</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Response._Builder setEf(net.morimekta.test.providence.reflect.ExceptionFields_OrBuilder value) {
                if (value == null) {
                    return clearEf();
                }

                tUnionField = Load_Response._Field.EF;
                modified = true;
                if (value instanceof net.morimekta.test.providence.reflect.ExceptionFields._Builder) {
                    value = ((net.morimekta.test.providence.reflect.ExceptionFields._Builder) value).build();
                } else if (!(value instanceof net.morimekta.test.providence.reflect.ExceptionFields)) {
                    throw new java.lang.IllegalArgumentException("Invalid type for providence.ExceptionFields: " + value.getClass().getName());
                }
                mEf = (net.morimekta.test.providence.reflect.ExceptionFields) value;
                mEf_builder = null;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>ef</code> field.
             *
             * @return True if ef has been set.
             */
            public boolean isSetEf() {
                return tUnionField == Load_Response._Field.EF;
            }

            /**
             * Checks for presence of the <code>ef</code> field.
             *
             * @return True if ef is present.
             */
            public boolean hasEf() {
                return tUnionField == Load_Response._Field.EF;
            }

            /**
             * Clear the <code>ef</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Load_Response._Builder clearEf() {
                if (tUnionField == Load_Response._Field.EF) tUnionField = null;
                modified = true;
                mEf = null;
                mEf_builder = null;
                return this;
            }

            /**
             * Get the builder for the contained <code>ef</code> message field.
             *
             * @return The field message builder
             */
            @javax.annotation.Nonnull
            public net.morimekta.test.providence.reflect.ExceptionFields._Builder mutableEf() {
                if (tUnionField != Load_Response._Field.EF) {
                    clearEf();
                }
                tUnionField = Load_Response._Field.EF;
                modified = true;

                if (mEf != null) {
                    mEf_builder = mEf.mutate();
                    mEf = null;
                } else if (mEf_builder == null) {
                    mEf_builder = net.morimekta.test.providence.reflect.ExceptionFields.builder();
                }
                return mEf_builder;
            }

            /**
             * @return The <code>ef</code> field value
             */
            public net.morimekta.test.providence.reflect.ExceptionFields getEf() {
                return mEf_builder != null ? mEf_builder.build() : mEf;
            }

            /**
             * @return Optional <code>ef</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<net.morimekta.test.providence.reflect.ExceptionFields> optionalEf() {
                return java.util.Optional.ofNullable(mEf_builder != null ? mEf_builder.build() : mEf);
            }

            /**
             * Checks if the <code>ContainerService.load.response</code> union has been modified since the
             * builder was created.
             *
             * @return True if ContainerService.load.response has been modified.
             */
            public boolean isUnionModified() {
                return modified;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Load_Response._Builder other = (Load_Response._Builder) o;
                return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                       java.util.Objects.equals(getSuccess(), other.getSuccess()) &&
                       java.util.Objects.equals(getEf(), other.getEf());
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Load_Response.class,
                        Load_Response._Field.SUCCESS, getSuccess(),
                        Load_Response._Field.EF, getEf());
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    case 0: return mutableSuccess();
                    case 1: return mutableEf();
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Load_Response._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 0: setSuccess((net.morimekta.test.providence.reflect.CompactFields) value); break;
                    case 1: setEf((net.morimekta.test.providence.reflect.ExceptionFields) value); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 0: return tUnionField == Load_Response._Field.SUCCESS;
                    case 1: return tUnionField == Load_Response._Field.EF;
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                return modified;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 0: return (T) getSuccess();
                    case 1: return (T) getEf();
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 0: return tUnionField == _Field.SUCCESS;
                    case 1: return tUnionField == _Field.EF;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Load_Response._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Load_Response._Builder clear(int key) {
                switch (key) {
                    case 0: clearSuccess(); break;
                    case 1: clearEf(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                if (tUnionField == null) {
                    return false;
                }

                switch (tUnionField) {
                    case SUCCESS: return mSuccess != null || mSuccess_builder != null;
                    case EF: return mEf != null || mEf_builder != null;
                    default: return true;
                }
            }

            @Override
            public Load_Response._Builder validate() {
                if (!valid()) {
                    throw new java.lang.IllegalStateException("No union field set in providence.ContainerService.load.response");
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PUnionDescriptor<Load_Response> descriptor() {
                return Load_Response.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 0: {
                            if (type == 12) {
                                mSuccess = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.CompactFields.kDescriptor, strict);
                                tUnionField = Load_Response._Field.SUCCESS;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.ContainerService.load.response.success, should be struct(12)");
                            }
                            break;
                        }
                        case 1: {
                            if (type == 12) {
                                mEf = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.reflect.ExceptionFields.kDescriptor, strict);
                                tUnionField = Load_Response._Field.EF;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.ContainerService.load.response.ef, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Load_Response build() {
                return new Load_Response(this);
            }
        }
    }

    protected ContainerService() {}
}
