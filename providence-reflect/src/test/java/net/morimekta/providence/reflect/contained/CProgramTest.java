/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.collect.UnmodifiableMap;
import net.morimekta.util.collect.UnmodifiableSet;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class CProgramTest {
    @Test
    public void testProgram() {
        CStructDescriptor declaredDesc = new CStructDescriptor(
                null,
                "prog",
                "Struct",
                UnmodifiableList.listOf(), UnmodifiableMap.mapOf(), null);
        CService service = new CService("Documentation",
                                        "prog",
                                        "Service",
                                        null,
                                        UnmodifiableList.listOf(),
                                        UnmodifiableMap.mapOf());
        CConst constant = new CConst(null, "prog",
                                     "kC",
                                     PPrimitive.STRING.provider(),
                                     () -> "value",
                                     UnmodifiableMap.mapOf());

        CProgram program = new CProgram(
                "prog.thrift",
                "Documentation",
                "prog",
                UnmodifiableMap.mapOf("java", "net.morimekta.providence",
                                "cpp", "morimekta.providence"),
                UnmodifiableSet.setOf("first", "second"),
                UnmodifiableList.listOf("../something/first.thrift", "second.thrift"),
                UnmodifiableMap.mapOf("S", "Struct"),
                UnmodifiableList.listOf(declaredDesc),
                UnmodifiableList.listOf(service),
                UnmodifiableList.listOf(constant));

        assertThat(program.getDocumentation(), is("Documentation"));
        assertThat(program.getProgramName(), is("prog"));
        assertThat(program.getTypedefs().entrySet(), hasSize(1));
        assertThat(program.getTypedefs().get("S"), is("Struct"));

        assertThat(program.getConstants(), hasSize(1));
        assertThat(program.getConstants(), hasItem(constant));
        assertThat(program.getServices(), hasSize(1));
        assertThat(program.getServices(), hasItem(service));
        assertThat(program.getDeclaredTypes(), hasSize(1));
        assertThat(program.getDeclaredTypes(), hasItem(declaredDesc));

        assertThat(program.getNamespaceForLanguage("java"), is("net.morimekta.providence"));
        assertThat(program.getNamespaceForLanguage("cpp"), is("morimekta.providence"));
    }

    @Test
    public void testEmptyProgram() {
        CProgram program = new CProgram("prog.thrift",null, "prog", null, null, null, null, null, null, null);

        assertThat(program.getProgramFilePath(), is(notNullValue()));
        assertThat(program.getDocumentation(), is(nullValue()));
        assertThat(program.getProgramName(), is("prog"));
        assertThat(program.getNamespaces(), is(Collections.EMPTY_MAP));
        assertThat(program.getIncludedPrograms(), is(Collections.EMPTY_SET));
        assertThat(program.getIncludedFiles(), is(Collections.EMPTY_LIST));
        assertThat(program.getTypedefs(), is(Collections.EMPTY_MAP));
        assertThat(program.getDeclaredTypes(), is(Collections.EMPTY_LIST));
        assertThat(program.getServices(), is(Collections.EMPTY_LIST));
        assertThat(program.getConstants(), is(Collections.EMPTY_LIST));
    }
}
