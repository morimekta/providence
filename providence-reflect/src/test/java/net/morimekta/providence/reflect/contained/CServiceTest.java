/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.descriptor.PAnnotation;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.reflect.ProgramRegistry;
import net.morimekta.testing.ResourceUtils;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.collect.UnmodifiableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static net.morimekta.providence.types.TypeReference.parseType;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class CServiceTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    private ProgramRegistry registry;

    @Before
    public void setUp() throws IOException {
        ResourceUtils.copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());
        File calculator = ResourceUtils.copyResourceTo("/parser/calculator/calculator.thrift", tmp.getRoot());
        registry = new ProgramLoader().load(calculator);
    }

    @Test
    public void testService() {
        CService base = (CService) registry.requireService(parseType("calculator.BaseCalculator"));
        assertThat(base.getExtendsService(), is(nullValue()));

        CService calc = (CService) registry.requireService(parseType("calculator.Calculator"));
        assertThat(calc.getExtendsService(), is(base));

        CServiceMethod iamalive = calc.getMethod("iamalive");  // yes, this goes
        assertThat(iamalive, is(notNullValue()));
        CServiceMethod numCalls = calc.getMethod("numCalls");  // yes, this goes
        assertThat(numCalls, is(notNullValue()));
        CServiceMethod calculate = calc.getMethod("calculate");
        assertThat(calculate, is(notNullValue()));

        assertThat(calc.getMethod("ping"), is(nullValue()));

        assertThat(base.getMethods(), is(UnmodifiableList.listOf(iamalive, numCalls)));
        assertThat(calc.getMethods(), is(UnmodifiableList.listOf(calculate)));

        assertThat(base.getAnnotations(), is(UnmodifiableSet.setOf("deprecated")));
        assertThat(calc.getAnnotations(), is(UnmodifiableSet.setOf()));

        assertThat(base.hasAnnotation("deprecated"), is(true));
        assertThat(base.hasAnnotation(PAnnotation.DEPRECATED), is(true));
        assertThat(base.hasAnnotation("foobar"), is(false));

        assertThat(base.getAnnotationValue("deprecated"), is("Because reasons"));
        assertThat(base.getAnnotationValue(PAnnotation.DEPRECATED), is("Because reasons"));
        assertThat(base.getAnnotationValue("foobar"), is(nullValue()));

        assertThat(base.getDocumentation(), is(nullValue()));
        assertThat(calc.getDocumentation(), is("Block comment on service"));
    }
}
