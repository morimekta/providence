/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.providence.reflect.parser;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.reflect.model.Declaration;
import net.morimekta.providence.reflect.model.ProgramDeclaration;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.Collectors;

import static net.morimekta.testing.ResourceUtils.copyResourceTo;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Stein Eldar Johnsen
 * @since 05.09.15
 */
@RunWith(DataProviderRunner.class)
public class ThriftParserTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testParse_calculator() throws IOException {
        copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());
        copyResourceTo("/parser/calculator/calculator.thrift", tmp.getRoot());

        File calculator = new File(tmp.getRoot(), "calculator.thrift");

        ThriftParser       parser  = new ThriftParser();
        ProgramDeclaration program = parser.parse(new FileInputStream(calculator), calculator.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("Operator",
                             "Operand",
                             "Operation",
                             "CalculateException",
                             "BaseCalculator",
                             "Calculator",
                             "PI",
                             "kComplexOperands")));
    }

    @Test
    public void testParse_calculator_strict() throws IOException {
        copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());
        copyResourceTo("/parser/calculator/calculator_strict.thrift", tmp.getRoot());

        File calculator = new File(tmp.getRoot(), "calculator_strict.thrift");

        ThriftParser       parser  = new ThriftParser(true, true, false, true);
        ProgramDeclaration program = parser.parse(new FileInputStream(calculator), calculator.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("Operator",
                             "Operand",
                             "Operation",
                             "CalculateException",
                             "Calculator",
                             "PI",
                             "kComplexOperands")));
    }

    @Test
    public void testParse_number() throws IOException {
        copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());

        File number = new File(tmp.getRoot(), "number.thrift");

        ThriftParser       parser  = new ThriftParser();
        ProgramDeclaration program = parser.parse(new FileInputStream(number), number.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("real",
                             "Imaginary",
                             "I",
                             "kSqrtMinusOne")));
    }

    @Test
    public void testParser_annotations() throws IOException {
        copyResourceTo("/parser/tests/annotations.thrift", tmp.getRoot());

        File annotations = new File(tmp.getRoot(), "annotations.thrift");

        ThriftParser parser  = new ThriftParser();
        ProgramDeclaration program = parser.parse(new FileInputStream(annotations), annotations.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("E",
                             "S",
                             "Srv")));
    }

    @Test
    public void testAutoId() throws IOException {
        copyResourceTo("/parser/tests/autoid.thrift", tmp.getRoot());

        File autoid = new File(tmp.getRoot(), "autoid.thrift");

        ThriftParser       parser  = new ThriftParser();
        ProgramDeclaration program = parser.parse(new FileInputStream(autoid), autoid.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("AutoId",
                             "AutoParam")));
    }

    @Test
    public void testAutoValue() throws IOException {
        copyResourceTo("/parser/tests/autovalue.thrift", tmp.getRoot());

        File autovalue = new File(tmp.getRoot(), "autovalue.thrift");

        ThriftParser       parser  = new ThriftParser();
        ProgramDeclaration program = parser.parse(new FileInputStream(autovalue), autovalue.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("AutoValue",
                             "PartialAutoValue")));
    }


    @Test
    public void testParseExceptions() {
        copyResourceTo("/failure/conflicting_field_name.thrift", tmp.getRoot());
        copyResourceTo("/failure/reserved_field_name.thrift", tmp.getRoot());
        copyResourceTo("/failure/duplicate_field_id.thrift", tmp.getRoot());
        copyResourceTo("/failure/duplicate_field_name.thrift", tmp.getRoot());
        copyResourceTo("/failure/invalid_namespace.thrift", tmp.getRoot());
        copyResourceTo("/failure/invalid_include.thrift", tmp.getRoot());
        copyResourceTo("/failure/valid_reference.thrift", tmp.getRoot());
        copyResourceTo("/failure/unknown_include.thrift", tmp.getRoot());
        copyResourceTo("/failure/unknown_program.thrift", tmp.getRoot());
        copyResourceTo("/failure/unknown_type.thrift", tmp.getRoot());
        copyResourceTo("/failure/valid_reference.thrift", tmp.getRoot());
        copyResourceTo("/failure/missing_ref_enum_type.thrift", tmp.getRoot());
        copyResourceTo("/failure/proto_stub_in_thrift.thrift", tmp.getRoot());
        copyResourceTo("/failure/proto_stub_missing.providence", tmp.getRoot());
        copyResourceTo("/failure/proto_stub_request_type.providence", tmp.getRoot());
        copyResourceTo("/failure/proto_stub_return_type.providence", tmp.getRoot());
        copyResourceTo("/failure/proto_stub_return_union.providence", tmp.getRoot());
        copyResourceTo("/failure/ref_enum_not_enum.thrift", tmp.getRoot());
        copyResourceTo("/failure/ref_enum_not_enum2.thrift", tmp.getRoot());

        assertBadThrift("Error in conflicting_field_name.thrift on line 5 row 10-22: Field with name 'separatedName' already exists on line 4\n" +
                        "  2: i32 separatedName;\n" +
                        "---------^^^^^^^^^^^^^",
                        "conflicting_field_name.thrift");
        assertBadThrift("Error in reserved_field_name.thrift on line 5 row 10-15: Field with reserved name: string\n" +
                        "  2: i32 string;\n" +
                        "---------^^^^^^",
                        "reserved_field_name.thrift");
        assertBadThrift("Error in duplicate_field_id.thrift on line 6 row 3: Field with id 1 already exists on line 5\n" +
                        "  1: i32 second;\n" +
                        "--^",
                        "duplicate_field_id.thrift");
        assertBadThrift("Error in duplicate_field_name.thrift on line 4 row 10-14: Field with name 'first' already exists on line 4\n" +
                        "  1: i32 first;\n" +
                        "---------^^^^^",
                        "duplicate_field_name.thrift");
        assertBadThrift("Error in invalid_namespace.thrift on line 1 row 16-27: Identifier with double '.'\n" +
                        "namespace java org.apache..test.failure\n" +
                        "---------------^^^^^^^^^^^^",
                        "invalid_namespace.thrift");
        assertBadThrift("Error in unknown_type.thrift on line 5 row 6-9: No type 'i128' in program 'unknown_type'\n" +
                        "  1: i128 longlong;\n" +
                        "-----^^^^",
                        "unknown_type.thrift");
        assertBadThrift("Error in invalid_include.thrift on line 8 row 1-7: Unexpected token 'include', expected type declaration\n" +
                        "include \"valid_reference.thrift\"\n" +
                        "^^^^^^^",
                        "invalid_include.thrift");
        assertBadThrift("Error in unknown_program.thrift on line 4 row 6-28: No program 'valid_reference' for type 'valid_reference.Message'\n" +
                        "  1: valid_reference.Message message;\n" +
                        "-----^^^^^^^^^^^^^^^^^^^^^^^",
                        "unknown_program.thrift");
        assertBadThrift("Error in unknown_include.thrift on line 3 row 9-29: Included file not found no_such_file.thrift\n" +
                        "include \"no_such_file.thrift\"\n" +
                        "--------^^^^^^^^^^^^^^^^^^^^^",
                        "unknown_include.thrift");
        assertBadThrift("Error in missing_ref_enum_type.thrift on line 5 row 38-48: Unknown ref.enum type 'ref.enum: \"NotExists\"' for 'name' in MyStruct\n" +
                        "    1: optional i32 name (ref.enum = \"NotExists\");\n" +
                        "-------------------------------------^^^^^^^^^^^",
                        "missing_ref_enum_type.thrift");
        assertBadThrift("Error in proto_stub_in_thrift.thrift on line 15 row 33: Expected field name, but got ')'\n" +
                        "    TextArgs protoMethod(Request);\n" +
                        "--------------------------------^",
                        "proto_stub_in_thrift.thrift");
        assertBadThrift("Error in proto_stub_missing.providence on line 10 row 26-32: No type 'Request' in program 'proto_stub_missing'\n" +
                        "    TextArgs protoMethod(Request);\n" +
                        "-------------------------^^^^^^^",
                        "proto_stub_missing.providence");
        assertBadThrift("Error in proto_stub_missing.providence on line 10 row 26-32: No type 'Request' in program 'proto_stub_missing'\n" +
                        "    TextArgs protoMethod(Request);\n" +
                        "-------------------------^^^^^^^",
                        "proto_stub_missing.providence");
        assertBadThrift("Error in proto_stub_request_type.providence on line 8 row 26-28: Primitive type not allowed as request type on stubs\n" +
                        "    Response protoMethod(i32);\n" +
                        "-------------------------^^^",
                        "proto_stub_request_type.providence");
        assertBadThrift("Error in proto_stub_return_type.providence on line 8 row 5-8: Response type not a struct on proto stub method\n" +
                        "    list<i32> protoMethod(Request);\n" +
                        "----^^^^",
                        "proto_stub_return_type.providence");
        assertBadThrift("Error in proto_stub_return_union.providence on line 12 row 5-12: Response type not a struct on proto stub method\n" +
                        "    Response protoMethod(Request);\n" +
                        "----^^^^^^^^",
                        "proto_stub_return_union.providence");
        assertBadThrift("Error in ref_enum_not_enum.thrift on line 9 row 38-45: 'MyEnum' is not an enum for ref.enum 'name' in MyStruct\n" +
                        "    1: optional i32 name (ref.enum = \"MyEnum\");\n" +
                        "-------------------------------------^^^^^^^^",
                        "ref_enum_not_enum.thrift");
        assertBadThrift("Error in ref_enum_not_enum2.thrift on line 5 row 38-45: Primitive type 'binary' for ref.enum for 'name' in MyStruct\n" +
                        "    1: optional i32 name (ref.enum = \"binary\");\n" +
                        "-------------------------------------^^^^^^^^",
                        "ref_enum_not_enum2.thrift");

        copyResourceTo("/failure/if_not_allowed.thrift", tmp.getRoot());
        assertBadThrift("Error in if_not_allowed.thrift on line 4 row 1-9: Unexpected token 'interface'\n" +
                        "interface ToImplement {\n" +
                        "^^^^^^^^^",
                        "if_not_allowed.thrift");

        copyResourceTo("/failure/missing_if_field.providence", tmp.getRoot());
        copyResourceTo("/failure/missing_if_type.providence", tmp.getRoot());
        copyResourceTo("/failure/if_field_mismatch.providence", tmp.getRoot());
        copyResourceTo("/failure/if_field_req_mismatch.providence", tmp.getRoot());
        copyResourceTo("/failure/missing_arguments_type.providence", tmp.getRoot());
        copyResourceTo("/failure/bad_arguments_type.providence", tmp.getRoot());
        copyResourceTo("/failure/bad_arguments_type2.providence", tmp.getRoot());

        assertBadThrift("Error in missing_if_field.providence on line 8 row 1-6: Missing interface field 'value' in MyStruct implementing ToImplement\n" +
                        "struct MyStruct implements ToImplement {\n" +
                        "^^^^^^",
                        "missing_if_field.providence");
        assertBadThrift("Error in missing_if_type.providence on line 4 row 28-38: No type 'ToImplement' in program 'missing_if_type'\n" +
                        "struct MyStruct implements ToImplement {\n" +
                        "---------------------------^^^^^^^^^^^",
                        "missing_if_type.providence");
        assertBadThrift("Error in if_field_mismatch.providence on line 9 row 17-22: Type mismatch for field 'value' in MyStruct implementing ToImplement, string != i32\n" +
                        "    1: optional string value;\n" +
                        "----------------^^^^^^",
                        "if_field_mismatch.providence");
        assertBadThrift("Error in if_field_req_mismatch.providence on line 9 row 8-15: Requirement mismatch for field 'value' in MyStruct implementing ToImplement, OPTIONAL != REQUIRED\n" +
                        "    1: optional i32 value;\n" +
                        "-------^^^^^^^^",
                        "if_field_req_mismatch.providence");

        assertBadThrift("Error in missing_arguments_type.providence on line 5 row 47-52: No type 'Args' for argument type in program 'missing_arguments_type'\n" +
                        "    1: optional string name (arguments.type = \"Args\");\n" +
                        "----------------------------------------------^^^^^^",
                        "missing_arguments_type.providence");
        assertBadThrift("Error in bad_arguments_type.providence on line 9 row 47-52: 'Args' is not a struct for argument type in program 'bad_arguments_type'\n" +
                        "    1: optional string name (arguments.type = \"Args\");\n" +
                        "----------------------------------------------^^^^^^",
                        "bad_arguments_type.providence");
        assertBadThrift("Error in bad_arguments_type2.providence on line 5 row 47-51: Primitive i32 not allowed as argument type\n" +
                        "    1: optional string name (arguments.type = \"i32\");\n" +
                        "----------------------------------------------^^^^^",
                        "bad_arguments_type2.providence");
    }

    @Test
    public void testParseStrictExceptions() {
        copyResourceTo("/parser/calculator/calculator.thrift", tmp.getRoot());
        copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());
        copyResourceTo("/failure/reserved_field_name.thrift", tmp.getRoot());

        assertBadStrictThrift("Error in calculator.thrift on line 14 row 5-7: Explicit enum value required, but missing\n" +
                              "    ADD,\n" +
                              "----^^^",
                              "calculator.thrift");
        assertBadStrictThrift("Error in reserved_field_name.thrift on line 4 row 10-15: Field with reserved name: global\n" +
                              "  1: i32 global;\n" +
                              "---------^^^^^^",
                              "reserved_field_name.thrift");
    }

    @Test
    public void testRegression() throws IOException {
        File regressed = copyResourceTo("/idl/backend-libraries/common_fileserver.thrift", tmp.getRoot());

        ThriftParser parser  = new ThriftParser();
        ProgramDeclaration  program = parser.parse(new FileInputStream(regressed), regressed.toPath());
        assertThat(program.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(listOf("FileServerConfig")));
    }

    private void assertBadThrift(String message, String fileName) {
        try {
            ProgramLoader loader = new ProgramLoader();
            File          file   = new File(tmp.getRoot(), fileName);
            loader.load(file.toPath());
            fail("No exception on bad thrift: " + fileName);
        } catch (ThriftException e) {
            try {
                assertThat(e.displayString().replaceAll("\\r", ""), is(message));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        } catch (IOException e) {
            try {
                assertThat(e.getMessage(), is(message));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    private void assertBadStrictThrift(String message, String fileName) {
        try {
            ThriftParser parser = new ThriftParser(true,
                                                   true,
                                                   false,
                                                   fileName.endsWith(".providence"));
            File file = new File(tmp.getRoot(), fileName);
            parser.parse(new FileInputStream(file), file.toPath());
            fail("No exception on bad thrift: " + fileName);
        } catch (ThriftException e) {
            assertEquals(message, e.displayString().replaceAll("\\r", ""));
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }
}
