// This is used as a valid reference in failure tests.
namespace java org.apache.test.failure

interface ToImplement {
    optional i32 value;
}

struct MyStruct implements ToImplement {
    1: optional i32 value;
}
