namespace java net.morimekta.test.providence.testing.math

include "../calculator/number.thrift"

struct OtherCalculator {
    1: optional number.Imaginary math;
}

const string kValue = "constant value";

const map<string,i32> kMap = {
    kValue : 12
};

enum Numbers {
    E,
    PI,
}

const map<i32, number.Imaginary> imaginaries = {
    Numbers.E: {
        'v': 3.141592;
        'i': -2.71828;
    },
}
