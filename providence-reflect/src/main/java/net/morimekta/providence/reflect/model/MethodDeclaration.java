package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * <pre>{@code
 * function ::= 'oneway'? {type} {name} '(' {field}* ')' ('throws' '(' {field}* ')')? {annotations}?
 * }</pre>
 */
public class MethodDeclaration extends Declaration {
    private final ThriftToken            oneway;
    private final List<ThriftToken>      returnTypeTokens;
    private final List<FieldDeclaration> params;
    private final ThriftToken            requestTypeToken;
    private final List<FieldDeclaration> throwing;

    public MethodDeclaration(@Nullable String documentation,
                             @Nullable ThriftToken oneway,
                             @Nonnull List<ThriftToken> returnTypeTokens,
                             @Nonnull ThriftToken name,
                             @Nullable List<FieldDeclaration> params,
                             @Nullable ThriftToken requestTypeToken,
                             @Nullable List<FieldDeclaration> throwing,
                             @Nullable List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        if (requestTypeToken == null && params == null) {
            throw new IllegalArgumentException("Either request type or params must be non-null");
        } else if (requestTypeToken != null && params != null) {
            throw new IllegalArgumentException("Only one of request type and params may be non-null");
        }

        this.oneway = oneway;
        this.returnTypeTokens = UnmodifiableList.copyOf(returnTypeTokens);
        this.requestTypeToken = requestTypeToken;
        this.params = requestTypeToken == null ? UnmodifiableList.copyOf(params) : null;
        this.throwing = throwing == null || throwing.isEmpty() ? null : UnmodifiableList.copyOf(throwing);
    }

    public boolean isOneWay() {
        return oneway != null;
    }

    @Nullable
    public ThriftToken getRequestTypeToken() {
        return requestTypeToken;
    }

    @Nullable
    public ThriftToken getOneWayToken() {
        return oneway;
    }

    @Nonnull
    public String getReturnType() {
        return DeclarationUtil.toTypeString(returnTypeTokens);
    }

    @Nonnull
    public List<ThriftToken> getReturnTypeTokens() {
        return returnTypeTokens;
    }

    @Nonnull
    public List<FieldDeclaration> getParams() {
        return params;
    }

    @Nullable
    public List<FieldDeclaration> getThrowing() {
        return throwing;
    }
}
