package net.morimekta.providence.reflect.parser;

import net.morimekta.util.lexer.TokenType;

import java.util.Locale;

public enum ThriftTokenType implements TokenType {
    IDENTIFIER,
    STRING,
    NUMBER,
    SYMBOL,
    DOCUMENTATION,
    // parsed as a token, and then skipped by the tokenizer, so totally ignored.
    COMMENT,
    ;

    @Override
    public String toString() {
        return "<" + name().toLowerCase(Locale.US) + ">";
    }
}
