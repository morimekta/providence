package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * <pre>{@code
 * enum ::= 'enum' {name} '{' {enum_value}* '}' {annotations}?
 * }</pre>
 */
public class EnumDeclaration extends Declaration {
    private final ThriftToken                enumToken;
    private final List<EnumValueDeclaration> values;

    public EnumDeclaration(@Nullable String documentation,
                           @Nonnull ThriftToken enumToken,
                           @Nonnull ThriftToken name,
                           @Nonnull List<EnumValueDeclaration> values,
                           @Nullable List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.enumToken = enumToken;
        this.values = values;
    }

    @Nonnull
    public ThriftToken getEnumToken() {
        return enumToken;
    }

    @Nonnull
    public List<EnumValueDeclaration> getValues() {
        return values;
    }
}
