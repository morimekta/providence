package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;

public class NamespaceDeclaration {
    private final ThriftToken namespaceToken;
    private final ThriftToken language;
    private final ThriftToken namespace;

    public NamespaceDeclaration(@Nonnull ThriftToken namespaceToken,
                                @Nonnull ThriftToken language,
                                @Nonnull ThriftToken namespace) {
        this.namespaceToken = namespaceToken;
        this.language = language;
        this.namespace = namespace;
    }

    @Nonnull
    public String getLanguage() {
        return language.toString();
    }

    @Nonnull
    public ThriftToken getNameToken() {
        return language;
    }

    @Nonnull
    public String getNamespace() {
        return namespace.toString();
    }

    @Nonnull
    public ThriftToken getNamespaceToken() {
        return namespace;
    }
}
