package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * <pre>{@code
 * typedef ::= 'typedef' {type} {name}
 * }</pre>
 */
public class TypedefDeclaration extends Declaration {
    private final ThriftToken       typedefToken;
    private final List<ThriftToken> type;

    public TypedefDeclaration(@Nullable String documentation,
                              @Nonnull ThriftToken typedefToken,
                              @Nonnull ThriftToken name,
                              @Nonnull List<ThriftToken> type) {
        super(documentation, name, null);
        this.typedefToken = typedefToken;
        this.type = type;
    }

    @Nonnull
    public ThriftToken getTypedefToken() {
        return typedefToken;
    }

    @Nonnull
    public List<ThriftToken> getTypeTokens() {
        return type;
    }

    @Nonnull
    public String getType() {
        return DeclarationUtil.toTypeString(type);
    }
}
