package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import java.util.List;

/**
 * <pre>{@code
 * enum_value ::= {name} ('=' {id})? {annotations}?
 * }</pre>
 */
public class EnumValueDeclaration extends Declaration {
    private final ThriftToken idToken;
    private final int         id;

    public EnumValueDeclaration(String documentation,
                                ThriftToken nameToken,
                                ThriftToken idToken,
                                int id,
                                List<AnnotationDeclaration> annotations) {
        super(documentation, nameToken, annotations);
        this.idToken = idToken;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ThriftToken getIdToken() {
        return idToken;
    }
}
