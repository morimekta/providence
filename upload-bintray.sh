#!/usr/bin/env bash

set -e

function __sort() {
   type -fp gsort >/dev/null && command gsort "$@" || sort "$@"
}

export pvd_version=$(git tag | grep '^v[0-9]\{1,4\}[.][0-9]\+' | __sort -V | tail -n 1 | cut -b '2-')

export deb="$(echo providence-tools/target/providence-${pvd_version}_all.deb | sed 's/-beta/~beta/')"
export rpm="providence-tools/target/rpm/providence/RPMS/noarch/providence-${pvd_version}-1.noarch.rpm"
export tgz="providence-tools/target/providence-${pvd_version}.tar.gz"

echo "This will copy files to dl.morimekta.net:"
echo
echo "($(ls -lh ${tgz} | cut -d ' ' -f '5')) ${tgz}"
echo "($(ls -lh ${deb} | cut -d ' ' -f '5')) ${deb}"
echo "($(ls -lh ${rpm} | cut -d ' ' -f '5')) ${rpm}"
echo

echo "PS: Login to docker with:"
echo "docker login registry.gitlab.com"
echo

CONFIRM=
echo
echo -n "Continue? (y/N):"
read -n 1 CONFIRM
echo
if [[ "$CONFIRM" != "y" ]]
then
    exit 0
fi

docker build . \
       -t registry.gitlab.com/morimekta/providence:${pvd_version} \
       -t registry.gitlab.com/morimekta/providence:latest
docker push registry.gitlab.com/morimekta/providence:latest
docker push registry.gitlab.com/morimekta/providence:${pvd_version}

if [[ -d "../dl.morimekta.net/dl" ]]
then

  echo cp ${tgz} ../dl.morimekta.net/dl/archive/
  cp ${tgz} ../dl.morimekta.net/dl/archive/
  echo cp ${deb} ../dl.morimekta.net/dl/deb/
  cp ${deb} ../dl.morimekta.net/dl/deb/
  echo cp ${rpm} ../dl.morimekta.net/dl/rpm/
  cp ${rpm} ../dl.morimekta.net/dl/rpm/

else

  echo "copy files shown above to correct folder in dl.morimekta.net"
  echo

fi

SHA256SUM=$(cat ${tgz} | sha256sum | sed 's/ .*//')

if [[ -f "../homebrew-tools/Formula/providence.rb" ]]
then

cat <<EOF > ../homebrew-tools/Formula/providence.rb
class Providence < Formula
    desc "Providence Tools"
    homepage "http://www.morimekta.net/providence"
    version "${pvd_version}"
    url "https://dl.morimekta.net/archive/providence-#{version}.tar.gz"
    sha256 "${SHA256SUM}"

    depends_on :java => "1.8+"

    def install
        bin.install Dir["bin/*"]
        share.install Dir["share/*"]
    end
end
EOF

    echo
    echo "And go to ../homebrew-tools and commit and push changes."
else
    echo
    echo "Please go to 'homebrew-tools/Formula/providence.rb' and set:"
    echo "    version ${pvd_version}"
    echo "    sha256 ${SHA256SUM}"
fi

echo
