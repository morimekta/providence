package net.morimekta.test.providence.core.no_rw_binary;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:no_rw_binary")
@javax.annotation.concurrent.Immutable
public class UnionFields
        implements UnionFields_OrBuilder,
                   net.morimekta.providence.PUnion<UnionFields>,
                   Comparable<UnionFields> {
    private final static boolean kDefaultBooleanValue = false;
    private final static byte kDefaultByteValue = (byte)0;
    private final static short kDefaultShortValue = (short)0;
    private final static int kDefaultIntegerValue = 0;
    private final static long kDefaultLongValue = 0L;
    private final static double kDefaultDoubleValue = 0.0d;

    private final transient Boolean mBooleanValue;
    private final transient Byte mByteValue;
    private final transient Short mShortValue;
    private final transient Integer mIntegerValue;
    private final transient Long mLongValue;
    private final transient Double mDoubleValue;
    private final transient String mStringValue;
    private final transient net.morimekta.util.Binary mBinaryValue;
    private final transient net.morimekta.test.providence.core.no_rw_binary.Value mEnumValue;
    private final transient net.morimekta.test.providence.core.no_rw_binary.CompactFields mCompactValue;
    private final transient java.util.List<String> mStringList;
    private final transient java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> mSetList;

    private transient final _Field tUnionField;

    private volatile transient int tHashCode;

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withBooleanValue(boolean value) {
        return new _Builder().setBooleanValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withByteValue(byte value) {
        return new _Builder().setByteValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withByteValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
        return new _Builder().setByteValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withShortValue(short value) {
        return new _Builder().setShortValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withShortValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
        return new _Builder().setShortValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withIntegerValue(int value) {
        return new _Builder().setIntegerValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withIntegerValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
        return new _Builder().setIntegerValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withLongValue(long value) {
        return new _Builder().setLongValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withDoubleValue(double value) {
        return new _Builder().setDoubleValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withStringValue(@javax.annotation.Nonnull String value) {
        return new _Builder().setStringValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withStringValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
        return new _Builder().setStringValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withBinaryValue(@javax.annotation.Nonnull net.morimekta.util.Binary value) {
        return new _Builder().setBinaryValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withEnumValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.Value value) {
        return new _Builder().setEnumValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withCompactValue(@javax.annotation.Nonnull net.morimekta.test.providence.core.no_rw_binary.CompactFields_OrBuilder value) {
        return new _Builder().setCompactValue(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withStringList(@javax.annotation.Nonnull java.util.Collection<String> value) {
        return new _Builder().setStringList(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionFields withSetList(@javax.annotation.Nonnull java.util.Collection<net.morimekta.test.providence.core.no_rw_binary.CompactFields> value) {
        return new _Builder().setSetList(value).build();
    }

    private UnionFields(_Builder builder) {
        tUnionField = builder.tUnionField;

        mBooleanValue = tUnionField == _Field.BOOLEAN_VALUE ? builder.mBooleanValue : null;
        mByteValue = tUnionField == _Field.BYTE_VALUE ? builder.mByteValue : null;
        mShortValue = tUnionField == _Field.SHORT_VALUE ? builder.mShortValue : null;
        mIntegerValue = tUnionField == _Field.INTEGER_VALUE ? builder.mIntegerValue : null;
        mLongValue = tUnionField == _Field.LONG_VALUE ? builder.mLongValue : null;
        mDoubleValue = tUnionField == _Field.DOUBLE_VALUE ? builder.mDoubleValue : null;
        mStringValue = tUnionField == _Field.STRING_VALUE ? builder.mStringValue : null;
        mBinaryValue = tUnionField == _Field.BINARY_VALUE ? builder.mBinaryValue : null;
        mEnumValue = tUnionField == _Field.ENUM_VALUE ? builder.mEnumValue : null;
        mCompactValue = tUnionField != _Field.COMPACT_VALUE
                ? null
                : builder.mCompactValue_builder != null ? builder.mCompactValue_builder.build() : builder.mCompactValue;
        mStringList = tUnionField == _Field.STRING_LIST ? net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mStringList) : null;
        mSetList = tUnionField == _Field.SET_LIST ? net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mSetList) : null;
    }

    public boolean hasBooleanValue() {
        return tUnionField == _Field.BOOLEAN_VALUE && mBooleanValue != null;
    }

    /**
     * @return The <code>booleanValue</code> value
     */
    public boolean isBooleanValue() {
        return hasBooleanValue() ? mBooleanValue : kDefaultBooleanValue;
    }

    /**
     * @return Optional of the <code>booleanValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Boolean> optionalBooleanValue() {
        return java.util.Optional.ofNullable(mBooleanValue);
    }

    public boolean hasByteValue() {
        return tUnionField == _Field.BYTE_VALUE && mByteValue != null;
    }

    /**
     * @return The <code>byteValue</code> value
     */
    public byte getByteValue() {
        return hasByteValue() ? mByteValue : kDefaultByteValue;
    }

    /**
     * @return The <code>EnumNames</code> ref for the <code>byteValue</code> field, if it has a known value.
     */
    @javax.annotation.Nullable
    public net.morimekta.test.providence.core.no_rw_binary.EnumNames refByteValue() {
        return net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById((int) getByteValue());
    }

    /**
     * @return Optional of the <code>byteValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Byte> optionalByteValue() {
        return java.util.Optional.ofNullable(mByteValue);
    }

    public boolean hasShortValue() {
        return tUnionField == _Field.SHORT_VALUE && mShortValue != null;
    }

    /**
     * @return The <code>shortValue</code> value
     */
    public short getShortValue() {
        return hasShortValue() ? mShortValue : kDefaultShortValue;
    }

    /**
     * @return The <code>EnumNames</code> ref for the <code>shortValue</code> field, if it has a known value.
     */
    @javax.annotation.Nullable
    public net.morimekta.test.providence.core.no_rw_binary.EnumNames refShortValue() {
        return net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById((int) getShortValue());
    }

    /**
     * @return Optional of the <code>shortValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Short> optionalShortValue() {
        return java.util.Optional.ofNullable(mShortValue);
    }

    public boolean hasIntegerValue() {
        return tUnionField == _Field.INTEGER_VALUE && mIntegerValue != null;
    }

    /**
     * @return The <code>integerValue</code> value
     */
    public int getIntegerValue() {
        return hasIntegerValue() ? mIntegerValue : kDefaultIntegerValue;
    }

    /**
     * @return The <code>EnumNames</code> ref for the <code>integerValue</code> field, if it has a known value.
     */
    @javax.annotation.Nullable
    public net.morimekta.test.providence.core.no_rw_binary.EnumNames refIntegerValue() {
        return net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById(getIntegerValue());
    }

    /**
     * @return Optional of the <code>integerValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Integer> optionalIntegerValue() {
        return java.util.Optional.ofNullable(mIntegerValue);
    }

    public boolean hasLongValue() {
        return tUnionField == _Field.LONG_VALUE && mLongValue != null;
    }

    /**
     * @return The <code>longValue</code> value
     */
    public long getLongValue() {
        return hasLongValue() ? mLongValue : kDefaultLongValue;
    }

    /**
     * @return Optional of the <code>longValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Long> optionalLongValue() {
        return java.util.Optional.ofNullable(mLongValue);
    }

    public boolean hasDoubleValue() {
        return tUnionField == _Field.DOUBLE_VALUE && mDoubleValue != null;
    }

    /**
     * @return The <code>doubleValue</code> value
     */
    public double getDoubleValue() {
        return hasDoubleValue() ? mDoubleValue : kDefaultDoubleValue;
    }

    /**
     * @return Optional of the <code>doubleValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Double> optionalDoubleValue() {
        return java.util.Optional.ofNullable(mDoubleValue);
    }

    public boolean hasStringValue() {
        return tUnionField == _Field.STRING_VALUE && mStringValue != null;
    }

    /**
     * @return The <code>stringValue</code> value
     */
    public String getStringValue() {
        return mStringValue;
    }

    /**
     * @return The <code>EnumNames</code> ref for the <code>stringValue</code> field, if it has a known value.
     */
    @javax.annotation.Nullable
    public net.morimekta.test.providence.core.no_rw_binary.EnumNames refStringValue() {
        return mStringValue != null ? net.morimekta.test.providence.core.no_rw_binary.EnumNames.findByName(mStringValue) : null;
    }

    /**
     * @return Optional of the <code>stringValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<String> optionalStringValue() {
        return java.util.Optional.ofNullable(mStringValue);
    }

    public boolean hasBinaryValue() {
        return tUnionField == _Field.BINARY_VALUE && mBinaryValue != null;
    }

    /**
     * @return The <code>binaryValue</code> value
     */
    public net.morimekta.util.Binary getBinaryValue() {
        return mBinaryValue;
    }

    /**
     * @return Optional of the <code>binaryValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.util.Binary> optionalBinaryValue() {
        return java.util.Optional.ofNullable(mBinaryValue);
    }

    public boolean hasEnumValue() {
        return tUnionField == _Field.ENUM_VALUE && mEnumValue != null;
    }

    /**
     * @return The <code>enumValue</code> value
     */
    public net.morimekta.test.providence.core.no_rw_binary.Value getEnumValue() {
        return mEnumValue;
    }

    /**
     * @return Optional of the <code>enumValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.Value> optionalEnumValue() {
        return java.util.Optional.ofNullable(mEnumValue);
    }

    public boolean hasCompactValue() {
        return tUnionField == _Field.COMPACT_VALUE && mCompactValue != null;
    }

    /**
     * @return The <code>compactValue</code> value
     */
    public net.morimekta.test.providence.core.no_rw_binary.CompactFields getCompactValue() {
        return mCompactValue;
    }

    /**
     * @return Optional of the <code>compactValue</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.CompactFields> optionalCompactValue() {
        return java.util.Optional.ofNullable(mCompactValue);
    }

    public int numStringList() {
        return tUnionField == _Field.STRING_LIST ? mStringList.size() : 0;
    }

    public boolean hasStringList() {
        return tUnionField == _Field.STRING_LIST && mStringList != null;
    }

    /**
     * @return The <code>stringList</code> value
     */
    public java.util.List<String> getStringList() {
        return mStringList;
    }

    /**
     * @return Optional of the <code>stringList</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<String>> optionalStringList() {
        return java.util.Optional.ofNullable(mStringList);
    }

    public int numSetList() {
        return tUnionField == _Field.SET_LIST ? mSetList.size() : 0;
    }

    public boolean hasSetList() {
        return tUnionField == _Field.SET_LIST && mSetList != null;
    }

    /**
     * @return The <code>setList</code> value
     */
    public java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> getSetList() {
        return mSetList;
    }

    /**
     * @return Optional of the <code>setList</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields>> optionalSetList() {
        return java.util.Optional.ofNullable(mSetList);
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            case 1: return tUnionField == _Field.BOOLEAN_VALUE;
            case 2: return tUnionField == _Field.BYTE_VALUE;
            case 3: return tUnionField == _Field.SHORT_VALUE;
            case 4: return tUnionField == _Field.INTEGER_VALUE;
            case 5: return tUnionField == _Field.LONG_VALUE;
            case 6: return tUnionField == _Field.DOUBLE_VALUE;
            case 7: return tUnionField == _Field.STRING_VALUE;
            case 8: return tUnionField == _Field.BINARY_VALUE;
            case 9: return tUnionField == _Field.ENUM_VALUE;
            case 10: return tUnionField == _Field.COMPACT_VALUE;
            case 11: return tUnionField == _Field.STRING_LIST;
            case 12: return tUnionField == _Field.SET_LIST;
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            case 1: return (T) mBooleanValue;
            case 2: return (T) mByteValue;
            case 3: return (T) mShortValue;
            case 4: return (T) mIntegerValue;
            case 5: return (T) mLongValue;
            case 6: return (T) mDoubleValue;
            case 7: return (T) mStringValue;
            case 8: return (T) mBinaryValue;
            case 9: return (T) mEnumValue;
            case 10: return (T) mCompactValue;
            case 11: return (T) mStringList;
            case 12: return (T) mSetList;
            default: return null;
        }
    }

    @Override
    public boolean unionFieldIsSet() {
        return tUnionField != null;
    }

    @Override
    @javax.annotation.Nonnull
    public _Field unionField() {
        if (tUnionField == null) throw new IllegalStateException("No union field set in providence.UnionFields");
        return tUnionField;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        UnionFields other = (UnionFields) o;
        return java.util.Objects.equals(tUnionField, other.tUnionField) &&
               java.util.Objects.equals(mBooleanValue, other.mBooleanValue) &&
               java.util.Objects.equals(mByteValue, other.mByteValue) &&
               java.util.Objects.equals(mShortValue, other.mShortValue) &&
               java.util.Objects.equals(mIntegerValue, other.mIntegerValue) &&
               java.util.Objects.equals(mLongValue, other.mLongValue) &&
               java.util.Objects.equals(mDoubleValue, other.mDoubleValue) &&
               java.util.Objects.equals(mStringValue, other.mStringValue) &&
               java.util.Objects.equals(mBinaryValue, other.mBinaryValue) &&
               java.util.Objects.equals(mEnumValue, other.mEnumValue) &&
               java.util.Objects.equals(mCompactValue, other.mCompactValue) &&
               java.util.Objects.equals(mStringList, other.mStringList) &&
               java.util.Objects.equals(mSetList, other.mSetList);
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    UnionFields.class,
                    _Field.BOOLEAN_VALUE, mBooleanValue,
                    _Field.BYTE_VALUE, mByteValue,
                    _Field.SHORT_VALUE, mShortValue,
                    _Field.INTEGER_VALUE, mIntegerValue,
                    _Field.LONG_VALUE, mLongValue,
                    _Field.DOUBLE_VALUE, mDoubleValue,
                    _Field.STRING_VALUE, mStringValue,
                    _Field.BINARY_VALUE, mBinaryValue,
                    _Field.ENUM_VALUE, mEnumValue,
                    _Field.COMPACT_VALUE, mCompactValue,
                    _Field.STRING_LIST, mStringList,
                    _Field.SET_LIST, mSetList);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "providence.UnionFields" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        switch (tUnionField) {
            case BOOLEAN_VALUE: {
                out.append("booleanValue:")
                   .append(mBooleanValue);
                break;
            }
            case BYTE_VALUE: {
                out.append("byteValue:")
                   .append((int) mByteValue);
                break;
            }
            case SHORT_VALUE: {
                out.append("shortValue:")
                   .append((int) mShortValue);
                break;
            }
            case INTEGER_VALUE: {
                out.append("integerValue:")
                   .append(mIntegerValue);
                break;
            }
            case LONG_VALUE: {
                out.append("longValue:")
                   .append(mLongValue);
                break;
            }
            case DOUBLE_VALUE: {
                out.append("doubleValue:")
                   .append(net.morimekta.util.Strings.asString(mDoubleValue));
                break;
            }
            case STRING_VALUE: {
                out.append("stringValue:")
                   .append('\"').append(net.morimekta.util.Strings.escape(mStringValue)).append('\"');
                break;
            }
            case BINARY_VALUE: {
                out.append("binaryValue:")
                   .append("b64(").append(mBinaryValue.toBase64()).append(')');
                break;
            }
            case ENUM_VALUE: {
                out.append("enumValue:")
                   .append(mEnumValue.asString());
                break;
            }
            case COMPACT_VALUE: {
                out.append("compactValue:")
                   .append(mCompactValue.asString());
                break;
            }
            case STRING_LIST: {
                out.append("stringList:")
                   .append(net.morimekta.util.Strings.asString(mStringList));
                break;
            }
            case SET_LIST: {
                out.append("setList:")
                   .append(net.morimekta.util.Strings.asString(mSetList));
                break;
            }
        }
        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(UnionFields other) {
        if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
        int c = tUnionField.compareTo(other.tUnionField);
        if (c != 0) return c;

        switch (tUnionField) {
            case BOOLEAN_VALUE:
                return Boolean.compare(mBooleanValue, other.mBooleanValue);
            case BYTE_VALUE:
                return Byte.compare(mByteValue, other.mByteValue);
            case SHORT_VALUE:
                return Short.compare(mShortValue, other.mShortValue);
            case INTEGER_VALUE:
                return Integer.compare(mIntegerValue, other.mIntegerValue);
            case LONG_VALUE:
                return Long.compare(mLongValue, other.mLongValue);
            case DOUBLE_VALUE:
                return Double.compare(mDoubleValue, other.mDoubleValue);
            case STRING_VALUE:
                return mStringValue.compareTo(other.mStringValue);
            case BINARY_VALUE:
                return mBinaryValue.compareTo(other.mBinaryValue);
            case ENUM_VALUE:
                return Integer.compare(mEnumValue.asInteger(), other.mEnumValue.asInteger());
            case COMPACT_VALUE:
                return mCompactValue.compareTo(other.mCompactValue);
            case STRING_LIST:
                return Integer.compare(mStringList.hashCode(), other.mStringList.hashCode());
            case SET_LIST:
                return Integer.compare(mSetList.hashCode(), other.mSetList.hashCode());
            default: return 0;
        }
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<UnionFields> {
        BOOLEAN_VALUE(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "booleanValue", "booleanValue", net.morimekta.providence.descriptor.PPrimitive.BOOL.provider(), null, null),
        BYTE_VALUE(2, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "byteValue", "byteValue", net.morimekta.providence.descriptor.PPrimitive.BYTE.provider(), null, null),
        SHORT_VALUE(3, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "shortValue", "shortValue", net.morimekta.providence.descriptor.PPrimitive.I16.provider(), null, null),
        INTEGER_VALUE(4, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "integerValue", "integerValue", net.morimekta.providence.descriptor.PPrimitive.I32.provider(), null, null),
        LONG_VALUE(5, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "longValue", "longValue", net.morimekta.providence.descriptor.PPrimitive.I64.provider(), null, null),
        DOUBLE_VALUE(6, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "doubleValue", "doubleValue", net.morimekta.providence.descriptor.PPrimitive.DOUBLE.provider(), null, null),
        STRING_VALUE(7, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "stringValue", "stringValue", net.morimekta.providence.descriptor.PPrimitive.STRING.provider(), null, null),
        BINARY_VALUE(8, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "binaryValue", "binaryValue", net.morimekta.providence.descriptor.PPrimitive.BINARY.provider(), null, null),
        ENUM_VALUE(9, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "enumValue", "enumValue", net.morimekta.test.providence.core.no_rw_binary.Value.provider(), null, null),
        COMPACT_VALUE(10, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "compactValue", "compactValue", net.morimekta.test.providence.core.no_rw_binary.CompactFields.provider(), null, null),
        STRING_LIST(11, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "stringList", "stringList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.STRING.provider()), null, null),
        SET_LIST(12, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "setList", "setList", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.test.providence.core.no_rw_binary.CompactFields.provider()), null, null),
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<UnionFields> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
                case 1: return _Field.BOOLEAN_VALUE;
                case 2: return _Field.BYTE_VALUE;
                case 3: return _Field.SHORT_VALUE;
                case 4: return _Field.INTEGER_VALUE;
                case 5: return _Field.LONG_VALUE;
                case 6: return _Field.DOUBLE_VALUE;
                case 7: return _Field.STRING_VALUE;
                case 8: return _Field.BINARY_VALUE;
                case 9: return _Field.ENUM_VALUE;
                case 10: return _Field.COMPACT_VALUE;
                case 11: return _Field.STRING_LIST;
                case 12: return _Field.SET_LIST;
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "booleanValue": return _Field.BOOLEAN_VALUE;
                case "byteValue": return _Field.BYTE_VALUE;
                case "shortValue": return _Field.SHORT_VALUE;
                case "integerValue": return _Field.INTEGER_VALUE;
                case "longValue": return _Field.LONG_VALUE;
                case "doubleValue": return _Field.DOUBLE_VALUE;
                case "stringValue": return _Field.STRING_VALUE;
                case "binaryValue": return _Field.BINARY_VALUE;
                case "enumValue": return _Field.ENUM_VALUE;
                case "compactValue": return _Field.COMPACT_VALUE;
                case "stringList": return _Field.STRING_LIST;
                case "setList": return _Field.SET_LIST;
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
                case "booleanValue": return _Field.BOOLEAN_VALUE;
                case "byteValue": return _Field.BYTE_VALUE;
                case "shortValue": return _Field.SHORT_VALUE;
                case "integerValue": return _Field.INTEGER_VALUE;
                case "longValue": return _Field.LONG_VALUE;
                case "doubleValue": return _Field.DOUBLE_VALUE;
                case "stringValue": return _Field.STRING_VALUE;
                case "binaryValue": return _Field.BINARY_VALUE;
                case "enumValue": return _Field.ENUM_VALUE;
                case "compactValue": return _Field.COMPACT_VALUE;
                case "stringList": return _Field.STRING_LIST;
                case "setList": return _Field.SET_LIST;
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in providence.UnionFields");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.UnionFields");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.UnionFields");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<UnionFields> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PUnionDescriptor<UnionFields> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PUnionDescriptor<UnionFields> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PUnionDescriptor<UnionFields> {
        public _Descriptor() {
            super("providence", "UnionFields", _Builder::new, false);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<UnionFields> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<UnionFields> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>providence.UnionFields</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<UnionFields>
            implements UnionFields_OrBuilder {
        private UnionFields._Field tUnionField;

        private boolean modified;

        private Boolean mBooleanValue;
        private Byte mByteValue;
        private Short mShortValue;
        private Integer mIntegerValue;
        private Long mLongValue;
        private Double mDoubleValue;
        private String mStringValue;
        private net.morimekta.util.Binary mBinaryValue;
        private net.morimekta.test.providence.core.no_rw_binary.Value mEnumValue;
        private net.morimekta.test.providence.core.no_rw_binary.CompactFields mCompactValue;
        private net.morimekta.test.providence.core.no_rw_binary.CompactFields._Builder mCompactValue_builder;
        private java.util.List<String> mStringList;
        private java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> mSetList;

        /**
         * Make a providence.UnionFields builder instance.
         */
        public _Builder() {
            modified = false;
        }

        /**
         * Make a mutating builder off a base providence.UnionFields.
         *
         * @param base The base UnionFields
         */
        public _Builder(UnionFields base) {
            this();

            tUnionField = base.tUnionField;

            mBooleanValue = base.mBooleanValue;
            mByteValue = base.mByteValue;
            mShortValue = base.mShortValue;
            mIntegerValue = base.mIntegerValue;
            mLongValue = base.mLongValue;
            mDoubleValue = base.mDoubleValue;
            mStringValue = base.mStringValue;
            mBinaryValue = base.mBinaryValue;
            mEnumValue = base.mEnumValue;
            mCompactValue = base.mCompactValue;
            if (base.hasStringList()) {
                mStringList = base.mStringList;
            }
            if (base.hasSetList()) {
                mSetList = base.mSetList;
            }
        }

        @javax.annotation.Nonnull
        @Override
        public UnionFields._Builder merge(UnionFields from) {
            if (!from.unionFieldIsSet()) {
                return this;
            }

            switch (from.unionField()) {
                case BOOLEAN_VALUE: {
                    setBooleanValue(from.isBooleanValue());
                    break;
                }
                case BYTE_VALUE: {
                    setByteValue(from.getByteValue());
                    break;
                }
                case SHORT_VALUE: {
                    setShortValue(from.getShortValue());
                    break;
                }
                case INTEGER_VALUE: {
                    setIntegerValue(from.getIntegerValue());
                    break;
                }
                case LONG_VALUE: {
                    setLongValue(from.getLongValue());
                    break;
                }
                case DOUBLE_VALUE: {
                    setDoubleValue(from.getDoubleValue());
                    break;
                }
                case STRING_VALUE: {
                    setStringValue(from.getStringValue());
                    break;
                }
                case BINARY_VALUE: {
                    setBinaryValue(from.getBinaryValue());
                    break;
                }
                case ENUM_VALUE: {
                    setEnumValue(from.getEnumValue());
                    break;
                }
                case COMPACT_VALUE: {
                    if (tUnionField == UnionFields._Field.COMPACT_VALUE && mCompactValue != null) {
                        mCompactValue = mCompactValue.mutate().merge(from.getCompactValue()).build();
                    } else {
                        setCompactValue(from.getCompactValue());
                    }
                    break;
                }
                case STRING_LIST: {
                    setStringList(from.getStringList());
                    break;
                }
                case SET_LIST: {
                    if (tUnionField == UnionFields._Field.SET_LIST) {
                        mutableSetList().addAll(from.getSetList());
                    } else {
                        setSetList(from.getSetList());
                    }
                    break;
                }
            }
            return this;
        }

        /**
         * Set the <code>booleanValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setBooleanValue(Boolean value) {
            if (value == null) {
                return clearBooleanValue();
            }

            tUnionField = UnionFields._Field.BOOLEAN_VALUE;
            modified = true;
            mBooleanValue = value;
            return this;
        }

        /**
         * Set the <code>booleanValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setBooleanValue(boolean value) {
            tUnionField = UnionFields._Field.BOOLEAN_VALUE;
            modified = true;
            mBooleanValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>booleanValue</code> field.
         *
         * @return True if booleanValue has been set.
         */
        public boolean isSetBooleanValue() {
            return tUnionField == UnionFields._Field.BOOLEAN_VALUE;
        }

        /**
         * Checks for presence of the <code>booleanValue</code> field.
         *
         * @return True if booleanValue is present.
         */
        public boolean hasBooleanValue() {
            return tUnionField == UnionFields._Field.BOOLEAN_VALUE;
        }

        /**
         * Clear the <code>booleanValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearBooleanValue() {
            if (tUnionField == UnionFields._Field.BOOLEAN_VALUE) tUnionField = null;
            modified = true;
            mBooleanValue = null;
            return this;
        }

        /**
         * @return The <code>booleanValue</code> field value
         */
        public boolean isBooleanValue() {
            return isSetBooleanValue() ? mBooleanValue : kDefaultBooleanValue;
        }

        /**
         * @return Optional <code>booleanValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Boolean> optionalBooleanValue() {
            return java.util.Optional.ofNullable(mBooleanValue);
        }

        /**
         * Set the <code>byteValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setByteValue(Byte value) {
            if (value == null) {
                return clearByteValue();
            }

            tUnionField = UnionFields._Field.BYTE_VALUE;
            modified = true;
            mByteValue = value;
            return this;
        }

        /**
         * Set the <code>byteValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setByteValue(byte value) {
            tUnionField = UnionFields._Field.BYTE_VALUE;
            modified = true;
            mByteValue = value;
            return this;
        }

        /**
         * Set the value of the <code>byteValue</code> field.
         *
         * @param value The new value ref
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setByteValue(net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
            if (value == null) return clearByteValue();
            return setByteValue((byte) value.asInteger());
        }

        /**
         * Checks for explicit presence of the <code>byteValue</code> field.
         *
         * @return True if byteValue has been set.
         */
        public boolean isSetByteValue() {
            return tUnionField == UnionFields._Field.BYTE_VALUE;
        }

        /**
         * Checks for presence of the <code>byteValue</code> field.
         *
         * @return True if byteValue is present.
         */
        public boolean hasByteValue() {
            return tUnionField == UnionFields._Field.BYTE_VALUE;
        }

        /**
         * Clear the <code>byteValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearByteValue() {
            if (tUnionField == UnionFields._Field.BYTE_VALUE) tUnionField = null;
            modified = true;
            mByteValue = null;
            return this;
        }

        /**
         * @return The <code>byteValue</code> field value
         */
        public byte getByteValue() {
            return isSetByteValue() ? mByteValue : kDefaultByteValue;
        }

        /**
         * @return The <code>EnumNames</code> ref for the <code>byteValue</code> field, if it has a known value.
         */
        @javax.annotation.Nullable
        public net.morimekta.test.providence.core.no_rw_binary.EnumNames refByteValue() {
            return mByteValue != null ? net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById(mByteValue.intValue()) : null;
        }

        /**
         * @return Optional <code>byteValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Byte> optionalByteValue() {
            return java.util.Optional.ofNullable(mByteValue);
        }

        /**
         * Set the <code>shortValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setShortValue(Short value) {
            if (value == null) {
                return clearShortValue();
            }

            tUnionField = UnionFields._Field.SHORT_VALUE;
            modified = true;
            mShortValue = value;
            return this;
        }

        /**
         * Set the <code>shortValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setShortValue(short value) {
            tUnionField = UnionFields._Field.SHORT_VALUE;
            modified = true;
            mShortValue = value;
            return this;
        }

        /**
         * Set the value of the <code>shortValue</code> field.
         *
         * @param value The new value ref
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setShortValue(net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
            if (value == null) return clearShortValue();
            return setShortValue((short) value.asInteger());
        }

        /**
         * Checks for explicit presence of the <code>shortValue</code> field.
         *
         * @return True if shortValue has been set.
         */
        public boolean isSetShortValue() {
            return tUnionField == UnionFields._Field.SHORT_VALUE;
        }

        /**
         * Checks for presence of the <code>shortValue</code> field.
         *
         * @return True if shortValue is present.
         */
        public boolean hasShortValue() {
            return tUnionField == UnionFields._Field.SHORT_VALUE;
        }

        /**
         * Clear the <code>shortValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearShortValue() {
            if (tUnionField == UnionFields._Field.SHORT_VALUE) tUnionField = null;
            modified = true;
            mShortValue = null;
            return this;
        }

        /**
         * @return The <code>shortValue</code> field value
         */
        public short getShortValue() {
            return isSetShortValue() ? mShortValue : kDefaultShortValue;
        }

        /**
         * @return The <code>EnumNames</code> ref for the <code>shortValue</code> field, if it has a known value.
         */
        @javax.annotation.Nullable
        public net.morimekta.test.providence.core.no_rw_binary.EnumNames refShortValue() {
            return mShortValue != null ? net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById(mShortValue.intValue()) : null;
        }

        /**
         * @return Optional <code>shortValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Short> optionalShortValue() {
            return java.util.Optional.ofNullable(mShortValue);
        }

        /**
         * Set the <code>integerValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setIntegerValue(Integer value) {
            if (value == null) {
                return clearIntegerValue();
            }

            tUnionField = UnionFields._Field.INTEGER_VALUE;
            modified = true;
            mIntegerValue = value;
            return this;
        }

        /**
         * Set the <code>integerValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setIntegerValue(int value) {
            tUnionField = UnionFields._Field.INTEGER_VALUE;
            modified = true;
            mIntegerValue = value;
            return this;
        }

        /**
         * Set the value of the <code>integerValue</code> field.
         *
         * @param value The new value ref
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setIntegerValue(net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
            if (value == null) return clearIntegerValue();
            return setIntegerValue(value.asInteger());
        }

        /**
         * Checks for explicit presence of the <code>integerValue</code> field.
         *
         * @return True if integerValue has been set.
         */
        public boolean isSetIntegerValue() {
            return tUnionField == UnionFields._Field.INTEGER_VALUE;
        }

        /**
         * Checks for presence of the <code>integerValue</code> field.
         *
         * @return True if integerValue is present.
         */
        public boolean hasIntegerValue() {
            return tUnionField == UnionFields._Field.INTEGER_VALUE;
        }

        /**
         * Clear the <code>integerValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearIntegerValue() {
            if (tUnionField == UnionFields._Field.INTEGER_VALUE) tUnionField = null;
            modified = true;
            mIntegerValue = null;
            return this;
        }

        /**
         * @return The <code>integerValue</code> field value
         */
        public int getIntegerValue() {
            return isSetIntegerValue() ? mIntegerValue : kDefaultIntegerValue;
        }

        /**
         * @return The <code>EnumNames</code> ref for the <code>integerValue</code> field, if it has a known value.
         */
        @javax.annotation.Nullable
        public net.morimekta.test.providence.core.no_rw_binary.EnumNames refIntegerValue() {
            return mIntegerValue != null ? net.morimekta.test.providence.core.no_rw_binary.EnumNames.findById(mIntegerValue.intValue()) : null;
        }

        /**
         * @return Optional <code>integerValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Integer> optionalIntegerValue() {
            return java.util.Optional.ofNullable(mIntegerValue);
        }

        /**
         * Set the <code>longValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setLongValue(Long value) {
            if (value == null) {
                return clearLongValue();
            }

            tUnionField = UnionFields._Field.LONG_VALUE;
            modified = true;
            mLongValue = value;
            return this;
        }

        /**
         * Set the <code>longValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setLongValue(long value) {
            tUnionField = UnionFields._Field.LONG_VALUE;
            modified = true;
            mLongValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>longValue</code> field.
         *
         * @return True if longValue has been set.
         */
        public boolean isSetLongValue() {
            return tUnionField == UnionFields._Field.LONG_VALUE;
        }

        /**
         * Checks for presence of the <code>longValue</code> field.
         *
         * @return True if longValue is present.
         */
        public boolean hasLongValue() {
            return tUnionField == UnionFields._Field.LONG_VALUE;
        }

        /**
         * Clear the <code>longValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearLongValue() {
            if (tUnionField == UnionFields._Field.LONG_VALUE) tUnionField = null;
            modified = true;
            mLongValue = null;
            return this;
        }

        /**
         * @return The <code>longValue</code> field value
         */
        public long getLongValue() {
            return isSetLongValue() ? mLongValue : kDefaultLongValue;
        }

        /**
         * @return Optional <code>longValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Long> optionalLongValue() {
            return java.util.Optional.ofNullable(mLongValue);
        }

        /**
         * Set the <code>doubleValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setDoubleValue(Double value) {
            if (value == null) {
                return clearDoubleValue();
            }

            tUnionField = UnionFields._Field.DOUBLE_VALUE;
            modified = true;
            mDoubleValue = value;
            return this;
        }

        /**
         * Set the <code>doubleValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setDoubleValue(double value) {
            tUnionField = UnionFields._Field.DOUBLE_VALUE;
            modified = true;
            mDoubleValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>doubleValue</code> field.
         *
         * @return True if doubleValue has been set.
         */
        public boolean isSetDoubleValue() {
            return tUnionField == UnionFields._Field.DOUBLE_VALUE;
        }

        /**
         * Checks for presence of the <code>doubleValue</code> field.
         *
         * @return True if doubleValue is present.
         */
        public boolean hasDoubleValue() {
            return tUnionField == UnionFields._Field.DOUBLE_VALUE;
        }

        /**
         * Clear the <code>doubleValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearDoubleValue() {
            if (tUnionField == UnionFields._Field.DOUBLE_VALUE) tUnionField = null;
            modified = true;
            mDoubleValue = null;
            return this;
        }

        /**
         * @return The <code>doubleValue</code> field value
         */
        public double getDoubleValue() {
            return isSetDoubleValue() ? mDoubleValue : kDefaultDoubleValue;
        }

        /**
         * @return Optional <code>doubleValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Double> optionalDoubleValue() {
            return java.util.Optional.ofNullable(mDoubleValue);
        }

        /**
         * Set the <code>stringValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setStringValue(String value) {
            if (value == null) {
                return clearStringValue();
            }

            tUnionField = UnionFields._Field.STRING_VALUE;
            modified = true;
            mStringValue = value;
            return this;
        }

        /**
         * Set the value of the <code>stringValue</code> field.
         *
         * @param value The new value ref
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setStringValue(net.morimekta.test.providence.core.no_rw_binary.EnumNames value) {
            if (value == null) return clearStringValue();
            return setStringValue(value.asString());
        }

        /**
         * Checks for explicit presence of the <code>stringValue</code> field.
         *
         * @return True if stringValue has been set.
         */
        public boolean isSetStringValue() {
            return tUnionField == UnionFields._Field.STRING_VALUE;
        }

        /**
         * Checks for presence of the <code>stringValue</code> field.
         *
         * @return True if stringValue is present.
         */
        public boolean hasStringValue() {
            return tUnionField == UnionFields._Field.STRING_VALUE;
        }

        /**
         * Clear the <code>stringValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearStringValue() {
            if (tUnionField == UnionFields._Field.STRING_VALUE) tUnionField = null;
            modified = true;
            mStringValue = null;
            return this;
        }

        /**
         * @return The <code>stringValue</code> field value
         */
        public String getStringValue() {
            return mStringValue;
        }

        /**
         * @return The <code>EnumNames</code> ref for the <code>stringValue</code> field, if it has a known value.
         */
        @javax.annotation.Nullable
        public net.morimekta.test.providence.core.no_rw_binary.EnumNames refStringValue() {
            return mStringValue != null ? net.morimekta.test.providence.core.no_rw_binary.EnumNames.findByName(mStringValue) : null;
        }

        /**
         * @return Optional <code>stringValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<String> optionalStringValue() {
            return java.util.Optional.ofNullable(mStringValue);
        }

        /**
         * Set the <code>binaryValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setBinaryValue(net.morimekta.util.Binary value) {
            if (value == null) {
                return clearBinaryValue();
            }

            tUnionField = UnionFields._Field.BINARY_VALUE;
            modified = true;
            mBinaryValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>binaryValue</code> field.
         *
         * @return True if binaryValue has been set.
         */
        public boolean isSetBinaryValue() {
            return tUnionField == UnionFields._Field.BINARY_VALUE;
        }

        /**
         * Checks for presence of the <code>binaryValue</code> field.
         *
         * @return True if binaryValue is present.
         */
        public boolean hasBinaryValue() {
            return tUnionField == UnionFields._Field.BINARY_VALUE;
        }

        /**
         * Clear the <code>binaryValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearBinaryValue() {
            if (tUnionField == UnionFields._Field.BINARY_VALUE) tUnionField = null;
            modified = true;
            mBinaryValue = null;
            return this;
        }

        /**
         * @return The <code>binaryValue</code> field value
         */
        public net.morimekta.util.Binary getBinaryValue() {
            return mBinaryValue;
        }

        /**
         * @return Optional <code>binaryValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.util.Binary> optionalBinaryValue() {
            return java.util.Optional.ofNullable(mBinaryValue);
        }

        /**
         * Set the <code>enumValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setEnumValue(net.morimekta.test.providence.core.no_rw_binary.Value value) {
            if (value == null) {
                return clearEnumValue();
            }

            tUnionField = UnionFields._Field.ENUM_VALUE;
            modified = true;
            mEnumValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>enumValue</code> field.
         *
         * @return True if enumValue has been set.
         */
        public boolean isSetEnumValue() {
            return tUnionField == UnionFields._Field.ENUM_VALUE;
        }

        /**
         * Checks for presence of the <code>enumValue</code> field.
         *
         * @return True if enumValue is present.
         */
        public boolean hasEnumValue() {
            return tUnionField == UnionFields._Field.ENUM_VALUE;
        }

        /**
         * Clear the <code>enumValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearEnumValue() {
            if (tUnionField == UnionFields._Field.ENUM_VALUE) tUnionField = null;
            modified = true;
            mEnumValue = null;
            return this;
        }

        /**
         * @return The <code>enumValue</code> field value
         */
        public net.morimekta.test.providence.core.no_rw_binary.Value getEnumValue() {
            return mEnumValue;
        }

        /**
         * @return Optional <code>enumValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.Value> optionalEnumValue() {
            return java.util.Optional.ofNullable(mEnumValue);
        }

        /**
         * Set the <code>compactValue</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setCompactValue(net.morimekta.test.providence.core.no_rw_binary.CompactFields_OrBuilder value) {
            if (value == null) {
                return clearCompactValue();
            }

            tUnionField = UnionFields._Field.COMPACT_VALUE;
            modified = true;
            if (value instanceof net.morimekta.test.providence.core.no_rw_binary.CompactFields._Builder) {
                value = ((net.morimekta.test.providence.core.no_rw_binary.CompactFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.no_rw_binary.CompactFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.CompactFields: " + value.getClass().getName());
            }
            mCompactValue = (net.morimekta.test.providence.core.no_rw_binary.CompactFields) value;
            mCompactValue_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>compactValue</code> field.
         *
         * @return True if compactValue has been set.
         */
        public boolean isSetCompactValue() {
            return tUnionField == UnionFields._Field.COMPACT_VALUE;
        }

        /**
         * Checks for presence of the <code>compactValue</code> field.
         *
         * @return True if compactValue is present.
         */
        public boolean hasCompactValue() {
            return tUnionField == UnionFields._Field.COMPACT_VALUE;
        }

        /**
         * Clear the <code>compactValue</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearCompactValue() {
            if (tUnionField == UnionFields._Field.COMPACT_VALUE) tUnionField = null;
            modified = true;
            mCompactValue = null;
            mCompactValue_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>compactValue</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.no_rw_binary.CompactFields._Builder mutableCompactValue() {
            if (tUnionField != UnionFields._Field.COMPACT_VALUE) {
                clearCompactValue();
            }
            tUnionField = UnionFields._Field.COMPACT_VALUE;
            modified = true;

            if (mCompactValue != null) {
                mCompactValue_builder = mCompactValue.mutate();
                mCompactValue = null;
            } else if (mCompactValue_builder == null) {
                mCompactValue_builder = net.morimekta.test.providence.core.no_rw_binary.CompactFields.builder();
            }
            return mCompactValue_builder;
        }

        /**
         * @return The <code>compactValue</code> field value
         */
        public net.morimekta.test.providence.core.no_rw_binary.CompactFields getCompactValue() {
            return mCompactValue_builder != null ? mCompactValue_builder.build() : mCompactValue;
        }

        /**
         * @return Optional <code>compactValue</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.CompactFields> optionalCompactValue() {
            return java.util.Optional.ofNullable(mCompactValue_builder != null ? mCompactValue_builder.build() : mCompactValue);
        }

        /**
         * Set the <code>stringList</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setStringList(java.util.Collection<String> value) {
            if (value == null) {
                return clearStringList();
            }

            tUnionField = UnionFields._Field.STRING_LIST;
            modified = true;
            mStringList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>stringList</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder addToStringList(String... values) {
            tUnionField = UnionFields._Field.STRING_LIST;
            modified = true;
            java.util.List<String> _container = mutableStringList();
            for (String item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>stringList</code> field.
         *
         * @return True if stringList has been set.
         */
        public boolean isSetStringList() {
            return tUnionField == UnionFields._Field.STRING_LIST;
        }

        /**
         * Checks for presence of the <code>stringList</code> field.
         *
         * @return True if stringList is present.
         */
        public boolean hasStringList() {
            return tUnionField == UnionFields._Field.STRING_LIST;
        }

        /**
         * Clear the <code>stringList</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearStringList() {
            if (tUnionField == UnionFields._Field.STRING_LIST) tUnionField = null;
            modified = true;
            mStringList = null;
            return this;
        }

        /**
         * @return The mutable <code>stringList</code> container
         */
        public java.util.List<String> mutableStringList() {
            if (tUnionField != UnionFields._Field.STRING_LIST) {
                clearStringList();
            }
            tUnionField = UnionFields._Field.STRING_LIST;
            modified = true;

            if (mStringList == null) {
                mStringList = new java.util.ArrayList<>();
            } else if (!(mStringList instanceof java.util.ArrayList)) {
                mStringList = new java.util.ArrayList<>(mStringList);
            }
            return mStringList;
        }

        /**
         * @return The <code>stringList</code> field value
         */
        public java.util.List<String> getStringList() {
            return mStringList;
        }

        /**
         * @return Optional <code>stringList</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<String>> optionalStringList() {
            return java.util.Optional.ofNullable(mStringList);
        }

        /**
         * @return Number of entries in <code>stringList</code>.
         */
        public int numStringList() {
            return mStringList != null ? mStringList.size() : 0;
        }

        /**
         * Set the <code>setList</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder setSetList(java.util.Collection<net.morimekta.test.providence.core.no_rw_binary.CompactFields> value) {
            if (value == null) {
                return clearSetList();
            }

            tUnionField = UnionFields._Field.SET_LIST;
            modified = true;
            mSetList = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>setList</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder addToSetList(net.morimekta.test.providence.core.no_rw_binary.CompactFields... values) {
            tUnionField = UnionFields._Field.SET_LIST;
            modified = true;
            java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> _container = mutableSetList();
            for (net.morimekta.test.providence.core.no_rw_binary.CompactFields item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>setList</code> field.
         *
         * @return True if setList has been set.
         */
        public boolean isSetSetList() {
            return tUnionField == UnionFields._Field.SET_LIST;
        }

        /**
         * Checks for presence of the <code>setList</code> field.
         *
         * @return True if setList is present.
         */
        public boolean hasSetList() {
            return tUnionField == UnionFields._Field.SET_LIST;
        }

        /**
         * Clear the <code>setList</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionFields._Builder clearSetList() {
            if (tUnionField == UnionFields._Field.SET_LIST) tUnionField = null;
            modified = true;
            mSetList = null;
            return this;
        }

        /**
         * @return The mutable <code>setList</code> container
         */
        public java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> mutableSetList() {
            if (tUnionField != UnionFields._Field.SET_LIST) {
                clearSetList();
            }
            tUnionField = UnionFields._Field.SET_LIST;
            modified = true;

            if (mSetList == null) {
                mSetList = new java.util.HashSet<>();
            } else if (!(mSetList instanceof java.util.HashSet)) {
                mSetList = new java.util.HashSet<>(mSetList);
            }
            return mSetList;
        }

        /**
         * @return The <code>setList</code> field value
         */
        public java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields> getSetList() {
            return mSetList;
        }

        /**
         * @return Optional <code>setList</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields>> optionalSetList() {
            return java.util.Optional.ofNullable(mSetList);
        }

        /**
         * @return Number of entries in <code>setList</code>.
         */
        public int numSetList() {
            return mSetList != null ? mSetList.size() : 0;
        }

        /**
         * Checks if the <code>UnionFields</code> union has been modified since the
         * builder was created.
         *
         * @return True if UnionFields has been modified.
         */
        public boolean isUnionModified() {
            return modified;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            UnionFields._Builder other = (UnionFields._Builder) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                   java.util.Objects.equals(mBooleanValue, other.mBooleanValue) &&
                   java.util.Objects.equals(mByteValue, other.mByteValue) &&
                   java.util.Objects.equals(mShortValue, other.mShortValue) &&
                   java.util.Objects.equals(mIntegerValue, other.mIntegerValue) &&
                   java.util.Objects.equals(mLongValue, other.mLongValue) &&
                   java.util.Objects.equals(mDoubleValue, other.mDoubleValue) &&
                   java.util.Objects.equals(mStringValue, other.mStringValue) &&
                   java.util.Objects.equals(mBinaryValue, other.mBinaryValue) &&
                   java.util.Objects.equals(mEnumValue, other.mEnumValue) &&
                   java.util.Objects.equals(getCompactValue(), other.getCompactValue()) &&
                   java.util.Objects.equals(mStringList, other.mStringList) &&
                   java.util.Objects.equals(mSetList, other.mSetList);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    UnionFields.class,
                    UnionFields._Field.BOOLEAN_VALUE, mBooleanValue,
                    UnionFields._Field.BYTE_VALUE, mByteValue,
                    UnionFields._Field.SHORT_VALUE, mShortValue,
                    UnionFields._Field.INTEGER_VALUE, mIntegerValue,
                    UnionFields._Field.LONG_VALUE, mLongValue,
                    UnionFields._Field.DOUBLE_VALUE, mDoubleValue,
                    UnionFields._Field.STRING_VALUE, mStringValue,
                    UnionFields._Field.BINARY_VALUE, mBinaryValue,
                    UnionFields._Field.ENUM_VALUE, mEnumValue,
                    UnionFields._Field.COMPACT_VALUE, getCompactValue(),
                    UnionFields._Field.STRING_LIST, mStringList,
                    UnionFields._Field.SET_LIST, mSetList);
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                case 10: return mutableCompactValue();
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public UnionFields._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                case 1: setBooleanValue((boolean) value); break;
                case 2: setByteValue((byte) value); break;
                case 3: setShortValue((short) value); break;
                case 4: setIntegerValue((int) value); break;
                case 5: setLongValue((long) value); break;
                case 6: setDoubleValue((double) value); break;
                case 7: setStringValue((String) value); break;
                case 8: setBinaryValue((net.morimekta.util.Binary) value); break;
                case 9: setEnumValue((net.morimekta.test.providence.core.no_rw_binary.Value) value); break;
                case 10: setCompactValue((net.morimekta.test.providence.core.no_rw_binary.CompactFields) value); break;
                case 11: setStringList((java.util.List<String>) value); break;
                case 12: setSetList((java.util.Set<net.morimekta.test.providence.core.no_rw_binary.CompactFields>) value); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                case 1: return tUnionField == UnionFields._Field.BOOLEAN_VALUE;
                case 2: return tUnionField == UnionFields._Field.BYTE_VALUE;
                case 3: return tUnionField == UnionFields._Field.SHORT_VALUE;
                case 4: return tUnionField == UnionFields._Field.INTEGER_VALUE;
                case 5: return tUnionField == UnionFields._Field.LONG_VALUE;
                case 6: return tUnionField == UnionFields._Field.DOUBLE_VALUE;
                case 7: return tUnionField == UnionFields._Field.STRING_VALUE;
                case 8: return tUnionField == UnionFields._Field.BINARY_VALUE;
                case 9: return tUnionField == UnionFields._Field.ENUM_VALUE;
                case 10: return tUnionField == UnionFields._Field.COMPACT_VALUE;
                case 11: return tUnionField == UnionFields._Field.STRING_LIST;
                case 12: return tUnionField == UnionFields._Field.SET_LIST;
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            return modified;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) (Object) isBooleanValue();
                case 2: return (T) (Object) getByteValue();
                case 3: return (T) (Object) getShortValue();
                case 4: return (T) (Object) getIntegerValue();
                case 5: return (T) (Object) getLongValue();
                case 6: return (T) (Object) getDoubleValue();
                case 7: return (T) getStringValue();
                case 8: return (T) getBinaryValue();
                case 9: return (T) getEnumValue();
                case 10: return (T) getCompactValue();
                case 11: return (T) getStringList();
                case 12: return (T) getSetList();
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return tUnionField == _Field.BOOLEAN_VALUE;
                case 2: return tUnionField == _Field.BYTE_VALUE;
                case 3: return tUnionField == _Field.SHORT_VALUE;
                case 4: return tUnionField == _Field.INTEGER_VALUE;
                case 5: return tUnionField == _Field.LONG_VALUE;
                case 6: return tUnionField == _Field.DOUBLE_VALUE;
                case 7: return tUnionField == _Field.STRING_VALUE;
                case 8: return tUnionField == _Field.BINARY_VALUE;
                case 9: return tUnionField == _Field.ENUM_VALUE;
                case 10: return tUnionField == _Field.COMPACT_VALUE;
                case 11: return tUnionField == _Field.STRING_LIST;
                case 12: return tUnionField == _Field.SET_LIST;
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public UnionFields._Builder addTo(int key, Object value) {
            switch (key) {
                case 11: addToStringList((String) value); break;
                case 12: addToSetList((net.morimekta.test.providence.core.no_rw_binary.CompactFields) value); break;
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public UnionFields._Builder clear(int key) {
            switch (key) {
                case 1: clearBooleanValue(); break;
                case 2: clearByteValue(); break;
                case 3: clearShortValue(); break;
                case 4: clearIntegerValue(); break;
                case 5: clearLongValue(); break;
                case 6: clearDoubleValue(); break;
                case 7: clearStringValue(); break;
                case 8: clearBinaryValue(); break;
                case 9: clearEnumValue(); break;
                case 10: clearCompactValue(); break;
                case 11: clearStringList(); break;
                case 12: clearSetList(); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            if (tUnionField == null) {
                return false;
            }

            switch (tUnionField) {
                case BOOLEAN_VALUE: return mBooleanValue != null;
                case BYTE_VALUE: return mByteValue != null;
                case SHORT_VALUE: return mShortValue != null;
                case INTEGER_VALUE: return mIntegerValue != null;
                case LONG_VALUE: return mLongValue != null;
                case DOUBLE_VALUE: return mDoubleValue != null;
                case STRING_VALUE: return mStringValue != null;
                case BINARY_VALUE: return mBinaryValue != null;
                case ENUM_VALUE: return mEnumValue != null;
                case COMPACT_VALUE: return mCompactValue != null || mCompactValue_builder != null;
                case STRING_LIST: return mStringList != null;
                case SET_LIST: return mSetList != null;
                default: return true;
            }
        }

        @Override
        public UnionFields._Builder validate() {
            if (!valid()) {
                throw new java.lang.IllegalStateException("No union field set in providence.UnionFields");
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PUnionDescriptor<UnionFields> descriptor() {
            return UnionFields.kDescriptor;
        }

        @Override
        @javax.annotation.Nonnull
        public UnionFields build() {
            return new UnionFields(this);
        }
    }
}
