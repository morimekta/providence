package net.morimekta.test.providence.core.calculator;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public class Calculator2 extends net.morimekta.test.providence.core.calculator.Calculator {
    public interface Iface extends net.morimekta.test.providence.core.calculator.Calculator.Iface {
        /**
         * @return The extra result.
         * @throws java.io.IOException On providence or non-declared exceptions.
         */
        @javax.annotation.Nonnull
        String extra()
                throws java.io.IOException;
    }

    /**
     * Client implementation for calculator.Calculator2
     */
    public static final class Client
            extends net.morimekta.providence.PClient
            implements Iface {
        private final net.morimekta.providence.PServiceCallHandler handler;

        /**
         * Create calculator.Calculator2 service client.
         *
         * @param handler The client handler.
         */
        public Client(net.morimekta.providence.PServiceCallHandler handler) {
            this.handler = handler;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.calculator.Operand calculate(
                net.morimekta.test.providence.core.calculator.Operation pOp)
                throws java.io.IOException,
                       net.morimekta.test.providence.core.calculator.CalculateException {
            Calculate_Request._Builder rq = Calculate_Request.builder();
            if (pOp != null) {
                rq.setOp(pOp);
            }

            net.morimekta.providence.PServiceCall<Calculate_Request> call = new net.morimekta.providence.PServiceCall<>("calculate", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, Calculator2.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Calculate_Response msg = (Calculate_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case CE:
                        throw msg.getCe();
                    case SUCCESS:
                        return msg.getSuccess();
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for calculator.Calculator2.calculate() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }

        @Override
        public void iamalive()
                throws java.io.IOException {
            Iamalive_Request._Builder rq = Iamalive_Request.builder();

            net.morimekta.providence.PServiceCall<Iamalive_Request> call = new net.morimekta.providence.PServiceCall<>("iamalive", net.morimekta.providence.PServiceCallType.ONEWAY, getNextSequenceId(), rq.build());
            handler.handleCall(call, Calculator2.kDescriptor);
        }

        @Override
        public void ping()
                throws java.io.IOException {
            Ping_Request._Builder rq = Ping_Request.builder();

            net.morimekta.providence.PServiceCall<Ping_Request> call = new net.morimekta.providence.PServiceCall<>("ping", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, Calculator2.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Ping_Response msg = (Ping_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case SUCCESS:
                        return;
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for calculator.Calculator2.ping() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }

        @Override
        @javax.annotation.Nonnull
        public String extra()
                throws java.io.IOException {
            Extra_Request._Builder rq = Extra_Request.builder();

            net.morimekta.providence.PServiceCall<Extra_Request> call = new net.morimekta.providence.PServiceCall<>("extra", net.morimekta.providence.PServiceCallType.CALL, getNextSequenceId(), rq.build());
            net.morimekta.providence.PServiceCall resp = handler.handleCall(call, Calculator2.kDescriptor);

            if (resp.getType() == net.morimekta.providence.PServiceCallType.EXCEPTION) {
                throw (net.morimekta.providence.PApplicationException) resp.getMessage();
            }

            Extra_Response msg = (Extra_Response) resp.getMessage();
            if (msg.unionFieldIsSet()) {
                switch (msg.unionField()) {
                    case SUCCESS:
                        return msg.getSuccess();
                }
            }

            throw new net.morimekta.providence.PApplicationException("Result field for calculator.Calculator2.extra() not set",
                                                                     net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
        }
    }

    public static final class Processor implements net.morimekta.providence.PProcessor {
        private final Iface impl;
        private final java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer;
        public Processor(@javax.annotation.Nonnull Iface impl) {
            this(impl, (m, e) -> e);
        }

        public Processor(@javax.annotation.Nonnull Iface impl,
                         @javax.annotation.Nonnull java.util.function.BiFunction<net.morimekta.providence.descriptor.PServiceMethod, Exception, Exception> exceptionTransformer) {
            this.impl = impl;
            this.exceptionTransformer = exceptionTransformer;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getDescriptor() {
            return kDescriptor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <Request extends net.morimekta.providence.PMessage<Request>,
                Response extends net.morimekta.providence.PMessage<Response>>
        net.morimekta.providence.PServiceCall<Response> handleCall(
                net.morimekta.providence.PServiceCall<Request> call,
                net.morimekta.providence.descriptor.PService service)
                throws net.morimekta.providence.PApplicationException {
            net.morimekta.providence.PApplicationException ex = null;
            switch(call.getMethod()) {
                case "calculate": {
                    Calculate_Response._Builder rsp = Calculate_Response.builder();
                    try {
                        Calculate_Request req = (Calculate_Request) call.getMessage();
                        try {
                            net.morimekta.test.providence.core.calculator.Operand result =
                                    impl.calculate(req.getOp());
                            if (result == null) {
                                ex = new net.morimekta.providence.PApplicationException(
                                        "Returning null value from calculate in calculator.Calculator2",
                                        net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
                                break;
                            }
                            rsp.setSuccess(result);
                        } catch (net.morimekta.test.providence.core.calculator.CalculateException e) {
                            throw e;
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.CALCULATE, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method calculate in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (net.morimekta.test.providence.core.calculator.CalculateException e) {
                        rsp.setCe(e);
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method calculate in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                case "iamalive": {
                    try {
                        Iamalive_Request req = (Iamalive_Request) call.getMessage();
                        try {
                            impl.iamalive();
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.IAMALIVE, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        throw new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method iamalive in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                    } catch (Exception e) {
                        throw new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method iamalive in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                    }
                    return null;
                }
                case "ping": {
                    Ping_Response._Builder rsp = Ping_Response.builder();
                    try {
                        Ping_Request req = (Ping_Request) call.getMessage();
                        try {
                            impl.ping();
                            rsp.setSuccess();
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.PING, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method ping in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method ping in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                case "extra": {
                    Extra_Response._Builder rsp = Extra_Response.builder();
                    try {
                        Extra_Request req = (Extra_Request) call.getMessage();
                        try {
                            String result =
                                    impl.extra();
                            if (result == null) {
                                ex = new net.morimekta.providence.PApplicationException(
                                        "Returning null value from extra in calculator.Calculator2",
                                        net.morimekta.providence.PApplicationExceptionType.MISSING_RESULT);
                                break;
                            }
                            rsp.setSuccess(result);
                        } catch (Exception e) {
                            throw exceptionTransformer.apply(Method.EXTRA, e);
                        }
                    } catch (net.morimekta.providence.PApplicationException e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Wrapped application error " + e.getType() + " in method extra in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    } catch (Exception e) {
                        ex = new net.morimekta.providence.PApplicationException(
                                "Unhandled " + e.getClass().getSimpleName() + " in method extra in calculator.Calculator2: " + e.getMessage(),
                                net.morimekta.providence.PApplicationExceptionType.INTERNAL_ERROR).initCause(e);
                        break;
                    }
                    net.morimekta.providence.PServiceCall reply =
                            new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                                        net.morimekta.providence.PServiceCallType.REPLY,
                                                                        call.getSequence(),
                                                                        rsp.build());
                    return reply;
                }
                default: {
                    ex = new net.morimekta.providence.PApplicationException(
                            "Unknown method \"" + call.getMethod() + "\" on calculator.Calculator2.",
                            net.morimekta.providence.PApplicationExceptionType.UNKNOWN_METHOD);
                    break;
                }
            }
            net.morimekta.providence.PServiceCall reply =
                    new net.morimekta.providence.PServiceCall<>(call.getMethod(),
                                                              net.morimekta.providence.PServiceCallType.EXCEPTION,
                                                              call.getSequence(),
                                                              ex);
            return reply;
        }
    }

    public enum Method implements net.morimekta.providence.descriptor.PServiceMethod {
        CALCULATE("calculate", false, false, Calculate_Request.kDescriptor, Calculate_Response.kDescriptor),
        IAMALIVE("iamalive", true, false, Iamalive_Request.kDescriptor, null),
        PING("ping", false, false, Ping_Request.kDescriptor, Ping_Response.kDescriptor),
        EXTRA("extra", false, false, Extra_Request.kDescriptor, Extra_Response.kDescriptor),
        ;

        private final String name;
        private final boolean oneway;
        private final boolean protoStub;
        private final net.morimekta.providence.descriptor.PStructDescriptor request;
        private final net.morimekta.providence.descriptor.PUnionDescriptor response;

        private Method(String name, boolean oneway, boolean protoStub, net.morimekta.providence.descriptor.PStructDescriptor request, net.morimekta.providence.descriptor.PUnionDescriptor response) {
            this.name = name;
            this.oneway = oneway;
            this.protoStub = protoStub;
            this.request = request;
            this.response = response;
        }

        @javax.annotation.Nonnull
        @Override
        public String getName() {
            return name;
        }

        public boolean isOneway() {
            return oneway;
        }

        public boolean isProtoStub() {
            return protoStub;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor getRequestType() {
            return request;
        }

        @javax.annotation.Nullable
        @Override
        public net.morimekta.providence.descriptor.PUnionDescriptor getResponseType() {
            return response;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }

        @javax.annotation.Nullable
        public static Method findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "calculate": return CALCULATE;
                case "iamalive": return IAMALIVE;
                case "ping": return PING;
                case "extra": return EXTRA;
            }
            return null;
        }
        @javax.annotation.Nonnull
        public static Method methodForName(@javax.annotation.Nonnull String name) {
            Method method = findByName(name);
            if (method == null) {
                throw new IllegalArgumentException("No such method \"" + name + "\" in service calculator.Calculator2");
            }
            return method;
        }
    }

    private static final class _Descriptor extends net.morimekta.providence.descriptor.PService {
        private _Descriptor() {
            super("calculator", "Calculator2", net.morimekta.test.providence.core.calculator.Calculator.provider(), Method.values());
        }

        @Override
        public Method getMethod(String name) {
            return Method.findByName(name);
        }
    }

    private static final class _Provider implements net.morimekta.providence.descriptor.PServiceProvider {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PService getService() {
            return kDescriptor;
        }
    }

    public static final net.morimekta.providence.descriptor.PService kDescriptor = new _Descriptor();

    public static net.morimekta.providence.descriptor.PServiceProvider provider() {
        return new _Provider();
    }

    // type --> Calculator2.extra.request
    @SuppressWarnings("unused")
    public interface Extra_Request_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Extra_Request> {
    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Extra_Request
            implements Extra_Request_OrBuilder,
                       net.morimekta.providence.PMessage<Extra_Request>,
                       Comparable<Extra_Request>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = 7756543901078345535L;


        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Extra_Request tSerializeInstance;

        private Extra_Request(_Builder builder) {
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                default: return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            return true;
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Extra_Request.class);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator2.extra.request" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Extra_Request other) {
            int c;

            return 0;
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Extra_Request> {
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Extra_Request> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator2.extra.request");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator2.extra.request");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator2.extra.request");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Extra_Request> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Extra_Request> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PStructDescriptor<Extra_Request> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PStructDescriptor<Extra_Request> {
            public _Descriptor() {
                super("calculator", "Calculator2.extra.request", _Builder::new, true);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Extra_Request> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PStructDescriptor<Extra_Request> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator2.extra.request</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Extra_Request>
                implements Extra_Request_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private java.util.BitSet optionals;
            private java.util.BitSet modified;

            /**
             * Make a calculator.Calculator2.extra.request builder instance.
             */
            public _Builder() {
                optionals = new java.util.BitSet(0);
                modified = new java.util.BitSet(0);
            }

            /**
             * Make a mutating builder off a base calculator.Calculator2.extra.request.
             *
             * @param base The base Calculator2.extra.request
             */
            public _Builder(Extra_Request base) {
                this();

            }

            @javax.annotation.Nonnull
            @Override
            public Extra_Request._Builder merge(Extra_Request from) {
                return this;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                return true;
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Extra_Request.class, optionals);
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Extra_Request._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                switch (key) {
                    default: break;
                }
                return false;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Extra_Request._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Extra_Request._Builder clear(int key) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                return true;
            }

            @Override
            public Extra_Request._Builder validate() {
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PStructDescriptor<Extra_Request> descriptor() {
                return Extra_Request.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Extra_Request build() {
                return new Extra_Request(this);
            }
        }
    }

    // type <-- Calculator2.extra.response
    @SuppressWarnings("unused")
    public interface Extra_Response_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Extra_Response> {
        /**
         * @return The success value.
         */
        String getSuccess();

        /**
         * @return Optional success value.
         */
        @javax.annotation.Nonnull
        java.util.Optional<String> optionalSuccess();

        /**
         * @return If success is present.
         */
        boolean hasSuccess();

    }

    @SuppressWarnings("unused")
    @javax.annotation.concurrent.Immutable
    public static class Extra_Response
            implements Extra_Response_OrBuilder,
                       net.morimekta.providence.PUnion<Extra_Response>,
                       Comparable<Extra_Response>,
                       java.io.Serializable,
                       net.morimekta.providence.serializer.binary.BinaryWriter {
        private final static long serialVersionUID = -3257175400506493499L;

        private final transient String mSuccess;

        private transient final _Field tUnionField;

        private volatile transient int tHashCode;

        // Transient object used during java deserialization.
        private transient Extra_Response tSerializeInstance;

        /**
         * @param value The union value
         * @return The created union.
         */
        @javax.annotation.Nonnull
        public static Extra_Response withSuccess(@javax.annotation.Nonnull String value) {
            return new _Builder().setSuccess(value).build();
        }

        private Extra_Response(_Builder builder) {
            tUnionField = builder.tUnionField;

            mSuccess = tUnionField == _Field.SUCCESS ? builder.mSuccess : null;
        }

        public boolean hasSuccess() {
            return tUnionField == _Field.SUCCESS && mSuccess != null;
        }

        /**
         * @return The <code>success</code> value
         */
        public String getSuccess() {
            return mSuccess;
        }

        /**
         * @return Optional of the <code>success</code> field value.
         */
        @javax.annotation.Nonnull
        public java.util.Optional<String> optionalSuccess() {
            return java.util.Optional.ofNullable(mSuccess);
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 0: return tUnionField == _Field.SUCCESS;
                default: return false;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 0: return (T) mSuccess;
                default: return null;
            }
        }

        @Override
        public boolean unionFieldIsSet() {
            return tUnionField != null;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field unionField() {
            if (tUnionField == null) throw new IllegalStateException("No union field set in calculator.Calculator2.extra.response");
            return tUnionField;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Extra_Response other = (Extra_Response) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                   java.util.Objects.equals(mSuccess, other.mSuccess);
        }

        @Override
        public int hashCode() {
            if (tHashCode == 0) {
                tHashCode = java.util.Objects.hash(
                        Extra_Response.class,
                        _Field.SUCCESS, mSuccess);
            }
            return tHashCode;
        }

        @Override
        public String toString() {
            return "calculator.Calculator2.extra.response" + asString();
        }

        @Override
        @javax.annotation.Nonnull
        public String asString() {
            StringBuilder out = new StringBuilder();
            out.append("{");

            switch (tUnionField) {
                case SUCCESS: {
                    out.append("success:")
                       .append('\"').append(net.morimekta.util.Strings.escape(mSuccess)).append('\"');
                    break;
                }
            }
            out.append('}');
            return out.toString();
        }

        @Override
        public int compareTo(Extra_Response other) {
            if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
            int c = tUnionField.compareTo(other.tUnionField);
            if (c != 0) return c;

            switch (tUnionField) {
                case SUCCESS:
                    return mSuccess.compareTo(other.mSuccess);
                default: return 0;
            }
        }

        private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
            oos.defaultWriteObject();
            net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
        }

        private void readObject(java.io.ObjectInputStream ois)
                throws java.io.IOException, ClassNotFoundException {
            ois.defaultReadObject();
            tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        }

        private Object readResolve() throws java.io.ObjectStreamException {
            return tSerializeInstance;
        }

        @Override
        public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
            int length = 0;

            if (tUnionField != null) {
                switch (tUnionField) {
                    case SUCCESS: {
                        length += writer.writeByte((byte) 11);
                        length += writer.writeShort((short) 0);
                        net.morimekta.util.Binary tmp_1 = net.morimekta.util.Binary.wrap(mSuccess.getBytes(java.nio.charset.StandardCharsets.UTF_8));
                        length += writer.writeUInt32(tmp_1.length());
                        length += writer.writeBinary(tmp_1);
                        break;
                    }
                    default: break;
                }
            }
            length += writer.writeByte((byte) 0);
            return length;
        }

        @javax.annotation.Nonnull
        @Override
        public _Builder mutate() {
            return new _Builder(this);
        }

        public enum _Field implements net.morimekta.providence.descriptor.PField<Extra_Response> {
            SUCCESS(0, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "success", "success", net.morimekta.providence.descriptor.PPrimitive.STRING.provider(), null, null),
            ;

            private final int mId;
            private final net.morimekta.providence.descriptor.PRequirement mRequired;
            private final String mName;
            private final String mPojoName;
            private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
            private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
            private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

            _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
                mId = id;
                mRequired = required;
                mName = name;
                mPojoName = pojoName;
                mTypeProvider = typeProvider;
                mArgumentsProvider = argumentsProvider;
                mDefaultValue = defaultValue;
            }

            @Override
            public int getId() { return mId; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

            @Override
            @javax.annotation.Nullable
            public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

            @javax.annotation.Nonnull
            @Override
            public String getName() { return mName; }

            @javax.annotation.Nonnull
            @Override
            public String getPojoName() { return mPojoName; }

            @Override
            public boolean hasDefaultValue() { return mDefaultValue != null; }

            @Override
            @javax.annotation.Nullable
            public Object getDefaultValue() {
                return hasDefaultValue() ? mDefaultValue.get() : null;
            }

            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PMessageDescriptor<Extra_Response> onMessageType() {
                return kDescriptor;
            }

            @Override
            public String toString() {
                return net.morimekta.providence.descriptor.PField.asString(this);
            }

            /**
             * @param id Field ID
             * @return The identified field or null
             */
            public static _Field findById(int id) {
                switch (id) {
                    case 0: return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param name Field name
             * @return The named field or null
             */
            public static _Field findByName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param name Field POJO name
             * @return The named field or null
             */
            public static _Field findByPojoName(String name) {
                if (name == null) return null;
                switch (name) {
                    case "success": return _Field.SUCCESS;
                }
                return null;
            }

            /**
             * @param id Field ID
             * @return The identified field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForId(int id) {
                _Field field = findById(id);
                if (field == null) {
                    throw new IllegalArgumentException("No such field id " + id + " in calculator.Calculator2.extra.response");
                }
                return field;
            }

            /**
             * @param name Field name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator2.extra.response");
                }
                return field;
            }

            /**
             * @param name Field POJO name
             * @return The named field
             * @throws IllegalArgumentException If no such field
             */
            public static _Field fieldForPojoName(String name) {
                if (name == null) {
                    throw new IllegalArgumentException("Null name argument");
                }
                _Field field = findByPojoName(name);
                if (field == null) {
                    throw new IllegalArgumentException("No such field \"" + name + "\" in calculator.Calculator2.extra.response");
                }
                return field;
            }
        }

        @javax.annotation.Nonnull
        public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<Extra_Response> provider() {
            return new _Provider();
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<Extra_Response> descriptor() {
            return kDescriptor;
        }

        public static final net.morimekta.providence.descriptor.PUnionDescriptor<Extra_Response> kDescriptor;

        private static final class _Descriptor
                extends net.morimekta.providence.descriptor.PUnionDescriptor<Extra_Response> {
            public _Descriptor() {
                super("calculator", "Calculator2.extra.response", _Builder::new, true);
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isInnerType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public boolean isAutoType() {
                return true;
            }

            @Override
            @javax.annotation.Nonnull
            public _Field[] getFields() {
                return _Field.values();
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByName(String name) {
                return _Field.findByName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldByPojoName(String name) {
                return _Field.findByPojoName(name);
            }

            @Override
            @javax.annotation.Nullable
            public _Field findFieldById(int id) {
                return _Field.findById(id);
            }
        }

        static {
            kDescriptor = new _Descriptor();
        }

        private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<Extra_Response> {
            @Override
            @javax.annotation.Nonnull
            public net.morimekta.providence.descriptor.PUnionDescriptor<Extra_Response> descriptor() {
                return kDescriptor;
            }
        }

        /**
         * Make a <code>calculator.Calculator2.extra.response</code> builder.
         * @return The builder instance.
         */
        public static _Builder builder() {
            return new _Builder();
        }

        public static class _Builder
                extends net.morimekta.providence.PMessageBuilder<Extra_Response>
                implements Extra_Response_OrBuilder,
                           net.morimekta.providence.serializer.binary.BinaryReader {
            private Extra_Response._Field tUnionField;

            private boolean modified;

            private String mSuccess;

            /**
             * Make a calculator.Calculator2.extra.response builder instance.
             */
            public _Builder() {
                modified = false;
            }

            /**
             * Make a mutating builder off a base calculator.Calculator2.extra.response.
             *
             * @param base The base Calculator2.extra.response
             */
            public _Builder(Extra_Response base) {
                this();

                tUnionField = base.tUnionField;

                mSuccess = base.mSuccess;
            }

            @javax.annotation.Nonnull
            @Override
            public Extra_Response._Builder merge(Extra_Response from) {
                if (!from.unionFieldIsSet()) {
                    return this;
                }

                switch (from.unionField()) {
                    case SUCCESS: {
                        setSuccess(from.getSuccess());
                        break;
                    }
                }
                return this;
            }

            /**
             * Set the <code>success</code> field value.
             *
             * @param value The new value
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Extra_Response._Builder setSuccess(String value) {
                if (value == null) {
                    return clearSuccess();
                }

                tUnionField = Extra_Response._Field.SUCCESS;
                modified = true;
                mSuccess = value;
                return this;
            }

            /**
             * Checks for explicit presence of the <code>success</code> field.
             *
             * @return True if success has been set.
             */
            public boolean isSetSuccess() {
                return tUnionField == Extra_Response._Field.SUCCESS;
            }

            /**
             * Checks for presence of the <code>success</code> field.
             *
             * @return True if success is present.
             */
            public boolean hasSuccess() {
                return tUnionField == Extra_Response._Field.SUCCESS;
            }

            /**
             * Clear the <code>success</code> field.
             *
             * @return The builder
             */
            @javax.annotation.Nonnull
            public Extra_Response._Builder clearSuccess() {
                if (tUnionField == Extra_Response._Field.SUCCESS) tUnionField = null;
                modified = true;
                mSuccess = null;
                return this;
            }

            /**
             * @return The <code>success</code> field value
             */
            public String getSuccess() {
                return mSuccess;
            }

            /**
             * @return Optional <code>success</code> field value
             */
            @javax.annotation.Nonnull
            public java.util.Optional<String> optionalSuccess() {
                return java.util.Optional.ofNullable(mSuccess);
            }

            /**
             * Checks if the <code>Calculator2.extra.response</code> union has been modified since the
             * builder was created.
             *
             * @return True if Calculator2.extra.response has been modified.
             */
            public boolean isUnionModified() {
                return modified;
            }

            @Override
            public boolean equals(Object o) {
                if (o == this) return true;
                if (o == null || !o.getClass().equals(getClass())) return false;
                Extra_Response._Builder other = (Extra_Response._Builder) o;
                return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                       java.util.Objects.equals(mSuccess, other.mSuccess);
            }

            @Override
            public int hashCode() {
                return java.util.Objects.hash(
                        Extra_Response.class,
                        Extra_Response._Field.SUCCESS, mSuccess);
            }

            @Override
            @SuppressWarnings("unchecked")
            public net.morimekta.providence.PMessageBuilder mutator(int key) {
                switch (key) {
                    default: throw new IllegalArgumentException("Not a message field ID: " + key);
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Extra_Response._Builder set(int key, Object value) {
                if (value == null) return clear(key);
                switch (key) {
                    case 0: setSuccess((String) value); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean isSet(int key) {
                switch (key) {
                    case 0: return tUnionField == Extra_Response._Field.SUCCESS;
                    default: break;
                }
                return false;
            }

            @Override
            public boolean isModified(int key) {
                return modified;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T get(int key) {
                switch(key) {
                    case 0: return (T) getSuccess();
                    default: return null;
                }
            }

            @Override
            public boolean has(int key) {
                switch(key) {
                    case 0: return tUnionField == _Field.SUCCESS;
                    default: return false;
                }
            }

            @javax.annotation.Nonnull
            @Override
            @SuppressWarnings("unchecked")
            public Extra_Response._Builder addTo(int key, Object value) {
                switch (key) {
                    default: break;
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public Extra_Response._Builder clear(int key) {
                switch (key) {
                    case 0: clearSuccess(); break;
                    default: break;
                }
                return this;
            }

            @Override
            public boolean valid() {
                if (tUnionField == null) {
                    return false;
                }

                switch (tUnionField) {
                    case SUCCESS: return mSuccess != null;
                    default: return true;
                }
            }

            @Override
            public Extra_Response._Builder validate() {
                if (!valid()) {
                    throw new java.lang.IllegalStateException("No union field set in calculator.Calculator2.extra.response");
                }
                return this;
            }

            @javax.annotation.Nonnull
            @Override
            public net.morimekta.providence.descriptor.PUnionDescriptor<Extra_Response> descriptor() {
                return Extra_Response.kDescriptor;
            }

            @Override
            public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
                byte type = reader.expectByte();
                while (type != 0) {
                    int field = reader.expectShort();
                    switch (field) {
                        case 0: {
                            if (type == 11) {
                                int len_1 = reader.expectUInt32();
                                mSuccess = new String(reader.expectBytes(len_1), java.nio.charset.StandardCharsets.UTF_8);
                                tUnionField = Extra_Response._Field.SUCCESS;
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for calculator.Calculator2.extra.response.success, should be struct(12)");
                            }
                            break;
                        }
                        default: {
                            net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                            break;
                        }
                    }
                    type = reader.expectByte();
                }
            }

            @Override
            @javax.annotation.Nonnull
            public Extra_Response build() {
                return new Extra_Response(this);
            }
        }
    }

    protected Calculator2() {}
}
