package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ConsumeAll_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ConsumeAll> {
}
