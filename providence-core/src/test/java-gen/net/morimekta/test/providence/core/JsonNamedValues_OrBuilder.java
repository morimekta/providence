package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface JsonNamedValues_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<JsonNamedValues> {
    /**
     * @return The string_value value.
     */
    String getStringValue();

    /**
     * @return Optional string_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalStringValue();

    /**
     * @return If string_value is present.
     */
    boolean hasStringValue();

    /**
     * @return The enum_value value.
     */
    net.morimekta.test.providence.core.JsonNamedEnum getEnumValue();

    /**
     * @return Optional enum_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.JsonNamedEnum> optionalEnumValue();

    /**
     * @return If enum_value is present.
     */
    boolean hasEnumValue();

}
