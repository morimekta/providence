package net.morimekta.test.providence.core;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@javax.annotation.concurrent.Immutable
public class Containers
        implements Containers_OrBuilder,
                   net.morimekta.providence.PMessage<Containers>,
                   Comparable<Containers>,
                   java.io.Serializable,
                   net.morimekta.providence.serializer.binary.BinaryWriter {
    private final static long serialVersionUID = 3106880629763954881L;

    private final transient java.util.List<Boolean> mBooleanList;
    private final transient java.util.List<Byte> mByteList;
    private final transient java.util.List<Short> mShortList;
    private final transient java.util.List<Integer> mIntegerList;
    private final transient java.util.List<Long> mLongList;
    private final transient java.util.List<Double> mDoubleList;
    private final transient java.util.List<String> mStringList;
    private final transient java.util.List<net.morimekta.util.Binary> mBinaryList;
    private final transient java.util.Set<Boolean> mBooleanSet;
    private final transient java.util.Set<Byte> mByteSet;
    private final transient java.util.Set<Short> mShortSet;
    private final transient java.util.Set<Integer> mIntegerSet;
    private final transient java.util.Set<Long> mLongSet;
    private final transient java.util.Set<Double> mDoubleSet;
    private final transient java.util.Set<String> mStringSet;
    private final transient java.util.Set<net.morimekta.util.Binary> mBinarySet;
    private final transient java.util.Map<Boolean,Boolean> mBooleanMap;
    private final transient java.util.Map<Byte,Byte> mByteMap;
    private final transient java.util.Map<Short,Short> mShortMap;
    private final transient java.util.Map<Integer,Integer> mIntegerMap;
    private final transient java.util.Map<Long,Long> mLongMap;
    private final transient java.util.Map<Double,Double> mDoubleMap;
    private final transient java.util.Map<String,String> mStringMap;
    private final transient java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> mBinaryMap;
    private final transient java.util.List<net.morimekta.test.providence.core.Value> mEnumList;
    private final transient java.util.Set<net.morimekta.test.providence.core.Value> mEnumSet;
    private final transient java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> mEnumMap;
    private final transient java.util.List<net.morimekta.test.providence.core.OptionalFields> mMessageList;
    private final transient java.util.Set<net.morimekta.test.providence.core.OptionalFields> mMessageSet;
    private final transient java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> mMessageMap;
    private final transient java.util.Map<net.morimekta.test.providence.core.CompactFields,String> mMessageKeyMap;
    private final transient net.morimekta.test.providence.core.RequiredFields mRequiredFields;
    private final transient net.morimekta.test.providence.core.DefaultFields mDefaultFields;
    private final transient net.morimekta.test.providence.core.OptionalFields mOptionalFields;
    private final transient net.morimekta.test.providence.core.UnionFields mUnionFields;
    private final transient net.morimekta.test.providence.core.ExceptionFields mExceptionFields;
    private final transient net.morimekta.test.providence.core.DefaultValues mDefaultValues;
    private final transient java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> mMapListCompact;
    private final transient java.util.List<java.util.Map<Integer,Integer>> mListMapNumbers;
    private final transient java.util.Set<java.util.List<Integer>> mSetListNumbers;
    private final transient java.util.List<java.util.List<Integer>> mListListNumbers;

    private volatile transient int tHashCode;

    // Transient object used during java deserialization.
    private transient Containers tSerializeInstance;

    private Containers(_Builder builder) {
        if (builder.isSetBooleanList()) {
            mBooleanList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mBooleanList);
        } else {
            mBooleanList = null;
        }
        if (builder.isSetByteList()) {
            mByteList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mByteList);
        } else {
            mByteList = null;
        }
        if (builder.isSetShortList()) {
            mShortList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mShortList);
        } else {
            mShortList = null;
        }
        if (builder.isSetIntegerList()) {
            mIntegerList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mIntegerList);
        } else {
            mIntegerList = null;
        }
        if (builder.isSetLongList()) {
            mLongList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mLongList);
        } else {
            mLongList = null;
        }
        if (builder.isSetDoubleList()) {
            mDoubleList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mDoubleList);
        } else {
            mDoubleList = null;
        }
        if (builder.isSetStringList()) {
            mStringList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mStringList);
        } else {
            mStringList = null;
        }
        if (builder.isSetBinaryList()) {
            mBinaryList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mBinaryList);
        } else {
            mBinaryList = null;
        }
        if (builder.isSetBooleanSet()) {
            mBooleanSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mBooleanSet);
        } else {
            mBooleanSet = null;
        }
        if (builder.isSetByteSet()) {
            mByteSet = net.morimekta.util.collect.UnmodifiableSortedSet.copyOf(builder.mByteSet);
        } else {
            mByteSet = null;
        }
        if (builder.isSetShortSet()) {
            mShortSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mShortSet);
        } else {
            mShortSet = null;
        }
        if (builder.isSetIntegerSet()) {
            mIntegerSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mIntegerSet);
        } else {
            mIntegerSet = null;
        }
        if (builder.isSetLongSet()) {
            mLongSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mLongSet);
        } else {
            mLongSet = null;
        }
        if (builder.isSetDoubleSet()) {
            mDoubleSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mDoubleSet);
        } else {
            mDoubleSet = null;
        }
        if (builder.isSetStringSet()) {
            mStringSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mStringSet);
        } else {
            mStringSet = null;
        }
        if (builder.isSetBinarySet()) {
            mBinarySet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mBinarySet);
        } else {
            mBinarySet = null;
        }
        if (builder.isSetBooleanMap()) {
            mBooleanMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mBooleanMap);
        } else {
            mBooleanMap = null;
        }
        if (builder.isSetByteMap()) {
            mByteMap = net.morimekta.util.collect.UnmodifiableSortedMap.copyOf(builder.mByteMap);
        } else {
            mByteMap = null;
        }
        if (builder.isSetShortMap()) {
            mShortMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mShortMap);
        } else {
            mShortMap = null;
        }
        if (builder.isSetIntegerMap()) {
            mIntegerMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mIntegerMap);
        } else {
            mIntegerMap = null;
        }
        if (builder.isSetLongMap()) {
            mLongMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mLongMap);
        } else {
            mLongMap = null;
        }
        if (builder.isSetDoubleMap()) {
            mDoubleMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mDoubleMap);
        } else {
            mDoubleMap = null;
        }
        if (builder.isSetStringMap()) {
            mStringMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mStringMap);
        } else {
            mStringMap = null;
        }
        if (builder.isSetBinaryMap()) {
            mBinaryMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mBinaryMap);
        } else {
            mBinaryMap = null;
        }
        if (builder.isSetEnumList()) {
            mEnumList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mEnumList);
        } else {
            mEnumList = null;
        }
        if (builder.isSetEnumSet()) {
            mEnumSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mEnumSet);
        } else {
            mEnumSet = null;
        }
        if (builder.isSetEnumMap()) {
            mEnumMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mEnumMap);
        } else {
            mEnumMap = null;
        }
        if (builder.isSetMessageList()) {
            mMessageList = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mMessageList);
        } else {
            mMessageList = null;
        }
        if (builder.isSetMessageSet()) {
            mMessageSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mMessageSet);
        } else {
            mMessageSet = null;
        }
        if (builder.isSetMessageMap()) {
            mMessageMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mMessageMap);
        } else {
            mMessageMap = null;
        }
        if (builder.isSetMessageKeyMap()) {
            mMessageKeyMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mMessageKeyMap);
        } else {
            mMessageKeyMap = null;
        }
        mRequiredFields = builder.mRequiredFields_builder != null ? builder.mRequiredFields_builder.build() : builder.mRequiredFields;
        mDefaultFields = builder.mDefaultFields_builder != null ? builder.mDefaultFields_builder.build() : builder.mDefaultFields;
        mOptionalFields = builder.mOptionalFields_builder != null ? builder.mOptionalFields_builder.build() : builder.mOptionalFields;
        mUnionFields = builder.mUnionFields_builder != null ? builder.mUnionFields_builder.build() : builder.mUnionFields;
        mExceptionFields = builder.mExceptionFields_builder != null ? builder.mExceptionFields_builder.build() : builder.mExceptionFields;
        mDefaultValues = builder.mDefaultValues_builder != null ? builder.mDefaultValues_builder.build() : builder.mDefaultValues;
        if (builder.isSetMapListCompact()) {
            mMapListCompact = net.morimekta.util.collect.UnmodifiableMap.copyOf(builder.mMapListCompact);
        } else {
            mMapListCompact = null;
        }
        if (builder.isSetListMapNumbers()) {
            mListMapNumbers = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mListMapNumbers);
        } else {
            mListMapNumbers = null;
        }
        if (builder.isSetSetListNumbers()) {
            mSetListNumbers = net.morimekta.util.collect.UnmodifiableSet.copyOf(builder.mSetListNumbers);
        } else {
            mSetListNumbers = null;
        }
        if (builder.isSetListListNumbers()) {
            mListListNumbers = net.morimekta.util.collect.UnmodifiableList.copyOf(builder.mListListNumbers);
        } else {
            mListListNumbers = null;
        }
    }

    public int numBooleanList() {
        return mBooleanList != null ? mBooleanList.size() : 0;
    }

    public boolean hasBooleanList() {
        return mBooleanList != null;
    }

    /**
     * @return The <code>boolean_list</code> value
     */
    public java.util.List<Boolean> getBooleanList() {
        return mBooleanList;
    }

    /**
     * @return Optional of the <code>boolean_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Boolean>> optionalBooleanList() {
        return java.util.Optional.ofNullable(mBooleanList);
    }

    public int numByteList() {
        return mByteList != null ? mByteList.size() : 0;
    }

    public boolean hasByteList() {
        return mByteList != null;
    }

    /**
     * @return The <code>byte_list</code> value
     */
    public java.util.List<Byte> getByteList() {
        return mByteList;
    }

    /**
     * @return Optional of the <code>byte_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Byte>> optionalByteList() {
        return java.util.Optional.ofNullable(mByteList);
    }

    public int numShortList() {
        return mShortList != null ? mShortList.size() : 0;
    }

    public boolean hasShortList() {
        return mShortList != null;
    }

    /**
     * @return The <code>short_list</code> value
     */
    public java.util.List<Short> getShortList() {
        return mShortList;
    }

    /**
     * @return Optional of the <code>short_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Short>> optionalShortList() {
        return java.util.Optional.ofNullable(mShortList);
    }

    public int numIntegerList() {
        return mIntegerList != null ? mIntegerList.size() : 0;
    }

    public boolean hasIntegerList() {
        return mIntegerList != null;
    }

    /**
     * @return The <code>integer_list</code> value
     */
    public java.util.List<Integer> getIntegerList() {
        return mIntegerList;
    }

    /**
     * @return Optional of the <code>integer_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Integer>> optionalIntegerList() {
        return java.util.Optional.ofNullable(mIntegerList);
    }

    public int numLongList() {
        return mLongList != null ? mLongList.size() : 0;
    }

    public boolean hasLongList() {
        return mLongList != null;
    }

    /**
     * @return The <code>long_list</code> value
     */
    public java.util.List<Long> getLongList() {
        return mLongList;
    }

    /**
     * @return Optional of the <code>long_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Long>> optionalLongList() {
        return java.util.Optional.ofNullable(mLongList);
    }

    public int numDoubleList() {
        return mDoubleList != null ? mDoubleList.size() : 0;
    }

    public boolean hasDoubleList() {
        return mDoubleList != null;
    }

    /**
     * @return The <code>double_list</code> value
     */
    public java.util.List<Double> getDoubleList() {
        return mDoubleList;
    }

    /**
     * @return Optional of the <code>double_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<Double>> optionalDoubleList() {
        return java.util.Optional.ofNullable(mDoubleList);
    }

    public int numStringList() {
        return mStringList != null ? mStringList.size() : 0;
    }

    public boolean hasStringList() {
        return mStringList != null;
    }

    /**
     * @return The <code>string_list</code> value
     */
    public java.util.List<String> getStringList() {
        return mStringList;
    }

    /**
     * @return Optional of the <code>string_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<String>> optionalStringList() {
        return java.util.Optional.ofNullable(mStringList);
    }

    public int numBinaryList() {
        return mBinaryList != null ? mBinaryList.size() : 0;
    }

    public boolean hasBinaryList() {
        return mBinaryList != null;
    }

    /**
     * @return The <code>binary_list</code> value
     */
    public java.util.List<net.morimekta.util.Binary> getBinaryList() {
        return mBinaryList;
    }

    /**
     * @return Optional of the <code>binary_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<net.morimekta.util.Binary>> optionalBinaryList() {
        return java.util.Optional.ofNullable(mBinaryList);
    }

    public int numBooleanSet() {
        return mBooleanSet != null ? mBooleanSet.size() : 0;
    }

    public boolean hasBooleanSet() {
        return mBooleanSet != null;
    }

    /**
     * @return The <code>boolean_set</code> value
     */
    public java.util.Set<Boolean> getBooleanSet() {
        return mBooleanSet;
    }

    /**
     * @return Optional of the <code>boolean_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Boolean>> optionalBooleanSet() {
        return java.util.Optional.ofNullable(mBooleanSet);
    }

    public int numByteSet() {
        return mByteSet != null ? mByteSet.size() : 0;
    }

    public boolean hasByteSet() {
        return mByteSet != null;
    }

    /**
     * @return The <code>byte_set</code> value
     */
    public java.util.Set<Byte> getByteSet() {
        return mByteSet;
    }

    /**
     * @return Optional of the <code>byte_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Byte>> optionalByteSet() {
        return java.util.Optional.ofNullable(mByteSet);
    }

    public int numShortSet() {
        return mShortSet != null ? mShortSet.size() : 0;
    }

    public boolean hasShortSet() {
        return mShortSet != null;
    }

    /**
     * @return The <code>short_set</code> value
     */
    public java.util.Set<Short> getShortSet() {
        return mShortSet;
    }

    /**
     * @return Optional of the <code>short_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Short>> optionalShortSet() {
        return java.util.Optional.ofNullable(mShortSet);
    }

    public int numIntegerSet() {
        return mIntegerSet != null ? mIntegerSet.size() : 0;
    }

    public boolean hasIntegerSet() {
        return mIntegerSet != null;
    }

    /**
     * @return The <code>integer_set</code> value
     */
    public java.util.Set<Integer> getIntegerSet() {
        return mIntegerSet;
    }

    /**
     * @return Optional of the <code>integer_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Integer>> optionalIntegerSet() {
        return java.util.Optional.ofNullable(mIntegerSet);
    }

    public int numLongSet() {
        return mLongSet != null ? mLongSet.size() : 0;
    }

    public boolean hasLongSet() {
        return mLongSet != null;
    }

    /**
     * @return The <code>long_set</code> value
     */
    public java.util.Set<Long> getLongSet() {
        return mLongSet;
    }

    /**
     * @return Optional of the <code>long_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Long>> optionalLongSet() {
        return java.util.Optional.ofNullable(mLongSet);
    }

    public int numDoubleSet() {
        return mDoubleSet != null ? mDoubleSet.size() : 0;
    }

    public boolean hasDoubleSet() {
        return mDoubleSet != null;
    }

    /**
     * @return The <code>double_set</code> value
     */
    public java.util.Set<Double> getDoubleSet() {
        return mDoubleSet;
    }

    /**
     * @return Optional of the <code>double_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<Double>> optionalDoubleSet() {
        return java.util.Optional.ofNullable(mDoubleSet);
    }

    public int numStringSet() {
        return mStringSet != null ? mStringSet.size() : 0;
    }

    public boolean hasStringSet() {
        return mStringSet != null;
    }

    /**
     * @return The <code>string_set</code> value
     */
    public java.util.Set<String> getStringSet() {
        return mStringSet;
    }

    /**
     * @return Optional of the <code>string_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<String>> optionalStringSet() {
        return java.util.Optional.ofNullable(mStringSet);
    }

    public int numBinarySet() {
        return mBinarySet != null ? mBinarySet.size() : 0;
    }

    public boolean hasBinarySet() {
        return mBinarySet != null;
    }

    /**
     * @return The <code>binary_set</code> value
     */
    public java.util.Set<net.morimekta.util.Binary> getBinarySet() {
        return mBinarySet;
    }

    /**
     * @return Optional of the <code>binary_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<net.morimekta.util.Binary>> optionalBinarySet() {
        return java.util.Optional.ofNullable(mBinarySet);
    }

    public int numBooleanMap() {
        return mBooleanMap != null ? mBooleanMap.size() : 0;
    }

    public boolean hasBooleanMap() {
        return mBooleanMap != null;
    }

    /**
     * @return The <code>boolean_map</code> value
     */
    public java.util.Map<Boolean,Boolean> getBooleanMap() {
        return mBooleanMap;
    }

    /**
     * @return Optional of the <code>boolean_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Boolean,Boolean>> optionalBooleanMap() {
        return java.util.Optional.ofNullable(mBooleanMap);
    }

    public int numByteMap() {
        return mByteMap != null ? mByteMap.size() : 0;
    }

    public boolean hasByteMap() {
        return mByteMap != null;
    }

    /**
     * @return The <code>byte_map</code> value
     */
    public java.util.Map<Byte,Byte> getByteMap() {
        return mByteMap;
    }

    /**
     * @return Optional of the <code>byte_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Byte,Byte>> optionalByteMap() {
        return java.util.Optional.ofNullable(mByteMap);
    }

    public int numShortMap() {
        return mShortMap != null ? mShortMap.size() : 0;
    }

    public boolean hasShortMap() {
        return mShortMap != null;
    }

    /**
     * @return The <code>short_map</code> value
     */
    public java.util.Map<Short,Short> getShortMap() {
        return mShortMap;
    }

    /**
     * @return Optional of the <code>short_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Short,Short>> optionalShortMap() {
        return java.util.Optional.ofNullable(mShortMap);
    }

    public int numIntegerMap() {
        return mIntegerMap != null ? mIntegerMap.size() : 0;
    }

    public boolean hasIntegerMap() {
        return mIntegerMap != null;
    }

    /**
     * @return The <code>integer_map</code> value
     */
    public java.util.Map<Integer,Integer> getIntegerMap() {
        return mIntegerMap;
    }

    /**
     * @return Optional of the <code>integer_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Integer,Integer>> optionalIntegerMap() {
        return java.util.Optional.ofNullable(mIntegerMap);
    }

    public int numLongMap() {
        return mLongMap != null ? mLongMap.size() : 0;
    }

    public boolean hasLongMap() {
        return mLongMap != null;
    }

    /**
     * @return The <code>long_map</code> value
     */
    public java.util.Map<Long,Long> getLongMap() {
        return mLongMap;
    }

    /**
     * @return Optional of the <code>long_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Long,Long>> optionalLongMap() {
        return java.util.Optional.ofNullable(mLongMap);
    }

    public int numDoubleMap() {
        return mDoubleMap != null ? mDoubleMap.size() : 0;
    }

    public boolean hasDoubleMap() {
        return mDoubleMap != null;
    }

    /**
     * @return The <code>double_map</code> value
     */
    public java.util.Map<Double,Double> getDoubleMap() {
        return mDoubleMap;
    }

    /**
     * @return Optional of the <code>double_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Double,Double>> optionalDoubleMap() {
        return java.util.Optional.ofNullable(mDoubleMap);
    }

    public int numStringMap() {
        return mStringMap != null ? mStringMap.size() : 0;
    }

    public boolean hasStringMap() {
        return mStringMap != null;
    }

    /**
     * @return The <code>string_map</code> value
     */
    public java.util.Map<String,String> getStringMap() {
        return mStringMap;
    }

    /**
     * @return Optional of the <code>string_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<String,String>> optionalStringMap() {
        return java.util.Optional.ofNullable(mStringMap);
    }

    public int numBinaryMap() {
        return mBinaryMap != null ? mBinaryMap.size() : 0;
    }

    public boolean hasBinaryMap() {
        return mBinaryMap != null;
    }

    /**
     * @return The <code>binary_map</code> value
     */
    public java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> getBinaryMap() {
        return mBinaryMap;
    }

    /**
     * @return Optional of the <code>binary_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary>> optionalBinaryMap() {
        return java.util.Optional.ofNullable(mBinaryMap);
    }

    public int numEnumList() {
        return mEnumList != null ? mEnumList.size() : 0;
    }

    public boolean hasEnumList() {
        return mEnumList != null;
    }

    /**
     * @return The <code>enum_list</code> value
     */
    public java.util.List<net.morimekta.test.providence.core.Value> getEnumList() {
        return mEnumList;
    }

    /**
     * @return Optional of the <code>enum_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<net.morimekta.test.providence.core.Value>> optionalEnumList() {
        return java.util.Optional.ofNullable(mEnumList);
    }

    public int numEnumSet() {
        return mEnumSet != null ? mEnumSet.size() : 0;
    }

    public boolean hasEnumSet() {
        return mEnumSet != null;
    }

    /**
     * @return The <code>enum_set</code> value
     */
    public java.util.Set<net.morimekta.test.providence.core.Value> getEnumSet() {
        return mEnumSet;
    }

    /**
     * @return Optional of the <code>enum_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.Value>> optionalEnumSet() {
        return java.util.Optional.ofNullable(mEnumSet);
    }

    public int numEnumMap() {
        return mEnumMap != null ? mEnumMap.size() : 0;
    }

    public boolean hasEnumMap() {
        return mEnumMap != null;
    }

    /**
     * @return The <code>enum_map</code> value
     */
    public java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> getEnumMap() {
        return mEnumMap;
    }

    /**
     * @return Optional of the <code>enum_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value>> optionalEnumMap() {
        return java.util.Optional.ofNullable(mEnumMap);
    }

    public int numMessageList() {
        return mMessageList != null ? mMessageList.size() : 0;
    }

    public boolean hasMessageList() {
        return mMessageList != null;
    }

    /**
     * @return The <code>message_list</code> value
     */
    public java.util.List<net.morimekta.test.providence.core.OptionalFields> getMessageList() {
        return mMessageList;
    }

    /**
     * @return Optional of the <code>message_list</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<net.morimekta.test.providence.core.OptionalFields>> optionalMessageList() {
        return java.util.Optional.ofNullable(mMessageList);
    }

    public int numMessageSet() {
        return mMessageSet != null ? mMessageSet.size() : 0;
    }

    public boolean hasMessageSet() {
        return mMessageSet != null;
    }

    /**
     * @return The <code>message_set</code> value
     */
    public java.util.Set<net.morimekta.test.providence.core.OptionalFields> getMessageSet() {
        return mMessageSet;
    }

    /**
     * @return Optional of the <code>message_set</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.OptionalFields>> optionalMessageSet() {
        return java.util.Optional.ofNullable(mMessageSet);
    }

    public int numMessageMap() {
        return mMessageMap != null ? mMessageMap.size() : 0;
    }

    public boolean hasMessageMap() {
        return mMessageMap != null;
    }

    /**
     * @return The <code>message_map</code> value
     */
    public java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> getMessageMap() {
        return mMessageMap;
    }

    /**
     * @return Optional of the <code>message_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<String,net.morimekta.test.providence.core.OptionalFields>> optionalMessageMap() {
        return java.util.Optional.ofNullable(mMessageMap);
    }

    public int numMessageKeyMap() {
        return mMessageKeyMap != null ? mMessageKeyMap.size() : 0;
    }

    public boolean hasMessageKeyMap() {
        return mMessageKeyMap != null;
    }

    /**
     * @return The <code>message_key_map</code> value
     */
    public java.util.Map<net.morimekta.test.providence.core.CompactFields,String> getMessageKeyMap() {
        return mMessageKeyMap;
    }

    /**
     * @return Optional of the <code>message_key_map</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<net.morimekta.test.providence.core.CompactFields,String>> optionalMessageKeyMap() {
        return java.util.Optional.ofNullable(mMessageKeyMap);
    }

    public boolean hasRequiredFields() {
        return mRequiredFields != null;
    }

    /**
     * @return The <code>required_fields</code> value
     */
    public net.morimekta.test.providence.core.RequiredFields getRequiredFields() {
        return mRequiredFields;
    }

    /**
     * @return Optional of the <code>required_fields</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.RequiredFields> optionalRequiredFields() {
        return java.util.Optional.ofNullable(mRequiredFields);
    }

    public boolean hasDefaultFields() {
        return mDefaultFields != null;
    }

    /**
     * @return The <code>default_fields</code> value
     */
    public net.morimekta.test.providence.core.DefaultFields getDefaultFields() {
        return mDefaultFields;
    }

    /**
     * @return Optional of the <code>default_fields</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.DefaultFields> optionalDefaultFields() {
        return java.util.Optional.ofNullable(mDefaultFields);
    }

    public boolean hasOptionalFields() {
        return mOptionalFields != null;
    }

    /**
     * @return The <code>optional_fields</code> value
     */
    public net.morimekta.test.providence.core.OptionalFields getOptionalFields() {
        return mOptionalFields;
    }

    /**
     * @return Optional of the <code>optional_fields</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOptionalFields() {
        return java.util.Optional.ofNullable(mOptionalFields);
    }

    public boolean hasUnionFields() {
        return mUnionFields != null;
    }

    /**
     * @return The <code>union_fields</code> value
     */
    public net.morimekta.test.providence.core.UnionFields getUnionFields() {
        return mUnionFields;
    }

    /**
     * @return Optional of the <code>union_fields</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.UnionFields> optionalUnionFields() {
        return java.util.Optional.ofNullable(mUnionFields);
    }

    public boolean hasExceptionFields() {
        return mExceptionFields != null;
    }

    /**
     * @return The <code>exception_fields</code> value
     */
    public net.morimekta.test.providence.core.ExceptionFields getExceptionFields() {
        return mExceptionFields;
    }

    /**
     * @return Optional of the <code>exception_fields</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.ExceptionFields> optionalExceptionFields() {
        return java.util.Optional.ofNullable(mExceptionFields);
    }

    public boolean hasDefaultValues() {
        return mDefaultValues != null;
    }

    /**
     * @return The <code>default_values</code> value
     */
    public net.morimekta.test.providence.core.DefaultValues getDefaultValues() {
        return mDefaultValues;
    }

    /**
     * @return Optional of the <code>default_values</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.DefaultValues> optionalDefaultValues() {
        return java.util.Optional.ofNullable(mDefaultValues);
    }

    public int numMapListCompact() {
        return mMapListCompact != null ? mMapListCompact.size() : 0;
    }

    public boolean hasMapListCompact() {
        return mMapListCompact != null;
    }

    /**
     * @return The <code>map_list_compact</code> value
     */
    public java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> getMapListCompact() {
        return mMapListCompact;
    }

    /**
     * @return Optional of the <code>map_list_compact</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>>> optionalMapListCompact() {
        return java.util.Optional.ofNullable(mMapListCompact);
    }

    public int numListMapNumbers() {
        return mListMapNumbers != null ? mListMapNumbers.size() : 0;
    }

    public boolean hasListMapNumbers() {
        return mListMapNumbers != null;
    }

    /**
     * @return The <code>list_map_numbers</code> value
     */
    public java.util.List<java.util.Map<Integer,Integer>> getListMapNumbers() {
        return mListMapNumbers;
    }

    /**
     * @return Optional of the <code>list_map_numbers</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<java.util.Map<Integer,Integer>>> optionalListMapNumbers() {
        return java.util.Optional.ofNullable(mListMapNumbers);
    }

    public int numSetListNumbers() {
        return mSetListNumbers != null ? mSetListNumbers.size() : 0;
    }

    public boolean hasSetListNumbers() {
        return mSetListNumbers != null;
    }

    /**
     * @return The <code>set_list_numbers</code> value
     */
    public java.util.Set<java.util.List<Integer>> getSetListNumbers() {
        return mSetListNumbers;
    }

    /**
     * @return Optional of the <code>set_list_numbers</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.Set<java.util.List<Integer>>> optionalSetListNumbers() {
        return java.util.Optional.ofNullable(mSetListNumbers);
    }

    public int numListListNumbers() {
        return mListListNumbers != null ? mListListNumbers.size() : 0;
    }

    public boolean hasListListNumbers() {
        return mListListNumbers != null;
    }

    /**
     * @return The <code>list_list_numbers</code> value
     */
    public java.util.List<java.util.List<Integer>> getListListNumbers() {
        return mListListNumbers;
    }

    /**
     * @return Optional of the <code>list_list_numbers</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<java.util.List<java.util.List<Integer>>> optionalListListNumbers() {
        return java.util.Optional.ofNullable(mListListNumbers);
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            case 1: return mBooleanList != null;
            case 2: return mByteList != null;
            case 3: return mShortList != null;
            case 4: return mIntegerList != null;
            case 5: return mLongList != null;
            case 6: return mDoubleList != null;
            case 7: return mStringList != null;
            case 8: return mBinaryList != null;
            case 11: return mBooleanSet != null;
            case 12: return mByteSet != null;
            case 13: return mShortSet != null;
            case 14: return mIntegerSet != null;
            case 15: return mLongSet != null;
            case 16: return mDoubleSet != null;
            case 17: return mStringSet != null;
            case 18: return mBinarySet != null;
            case 21: return mBooleanMap != null;
            case 22: return mByteMap != null;
            case 23: return mShortMap != null;
            case 24: return mIntegerMap != null;
            case 25: return mLongMap != null;
            case 26: return mDoubleMap != null;
            case 27: return mStringMap != null;
            case 28: return mBinaryMap != null;
            case 31: return mEnumList != null;
            case 32: return mEnumSet != null;
            case 33: return mEnumMap != null;
            case 41: return mMessageList != null;
            case 42: return mMessageSet != null;
            case 43: return mMessageMap != null;
            case 44: return mMessageKeyMap != null;
            case 51: return mRequiredFields != null;
            case 52: return mDefaultFields != null;
            case 53: return mOptionalFields != null;
            case 54: return mUnionFields != null;
            case 55: return mExceptionFields != null;
            case 56: return mDefaultValues != null;
            case 61: return mMapListCompact != null;
            case 62: return mListMapNumbers != null;
            case 63: return mSetListNumbers != null;
            case 64: return mListListNumbers != null;
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            case 1: return (T) mBooleanList;
            case 2: return (T) mByteList;
            case 3: return (T) mShortList;
            case 4: return (T) mIntegerList;
            case 5: return (T) mLongList;
            case 6: return (T) mDoubleList;
            case 7: return (T) mStringList;
            case 8: return (T) mBinaryList;
            case 11: return (T) mBooleanSet;
            case 12: return (T) mByteSet;
            case 13: return (T) mShortSet;
            case 14: return (T) mIntegerSet;
            case 15: return (T) mLongSet;
            case 16: return (T) mDoubleSet;
            case 17: return (T) mStringSet;
            case 18: return (T) mBinarySet;
            case 21: return (T) mBooleanMap;
            case 22: return (T) mByteMap;
            case 23: return (T) mShortMap;
            case 24: return (T) mIntegerMap;
            case 25: return (T) mLongMap;
            case 26: return (T) mDoubleMap;
            case 27: return (T) mStringMap;
            case 28: return (T) mBinaryMap;
            case 31: return (T) mEnumList;
            case 32: return (T) mEnumSet;
            case 33: return (T) mEnumMap;
            case 41: return (T) mMessageList;
            case 42: return (T) mMessageSet;
            case 43: return (T) mMessageMap;
            case 44: return (T) mMessageKeyMap;
            case 51: return (T) mRequiredFields;
            case 52: return (T) mDefaultFields;
            case 53: return (T) mOptionalFields;
            case 54: return (T) mUnionFields;
            case 55: return (T) mExceptionFields;
            case 56: return (T) mDefaultValues;
            case 61: return (T) mMapListCompact;
            case 62: return (T) mListMapNumbers;
            case 63: return (T) mSetListNumbers;
            case 64: return (T) mListListNumbers;
            default: return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        Containers other = (Containers) o;
        return java.util.Objects.equals(mBooleanList, other.mBooleanList) &&
               java.util.Objects.equals(mByteList, other.mByteList) &&
               java.util.Objects.equals(mShortList, other.mShortList) &&
               java.util.Objects.equals(mIntegerList, other.mIntegerList) &&
               java.util.Objects.equals(mLongList, other.mLongList) &&
               java.util.Objects.equals(mDoubleList, other.mDoubleList) &&
               java.util.Objects.equals(mStringList, other.mStringList) &&
               java.util.Objects.equals(mBinaryList, other.mBinaryList) &&
               java.util.Objects.equals(mBooleanSet, other.mBooleanSet) &&
               java.util.Objects.equals(mByteSet, other.mByteSet) &&
               java.util.Objects.equals(mShortSet, other.mShortSet) &&
               java.util.Objects.equals(mIntegerSet, other.mIntegerSet) &&
               java.util.Objects.equals(mLongSet, other.mLongSet) &&
               java.util.Objects.equals(mDoubleSet, other.mDoubleSet) &&
               java.util.Objects.equals(mStringSet, other.mStringSet) &&
               java.util.Objects.equals(mBinarySet, other.mBinarySet) &&
               java.util.Objects.equals(mBooleanMap, other.mBooleanMap) &&
               java.util.Objects.equals(mByteMap, other.mByteMap) &&
               java.util.Objects.equals(mShortMap, other.mShortMap) &&
               java.util.Objects.equals(mIntegerMap, other.mIntegerMap) &&
               java.util.Objects.equals(mLongMap, other.mLongMap) &&
               java.util.Objects.equals(mDoubleMap, other.mDoubleMap) &&
               java.util.Objects.equals(mStringMap, other.mStringMap) &&
               java.util.Objects.equals(mBinaryMap, other.mBinaryMap) &&
               java.util.Objects.equals(mEnumList, other.mEnumList) &&
               java.util.Objects.equals(mEnumSet, other.mEnumSet) &&
               java.util.Objects.equals(mEnumMap, other.mEnumMap) &&
               java.util.Objects.equals(mMessageList, other.mMessageList) &&
               java.util.Objects.equals(mMessageSet, other.mMessageSet) &&
               java.util.Objects.equals(mMessageMap, other.mMessageMap) &&
               java.util.Objects.equals(mMessageKeyMap, other.mMessageKeyMap) &&
               java.util.Objects.equals(mRequiredFields, other.mRequiredFields) &&
               java.util.Objects.equals(mDefaultFields, other.mDefaultFields) &&
               java.util.Objects.equals(mOptionalFields, other.mOptionalFields) &&
               java.util.Objects.equals(mUnionFields, other.mUnionFields) &&
               java.util.Objects.equals(mExceptionFields, other.mExceptionFields) &&
               java.util.Objects.equals(mDefaultValues, other.mDefaultValues) &&
               java.util.Objects.equals(mMapListCompact, other.mMapListCompact) &&
               java.util.Objects.equals(mListMapNumbers, other.mListMapNumbers) &&
               java.util.Objects.equals(mSetListNumbers, other.mSetListNumbers) &&
               java.util.Objects.equals(mListListNumbers, other.mListListNumbers);
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    Containers.class,
                    _Field.BOOLEAN_LIST, mBooleanList,
                    _Field.BYTE_LIST, mByteList,
                    _Field.SHORT_LIST, mShortList,
                    _Field.INTEGER_LIST, mIntegerList,
                    _Field.LONG_LIST, mLongList,
                    _Field.DOUBLE_LIST, mDoubleList,
                    _Field.STRING_LIST, mStringList,
                    _Field.BINARY_LIST, mBinaryList,
                    _Field.BOOLEAN_SET, mBooleanSet,
                    _Field.BYTE_SET, mByteSet,
                    _Field.SHORT_SET, mShortSet,
                    _Field.INTEGER_SET, mIntegerSet,
                    _Field.LONG_SET, mLongSet,
                    _Field.DOUBLE_SET, mDoubleSet,
                    _Field.STRING_SET, mStringSet,
                    _Field.BINARY_SET, mBinarySet,
                    _Field.BOOLEAN_MAP, mBooleanMap,
                    _Field.BYTE_MAP, mByteMap,
                    _Field.SHORT_MAP, mShortMap,
                    _Field.INTEGER_MAP, mIntegerMap,
                    _Field.LONG_MAP, mLongMap,
                    _Field.DOUBLE_MAP, mDoubleMap,
                    _Field.STRING_MAP, mStringMap,
                    _Field.BINARY_MAP, mBinaryMap,
                    _Field.ENUM_LIST, mEnumList,
                    _Field.ENUM_SET, mEnumSet,
                    _Field.ENUM_MAP, mEnumMap,
                    _Field.MESSAGE_LIST, mMessageList,
                    _Field.MESSAGE_SET, mMessageSet,
                    _Field.MESSAGE_MAP, mMessageMap,
                    _Field.MESSAGE_KEY_MAP, mMessageKeyMap,
                    _Field.REQUIRED_FIELDS, mRequiredFields,
                    _Field.DEFAULT_FIELDS, mDefaultFields,
                    _Field.OPTIONAL_FIELDS, mOptionalFields,
                    _Field.UNION_FIELDS, mUnionFields,
                    _Field.EXCEPTION_FIELDS, mExceptionFields,
                    _Field.DEFAULT_VALUES, mDefaultValues,
                    _Field.MAP_LIST_COMPACT, mMapListCompact,
                    _Field.LIST_MAP_NUMBERS, mListMapNumbers,
                    _Field.SET_LIST_NUMBERS, mSetListNumbers,
                    _Field.LIST_LIST_NUMBERS, mListListNumbers);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "providence.Containers" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        boolean first = true;
        if (hasBooleanList()) {
            first = false;
            out.append("boolean_list:")
               .append(net.morimekta.util.Strings.asString(mBooleanList));
        }
        if (hasByteList()) {
            if (first) first = false;
            else out.append(',');
            out.append("byte_list:")
               .append(net.morimekta.util.Strings.asString(mByteList));
        }
        if (hasShortList()) {
            if (first) first = false;
            else out.append(',');
            out.append("short_list:")
               .append(net.morimekta.util.Strings.asString(mShortList));
        }
        if (hasIntegerList()) {
            if (first) first = false;
            else out.append(',');
            out.append("integer_list:")
               .append(net.morimekta.util.Strings.asString(mIntegerList));
        }
        if (hasLongList()) {
            if (first) first = false;
            else out.append(',');
            out.append("long_list:")
               .append(net.morimekta.util.Strings.asString(mLongList));
        }
        if (hasDoubleList()) {
            if (first) first = false;
            else out.append(',');
            out.append("double_list:")
               .append(net.morimekta.util.Strings.asString(mDoubleList));
        }
        if (hasStringList()) {
            if (first) first = false;
            else out.append(',');
            out.append("string_list:")
               .append(net.morimekta.util.Strings.asString(mStringList));
        }
        if (hasBinaryList()) {
            if (first) first = false;
            else out.append(',');
            out.append("binary_list:")
               .append(net.morimekta.util.Strings.asString(mBinaryList));
        }
        if (hasBooleanSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("boolean_set:")
               .append(net.morimekta.util.Strings.asString(mBooleanSet));
        }
        if (hasByteSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("byte_set:")
               .append(net.morimekta.util.Strings.asString(mByteSet));
        }
        if (hasShortSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("short_set:")
               .append(net.morimekta.util.Strings.asString(mShortSet));
        }
        if (hasIntegerSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("integer_set:")
               .append(net.morimekta.util.Strings.asString(mIntegerSet));
        }
        if (hasLongSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("long_set:")
               .append(net.morimekta.util.Strings.asString(mLongSet));
        }
        if (hasDoubleSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("double_set:")
               .append(net.morimekta.util.Strings.asString(mDoubleSet));
        }
        if (hasStringSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("string_set:")
               .append(net.morimekta.util.Strings.asString(mStringSet));
        }
        if (hasBinarySet()) {
            if (first) first = false;
            else out.append(',');
            out.append("binary_set:")
               .append(net.morimekta.util.Strings.asString(mBinarySet));
        }
        if (hasBooleanMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("boolean_map:")
               .append(net.morimekta.util.Strings.asString(mBooleanMap));
        }
        if (hasByteMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("byte_map:")
               .append(net.morimekta.util.Strings.asString(mByteMap));
        }
        if (hasShortMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("short_map:")
               .append(net.morimekta.util.Strings.asString(mShortMap));
        }
        if (hasIntegerMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("integer_map:")
               .append(net.morimekta.util.Strings.asString(mIntegerMap));
        }
        if (hasLongMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("long_map:")
               .append(net.morimekta.util.Strings.asString(mLongMap));
        }
        if (hasDoubleMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("double_map:")
               .append(net.morimekta.util.Strings.asString(mDoubleMap));
        }
        if (hasStringMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("string_map:")
               .append(net.morimekta.util.Strings.asString(mStringMap));
        }
        if (hasBinaryMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("binary_map:")
               .append(net.morimekta.util.Strings.asString(mBinaryMap));
        }
        if (hasEnumList()) {
            if (first) first = false;
            else out.append(',');
            out.append("enum_list:")
               .append(net.morimekta.util.Strings.asString(mEnumList));
        }
        if (hasEnumSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("enum_set:")
               .append(net.morimekta.util.Strings.asString(mEnumSet));
        }
        if (hasEnumMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("enum_map:")
               .append(net.morimekta.util.Strings.asString(mEnumMap));
        }
        if (hasMessageList()) {
            if (first) first = false;
            else out.append(',');
            out.append("message_list:")
               .append(net.morimekta.util.Strings.asString(mMessageList));
        }
        if (hasMessageSet()) {
            if (first) first = false;
            else out.append(',');
            out.append("message_set:")
               .append(net.morimekta.util.Strings.asString(mMessageSet));
        }
        if (hasMessageMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("message_map:")
               .append(net.morimekta.util.Strings.asString(mMessageMap));
        }
        if (hasMessageKeyMap()) {
            if (first) first = false;
            else out.append(',');
            out.append("message_key_map:")
               .append(net.morimekta.util.Strings.asString(mMessageKeyMap));
        }
        if (hasRequiredFields()) {
            if (first) first = false;
            else out.append(',');
            out.append("required_fields:")
               .append(mRequiredFields.asString());
        }
        if (hasDefaultFields()) {
            if (first) first = false;
            else out.append(',');
            out.append("default_fields:")
               .append(mDefaultFields.asString());
        }
        if (hasOptionalFields()) {
            if (first) first = false;
            else out.append(',');
            out.append("optional_fields:")
               .append(mOptionalFields.asString());
        }
        if (hasUnionFields()) {
            if (first) first = false;
            else out.append(',');
            out.append("union_fields:")
               .append(mUnionFields.asString());
        }
        if (hasExceptionFields()) {
            if (first) first = false;
            else out.append(',');
            out.append("exception_fields:")
               .append(mExceptionFields.asString());
        }
        if (hasDefaultValues()) {
            if (first) first = false;
            else out.append(',');
            out.append("default_values:")
               .append(mDefaultValues.asString());
        }
        if (hasMapListCompact()) {
            if (first) first = false;
            else out.append(',');
            out.append("map_list_compact:")
               .append(net.morimekta.util.Strings.asString(mMapListCompact));
        }
        if (hasListMapNumbers()) {
            if (first) first = false;
            else out.append(',');
            out.append("list_map_numbers:")
               .append(net.morimekta.util.Strings.asString(mListMapNumbers));
        }
        if (hasSetListNumbers()) {
            if (first) first = false;
            else out.append(',');
            out.append("set_list_numbers:")
               .append(net.morimekta.util.Strings.asString(mSetListNumbers));
        }
        if (hasListListNumbers()) {
            if (!first) out.append(',');
            out.append("list_list_numbers:")
               .append(net.morimekta.util.Strings.asString(mListListNumbers));
        }
        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(Containers other) {
        int c;

        c = Boolean.compare(mBooleanList != null, other.mBooleanList != null);
        if (c != 0) return c;
        if (mBooleanList != null) {
            c = Integer.compare(mBooleanList.hashCode(), other.mBooleanList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mByteList != null, other.mByteList != null);
        if (c != 0) return c;
        if (mByteList != null) {
            c = Integer.compare(mByteList.hashCode(), other.mByteList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mShortList != null, other.mShortList != null);
        if (c != 0) return c;
        if (mShortList != null) {
            c = Integer.compare(mShortList.hashCode(), other.mShortList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mIntegerList != null, other.mIntegerList != null);
        if (c != 0) return c;
        if (mIntegerList != null) {
            c = Integer.compare(mIntegerList.hashCode(), other.mIntegerList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mLongList != null, other.mLongList != null);
        if (c != 0) return c;
        if (mLongList != null) {
            c = Integer.compare(mLongList.hashCode(), other.mLongList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mDoubleList != null, other.mDoubleList != null);
        if (c != 0) return c;
        if (mDoubleList != null) {
            c = Integer.compare(mDoubleList.hashCode(), other.mDoubleList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mStringList != null, other.mStringList != null);
        if (c != 0) return c;
        if (mStringList != null) {
            c = Integer.compare(mStringList.hashCode(), other.mStringList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mBinaryList != null, other.mBinaryList != null);
        if (c != 0) return c;
        if (mBinaryList != null) {
            c = Integer.compare(mBinaryList.hashCode(), other.mBinaryList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mBooleanSet != null, other.mBooleanSet != null);
        if (c != 0) return c;
        if (mBooleanSet != null) {
            c = Integer.compare(mBooleanSet.hashCode(), other.mBooleanSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mByteSet != null, other.mByteSet != null);
        if (c != 0) return c;
        if (mByteSet != null) {
            c = Integer.compare(mByteSet.hashCode(), other.mByteSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mShortSet != null, other.mShortSet != null);
        if (c != 0) return c;
        if (mShortSet != null) {
            c = Integer.compare(mShortSet.hashCode(), other.mShortSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mIntegerSet != null, other.mIntegerSet != null);
        if (c != 0) return c;
        if (mIntegerSet != null) {
            c = Integer.compare(mIntegerSet.hashCode(), other.mIntegerSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mLongSet != null, other.mLongSet != null);
        if (c != 0) return c;
        if (mLongSet != null) {
            c = Integer.compare(mLongSet.hashCode(), other.mLongSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mDoubleSet != null, other.mDoubleSet != null);
        if (c != 0) return c;
        if (mDoubleSet != null) {
            c = Integer.compare(mDoubleSet.hashCode(), other.mDoubleSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mStringSet != null, other.mStringSet != null);
        if (c != 0) return c;
        if (mStringSet != null) {
            c = Integer.compare(mStringSet.hashCode(), other.mStringSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mBinarySet != null, other.mBinarySet != null);
        if (c != 0) return c;
        if (mBinarySet != null) {
            c = Integer.compare(mBinarySet.hashCode(), other.mBinarySet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mBooleanMap != null, other.mBooleanMap != null);
        if (c != 0) return c;
        if (mBooleanMap != null) {
            c = Integer.compare(mBooleanMap.hashCode(), other.mBooleanMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mByteMap != null, other.mByteMap != null);
        if (c != 0) return c;
        if (mByteMap != null) {
            c = Integer.compare(mByteMap.hashCode(), other.mByteMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mShortMap != null, other.mShortMap != null);
        if (c != 0) return c;
        if (mShortMap != null) {
            c = Integer.compare(mShortMap.hashCode(), other.mShortMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mIntegerMap != null, other.mIntegerMap != null);
        if (c != 0) return c;
        if (mIntegerMap != null) {
            c = Integer.compare(mIntegerMap.hashCode(), other.mIntegerMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mLongMap != null, other.mLongMap != null);
        if (c != 0) return c;
        if (mLongMap != null) {
            c = Integer.compare(mLongMap.hashCode(), other.mLongMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mDoubleMap != null, other.mDoubleMap != null);
        if (c != 0) return c;
        if (mDoubleMap != null) {
            c = Integer.compare(mDoubleMap.hashCode(), other.mDoubleMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mStringMap != null, other.mStringMap != null);
        if (c != 0) return c;
        if (mStringMap != null) {
            c = Integer.compare(mStringMap.hashCode(), other.mStringMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mBinaryMap != null, other.mBinaryMap != null);
        if (c != 0) return c;
        if (mBinaryMap != null) {
            c = Integer.compare(mBinaryMap.hashCode(), other.mBinaryMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mEnumList != null, other.mEnumList != null);
        if (c != 0) return c;
        if (mEnumList != null) {
            c = Integer.compare(mEnumList.hashCode(), other.mEnumList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mEnumSet != null, other.mEnumSet != null);
        if (c != 0) return c;
        if (mEnumSet != null) {
            c = Integer.compare(mEnumSet.hashCode(), other.mEnumSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mEnumMap != null, other.mEnumMap != null);
        if (c != 0) return c;
        if (mEnumMap != null) {
            c = Integer.compare(mEnumMap.hashCode(), other.mEnumMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mMessageList != null, other.mMessageList != null);
        if (c != 0) return c;
        if (mMessageList != null) {
            c = Integer.compare(mMessageList.hashCode(), other.mMessageList.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mMessageSet != null, other.mMessageSet != null);
        if (c != 0) return c;
        if (mMessageSet != null) {
            c = Integer.compare(mMessageSet.hashCode(), other.mMessageSet.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mMessageMap != null, other.mMessageMap != null);
        if (c != 0) return c;
        if (mMessageMap != null) {
            c = Integer.compare(mMessageMap.hashCode(), other.mMessageMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mMessageKeyMap != null, other.mMessageKeyMap != null);
        if (c != 0) return c;
        if (mMessageKeyMap != null) {
            c = Integer.compare(mMessageKeyMap.hashCode(), other.mMessageKeyMap.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mRequiredFields != null, other.mRequiredFields != null);
        if (c != 0) return c;
        if (mRequiredFields != null) {
            c = mRequiredFields.compareTo(other.mRequiredFields);
            if (c != 0) return c;
        }

        c = Boolean.compare(mDefaultFields != null, other.mDefaultFields != null);
        if (c != 0) return c;
        if (mDefaultFields != null) {
            c = mDefaultFields.compareTo(other.mDefaultFields);
            if (c != 0) return c;
        }

        c = Boolean.compare(mOptionalFields != null, other.mOptionalFields != null);
        if (c != 0) return c;
        if (mOptionalFields != null) {
            c = mOptionalFields.compareTo(other.mOptionalFields);
            if (c != 0) return c;
        }

        c = Boolean.compare(mUnionFields != null, other.mUnionFields != null);
        if (c != 0) return c;
        if (mUnionFields != null) {
            c = mUnionFields.compareTo(other.mUnionFields);
            if (c != 0) return c;
        }

        c = Boolean.compare(mExceptionFields != null, other.mExceptionFields != null);
        if (c != 0) return c;
        if (mExceptionFields != null) {
            c = mExceptionFields.compareTo(other.mExceptionFields);
            if (c != 0) return c;
        }

        c = Boolean.compare(mDefaultValues != null, other.mDefaultValues != null);
        if (c != 0) return c;
        if (mDefaultValues != null) {
            c = mDefaultValues.compareTo(other.mDefaultValues);
            if (c != 0) return c;
        }

        c = Boolean.compare(mMapListCompact != null, other.mMapListCompact != null);
        if (c != 0) return c;
        if (mMapListCompact != null) {
            c = Integer.compare(mMapListCompact.hashCode(), other.mMapListCompact.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mListMapNumbers != null, other.mListMapNumbers != null);
        if (c != 0) return c;
        if (mListMapNumbers != null) {
            c = Integer.compare(mListMapNumbers.hashCode(), other.mListMapNumbers.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mSetListNumbers != null, other.mSetListNumbers != null);
        if (c != 0) return c;
        if (mSetListNumbers != null) {
            c = Integer.compare(mSetListNumbers.hashCode(), other.mSetListNumbers.hashCode());
            if (c != 0) return c;
        }

        c = Boolean.compare(mListListNumbers != null, other.mListListNumbers != null);
        if (c != 0) return c;
        if (mListListNumbers != null) {
            c = Integer.compare(mListListNumbers.hashCode(), other.mListListNumbers.hashCode());
            if (c != 0) return c;
        }

        return 0;
    }

    private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
        oos.defaultWriteObject();
        net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
    }

    private Object readResolve() throws java.io.ObjectStreamException {
        return tSerializeInstance;
    }

    @Override
    public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
        int length = 0;

        if (hasBooleanList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 1);
            length += writer.writeByte((byte) 2);
            length += writer.writeUInt32(mBooleanList.size());
            for (boolean entry_1 : mBooleanList) {
                length += writer.writeUInt8(entry_1 ? (byte) 1 : (byte) 0);
            }
        }

        if (hasByteList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 2);
            length += writer.writeByte((byte) 3);
            length += writer.writeUInt32(mByteList.size());
            for (byte entry_2 : mByteList) {
                length += writer.writeByte(entry_2);
            }
        }

        if (hasShortList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 3);
            length += writer.writeByte((byte) 6);
            length += writer.writeUInt32(mShortList.size());
            for (short entry_3 : mShortList) {
                length += writer.writeShort(entry_3);
            }
        }

        if (hasIntegerList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 4);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mIntegerList.size());
            for (int entry_4 : mIntegerList) {
                length += writer.writeInt(entry_4);
            }
        }

        if (hasLongList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 5);
            length += writer.writeByte((byte) 10);
            length += writer.writeUInt32(mLongList.size());
            for (long entry_5 : mLongList) {
                length += writer.writeLong(entry_5);
            }
        }

        if (hasDoubleList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 6);
            length += writer.writeByte((byte) 4);
            length += writer.writeUInt32(mDoubleList.size());
            for (double entry_6 : mDoubleList) {
                length += writer.writeDouble(entry_6);
            }
        }

        if (hasStringList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 7);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mStringList.size());
            for (String entry_7 : mStringList) {
                net.morimekta.util.Binary tmp_8 = net.morimekta.util.Binary.wrap(entry_7.getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_8.length());
                length += writer.writeBinary(tmp_8);
            }
        }

        if (hasBinaryList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 8);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mBinaryList.size());
            for (net.morimekta.util.Binary entry_9 : mBinaryList) {
                length += writer.writeUInt32(entry_9.length());
                length += writer.writeBinary(entry_9);
            }
        }

        if (hasBooleanSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 11);
            length += writer.writeByte((byte) 2);
            length += writer.writeUInt32(mBooleanSet.size());
            for (boolean entry_10 : mBooleanSet) {
                length += writer.writeUInt8(entry_10 ? (byte) 1 : (byte) 0);
            }
        }

        if (hasByteSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 12);
            length += writer.writeByte((byte) 3);
            length += writer.writeUInt32(mByteSet.size());
            for (byte entry_11 : mByteSet) {
                length += writer.writeByte(entry_11);
            }
        }

        if (hasShortSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 13);
            length += writer.writeByte((byte) 6);
            length += writer.writeUInt32(mShortSet.size());
            for (short entry_12 : mShortSet) {
                length += writer.writeShort(entry_12);
            }
        }

        if (hasIntegerSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 14);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mIntegerSet.size());
            for (int entry_13 : mIntegerSet) {
                length += writer.writeInt(entry_13);
            }
        }

        if (hasLongSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 15);
            length += writer.writeByte((byte) 10);
            length += writer.writeUInt32(mLongSet.size());
            for (long entry_14 : mLongSet) {
                length += writer.writeLong(entry_14);
            }
        }

        if (hasDoubleSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 16);
            length += writer.writeByte((byte) 4);
            length += writer.writeUInt32(mDoubleSet.size());
            for (double entry_15 : mDoubleSet) {
                length += writer.writeDouble(entry_15);
            }
        }

        if (hasStringSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 17);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mStringSet.size());
            for (String entry_16 : mStringSet) {
                net.morimekta.util.Binary tmp_17 = net.morimekta.util.Binary.wrap(entry_16.getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_17.length());
                length += writer.writeBinary(tmp_17);
            }
        }

        if (hasBinarySet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 18);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mBinarySet.size());
            for (net.morimekta.util.Binary entry_18 : mBinarySet) {
                length += writer.writeUInt32(entry_18.length());
                length += writer.writeBinary(entry_18);
            }
        }

        if (hasBooleanMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 21);
            length += writer.writeByte((byte) 2);
            length += writer.writeByte((byte) 2);
            length += writer.writeUInt32(mBooleanMap.size());
            for (java.util.Map.Entry<Boolean,Boolean> entry_19 : mBooleanMap.entrySet()) {
                length += writer.writeUInt8(entry_19.getKey() ? (byte) 1 : (byte) 0);
                length += writer.writeUInt8(entry_19.getValue() ? (byte) 1 : (byte) 0);
            }
        }

        if (hasByteMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 22);
            length += writer.writeByte((byte) 3);
            length += writer.writeByte((byte) 3);
            length += writer.writeUInt32(mByteMap.size());
            for (java.util.Map.Entry<Byte,Byte> entry_20 : mByteMap.entrySet()) {
                length += writer.writeByte(entry_20.getKey());
                length += writer.writeByte(entry_20.getValue());
            }
        }

        if (hasShortMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 23);
            length += writer.writeByte((byte) 6);
            length += writer.writeByte((byte) 6);
            length += writer.writeUInt32(mShortMap.size());
            for (java.util.Map.Entry<Short,Short> entry_21 : mShortMap.entrySet()) {
                length += writer.writeShort(entry_21.getKey());
                length += writer.writeShort(entry_21.getValue());
            }
        }

        if (hasIntegerMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 24);
            length += writer.writeByte((byte) 8);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mIntegerMap.size());
            for (java.util.Map.Entry<Integer,Integer> entry_22 : mIntegerMap.entrySet()) {
                length += writer.writeInt(entry_22.getKey());
                length += writer.writeInt(entry_22.getValue());
            }
        }

        if (hasLongMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 25);
            length += writer.writeByte((byte) 10);
            length += writer.writeByte((byte) 10);
            length += writer.writeUInt32(mLongMap.size());
            for (java.util.Map.Entry<Long,Long> entry_23 : mLongMap.entrySet()) {
                length += writer.writeLong(entry_23.getKey());
                length += writer.writeLong(entry_23.getValue());
            }
        }

        if (hasDoubleMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 26);
            length += writer.writeByte((byte) 4);
            length += writer.writeByte((byte) 4);
            length += writer.writeUInt32(mDoubleMap.size());
            for (java.util.Map.Entry<Double,Double> entry_24 : mDoubleMap.entrySet()) {
                length += writer.writeDouble(entry_24.getKey());
                length += writer.writeDouble(entry_24.getValue());
            }
        }

        if (hasStringMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 27);
            length += writer.writeByte((byte) 11);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mStringMap.size());
            for (java.util.Map.Entry<String,String> entry_25 : mStringMap.entrySet()) {
                net.morimekta.util.Binary tmp_26 = net.morimekta.util.Binary.wrap(entry_25.getKey().getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_26.length());
                length += writer.writeBinary(tmp_26);
                net.morimekta.util.Binary tmp_27 = net.morimekta.util.Binary.wrap(entry_25.getValue().getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_27.length());
                length += writer.writeBinary(tmp_27);
            }
        }

        if (hasBinaryMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 28);
            length += writer.writeByte((byte) 11);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mBinaryMap.size());
            for (java.util.Map.Entry<net.morimekta.util.Binary,net.morimekta.util.Binary> entry_28 : mBinaryMap.entrySet()) {
                length += writer.writeUInt32(entry_28.getKey().length());
                length += writer.writeBinary(entry_28.getKey());
                length += writer.writeUInt32(entry_28.getValue().length());
                length += writer.writeBinary(entry_28.getValue());
            }
        }

        if (hasEnumList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 31);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mEnumList.size());
            for (net.morimekta.test.providence.core.Value entry_29 : mEnumList) {
                length += writer.writeInt(entry_29.asInteger());
            }
        }

        if (hasEnumSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 32);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mEnumSet.size());
            for (net.morimekta.test.providence.core.Value entry_30 : mEnumSet) {
                length += writer.writeInt(entry_30.asInteger());
            }
        }

        if (hasEnumMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 33);
            length += writer.writeByte((byte) 8);
            length += writer.writeByte((byte) 8);
            length += writer.writeUInt32(mEnumMap.size());
            for (java.util.Map.Entry<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> entry_31 : mEnumMap.entrySet()) {
                length += writer.writeInt(entry_31.getKey().asInteger());
                length += writer.writeInt(entry_31.getValue().asInteger());
            }
        }

        if (hasMessageList()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 41);
            length += writer.writeByte((byte) 12);
            length += writer.writeUInt32(mMessageList.size());
            for (net.morimekta.test.providence.core.OptionalFields entry_32 : mMessageList) {
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, entry_32);
            }
        }

        if (hasMessageSet()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 42);
            length += writer.writeByte((byte) 12);
            length += writer.writeUInt32(mMessageSet.size());
            for (net.morimekta.test.providence.core.OptionalFields entry_33 : mMessageSet) {
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, entry_33);
            }
        }

        if (hasMessageMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 43);
            length += writer.writeByte((byte) 11);
            length += writer.writeByte((byte) 12);
            length += writer.writeUInt32(mMessageMap.size());
            for (java.util.Map.Entry<String,net.morimekta.test.providence.core.OptionalFields> entry_34 : mMessageMap.entrySet()) {
                net.morimekta.util.Binary tmp_35 = net.morimekta.util.Binary.wrap(entry_34.getKey().getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_35.length());
                length += writer.writeBinary(tmp_35);
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, entry_34.getValue());
            }
        }

        if (hasMessageKeyMap()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 44);
            length += writer.writeByte((byte) 12);
            length += writer.writeByte((byte) 11);
            length += writer.writeUInt32(mMessageKeyMap.size());
            for (java.util.Map.Entry<net.morimekta.test.providence.core.CompactFields,String> entry_36 : mMessageKeyMap.entrySet()) {
                length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, entry_36.getKey());
                net.morimekta.util.Binary tmp_37 = net.morimekta.util.Binary.wrap(entry_36.getValue().getBytes(java.nio.charset.StandardCharsets.UTF_8));
                length += writer.writeUInt32(tmp_37.length());
                length += writer.writeBinary(tmp_37);
            }
        }

        if (hasRequiredFields()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 51);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mRequiredFields);
        }

        if (hasDefaultFields()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 52);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mDefaultFields);
        }

        if (hasOptionalFields()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 53);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mOptionalFields);
        }

        if (hasUnionFields()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 54);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mUnionFields);
        }

        if (hasExceptionFields()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 55);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mExceptionFields);
        }

        if (hasDefaultValues()) {
            length += writer.writeByte((byte) 12);
            length += writer.writeShort((short) 56);
            length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mDefaultValues);
        }

        if (hasMapListCompact()) {
            length += writer.writeByte((byte) 13);
            length += writer.writeShort((short) 61);
            length += writer.writeByte((byte) 8);
            length += writer.writeByte((byte) 15);
            length += writer.writeUInt32(mMapListCompact.size());
            for (java.util.Map.Entry<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> entry_38 : mMapListCompact.entrySet()) {
                length += writer.writeInt(entry_38.getKey());
                length += writer.writeByte((byte) 12);
                length += writer.writeUInt32(entry_38.getValue().size());
                for (net.morimekta.test.providence.core.CompactFields entry_39 : entry_38.getValue()) {
                    length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, entry_39);
                }
            }
        }

        if (hasListMapNumbers()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 62);
            length += writer.writeByte((byte) 13);
            length += writer.writeUInt32(mListMapNumbers.size());
            for (java.util.Map<Integer,Integer> entry_40 : mListMapNumbers) {
                length += writer.writeByte((byte) 8);
                length += writer.writeByte((byte) 8);
                length += writer.writeUInt32(entry_40.size());
                for (java.util.Map.Entry<Integer,Integer> entry_41 : entry_40.entrySet()) {
                    length += writer.writeInt(entry_41.getKey());
                    length += writer.writeInt(entry_41.getValue());
                }
            }
        }

        if (hasSetListNumbers()) {
            length += writer.writeByte((byte) 14);
            length += writer.writeShort((short) 63);
            length += writer.writeByte((byte) 15);
            length += writer.writeUInt32(mSetListNumbers.size());
            for (java.util.List<Integer> entry_42 : mSetListNumbers) {
                length += writer.writeByte((byte) 8);
                length += writer.writeUInt32(entry_42.size());
                for (int entry_43 : entry_42) {
                    length += writer.writeInt(entry_43);
                }
            }
        }

        if (hasListListNumbers()) {
            length += writer.writeByte((byte) 15);
            length += writer.writeShort((short) 64);
            length += writer.writeByte((byte) 15);
            length += writer.writeUInt32(mListListNumbers.size());
            for (java.util.List<Integer> entry_44 : mListListNumbers) {
                length += writer.writeByte((byte) 8);
                length += writer.writeUInt32(entry_44.size());
                for (int entry_45 : entry_44) {
                    length += writer.writeInt(entry_45);
                }
            }
        }

        length += writer.writeByte((byte) 0);
        return length;
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<Containers> {
        BOOLEAN_LIST(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "boolean_list", "booleanList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.BOOL.provider()), null, null),
        BYTE_LIST(2, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "byte_list", "byteList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.BYTE.provider()), null, null),
        SHORT_LIST(3, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "short_list", "shortList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.I16.provider()), null, null),
        INTEGER_LIST(4, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "integer_list", "integerList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider()), null, null),
        LONG_LIST(5, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "long_list", "longList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.I64.provider()), null, null),
        DOUBLE_LIST(6, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "double_list", "doubleList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.DOUBLE.provider()), null, null),
        STRING_LIST(7, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "string_list", "stringList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.STRING.provider()), null, null),
        BINARY_LIST(8, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "binary_list", "binaryList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.BINARY.provider()), null, null),
        BOOLEAN_SET(11, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "boolean_set", "booleanSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.BOOL.provider()), null, null),
        BYTE_SET(12, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "byte_set", "byteSet", net.morimekta.providence.descriptor.PSet.sortedProvider(net.morimekta.providence.descriptor.PPrimitive.BYTE.provider()), null, null),
        SHORT_SET(13, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "short_set", "shortSet", net.morimekta.providence.descriptor.PSet.orderedProvider(net.morimekta.providence.descriptor.PPrimitive.I16.provider()), null, null),
        INTEGER_SET(14, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "integer_set", "integerSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider()), null, null),
        LONG_SET(15, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "long_set", "longSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.I64.provider()), null, null),
        DOUBLE_SET(16, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "double_set", "doubleSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.DOUBLE.provider()), null, null),
        STRING_SET(17, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "string_set", "stringSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.STRING.provider()), null, null),
        BINARY_SET(18, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "binary_set", "binarySet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PPrimitive.BINARY.provider()), null, null),
        BOOLEAN_MAP(21, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "boolean_map", "booleanMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.BOOL.provider(),net.morimekta.providence.descriptor.PPrimitive.BOOL.provider()), null, null),
        BYTE_MAP(22, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "byte_map", "byteMap", net.morimekta.providence.descriptor.PMap.sortedProvider(net.morimekta.providence.descriptor.PPrimitive.BYTE.provider(),net.morimekta.providence.descriptor.PPrimitive.BYTE.provider()), null, null),
        SHORT_MAP(23, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "short_map", "shortMap", net.morimekta.providence.descriptor.PMap.orderedProvider(net.morimekta.providence.descriptor.PPrimitive.I16.provider(),net.morimekta.providence.descriptor.PPrimitive.I16.provider()), null, null),
        INTEGER_MAP(24, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "integer_map", "integerMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider(),net.morimekta.providence.descriptor.PPrimitive.I32.provider()), null, null),
        LONG_MAP(25, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "long_map", "longMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.I64.provider(),net.morimekta.providence.descriptor.PPrimitive.I64.provider()), null, null),
        DOUBLE_MAP(26, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "double_map", "doubleMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.DOUBLE.provider(),net.morimekta.providence.descriptor.PPrimitive.DOUBLE.provider()), null, null),
        STRING_MAP(27, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "string_map", "stringMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.STRING.provider(),net.morimekta.providence.descriptor.PPrimitive.STRING.provider()), null, null),
        BINARY_MAP(28, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "binary_map", "binaryMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.BINARY.provider(),net.morimekta.providence.descriptor.PPrimitive.BINARY.provider()), null, null),
        ENUM_LIST(31, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "enum_list", "enumList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.test.providence.core.Value.provider()), null, null),
        ENUM_SET(32, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "enum_set", "enumSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.test.providence.core.Value.provider()), null, null),
        ENUM_MAP(33, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "enum_map", "enumMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.test.providence.core.Value.provider(),net.morimekta.test.providence.core.Value.provider()), null, null),
        MESSAGE_LIST(41, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "message_list", "messageList", net.morimekta.providence.descriptor.PList.provider(net.morimekta.test.providence.core.OptionalFields.provider()), null, null),
        MESSAGE_SET(42, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "message_set", "messageSet", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.test.providence.core.OptionalFields.provider()), null, null),
        MESSAGE_MAP(43, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "message_map", "messageMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.STRING.provider(),net.morimekta.test.providence.core.OptionalFields.provider()), null, null),
        MESSAGE_KEY_MAP(44, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "message_key_map", "messageKeyMap", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.test.providence.core.CompactFields.provider(),net.morimekta.providence.descriptor.PPrimitive.STRING.provider()), null, null),
        REQUIRED_FIELDS(51, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "required_fields", "requiredFields", net.morimekta.test.providence.core.RequiredFields.provider(), null, null),
        DEFAULT_FIELDS(52, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "default_fields", "defaultFields", net.morimekta.test.providence.core.DefaultFields.provider(), null, null),
        OPTIONAL_FIELDS(53, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "optional_fields", "optionalFields", net.morimekta.test.providence.core.OptionalFields.provider(), null, null),
        UNION_FIELDS(54, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "union_fields", "unionFields", net.morimekta.test.providence.core.UnionFields.provider(), null, null),
        EXCEPTION_FIELDS(55, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "exception_fields", "exceptionFields", net.morimekta.test.providence.core.ExceptionFields.provider(), null, null),
        DEFAULT_VALUES(56, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "default_values", "defaultValues", net.morimekta.test.providence.core.DefaultValues.provider(), null, null),
        MAP_LIST_COMPACT(61, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "map_list_compact", "mapListCompact", net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider(),net.morimekta.providence.descriptor.PList.provider(net.morimekta.test.providence.core.CompactFields.provider())), null, null),
        LIST_MAP_NUMBERS(62, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "list_map_numbers", "listMapNumbers", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PMap.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider(),net.morimekta.providence.descriptor.PPrimitive.I32.provider())), null, null),
        SET_LIST_NUMBERS(63, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "set_list_numbers", "setListNumbers", net.morimekta.providence.descriptor.PSet.provider(net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider())), null, null),
        LIST_LIST_NUMBERS(64, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "list_list_numbers", "listListNumbers", net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PList.provider(net.morimekta.providence.descriptor.PPrimitive.I32.provider())), null, null),
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<Containers> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
                case 1: return _Field.BOOLEAN_LIST;
                case 2: return _Field.BYTE_LIST;
                case 3: return _Field.SHORT_LIST;
                case 4: return _Field.INTEGER_LIST;
                case 5: return _Field.LONG_LIST;
                case 6: return _Field.DOUBLE_LIST;
                case 7: return _Field.STRING_LIST;
                case 8: return _Field.BINARY_LIST;
                case 11: return _Field.BOOLEAN_SET;
                case 12: return _Field.BYTE_SET;
                case 13: return _Field.SHORT_SET;
                case 14: return _Field.INTEGER_SET;
                case 15: return _Field.LONG_SET;
                case 16: return _Field.DOUBLE_SET;
                case 17: return _Field.STRING_SET;
                case 18: return _Field.BINARY_SET;
                case 21: return _Field.BOOLEAN_MAP;
                case 22: return _Field.BYTE_MAP;
                case 23: return _Field.SHORT_MAP;
                case 24: return _Field.INTEGER_MAP;
                case 25: return _Field.LONG_MAP;
                case 26: return _Field.DOUBLE_MAP;
                case 27: return _Field.STRING_MAP;
                case 28: return _Field.BINARY_MAP;
                case 31: return _Field.ENUM_LIST;
                case 32: return _Field.ENUM_SET;
                case 33: return _Field.ENUM_MAP;
                case 41: return _Field.MESSAGE_LIST;
                case 42: return _Field.MESSAGE_SET;
                case 43: return _Field.MESSAGE_MAP;
                case 44: return _Field.MESSAGE_KEY_MAP;
                case 51: return _Field.REQUIRED_FIELDS;
                case 52: return _Field.DEFAULT_FIELDS;
                case 53: return _Field.OPTIONAL_FIELDS;
                case 54: return _Field.UNION_FIELDS;
                case 55: return _Field.EXCEPTION_FIELDS;
                case 56: return _Field.DEFAULT_VALUES;
                case 61: return _Field.MAP_LIST_COMPACT;
                case 62: return _Field.LIST_MAP_NUMBERS;
                case 63: return _Field.SET_LIST_NUMBERS;
                case 64: return _Field.LIST_LIST_NUMBERS;
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "boolean_list": return _Field.BOOLEAN_LIST;
                case "byte_list": return _Field.BYTE_LIST;
                case "short_list": return _Field.SHORT_LIST;
                case "integer_list": return _Field.INTEGER_LIST;
                case "long_list": return _Field.LONG_LIST;
                case "double_list": return _Field.DOUBLE_LIST;
                case "string_list": return _Field.STRING_LIST;
                case "binary_list": return _Field.BINARY_LIST;
                case "boolean_set": return _Field.BOOLEAN_SET;
                case "byte_set": return _Field.BYTE_SET;
                case "short_set": return _Field.SHORT_SET;
                case "integer_set": return _Field.INTEGER_SET;
                case "long_set": return _Field.LONG_SET;
                case "double_set": return _Field.DOUBLE_SET;
                case "string_set": return _Field.STRING_SET;
                case "binary_set": return _Field.BINARY_SET;
                case "boolean_map": return _Field.BOOLEAN_MAP;
                case "byte_map": return _Field.BYTE_MAP;
                case "short_map": return _Field.SHORT_MAP;
                case "integer_map": return _Field.INTEGER_MAP;
                case "long_map": return _Field.LONG_MAP;
                case "double_map": return _Field.DOUBLE_MAP;
                case "string_map": return _Field.STRING_MAP;
                case "binary_map": return _Field.BINARY_MAP;
                case "enum_list": return _Field.ENUM_LIST;
                case "enum_set": return _Field.ENUM_SET;
                case "enum_map": return _Field.ENUM_MAP;
                case "message_list": return _Field.MESSAGE_LIST;
                case "message_set": return _Field.MESSAGE_SET;
                case "message_map": return _Field.MESSAGE_MAP;
                case "message_key_map": return _Field.MESSAGE_KEY_MAP;
                case "required_fields": return _Field.REQUIRED_FIELDS;
                case "default_fields": return _Field.DEFAULT_FIELDS;
                case "optional_fields": return _Field.OPTIONAL_FIELDS;
                case "union_fields": return _Field.UNION_FIELDS;
                case "exception_fields": return _Field.EXCEPTION_FIELDS;
                case "default_values": return _Field.DEFAULT_VALUES;
                case "map_list_compact": return _Field.MAP_LIST_COMPACT;
                case "list_map_numbers": return _Field.LIST_MAP_NUMBERS;
                case "set_list_numbers": return _Field.SET_LIST_NUMBERS;
                case "list_list_numbers": return _Field.LIST_LIST_NUMBERS;
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
                case "booleanList": return _Field.BOOLEAN_LIST;
                case "boolean_list": return _Field.BOOLEAN_LIST;
                case "byteList": return _Field.BYTE_LIST;
                case "byte_list": return _Field.BYTE_LIST;
                case "shortList": return _Field.SHORT_LIST;
                case "short_list": return _Field.SHORT_LIST;
                case "integerList": return _Field.INTEGER_LIST;
                case "integer_list": return _Field.INTEGER_LIST;
                case "longList": return _Field.LONG_LIST;
                case "long_list": return _Field.LONG_LIST;
                case "doubleList": return _Field.DOUBLE_LIST;
                case "double_list": return _Field.DOUBLE_LIST;
                case "stringList": return _Field.STRING_LIST;
                case "string_list": return _Field.STRING_LIST;
                case "binaryList": return _Field.BINARY_LIST;
                case "binary_list": return _Field.BINARY_LIST;
                case "booleanSet": return _Field.BOOLEAN_SET;
                case "boolean_set": return _Field.BOOLEAN_SET;
                case "byteSet": return _Field.BYTE_SET;
                case "byte_set": return _Field.BYTE_SET;
                case "shortSet": return _Field.SHORT_SET;
                case "short_set": return _Field.SHORT_SET;
                case "integerSet": return _Field.INTEGER_SET;
                case "integer_set": return _Field.INTEGER_SET;
                case "longSet": return _Field.LONG_SET;
                case "long_set": return _Field.LONG_SET;
                case "doubleSet": return _Field.DOUBLE_SET;
                case "double_set": return _Field.DOUBLE_SET;
                case "stringSet": return _Field.STRING_SET;
                case "string_set": return _Field.STRING_SET;
                case "binarySet": return _Field.BINARY_SET;
                case "binary_set": return _Field.BINARY_SET;
                case "booleanMap": return _Field.BOOLEAN_MAP;
                case "boolean_map": return _Field.BOOLEAN_MAP;
                case "byteMap": return _Field.BYTE_MAP;
                case "byte_map": return _Field.BYTE_MAP;
                case "shortMap": return _Field.SHORT_MAP;
                case "short_map": return _Field.SHORT_MAP;
                case "integerMap": return _Field.INTEGER_MAP;
                case "integer_map": return _Field.INTEGER_MAP;
                case "longMap": return _Field.LONG_MAP;
                case "long_map": return _Field.LONG_MAP;
                case "doubleMap": return _Field.DOUBLE_MAP;
                case "double_map": return _Field.DOUBLE_MAP;
                case "stringMap": return _Field.STRING_MAP;
                case "string_map": return _Field.STRING_MAP;
                case "binaryMap": return _Field.BINARY_MAP;
                case "binary_map": return _Field.BINARY_MAP;
                case "enumList": return _Field.ENUM_LIST;
                case "enum_list": return _Field.ENUM_LIST;
                case "enumSet": return _Field.ENUM_SET;
                case "enum_set": return _Field.ENUM_SET;
                case "enumMap": return _Field.ENUM_MAP;
                case "enum_map": return _Field.ENUM_MAP;
                case "messageList": return _Field.MESSAGE_LIST;
                case "message_list": return _Field.MESSAGE_LIST;
                case "messageSet": return _Field.MESSAGE_SET;
                case "message_set": return _Field.MESSAGE_SET;
                case "messageMap": return _Field.MESSAGE_MAP;
                case "message_map": return _Field.MESSAGE_MAP;
                case "messageKeyMap": return _Field.MESSAGE_KEY_MAP;
                case "message_key_map": return _Field.MESSAGE_KEY_MAP;
                case "requiredFields": return _Field.REQUIRED_FIELDS;
                case "required_fields": return _Field.REQUIRED_FIELDS;
                case "defaultFields": return _Field.DEFAULT_FIELDS;
                case "default_fields": return _Field.DEFAULT_FIELDS;
                case "optionalFields": return _Field.OPTIONAL_FIELDS;
                case "optional_fields": return _Field.OPTIONAL_FIELDS;
                case "unionFields": return _Field.UNION_FIELDS;
                case "union_fields": return _Field.UNION_FIELDS;
                case "exceptionFields": return _Field.EXCEPTION_FIELDS;
                case "exception_fields": return _Field.EXCEPTION_FIELDS;
                case "defaultValues": return _Field.DEFAULT_VALUES;
                case "default_values": return _Field.DEFAULT_VALUES;
                case "mapListCompact": return _Field.MAP_LIST_COMPACT;
                case "map_list_compact": return _Field.MAP_LIST_COMPACT;
                case "listMapNumbers": return _Field.LIST_MAP_NUMBERS;
                case "list_map_numbers": return _Field.LIST_MAP_NUMBERS;
                case "setListNumbers": return _Field.SET_LIST_NUMBERS;
                case "set_list_numbers": return _Field.SET_LIST_NUMBERS;
                case "listListNumbers": return _Field.LIST_LIST_NUMBERS;
                case "list_list_numbers": return _Field.LIST_LIST_NUMBERS;
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in providence.Containers");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.Containers");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.Containers");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PStructDescriptorProvider<Containers> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PStructDescriptor<Containers> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PStructDescriptor<Containers> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PStructDescriptor<Containers> {
        public _Descriptor() {
            super("providence", "Containers", _Builder::new, false);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<Containers> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<Containers> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>providence.Containers</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<Containers>
            implements Containers_OrBuilder,
                       net.morimekta.providence.serializer.binary.BinaryReader {
        private java.util.BitSet optionals;
        private java.util.BitSet modified;

        private java.util.List<Boolean> mBooleanList;
        private java.util.List<Byte> mByteList;
        private java.util.List<Short> mShortList;
        private java.util.List<Integer> mIntegerList;
        private java.util.List<Long> mLongList;
        private java.util.List<Double> mDoubleList;
        private java.util.List<String> mStringList;
        private java.util.List<net.morimekta.util.Binary> mBinaryList;
        private java.util.Set<Boolean> mBooleanSet;
        private java.util.Set<Byte> mByteSet;
        private java.util.Set<Short> mShortSet;
        private java.util.Set<Integer> mIntegerSet;
        private java.util.Set<Long> mLongSet;
        private java.util.Set<Double> mDoubleSet;
        private java.util.Set<String> mStringSet;
        private java.util.Set<net.morimekta.util.Binary> mBinarySet;
        private java.util.Map<Boolean,Boolean> mBooleanMap;
        private java.util.Map<Byte,Byte> mByteMap;
        private java.util.Map<Short,Short> mShortMap;
        private java.util.Map<Integer,Integer> mIntegerMap;
        private java.util.Map<Long,Long> mLongMap;
        private java.util.Map<Double,Double> mDoubleMap;
        private java.util.Map<String,String> mStringMap;
        private java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> mBinaryMap;
        private java.util.List<net.morimekta.test.providence.core.Value> mEnumList;
        private java.util.Set<net.morimekta.test.providence.core.Value> mEnumSet;
        private java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> mEnumMap;
        private java.util.List<net.morimekta.test.providence.core.OptionalFields> mMessageList;
        private java.util.Set<net.morimekta.test.providence.core.OptionalFields> mMessageSet;
        private java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> mMessageMap;
        private java.util.Map<net.morimekta.test.providence.core.CompactFields,String> mMessageKeyMap;
        private net.morimekta.test.providence.core.RequiredFields mRequiredFields;
        private net.morimekta.test.providence.core.RequiredFields._Builder mRequiredFields_builder;
        private net.morimekta.test.providence.core.DefaultFields mDefaultFields;
        private net.morimekta.test.providence.core.DefaultFields._Builder mDefaultFields_builder;
        private net.morimekta.test.providence.core.OptionalFields mOptionalFields;
        private net.morimekta.test.providence.core.OptionalFields._Builder mOptionalFields_builder;
        private net.morimekta.test.providence.core.UnionFields mUnionFields;
        private net.morimekta.test.providence.core.UnionFields._Builder mUnionFields_builder;
        private net.morimekta.test.providence.core.ExceptionFields mExceptionFields;
        private net.morimekta.test.providence.core.ExceptionFields._Builder mExceptionFields_builder;
        private net.morimekta.test.providence.core.DefaultValues mDefaultValues;
        private net.morimekta.test.providence.core.DefaultValues._Builder mDefaultValues_builder;
        private java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> mMapListCompact;
        private java.util.List<java.util.Map<Integer,Integer>> mListMapNumbers;
        private java.util.Set<java.util.List<Integer>> mSetListNumbers;
        private java.util.List<java.util.List<Integer>> mListListNumbers;

        /**
         * Make a providence.Containers builder instance.
         */
        public _Builder() {
            optionals = new java.util.BitSet(41);
            modified = new java.util.BitSet(41);
        }

        /**
         * Make a mutating builder off a base providence.Containers.
         *
         * @param base The base Containers
         */
        public _Builder(Containers base) {
            this();

            if (base.hasBooleanList()) {
                optionals.set(0);
                mBooleanList = base.mBooleanList;
            }
            if (base.hasByteList()) {
                optionals.set(1);
                mByteList = base.mByteList;
            }
            if (base.hasShortList()) {
                optionals.set(2);
                mShortList = base.mShortList;
            }
            if (base.hasIntegerList()) {
                optionals.set(3);
                mIntegerList = base.mIntegerList;
            }
            if (base.hasLongList()) {
                optionals.set(4);
                mLongList = base.mLongList;
            }
            if (base.hasDoubleList()) {
                optionals.set(5);
                mDoubleList = base.mDoubleList;
            }
            if (base.hasStringList()) {
                optionals.set(6);
                mStringList = base.mStringList;
            }
            if (base.hasBinaryList()) {
                optionals.set(7);
                mBinaryList = base.mBinaryList;
            }
            if (base.hasBooleanSet()) {
                optionals.set(8);
                mBooleanSet = base.mBooleanSet;
            }
            if (base.hasByteSet()) {
                optionals.set(9);
                mByteSet = base.mByteSet;
            }
            if (base.hasShortSet()) {
                optionals.set(10);
                mShortSet = base.mShortSet;
            }
            if (base.hasIntegerSet()) {
                optionals.set(11);
                mIntegerSet = base.mIntegerSet;
            }
            if (base.hasLongSet()) {
                optionals.set(12);
                mLongSet = base.mLongSet;
            }
            if (base.hasDoubleSet()) {
                optionals.set(13);
                mDoubleSet = base.mDoubleSet;
            }
            if (base.hasStringSet()) {
                optionals.set(14);
                mStringSet = base.mStringSet;
            }
            if (base.hasBinarySet()) {
                optionals.set(15);
                mBinarySet = base.mBinarySet;
            }
            if (base.hasBooleanMap()) {
                optionals.set(16);
                mBooleanMap = base.mBooleanMap;
            }
            if (base.hasByteMap()) {
                optionals.set(17);
                mByteMap = base.mByteMap;
            }
            if (base.hasShortMap()) {
                optionals.set(18);
                mShortMap = base.mShortMap;
            }
            if (base.hasIntegerMap()) {
                optionals.set(19);
                mIntegerMap = base.mIntegerMap;
            }
            if (base.hasLongMap()) {
                optionals.set(20);
                mLongMap = base.mLongMap;
            }
            if (base.hasDoubleMap()) {
                optionals.set(21);
                mDoubleMap = base.mDoubleMap;
            }
            if (base.hasStringMap()) {
                optionals.set(22);
                mStringMap = base.mStringMap;
            }
            if (base.hasBinaryMap()) {
                optionals.set(23);
                mBinaryMap = base.mBinaryMap;
            }
            if (base.hasEnumList()) {
                optionals.set(24);
                mEnumList = base.mEnumList;
            }
            if (base.hasEnumSet()) {
                optionals.set(25);
                mEnumSet = base.mEnumSet;
            }
            if (base.hasEnumMap()) {
                optionals.set(26);
                mEnumMap = base.mEnumMap;
            }
            if (base.hasMessageList()) {
                optionals.set(27);
                mMessageList = base.mMessageList;
            }
            if (base.hasMessageSet()) {
                optionals.set(28);
                mMessageSet = base.mMessageSet;
            }
            if (base.hasMessageMap()) {
                optionals.set(29);
                mMessageMap = base.mMessageMap;
            }
            if (base.hasMessageKeyMap()) {
                optionals.set(30);
                mMessageKeyMap = base.mMessageKeyMap;
            }
            if (base.hasRequiredFields()) {
                optionals.set(31);
                mRequiredFields = base.mRequiredFields;
            }
            if (base.hasDefaultFields()) {
                optionals.set(32);
                mDefaultFields = base.mDefaultFields;
            }
            if (base.hasOptionalFields()) {
                optionals.set(33);
                mOptionalFields = base.mOptionalFields;
            }
            if (base.hasUnionFields()) {
                optionals.set(34);
                mUnionFields = base.mUnionFields;
            }
            if (base.hasExceptionFields()) {
                optionals.set(35);
                mExceptionFields = base.mExceptionFields;
            }
            if (base.hasDefaultValues()) {
                optionals.set(36);
                mDefaultValues = base.mDefaultValues;
            }
            if (base.hasMapListCompact()) {
                optionals.set(37);
                mMapListCompact = base.mMapListCompact;
            }
            if (base.hasListMapNumbers()) {
                optionals.set(38);
                mListMapNumbers = base.mListMapNumbers;
            }
            if (base.hasSetListNumbers()) {
                optionals.set(39);
                mSetListNumbers = base.mSetListNumbers;
            }
            if (base.hasListListNumbers()) {
                optionals.set(40);
                mListListNumbers = base.mListListNumbers;
            }
        }

        @javax.annotation.Nonnull
        @Override
        public Containers._Builder merge(Containers from) {
            if (from.hasBooleanList()) {
                optionals.set(0);
                modified.set(0);
                mBooleanList = from.getBooleanList();
            }

            if (from.hasByteList()) {
                optionals.set(1);
                modified.set(1);
                mByteList = from.getByteList();
            }

            if (from.hasShortList()) {
                optionals.set(2);
                modified.set(2);
                mShortList = from.getShortList();
            }

            if (from.hasIntegerList()) {
                optionals.set(3);
                modified.set(3);
                mIntegerList = from.getIntegerList();
            }

            if (from.hasLongList()) {
                optionals.set(4);
                modified.set(4);
                mLongList = from.getLongList();
            }

            if (from.hasDoubleList()) {
                optionals.set(5);
                modified.set(5);
                mDoubleList = from.getDoubleList();
            }

            if (from.hasStringList()) {
                optionals.set(6);
                modified.set(6);
                mStringList = from.getStringList();
            }

            if (from.hasBinaryList()) {
                optionals.set(7);
                modified.set(7);
                mBinaryList = from.getBinaryList();
            }

            if (from.hasBooleanSet()) {
                optionals.set(8);
                modified.set(8);
                mutableBooleanSet().addAll(from.getBooleanSet());
            }

            if (from.hasByteSet()) {
                optionals.set(9);
                modified.set(9);
                mutableByteSet().addAll(from.getByteSet());
            }

            if (from.hasShortSet()) {
                optionals.set(10);
                modified.set(10);
                mutableShortSet().addAll(from.getShortSet());
            }

            if (from.hasIntegerSet()) {
                optionals.set(11);
                modified.set(11);
                mutableIntegerSet().addAll(from.getIntegerSet());
            }

            if (from.hasLongSet()) {
                optionals.set(12);
                modified.set(12);
                mutableLongSet().addAll(from.getLongSet());
            }

            if (from.hasDoubleSet()) {
                optionals.set(13);
                modified.set(13);
                mutableDoubleSet().addAll(from.getDoubleSet());
            }

            if (from.hasStringSet()) {
                optionals.set(14);
                modified.set(14);
                mutableStringSet().addAll(from.getStringSet());
            }

            if (from.hasBinarySet()) {
                optionals.set(15);
                modified.set(15);
                mutableBinarySet().addAll(from.getBinarySet());
            }

            if (from.hasBooleanMap()) {
                optionals.set(16);
                modified.set(16);
                mutableBooleanMap().putAll(from.getBooleanMap());
            }

            if (from.hasByteMap()) {
                optionals.set(17);
                modified.set(17);
                mutableByteMap().putAll(from.getByteMap());
            }

            if (from.hasShortMap()) {
                optionals.set(18);
                modified.set(18);
                mutableShortMap().putAll(from.getShortMap());
            }

            if (from.hasIntegerMap()) {
                optionals.set(19);
                modified.set(19);
                mutableIntegerMap().putAll(from.getIntegerMap());
            }

            if (from.hasLongMap()) {
                optionals.set(20);
                modified.set(20);
                mutableLongMap().putAll(from.getLongMap());
            }

            if (from.hasDoubleMap()) {
                optionals.set(21);
                modified.set(21);
                mutableDoubleMap().putAll(from.getDoubleMap());
            }

            if (from.hasStringMap()) {
                optionals.set(22);
                modified.set(22);
                mutableStringMap().putAll(from.getStringMap());
            }

            if (from.hasBinaryMap()) {
                optionals.set(23);
                modified.set(23);
                mutableBinaryMap().putAll(from.getBinaryMap());
            }

            if (from.hasEnumList()) {
                optionals.set(24);
                modified.set(24);
                mEnumList = from.getEnumList();
            }

            if (from.hasEnumSet()) {
                optionals.set(25);
                modified.set(25);
                mutableEnumSet().addAll(from.getEnumSet());
            }

            if (from.hasEnumMap()) {
                optionals.set(26);
                modified.set(26);
                mutableEnumMap().putAll(from.getEnumMap());
            }

            if (from.hasMessageList()) {
                optionals.set(27);
                modified.set(27);
                mMessageList = from.getMessageList();
            }

            if (from.hasMessageSet()) {
                optionals.set(28);
                modified.set(28);
                mutableMessageSet().addAll(from.getMessageSet());
            }

            if (from.hasMessageMap()) {
                optionals.set(29);
                modified.set(29);
                mutableMessageMap().putAll(from.getMessageMap());
            }

            if (from.hasMessageKeyMap()) {
                optionals.set(30);
                modified.set(30);
                mutableMessageKeyMap().putAll(from.getMessageKeyMap());
            }

            if (from.hasRequiredFields()) {
                optionals.set(31);
                modified.set(31);
                if (mRequiredFields_builder != null) {
                    mRequiredFields_builder.merge(from.getRequiredFields());
                } else if (mRequiredFields != null) {
                    mRequiredFields_builder = mRequiredFields.mutate().merge(from.getRequiredFields());
                    mRequiredFields = null;
                } else {
                    mRequiredFields = from.getRequiredFields();
                }
            }

            if (from.hasDefaultFields()) {
                optionals.set(32);
                modified.set(32);
                if (mDefaultFields_builder != null) {
                    mDefaultFields_builder.merge(from.getDefaultFields());
                } else if (mDefaultFields != null) {
                    mDefaultFields_builder = mDefaultFields.mutate().merge(from.getDefaultFields());
                    mDefaultFields = null;
                } else {
                    mDefaultFields = from.getDefaultFields();
                }
            }

            if (from.hasOptionalFields()) {
                optionals.set(33);
                modified.set(33);
                if (mOptionalFields_builder != null) {
                    mOptionalFields_builder.merge(from.getOptionalFields());
                } else if (mOptionalFields != null) {
                    mOptionalFields_builder = mOptionalFields.mutate().merge(from.getOptionalFields());
                    mOptionalFields = null;
                } else {
                    mOptionalFields = from.getOptionalFields();
                }
            }

            if (from.hasUnionFields()) {
                optionals.set(34);
                modified.set(34);
                if (mUnionFields_builder != null) {
                    mUnionFields_builder.merge(from.getUnionFields());
                } else if (mUnionFields != null) {
                    mUnionFields_builder = mUnionFields.mutate().merge(from.getUnionFields());
                    mUnionFields = null;
                } else {
                    mUnionFields = from.getUnionFields();
                }
            }

            if (from.hasExceptionFields()) {
                optionals.set(35);
                modified.set(35);
                if (mExceptionFields_builder != null) {
                    mExceptionFields_builder.merge(from.getExceptionFields());
                } else if (mExceptionFields != null) {
                    mExceptionFields_builder = mExceptionFields.mutate().merge(from.getExceptionFields());
                    mExceptionFields = null;
                } else {
                    mExceptionFields = from.getExceptionFields();
                }
            }

            if (from.hasDefaultValues()) {
                optionals.set(36);
                modified.set(36);
                if (mDefaultValues_builder != null) {
                    mDefaultValues_builder.merge(from.getDefaultValues());
                } else if (mDefaultValues != null) {
                    mDefaultValues_builder = mDefaultValues.mutate().merge(from.getDefaultValues());
                    mDefaultValues = null;
                } else {
                    mDefaultValues = from.getDefaultValues();
                }
            }

            if (from.hasMapListCompact()) {
                optionals.set(37);
                modified.set(37);
                mutableMapListCompact().putAll(from.getMapListCompact());
            }

            if (from.hasListMapNumbers()) {
                optionals.set(38);
                modified.set(38);
                mListMapNumbers = from.getListMapNumbers();
            }

            if (from.hasSetListNumbers()) {
                optionals.set(39);
                modified.set(39);
                mutableSetListNumbers().addAll(from.getSetListNumbers());
            }

            if (from.hasListListNumbers()) {
                optionals.set(40);
                modified.set(40);
                mListListNumbers = from.getListListNumbers();
            }
            return this;
        }

        /**
         * Set the <code>boolean_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBooleanList(java.util.Collection<Boolean> value) {
            if (value == null) {
                return clearBooleanList();
            }

            optionals.set(0);
            modified.set(0);
            mBooleanList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>boolean_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToBooleanList(boolean... values) {
            optionals.set(0);
            modified.set(0);
            java.util.List<Boolean> _container = mutableBooleanList();
            for (boolean item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>boolean_list</code> field.
         *
         * @return True if boolean_list has been set.
         */
        public boolean isSetBooleanList() {
            return optionals.get(0);
        }

        /**
         * Checks for presence of the <code>boolean_list</code> field.
         *
         * @return True if boolean_list is present.
         */
        public boolean hasBooleanList() {
            return optionals.get(0);
        }

        /**
         * Checks if the <code>boolean_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if boolean_list has been modified.
         */
        public boolean isModifiedBooleanList() {
            return modified.get(0);
        }

        /**
         * Clear the <code>boolean_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBooleanList() {
            optionals.clear(0);
            modified.set(0);
            mBooleanList = null;
            return this;
        }

        /**
         * @return The mutable <code>boolean_list</code> container
         */
        public java.util.List<Boolean> mutableBooleanList() {
            optionals.set(0);
            modified.set(0);

            if (mBooleanList == null) {
                mBooleanList = new java.util.ArrayList<>();
            } else if (!(mBooleanList instanceof java.util.ArrayList)) {
                mBooleanList = new java.util.ArrayList<>(mBooleanList);
            }
            return mBooleanList;
        }

        /**
         * @return The <code>boolean_list</code> field value
         */
        public java.util.List<Boolean> getBooleanList() {
            return mBooleanList;
        }

        /**
         * @return Optional <code>boolean_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Boolean>> optionalBooleanList() {
            return java.util.Optional.ofNullable(mBooleanList);
        }

        /**
         * @return Number of entries in <code>boolean_list</code>.
         */
        public int numBooleanList() {
            return mBooleanList != null ? mBooleanList.size() : 0;
        }

        /**
         * Set the <code>byte_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setByteList(java.util.Collection<Byte> value) {
            if (value == null) {
                return clearByteList();
            }

            optionals.set(1);
            modified.set(1);
            mByteList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>byte_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToByteList(byte... values) {
            optionals.set(1);
            modified.set(1);
            java.util.List<Byte> _container = mutableByteList();
            for (byte item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>byte_list</code> field.
         *
         * @return True if byte_list has been set.
         */
        public boolean isSetByteList() {
            return optionals.get(1);
        }

        /**
         * Checks for presence of the <code>byte_list</code> field.
         *
         * @return True if byte_list is present.
         */
        public boolean hasByteList() {
            return optionals.get(1);
        }

        /**
         * Checks if the <code>byte_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if byte_list has been modified.
         */
        public boolean isModifiedByteList() {
            return modified.get(1);
        }

        /**
         * Clear the <code>byte_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearByteList() {
            optionals.clear(1);
            modified.set(1);
            mByteList = null;
            return this;
        }

        /**
         * @return The mutable <code>byte_list</code> container
         */
        public java.util.List<Byte> mutableByteList() {
            optionals.set(1);
            modified.set(1);

            if (mByteList == null) {
                mByteList = new java.util.ArrayList<>();
            } else if (!(mByteList instanceof java.util.ArrayList)) {
                mByteList = new java.util.ArrayList<>(mByteList);
            }
            return mByteList;
        }

        /**
         * @return The <code>byte_list</code> field value
         */
        public java.util.List<Byte> getByteList() {
            return mByteList;
        }

        /**
         * @return Optional <code>byte_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Byte>> optionalByteList() {
            return java.util.Optional.ofNullable(mByteList);
        }

        /**
         * @return Number of entries in <code>byte_list</code>.
         */
        public int numByteList() {
            return mByteList != null ? mByteList.size() : 0;
        }

        /**
         * Set the <code>short_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setShortList(java.util.Collection<Short> value) {
            if (value == null) {
                return clearShortList();
            }

            optionals.set(2);
            modified.set(2);
            mShortList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>short_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToShortList(short... values) {
            optionals.set(2);
            modified.set(2);
            java.util.List<Short> _container = mutableShortList();
            for (short item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>short_list</code> field.
         *
         * @return True if short_list has been set.
         */
        public boolean isSetShortList() {
            return optionals.get(2);
        }

        /**
         * Checks for presence of the <code>short_list</code> field.
         *
         * @return True if short_list is present.
         */
        public boolean hasShortList() {
            return optionals.get(2);
        }

        /**
         * Checks if the <code>short_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if short_list has been modified.
         */
        public boolean isModifiedShortList() {
            return modified.get(2);
        }

        /**
         * Clear the <code>short_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearShortList() {
            optionals.clear(2);
            modified.set(2);
            mShortList = null;
            return this;
        }

        /**
         * @return The mutable <code>short_list</code> container
         */
        public java.util.List<Short> mutableShortList() {
            optionals.set(2);
            modified.set(2);

            if (mShortList == null) {
                mShortList = new java.util.ArrayList<>();
            } else if (!(mShortList instanceof java.util.ArrayList)) {
                mShortList = new java.util.ArrayList<>(mShortList);
            }
            return mShortList;
        }

        /**
         * @return The <code>short_list</code> field value
         */
        public java.util.List<Short> getShortList() {
            return mShortList;
        }

        /**
         * @return Optional <code>short_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Short>> optionalShortList() {
            return java.util.Optional.ofNullable(mShortList);
        }

        /**
         * @return Number of entries in <code>short_list</code>.
         */
        public int numShortList() {
            return mShortList != null ? mShortList.size() : 0;
        }

        /**
         * Set the <code>integer_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setIntegerList(java.util.Collection<Integer> value) {
            if (value == null) {
                return clearIntegerList();
            }

            optionals.set(3);
            modified.set(3);
            mIntegerList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>integer_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToIntegerList(int... values) {
            optionals.set(3);
            modified.set(3);
            java.util.List<Integer> _container = mutableIntegerList();
            for (int item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>integer_list</code> field.
         *
         * @return True if integer_list has been set.
         */
        public boolean isSetIntegerList() {
            return optionals.get(3);
        }

        /**
         * Checks for presence of the <code>integer_list</code> field.
         *
         * @return True if integer_list is present.
         */
        public boolean hasIntegerList() {
            return optionals.get(3);
        }

        /**
         * Checks if the <code>integer_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if integer_list has been modified.
         */
        public boolean isModifiedIntegerList() {
            return modified.get(3);
        }

        /**
         * Clear the <code>integer_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearIntegerList() {
            optionals.clear(3);
            modified.set(3);
            mIntegerList = null;
            return this;
        }

        /**
         * @return The mutable <code>integer_list</code> container
         */
        public java.util.List<Integer> mutableIntegerList() {
            optionals.set(3);
            modified.set(3);

            if (mIntegerList == null) {
                mIntegerList = new java.util.ArrayList<>();
            } else if (!(mIntegerList instanceof java.util.ArrayList)) {
                mIntegerList = new java.util.ArrayList<>(mIntegerList);
            }
            return mIntegerList;
        }

        /**
         * @return The <code>integer_list</code> field value
         */
        public java.util.List<Integer> getIntegerList() {
            return mIntegerList;
        }

        /**
         * @return Optional <code>integer_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Integer>> optionalIntegerList() {
            return java.util.Optional.ofNullable(mIntegerList);
        }

        /**
         * @return Number of entries in <code>integer_list</code>.
         */
        public int numIntegerList() {
            return mIntegerList != null ? mIntegerList.size() : 0;
        }

        /**
         * Set the <code>long_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setLongList(java.util.Collection<Long> value) {
            if (value == null) {
                return clearLongList();
            }

            optionals.set(4);
            modified.set(4);
            mLongList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>long_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToLongList(long... values) {
            optionals.set(4);
            modified.set(4);
            java.util.List<Long> _container = mutableLongList();
            for (long item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>long_list</code> field.
         *
         * @return True if long_list has been set.
         */
        public boolean isSetLongList() {
            return optionals.get(4);
        }

        /**
         * Checks for presence of the <code>long_list</code> field.
         *
         * @return True if long_list is present.
         */
        public boolean hasLongList() {
            return optionals.get(4);
        }

        /**
         * Checks if the <code>long_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if long_list has been modified.
         */
        public boolean isModifiedLongList() {
            return modified.get(4);
        }

        /**
         * Clear the <code>long_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearLongList() {
            optionals.clear(4);
            modified.set(4);
            mLongList = null;
            return this;
        }

        /**
         * @return The mutable <code>long_list</code> container
         */
        public java.util.List<Long> mutableLongList() {
            optionals.set(4);
            modified.set(4);

            if (mLongList == null) {
                mLongList = new java.util.ArrayList<>();
            } else if (!(mLongList instanceof java.util.ArrayList)) {
                mLongList = new java.util.ArrayList<>(mLongList);
            }
            return mLongList;
        }

        /**
         * @return The <code>long_list</code> field value
         */
        public java.util.List<Long> getLongList() {
            return mLongList;
        }

        /**
         * @return Optional <code>long_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Long>> optionalLongList() {
            return java.util.Optional.ofNullable(mLongList);
        }

        /**
         * @return Number of entries in <code>long_list</code>.
         */
        public int numLongList() {
            return mLongList != null ? mLongList.size() : 0;
        }

        /**
         * Set the <code>double_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setDoubleList(java.util.Collection<Double> value) {
            if (value == null) {
                return clearDoubleList();
            }

            optionals.set(5);
            modified.set(5);
            mDoubleList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>double_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToDoubleList(double... values) {
            optionals.set(5);
            modified.set(5);
            java.util.List<Double> _container = mutableDoubleList();
            for (double item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>double_list</code> field.
         *
         * @return True if double_list has been set.
         */
        public boolean isSetDoubleList() {
            return optionals.get(5);
        }

        /**
         * Checks for presence of the <code>double_list</code> field.
         *
         * @return True if double_list is present.
         */
        public boolean hasDoubleList() {
            return optionals.get(5);
        }

        /**
         * Checks if the <code>double_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if double_list has been modified.
         */
        public boolean isModifiedDoubleList() {
            return modified.get(5);
        }

        /**
         * Clear the <code>double_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearDoubleList() {
            optionals.clear(5);
            modified.set(5);
            mDoubleList = null;
            return this;
        }

        /**
         * @return The mutable <code>double_list</code> container
         */
        public java.util.List<Double> mutableDoubleList() {
            optionals.set(5);
            modified.set(5);

            if (mDoubleList == null) {
                mDoubleList = new java.util.ArrayList<>();
            } else if (!(mDoubleList instanceof java.util.ArrayList)) {
                mDoubleList = new java.util.ArrayList<>(mDoubleList);
            }
            return mDoubleList;
        }

        /**
         * @return The <code>double_list</code> field value
         */
        public java.util.List<Double> getDoubleList() {
            return mDoubleList;
        }

        /**
         * @return Optional <code>double_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<Double>> optionalDoubleList() {
            return java.util.Optional.ofNullable(mDoubleList);
        }

        /**
         * @return Number of entries in <code>double_list</code>.
         */
        public int numDoubleList() {
            return mDoubleList != null ? mDoubleList.size() : 0;
        }

        /**
         * Set the <code>string_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setStringList(java.util.Collection<String> value) {
            if (value == null) {
                return clearStringList();
            }

            optionals.set(6);
            modified.set(6);
            mStringList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>string_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToStringList(String... values) {
            optionals.set(6);
            modified.set(6);
            java.util.List<String> _container = mutableStringList();
            for (String item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>string_list</code> field.
         *
         * @return True if string_list has been set.
         */
        public boolean isSetStringList() {
            return optionals.get(6);
        }

        /**
         * Checks for presence of the <code>string_list</code> field.
         *
         * @return True if string_list is present.
         */
        public boolean hasStringList() {
            return optionals.get(6);
        }

        /**
         * Checks if the <code>string_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if string_list has been modified.
         */
        public boolean isModifiedStringList() {
            return modified.get(6);
        }

        /**
         * Clear the <code>string_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearStringList() {
            optionals.clear(6);
            modified.set(6);
            mStringList = null;
            return this;
        }

        /**
         * @return The mutable <code>string_list</code> container
         */
        public java.util.List<String> mutableStringList() {
            optionals.set(6);
            modified.set(6);

            if (mStringList == null) {
                mStringList = new java.util.ArrayList<>();
            } else if (!(mStringList instanceof java.util.ArrayList)) {
                mStringList = new java.util.ArrayList<>(mStringList);
            }
            return mStringList;
        }

        /**
         * @return The <code>string_list</code> field value
         */
        public java.util.List<String> getStringList() {
            return mStringList;
        }

        /**
         * @return Optional <code>string_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<String>> optionalStringList() {
            return java.util.Optional.ofNullable(mStringList);
        }

        /**
         * @return Number of entries in <code>string_list</code>.
         */
        public int numStringList() {
            return mStringList != null ? mStringList.size() : 0;
        }

        /**
         * Set the <code>binary_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBinaryList(java.util.Collection<net.morimekta.util.Binary> value) {
            if (value == null) {
                return clearBinaryList();
            }

            optionals.set(7);
            modified.set(7);
            mBinaryList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>binary_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToBinaryList(net.morimekta.util.Binary... values) {
            optionals.set(7);
            modified.set(7);
            java.util.List<net.morimekta.util.Binary> _container = mutableBinaryList();
            for (net.morimekta.util.Binary item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>binary_list</code> field.
         *
         * @return True if binary_list has been set.
         */
        public boolean isSetBinaryList() {
            return optionals.get(7);
        }

        /**
         * Checks for presence of the <code>binary_list</code> field.
         *
         * @return True if binary_list is present.
         */
        public boolean hasBinaryList() {
            return optionals.get(7);
        }

        /**
         * Checks if the <code>binary_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if binary_list has been modified.
         */
        public boolean isModifiedBinaryList() {
            return modified.get(7);
        }

        /**
         * Clear the <code>binary_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBinaryList() {
            optionals.clear(7);
            modified.set(7);
            mBinaryList = null;
            return this;
        }

        /**
         * @return The mutable <code>binary_list</code> container
         */
        public java.util.List<net.morimekta.util.Binary> mutableBinaryList() {
            optionals.set(7);
            modified.set(7);

            if (mBinaryList == null) {
                mBinaryList = new java.util.ArrayList<>();
            } else if (!(mBinaryList instanceof java.util.ArrayList)) {
                mBinaryList = new java.util.ArrayList<>(mBinaryList);
            }
            return mBinaryList;
        }

        /**
         * @return The <code>binary_list</code> field value
         */
        public java.util.List<net.morimekta.util.Binary> getBinaryList() {
            return mBinaryList;
        }

        /**
         * @return Optional <code>binary_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<net.morimekta.util.Binary>> optionalBinaryList() {
            return java.util.Optional.ofNullable(mBinaryList);
        }

        /**
         * @return Number of entries in <code>binary_list</code>.
         */
        public int numBinaryList() {
            return mBinaryList != null ? mBinaryList.size() : 0;
        }

        /**
         * Set the <code>boolean_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBooleanSet(java.util.Collection<Boolean> value) {
            if (value == null) {
                return clearBooleanSet();
            }

            optionals.set(8);
            modified.set(8);
            mBooleanSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>boolean_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToBooleanSet(boolean... values) {
            optionals.set(8);
            modified.set(8);
            java.util.Set<Boolean> _container = mutableBooleanSet();
            for (boolean item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>boolean_set</code> field.
         *
         * @return True if boolean_set has been set.
         */
        public boolean isSetBooleanSet() {
            return optionals.get(8);
        }

        /**
         * Checks for presence of the <code>boolean_set</code> field.
         *
         * @return True if boolean_set is present.
         */
        public boolean hasBooleanSet() {
            return optionals.get(8);
        }

        /**
         * Checks if the <code>boolean_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if boolean_set has been modified.
         */
        public boolean isModifiedBooleanSet() {
            return modified.get(8);
        }

        /**
         * Clear the <code>boolean_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBooleanSet() {
            optionals.clear(8);
            modified.set(8);
            mBooleanSet = null;
            return this;
        }

        /**
         * @return The mutable <code>boolean_set</code> container
         */
        public java.util.Set<Boolean> mutableBooleanSet() {
            optionals.set(8);
            modified.set(8);

            if (mBooleanSet == null) {
                mBooleanSet = new java.util.HashSet<>();
            } else if (!(mBooleanSet instanceof java.util.HashSet)) {
                mBooleanSet = new java.util.HashSet<>(mBooleanSet);
            }
            return mBooleanSet;
        }

        /**
         * @return The <code>boolean_set</code> field value
         */
        public java.util.Set<Boolean> getBooleanSet() {
            return mBooleanSet;
        }

        /**
         * @return Optional <code>boolean_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Boolean>> optionalBooleanSet() {
            return java.util.Optional.ofNullable(mBooleanSet);
        }

        /**
         * @return Number of entries in <code>boolean_set</code>.
         */
        public int numBooleanSet() {
            return mBooleanSet != null ? mBooleanSet.size() : 0;
        }

        /**
         * Set the <code>byte_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setByteSet(java.util.Collection<Byte> value) {
            if (value == null) {
                return clearByteSet();
            }

            optionals.set(9);
            modified.set(9);
            mByteSet = net.morimekta.util.collect.UnmodifiableSortedSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>byte_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToByteSet(byte... values) {
            optionals.set(9);
            modified.set(9);
            java.util.Set<Byte> _container = mutableByteSet();
            for (byte item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>byte_set</code> field.
         *
         * @return True if byte_set has been set.
         */
        public boolean isSetByteSet() {
            return optionals.get(9);
        }

        /**
         * Checks for presence of the <code>byte_set</code> field.
         *
         * @return True if byte_set is present.
         */
        public boolean hasByteSet() {
            return optionals.get(9);
        }

        /**
         * Checks if the <code>byte_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if byte_set has been modified.
         */
        public boolean isModifiedByteSet() {
            return modified.get(9);
        }

        /**
         * Clear the <code>byte_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearByteSet() {
            optionals.clear(9);
            modified.set(9);
            mByteSet = null;
            return this;
        }

        /**
         * @return The mutable <code>byte_set</code> container
         */
        public java.util.Set<Byte> mutableByteSet() {
            optionals.set(9);
            modified.set(9);

            if (mByteSet == null) {
                mByteSet = new java.util.TreeSet<>();
            } else if (!(mByteSet instanceof java.util.TreeSet)) {
                mByteSet = new java.util.TreeSet<>(mByteSet);
            }
            return mByteSet;
        }

        /**
         * @return The <code>byte_set</code> field value
         */
        public java.util.Set<Byte> getByteSet() {
            return mByteSet;
        }

        /**
         * @return Optional <code>byte_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Byte>> optionalByteSet() {
            return java.util.Optional.ofNullable(mByteSet);
        }

        /**
         * @return Number of entries in <code>byte_set</code>.
         */
        public int numByteSet() {
            return mByteSet != null ? mByteSet.size() : 0;
        }

        /**
         * Set the <code>short_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setShortSet(java.util.Collection<Short> value) {
            if (value == null) {
                return clearShortSet();
            }

            optionals.set(10);
            modified.set(10);
            mShortSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>short_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToShortSet(short... values) {
            optionals.set(10);
            modified.set(10);
            java.util.Set<Short> _container = mutableShortSet();
            for (short item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>short_set</code> field.
         *
         * @return True if short_set has been set.
         */
        public boolean isSetShortSet() {
            return optionals.get(10);
        }

        /**
         * Checks for presence of the <code>short_set</code> field.
         *
         * @return True if short_set is present.
         */
        public boolean hasShortSet() {
            return optionals.get(10);
        }

        /**
         * Checks if the <code>short_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if short_set has been modified.
         */
        public boolean isModifiedShortSet() {
            return modified.get(10);
        }

        /**
         * Clear the <code>short_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearShortSet() {
            optionals.clear(10);
            modified.set(10);
            mShortSet = null;
            return this;
        }

        /**
         * @return The mutable <code>short_set</code> container
         */
        public java.util.Set<Short> mutableShortSet() {
            optionals.set(10);
            modified.set(10);

            if (mShortSet == null) {
                mShortSet = new java.util.LinkedHashSet<>();
            } else if (!(mShortSet instanceof java.util.LinkedHashSet)) {
                mShortSet = new java.util.LinkedHashSet<>(mShortSet);
            }
            return mShortSet;
        }

        /**
         * @return The <code>short_set</code> field value
         */
        public java.util.Set<Short> getShortSet() {
            return mShortSet;
        }

        /**
         * @return Optional <code>short_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Short>> optionalShortSet() {
            return java.util.Optional.ofNullable(mShortSet);
        }

        /**
         * @return Number of entries in <code>short_set</code>.
         */
        public int numShortSet() {
            return mShortSet != null ? mShortSet.size() : 0;
        }

        /**
         * Set the <code>integer_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setIntegerSet(java.util.Collection<Integer> value) {
            if (value == null) {
                return clearIntegerSet();
            }

            optionals.set(11);
            modified.set(11);
            mIntegerSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>integer_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToIntegerSet(int... values) {
            optionals.set(11);
            modified.set(11);
            java.util.Set<Integer> _container = mutableIntegerSet();
            for (int item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>integer_set</code> field.
         *
         * @return True if integer_set has been set.
         */
        public boolean isSetIntegerSet() {
            return optionals.get(11);
        }

        /**
         * Checks for presence of the <code>integer_set</code> field.
         *
         * @return True if integer_set is present.
         */
        public boolean hasIntegerSet() {
            return optionals.get(11);
        }

        /**
         * Checks if the <code>integer_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if integer_set has been modified.
         */
        public boolean isModifiedIntegerSet() {
            return modified.get(11);
        }

        /**
         * Clear the <code>integer_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearIntegerSet() {
            optionals.clear(11);
            modified.set(11);
            mIntegerSet = null;
            return this;
        }

        /**
         * @return The mutable <code>integer_set</code> container
         */
        public java.util.Set<Integer> mutableIntegerSet() {
            optionals.set(11);
            modified.set(11);

            if (mIntegerSet == null) {
                mIntegerSet = new java.util.HashSet<>();
            } else if (!(mIntegerSet instanceof java.util.HashSet)) {
                mIntegerSet = new java.util.HashSet<>(mIntegerSet);
            }
            return mIntegerSet;
        }

        /**
         * @return The <code>integer_set</code> field value
         */
        public java.util.Set<Integer> getIntegerSet() {
            return mIntegerSet;
        }

        /**
         * @return Optional <code>integer_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Integer>> optionalIntegerSet() {
            return java.util.Optional.ofNullable(mIntegerSet);
        }

        /**
         * @return Number of entries in <code>integer_set</code>.
         */
        public int numIntegerSet() {
            return mIntegerSet != null ? mIntegerSet.size() : 0;
        }

        /**
         * Set the <code>long_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setLongSet(java.util.Collection<Long> value) {
            if (value == null) {
                return clearLongSet();
            }

            optionals.set(12);
            modified.set(12);
            mLongSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>long_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToLongSet(long... values) {
            optionals.set(12);
            modified.set(12);
            java.util.Set<Long> _container = mutableLongSet();
            for (long item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>long_set</code> field.
         *
         * @return True if long_set has been set.
         */
        public boolean isSetLongSet() {
            return optionals.get(12);
        }

        /**
         * Checks for presence of the <code>long_set</code> field.
         *
         * @return True if long_set is present.
         */
        public boolean hasLongSet() {
            return optionals.get(12);
        }

        /**
         * Checks if the <code>long_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if long_set has been modified.
         */
        public boolean isModifiedLongSet() {
            return modified.get(12);
        }

        /**
         * Clear the <code>long_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearLongSet() {
            optionals.clear(12);
            modified.set(12);
            mLongSet = null;
            return this;
        }

        /**
         * @return The mutable <code>long_set</code> container
         */
        public java.util.Set<Long> mutableLongSet() {
            optionals.set(12);
            modified.set(12);

            if (mLongSet == null) {
                mLongSet = new java.util.HashSet<>();
            } else if (!(mLongSet instanceof java.util.HashSet)) {
                mLongSet = new java.util.HashSet<>(mLongSet);
            }
            return mLongSet;
        }

        /**
         * @return The <code>long_set</code> field value
         */
        public java.util.Set<Long> getLongSet() {
            return mLongSet;
        }

        /**
         * @return Optional <code>long_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Long>> optionalLongSet() {
            return java.util.Optional.ofNullable(mLongSet);
        }

        /**
         * @return Number of entries in <code>long_set</code>.
         */
        public int numLongSet() {
            return mLongSet != null ? mLongSet.size() : 0;
        }

        /**
         * Set the <code>double_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setDoubleSet(java.util.Collection<Double> value) {
            if (value == null) {
                return clearDoubleSet();
            }

            optionals.set(13);
            modified.set(13);
            mDoubleSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>double_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToDoubleSet(double... values) {
            optionals.set(13);
            modified.set(13);
            java.util.Set<Double> _container = mutableDoubleSet();
            for (double item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>double_set</code> field.
         *
         * @return True if double_set has been set.
         */
        public boolean isSetDoubleSet() {
            return optionals.get(13);
        }

        /**
         * Checks for presence of the <code>double_set</code> field.
         *
         * @return True if double_set is present.
         */
        public boolean hasDoubleSet() {
            return optionals.get(13);
        }

        /**
         * Checks if the <code>double_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if double_set has been modified.
         */
        public boolean isModifiedDoubleSet() {
            return modified.get(13);
        }

        /**
         * Clear the <code>double_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearDoubleSet() {
            optionals.clear(13);
            modified.set(13);
            mDoubleSet = null;
            return this;
        }

        /**
         * @return The mutable <code>double_set</code> container
         */
        public java.util.Set<Double> mutableDoubleSet() {
            optionals.set(13);
            modified.set(13);

            if (mDoubleSet == null) {
                mDoubleSet = new java.util.HashSet<>();
            } else if (!(mDoubleSet instanceof java.util.HashSet)) {
                mDoubleSet = new java.util.HashSet<>(mDoubleSet);
            }
            return mDoubleSet;
        }

        /**
         * @return The <code>double_set</code> field value
         */
        public java.util.Set<Double> getDoubleSet() {
            return mDoubleSet;
        }

        /**
         * @return Optional <code>double_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<Double>> optionalDoubleSet() {
            return java.util.Optional.ofNullable(mDoubleSet);
        }

        /**
         * @return Number of entries in <code>double_set</code>.
         */
        public int numDoubleSet() {
            return mDoubleSet != null ? mDoubleSet.size() : 0;
        }

        /**
         * Set the <code>string_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setStringSet(java.util.Collection<String> value) {
            if (value == null) {
                return clearStringSet();
            }

            optionals.set(14);
            modified.set(14);
            mStringSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>string_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToStringSet(String... values) {
            optionals.set(14);
            modified.set(14);
            java.util.Set<String> _container = mutableStringSet();
            for (String item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>string_set</code> field.
         *
         * @return True if string_set has been set.
         */
        public boolean isSetStringSet() {
            return optionals.get(14);
        }

        /**
         * Checks for presence of the <code>string_set</code> field.
         *
         * @return True if string_set is present.
         */
        public boolean hasStringSet() {
            return optionals.get(14);
        }

        /**
         * Checks if the <code>string_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if string_set has been modified.
         */
        public boolean isModifiedStringSet() {
            return modified.get(14);
        }

        /**
         * Clear the <code>string_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearStringSet() {
            optionals.clear(14);
            modified.set(14);
            mStringSet = null;
            return this;
        }

        /**
         * @return The mutable <code>string_set</code> container
         */
        public java.util.Set<String> mutableStringSet() {
            optionals.set(14);
            modified.set(14);

            if (mStringSet == null) {
                mStringSet = new java.util.HashSet<>();
            } else if (!(mStringSet instanceof java.util.HashSet)) {
                mStringSet = new java.util.HashSet<>(mStringSet);
            }
            return mStringSet;
        }

        /**
         * @return The <code>string_set</code> field value
         */
        public java.util.Set<String> getStringSet() {
            return mStringSet;
        }

        /**
         * @return Optional <code>string_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<String>> optionalStringSet() {
            return java.util.Optional.ofNullable(mStringSet);
        }

        /**
         * @return Number of entries in <code>string_set</code>.
         */
        public int numStringSet() {
            return mStringSet != null ? mStringSet.size() : 0;
        }

        /**
         * Set the <code>binary_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBinarySet(java.util.Collection<net.morimekta.util.Binary> value) {
            if (value == null) {
                return clearBinarySet();
            }

            optionals.set(15);
            modified.set(15);
            mBinarySet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>binary_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToBinarySet(net.morimekta.util.Binary... values) {
            optionals.set(15);
            modified.set(15);
            java.util.Set<net.morimekta.util.Binary> _container = mutableBinarySet();
            for (net.morimekta.util.Binary item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>binary_set</code> field.
         *
         * @return True if binary_set has been set.
         */
        public boolean isSetBinarySet() {
            return optionals.get(15);
        }

        /**
         * Checks for presence of the <code>binary_set</code> field.
         *
         * @return True if binary_set is present.
         */
        public boolean hasBinarySet() {
            return optionals.get(15);
        }

        /**
         * Checks if the <code>binary_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if binary_set has been modified.
         */
        public boolean isModifiedBinarySet() {
            return modified.get(15);
        }

        /**
         * Clear the <code>binary_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBinarySet() {
            optionals.clear(15);
            modified.set(15);
            mBinarySet = null;
            return this;
        }

        /**
         * @return The mutable <code>binary_set</code> container
         */
        public java.util.Set<net.morimekta.util.Binary> mutableBinarySet() {
            optionals.set(15);
            modified.set(15);

            if (mBinarySet == null) {
                mBinarySet = new java.util.HashSet<>();
            } else if (!(mBinarySet instanceof java.util.HashSet)) {
                mBinarySet = new java.util.HashSet<>(mBinarySet);
            }
            return mBinarySet;
        }

        /**
         * @return The <code>binary_set</code> field value
         */
        public java.util.Set<net.morimekta.util.Binary> getBinarySet() {
            return mBinarySet;
        }

        /**
         * @return Optional <code>binary_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<net.morimekta.util.Binary>> optionalBinarySet() {
            return java.util.Optional.ofNullable(mBinarySet);
        }

        /**
         * @return Number of entries in <code>binary_set</code>.
         */
        public int numBinarySet() {
            return mBinarySet != null ? mBinarySet.size() : 0;
        }

        /**
         * Set the <code>boolean_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBooleanMap(java.util.Map<Boolean,Boolean> value) {
            if (value == null) {
                return clearBooleanMap();
            }

            optionals.set(16);
            modified.set(16);
            mBooleanMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>boolean_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInBooleanMap(boolean key, boolean value) {
            optionals.set(16);
            modified.set(16);
            mutableBooleanMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>boolean_map</code> field.
         *
         * @return True if boolean_map has been set.
         */
        public boolean isSetBooleanMap() {
            return optionals.get(16);
        }

        /**
         * Checks for presence of the <code>boolean_map</code> field.
         *
         * @return True if boolean_map is present.
         */
        public boolean hasBooleanMap() {
            return optionals.get(16);
        }

        /**
         * Checks if the <code>boolean_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if boolean_map has been modified.
         */
        public boolean isModifiedBooleanMap() {
            return modified.get(16);
        }

        /**
         * Clear the <code>boolean_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBooleanMap() {
            optionals.clear(16);
            modified.set(16);
            mBooleanMap = null;
            return this;
        }

        /**
         * @return The mutable <code>boolean_map</code> container
         */
        public java.util.Map<Boolean,Boolean> mutableBooleanMap() {
            optionals.set(16);
            modified.set(16);

            if (mBooleanMap == null) {
                mBooleanMap = new java.util.HashMap<>();
            } else if (!(mBooleanMap instanceof java.util.HashMap)) {
                mBooleanMap = new java.util.HashMap<>(mBooleanMap);
            }
            return mBooleanMap;
        }

        /**
         * @return The <code>boolean_map</code> field value
         */
        public java.util.Map<Boolean,Boolean> getBooleanMap() {
            return mBooleanMap;
        }

        /**
         * @return Optional <code>boolean_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Boolean,Boolean>> optionalBooleanMap() {
            return java.util.Optional.ofNullable(mBooleanMap);
        }

        /**
         * @return Number of entries in <code>boolean_map</code>.
         */
        public int numBooleanMap() {
            return mBooleanMap != null ? mBooleanMap.size() : 0;
        }

        /**
         * Set the <code>byte_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setByteMap(java.util.Map<Byte,Byte> value) {
            if (value == null) {
                return clearByteMap();
            }

            optionals.set(17);
            modified.set(17);
            mByteMap = net.morimekta.util.collect.UnmodifiableSortedMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>byte_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInByteMap(byte key, byte value) {
            optionals.set(17);
            modified.set(17);
            mutableByteMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>byte_map</code> field.
         *
         * @return True if byte_map has been set.
         */
        public boolean isSetByteMap() {
            return optionals.get(17);
        }

        /**
         * Checks for presence of the <code>byte_map</code> field.
         *
         * @return True if byte_map is present.
         */
        public boolean hasByteMap() {
            return optionals.get(17);
        }

        /**
         * Checks if the <code>byte_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if byte_map has been modified.
         */
        public boolean isModifiedByteMap() {
            return modified.get(17);
        }

        /**
         * Clear the <code>byte_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearByteMap() {
            optionals.clear(17);
            modified.set(17);
            mByteMap = null;
            return this;
        }

        /**
         * @return The mutable <code>byte_map</code> container
         */
        public java.util.Map<Byte,Byte> mutableByteMap() {
            optionals.set(17);
            modified.set(17);

            if (mByteMap == null) {
                mByteMap = new java.util.TreeMap<>();
            } else if (!(mByteMap instanceof java.util.TreeMap)) {
                mByteMap = new java.util.TreeMap<>(mByteMap);
            }
            return mByteMap;
        }

        /**
         * @return The <code>byte_map</code> field value
         */
        public java.util.Map<Byte,Byte> getByteMap() {
            return mByteMap;
        }

        /**
         * @return Optional <code>byte_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Byte,Byte>> optionalByteMap() {
            return java.util.Optional.ofNullable(mByteMap);
        }

        /**
         * @return Number of entries in <code>byte_map</code>.
         */
        public int numByteMap() {
            return mByteMap != null ? mByteMap.size() : 0;
        }

        /**
         * Set the <code>short_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setShortMap(java.util.Map<Short,Short> value) {
            if (value == null) {
                return clearShortMap();
            }

            optionals.set(18);
            modified.set(18);
            mShortMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>short_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInShortMap(short key, short value) {
            optionals.set(18);
            modified.set(18);
            mutableShortMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>short_map</code> field.
         *
         * @return True if short_map has been set.
         */
        public boolean isSetShortMap() {
            return optionals.get(18);
        }

        /**
         * Checks for presence of the <code>short_map</code> field.
         *
         * @return True if short_map is present.
         */
        public boolean hasShortMap() {
            return optionals.get(18);
        }

        /**
         * Checks if the <code>short_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if short_map has been modified.
         */
        public boolean isModifiedShortMap() {
            return modified.get(18);
        }

        /**
         * Clear the <code>short_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearShortMap() {
            optionals.clear(18);
            modified.set(18);
            mShortMap = null;
            return this;
        }

        /**
         * @return The mutable <code>short_map</code> container
         */
        public java.util.Map<Short,Short> mutableShortMap() {
            optionals.set(18);
            modified.set(18);

            if (mShortMap == null) {
                mShortMap = new java.util.LinkedHashMap<>();
            } else if (!(mShortMap instanceof java.util.LinkedHashMap)) {
                mShortMap = new java.util.LinkedHashMap<>(mShortMap);
            }
            return mShortMap;
        }

        /**
         * @return The <code>short_map</code> field value
         */
        public java.util.Map<Short,Short> getShortMap() {
            return mShortMap;
        }

        /**
         * @return Optional <code>short_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Short,Short>> optionalShortMap() {
            return java.util.Optional.ofNullable(mShortMap);
        }

        /**
         * @return Number of entries in <code>short_map</code>.
         */
        public int numShortMap() {
            return mShortMap != null ? mShortMap.size() : 0;
        }

        /**
         * Set the <code>integer_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setIntegerMap(java.util.Map<Integer,Integer> value) {
            if (value == null) {
                return clearIntegerMap();
            }

            optionals.set(19);
            modified.set(19);
            mIntegerMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>integer_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInIntegerMap(int key, int value) {
            optionals.set(19);
            modified.set(19);
            mutableIntegerMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>integer_map</code> field.
         *
         * @return True if integer_map has been set.
         */
        public boolean isSetIntegerMap() {
            return optionals.get(19);
        }

        /**
         * Checks for presence of the <code>integer_map</code> field.
         *
         * @return True if integer_map is present.
         */
        public boolean hasIntegerMap() {
            return optionals.get(19);
        }

        /**
         * Checks if the <code>integer_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if integer_map has been modified.
         */
        public boolean isModifiedIntegerMap() {
            return modified.get(19);
        }

        /**
         * Clear the <code>integer_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearIntegerMap() {
            optionals.clear(19);
            modified.set(19);
            mIntegerMap = null;
            return this;
        }

        /**
         * @return The mutable <code>integer_map</code> container
         */
        public java.util.Map<Integer,Integer> mutableIntegerMap() {
            optionals.set(19);
            modified.set(19);

            if (mIntegerMap == null) {
                mIntegerMap = new java.util.HashMap<>();
            } else if (!(mIntegerMap instanceof java.util.HashMap)) {
                mIntegerMap = new java.util.HashMap<>(mIntegerMap);
            }
            return mIntegerMap;
        }

        /**
         * @return The <code>integer_map</code> field value
         */
        public java.util.Map<Integer,Integer> getIntegerMap() {
            return mIntegerMap;
        }

        /**
         * @return Optional <code>integer_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Integer,Integer>> optionalIntegerMap() {
            return java.util.Optional.ofNullable(mIntegerMap);
        }

        /**
         * @return Number of entries in <code>integer_map</code>.
         */
        public int numIntegerMap() {
            return mIntegerMap != null ? mIntegerMap.size() : 0;
        }

        /**
         * Set the <code>long_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setLongMap(java.util.Map<Long,Long> value) {
            if (value == null) {
                return clearLongMap();
            }

            optionals.set(20);
            modified.set(20);
            mLongMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>long_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInLongMap(long key, long value) {
            optionals.set(20);
            modified.set(20);
            mutableLongMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>long_map</code> field.
         *
         * @return True if long_map has been set.
         */
        public boolean isSetLongMap() {
            return optionals.get(20);
        }

        /**
         * Checks for presence of the <code>long_map</code> field.
         *
         * @return True if long_map is present.
         */
        public boolean hasLongMap() {
            return optionals.get(20);
        }

        /**
         * Checks if the <code>long_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if long_map has been modified.
         */
        public boolean isModifiedLongMap() {
            return modified.get(20);
        }

        /**
         * Clear the <code>long_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearLongMap() {
            optionals.clear(20);
            modified.set(20);
            mLongMap = null;
            return this;
        }

        /**
         * @return The mutable <code>long_map</code> container
         */
        public java.util.Map<Long,Long> mutableLongMap() {
            optionals.set(20);
            modified.set(20);

            if (mLongMap == null) {
                mLongMap = new java.util.HashMap<>();
            } else if (!(mLongMap instanceof java.util.HashMap)) {
                mLongMap = new java.util.HashMap<>(mLongMap);
            }
            return mLongMap;
        }

        /**
         * @return The <code>long_map</code> field value
         */
        public java.util.Map<Long,Long> getLongMap() {
            return mLongMap;
        }

        /**
         * @return Optional <code>long_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Long,Long>> optionalLongMap() {
            return java.util.Optional.ofNullable(mLongMap);
        }

        /**
         * @return Number of entries in <code>long_map</code>.
         */
        public int numLongMap() {
            return mLongMap != null ? mLongMap.size() : 0;
        }

        /**
         * Set the <code>double_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setDoubleMap(java.util.Map<Double,Double> value) {
            if (value == null) {
                return clearDoubleMap();
            }

            optionals.set(21);
            modified.set(21);
            mDoubleMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>double_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInDoubleMap(double key, double value) {
            optionals.set(21);
            modified.set(21);
            mutableDoubleMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>double_map</code> field.
         *
         * @return True if double_map has been set.
         */
        public boolean isSetDoubleMap() {
            return optionals.get(21);
        }

        /**
         * Checks for presence of the <code>double_map</code> field.
         *
         * @return True if double_map is present.
         */
        public boolean hasDoubleMap() {
            return optionals.get(21);
        }

        /**
         * Checks if the <code>double_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if double_map has been modified.
         */
        public boolean isModifiedDoubleMap() {
            return modified.get(21);
        }

        /**
         * Clear the <code>double_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearDoubleMap() {
            optionals.clear(21);
            modified.set(21);
            mDoubleMap = null;
            return this;
        }

        /**
         * @return The mutable <code>double_map</code> container
         */
        public java.util.Map<Double,Double> mutableDoubleMap() {
            optionals.set(21);
            modified.set(21);

            if (mDoubleMap == null) {
                mDoubleMap = new java.util.HashMap<>();
            } else if (!(mDoubleMap instanceof java.util.HashMap)) {
                mDoubleMap = new java.util.HashMap<>(mDoubleMap);
            }
            return mDoubleMap;
        }

        /**
         * @return The <code>double_map</code> field value
         */
        public java.util.Map<Double,Double> getDoubleMap() {
            return mDoubleMap;
        }

        /**
         * @return Optional <code>double_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Double,Double>> optionalDoubleMap() {
            return java.util.Optional.ofNullable(mDoubleMap);
        }

        /**
         * @return Number of entries in <code>double_map</code>.
         */
        public int numDoubleMap() {
            return mDoubleMap != null ? mDoubleMap.size() : 0;
        }

        /**
         * Set the <code>string_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setStringMap(java.util.Map<String,String> value) {
            if (value == null) {
                return clearStringMap();
            }

            optionals.set(22);
            modified.set(22);
            mStringMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>string_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInStringMap(String key, String value) {
            optionals.set(22);
            modified.set(22);
            mutableStringMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>string_map</code> field.
         *
         * @return True if string_map has been set.
         */
        public boolean isSetStringMap() {
            return optionals.get(22);
        }

        /**
         * Checks for presence of the <code>string_map</code> field.
         *
         * @return True if string_map is present.
         */
        public boolean hasStringMap() {
            return optionals.get(22);
        }

        /**
         * Checks if the <code>string_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if string_map has been modified.
         */
        public boolean isModifiedStringMap() {
            return modified.get(22);
        }

        /**
         * Clear the <code>string_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearStringMap() {
            optionals.clear(22);
            modified.set(22);
            mStringMap = null;
            return this;
        }

        /**
         * @return The mutable <code>string_map</code> container
         */
        public java.util.Map<String,String> mutableStringMap() {
            optionals.set(22);
            modified.set(22);

            if (mStringMap == null) {
                mStringMap = new java.util.HashMap<>();
            } else if (!(mStringMap instanceof java.util.HashMap)) {
                mStringMap = new java.util.HashMap<>(mStringMap);
            }
            return mStringMap;
        }

        /**
         * @return The <code>string_map</code> field value
         */
        public java.util.Map<String,String> getStringMap() {
            return mStringMap;
        }

        /**
         * @return Optional <code>string_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<String,String>> optionalStringMap() {
            return java.util.Optional.ofNullable(mStringMap);
        }

        /**
         * @return Number of entries in <code>string_map</code>.
         */
        public int numStringMap() {
            return mStringMap != null ? mStringMap.size() : 0;
        }

        /**
         * Set the <code>binary_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setBinaryMap(java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> value) {
            if (value == null) {
                return clearBinaryMap();
            }

            optionals.set(23);
            modified.set(23);
            mBinaryMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>binary_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInBinaryMap(net.morimekta.util.Binary key, net.morimekta.util.Binary value) {
            optionals.set(23);
            modified.set(23);
            mutableBinaryMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>binary_map</code> field.
         *
         * @return True if binary_map has been set.
         */
        public boolean isSetBinaryMap() {
            return optionals.get(23);
        }

        /**
         * Checks for presence of the <code>binary_map</code> field.
         *
         * @return True if binary_map is present.
         */
        public boolean hasBinaryMap() {
            return optionals.get(23);
        }

        /**
         * Checks if the <code>binary_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if binary_map has been modified.
         */
        public boolean isModifiedBinaryMap() {
            return modified.get(23);
        }

        /**
         * Clear the <code>binary_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearBinaryMap() {
            optionals.clear(23);
            modified.set(23);
            mBinaryMap = null;
            return this;
        }

        /**
         * @return The mutable <code>binary_map</code> container
         */
        public java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> mutableBinaryMap() {
            optionals.set(23);
            modified.set(23);

            if (mBinaryMap == null) {
                mBinaryMap = new java.util.HashMap<>();
            } else if (!(mBinaryMap instanceof java.util.HashMap)) {
                mBinaryMap = new java.util.HashMap<>(mBinaryMap);
            }
            return mBinaryMap;
        }

        /**
         * @return The <code>binary_map</code> field value
         */
        public java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> getBinaryMap() {
            return mBinaryMap;
        }

        /**
         * @return Optional <code>binary_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary>> optionalBinaryMap() {
            return java.util.Optional.ofNullable(mBinaryMap);
        }

        /**
         * @return Number of entries in <code>binary_map</code>.
         */
        public int numBinaryMap() {
            return mBinaryMap != null ? mBinaryMap.size() : 0;
        }

        /**
         * Set the <code>enum_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setEnumList(java.util.Collection<net.morimekta.test.providence.core.Value> value) {
            if (value == null) {
                return clearEnumList();
            }

            optionals.set(24);
            modified.set(24);
            mEnumList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>enum_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToEnumList(net.morimekta.test.providence.core.Value... values) {
            optionals.set(24);
            modified.set(24);
            java.util.List<net.morimekta.test.providence.core.Value> _container = mutableEnumList();
            for (net.morimekta.test.providence.core.Value item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>enum_list</code> field.
         *
         * @return True if enum_list has been set.
         */
        public boolean isSetEnumList() {
            return optionals.get(24);
        }

        /**
         * Checks for presence of the <code>enum_list</code> field.
         *
         * @return True if enum_list is present.
         */
        public boolean hasEnumList() {
            return optionals.get(24);
        }

        /**
         * Checks if the <code>enum_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if enum_list has been modified.
         */
        public boolean isModifiedEnumList() {
            return modified.get(24);
        }

        /**
         * Clear the <code>enum_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearEnumList() {
            optionals.clear(24);
            modified.set(24);
            mEnumList = null;
            return this;
        }

        /**
         * @return The mutable <code>enum_list</code> container
         */
        public java.util.List<net.morimekta.test.providence.core.Value> mutableEnumList() {
            optionals.set(24);
            modified.set(24);

            if (mEnumList == null) {
                mEnumList = new java.util.ArrayList<>();
            } else if (!(mEnumList instanceof java.util.ArrayList)) {
                mEnumList = new java.util.ArrayList<>(mEnumList);
            }
            return mEnumList;
        }

        /**
         * @return The <code>enum_list</code> field value
         */
        public java.util.List<net.morimekta.test.providence.core.Value> getEnumList() {
            return mEnumList;
        }

        /**
         * @return Optional <code>enum_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<net.morimekta.test.providence.core.Value>> optionalEnumList() {
            return java.util.Optional.ofNullable(mEnumList);
        }

        /**
         * @return Number of entries in <code>enum_list</code>.
         */
        public int numEnumList() {
            return mEnumList != null ? mEnumList.size() : 0;
        }

        /**
         * Set the <code>enum_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setEnumSet(java.util.Collection<net.morimekta.test.providence.core.Value> value) {
            if (value == null) {
                return clearEnumSet();
            }

            optionals.set(25);
            modified.set(25);
            mEnumSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>enum_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToEnumSet(net.morimekta.test.providence.core.Value... values) {
            optionals.set(25);
            modified.set(25);
            java.util.Set<net.morimekta.test.providence.core.Value> _container = mutableEnumSet();
            for (net.morimekta.test.providence.core.Value item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>enum_set</code> field.
         *
         * @return True if enum_set has been set.
         */
        public boolean isSetEnumSet() {
            return optionals.get(25);
        }

        /**
         * Checks for presence of the <code>enum_set</code> field.
         *
         * @return True if enum_set is present.
         */
        public boolean hasEnumSet() {
            return optionals.get(25);
        }

        /**
         * Checks if the <code>enum_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if enum_set has been modified.
         */
        public boolean isModifiedEnumSet() {
            return modified.get(25);
        }

        /**
         * Clear the <code>enum_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearEnumSet() {
            optionals.clear(25);
            modified.set(25);
            mEnumSet = null;
            return this;
        }

        /**
         * @return The mutable <code>enum_set</code> container
         */
        public java.util.Set<net.morimekta.test.providence.core.Value> mutableEnumSet() {
            optionals.set(25);
            modified.set(25);

            if (mEnumSet == null) {
                mEnumSet = new java.util.HashSet<>();
            } else if (!(mEnumSet instanceof java.util.HashSet)) {
                mEnumSet = new java.util.HashSet<>(mEnumSet);
            }
            return mEnumSet;
        }

        /**
         * @return The <code>enum_set</code> field value
         */
        public java.util.Set<net.morimekta.test.providence.core.Value> getEnumSet() {
            return mEnumSet;
        }

        /**
         * @return Optional <code>enum_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.Value>> optionalEnumSet() {
            return java.util.Optional.ofNullable(mEnumSet);
        }

        /**
         * @return Number of entries in <code>enum_set</code>.
         */
        public int numEnumSet() {
            return mEnumSet != null ? mEnumSet.size() : 0;
        }

        /**
         * Set the <code>enum_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setEnumMap(java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> value) {
            if (value == null) {
                return clearEnumMap();
            }

            optionals.set(26);
            modified.set(26);
            mEnumMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>enum_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInEnumMap(net.morimekta.test.providence.core.Value key, net.morimekta.test.providence.core.Value value) {
            optionals.set(26);
            modified.set(26);
            mutableEnumMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>enum_map</code> field.
         *
         * @return True if enum_map has been set.
         */
        public boolean isSetEnumMap() {
            return optionals.get(26);
        }

        /**
         * Checks for presence of the <code>enum_map</code> field.
         *
         * @return True if enum_map is present.
         */
        public boolean hasEnumMap() {
            return optionals.get(26);
        }

        /**
         * Checks if the <code>enum_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if enum_map has been modified.
         */
        public boolean isModifiedEnumMap() {
            return modified.get(26);
        }

        /**
         * Clear the <code>enum_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearEnumMap() {
            optionals.clear(26);
            modified.set(26);
            mEnumMap = null;
            return this;
        }

        /**
         * @return The mutable <code>enum_map</code> container
         */
        public java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> mutableEnumMap() {
            optionals.set(26);
            modified.set(26);

            if (mEnumMap == null) {
                mEnumMap = new java.util.HashMap<>();
            } else if (!(mEnumMap instanceof java.util.HashMap)) {
                mEnumMap = new java.util.HashMap<>(mEnumMap);
            }
            return mEnumMap;
        }

        /**
         * @return The <code>enum_map</code> field value
         */
        public java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> getEnumMap() {
            return mEnumMap;
        }

        /**
         * @return Optional <code>enum_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value>> optionalEnumMap() {
            return java.util.Optional.ofNullable(mEnumMap);
        }

        /**
         * @return Number of entries in <code>enum_map</code>.
         */
        public int numEnumMap() {
            return mEnumMap != null ? mEnumMap.size() : 0;
        }

        /**
         * Set the <code>message_list</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setMessageList(java.util.Collection<net.morimekta.test.providence.core.OptionalFields> value) {
            if (value == null) {
                return clearMessageList();
            }

            optionals.set(27);
            modified.set(27);
            mMessageList = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>message_list</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToMessageList(net.morimekta.test.providence.core.OptionalFields... values) {
            optionals.set(27);
            modified.set(27);
            java.util.List<net.morimekta.test.providence.core.OptionalFields> _container = mutableMessageList();
            for (net.morimekta.test.providence.core.OptionalFields item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>message_list</code> field.
         *
         * @return True if message_list has been set.
         */
        public boolean isSetMessageList() {
            return optionals.get(27);
        }

        /**
         * Checks for presence of the <code>message_list</code> field.
         *
         * @return True if message_list is present.
         */
        public boolean hasMessageList() {
            return optionals.get(27);
        }

        /**
         * Checks if the <code>message_list</code> field has been modified since the
         * builder was created.
         *
         * @return True if message_list has been modified.
         */
        public boolean isModifiedMessageList() {
            return modified.get(27);
        }

        /**
         * Clear the <code>message_list</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearMessageList() {
            optionals.clear(27);
            modified.set(27);
            mMessageList = null;
            return this;
        }

        /**
         * @return The mutable <code>message_list</code> container
         */
        public java.util.List<net.morimekta.test.providence.core.OptionalFields> mutableMessageList() {
            optionals.set(27);
            modified.set(27);

            if (mMessageList == null) {
                mMessageList = new java.util.ArrayList<>();
            } else if (!(mMessageList instanceof java.util.ArrayList)) {
                mMessageList = new java.util.ArrayList<>(mMessageList);
            }
            return mMessageList;
        }

        /**
         * @return The <code>message_list</code> field value
         */
        public java.util.List<net.morimekta.test.providence.core.OptionalFields> getMessageList() {
            return mMessageList;
        }

        /**
         * @return Optional <code>message_list</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<net.morimekta.test.providence.core.OptionalFields>> optionalMessageList() {
            return java.util.Optional.ofNullable(mMessageList);
        }

        /**
         * @return Number of entries in <code>message_list</code>.
         */
        public int numMessageList() {
            return mMessageList != null ? mMessageList.size() : 0;
        }

        /**
         * Set the <code>message_set</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setMessageSet(java.util.Collection<net.morimekta.test.providence.core.OptionalFields> value) {
            if (value == null) {
                return clearMessageSet();
            }

            optionals.set(28);
            modified.set(28);
            mMessageSet = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>message_set</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder addToMessageSet(net.morimekta.test.providence.core.OptionalFields... values) {
            optionals.set(28);
            modified.set(28);
            java.util.Set<net.morimekta.test.providence.core.OptionalFields> _container = mutableMessageSet();
            for (net.morimekta.test.providence.core.OptionalFields item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>message_set</code> field.
         *
         * @return True if message_set has been set.
         */
        public boolean isSetMessageSet() {
            return optionals.get(28);
        }

        /**
         * Checks for presence of the <code>message_set</code> field.
         *
         * @return True if message_set is present.
         */
        public boolean hasMessageSet() {
            return optionals.get(28);
        }

        /**
         * Checks if the <code>message_set</code> field has been modified since the
         * builder was created.
         *
         * @return True if message_set has been modified.
         */
        public boolean isModifiedMessageSet() {
            return modified.get(28);
        }

        /**
         * Clear the <code>message_set</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearMessageSet() {
            optionals.clear(28);
            modified.set(28);
            mMessageSet = null;
            return this;
        }

        /**
         * @return The mutable <code>message_set</code> container
         */
        public java.util.Set<net.morimekta.test.providence.core.OptionalFields> mutableMessageSet() {
            optionals.set(28);
            modified.set(28);

            if (mMessageSet == null) {
                mMessageSet = new java.util.HashSet<>();
            } else if (!(mMessageSet instanceof java.util.HashSet)) {
                mMessageSet = new java.util.HashSet<>(mMessageSet);
            }
            return mMessageSet;
        }

        /**
         * @return The <code>message_set</code> field value
         */
        public java.util.Set<net.morimekta.test.providence.core.OptionalFields> getMessageSet() {
            return mMessageSet;
        }

        /**
         * @return Optional <code>message_set</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<net.morimekta.test.providence.core.OptionalFields>> optionalMessageSet() {
            return java.util.Optional.ofNullable(mMessageSet);
        }

        /**
         * @return Number of entries in <code>message_set</code>.
         */
        public int numMessageSet() {
            return mMessageSet != null ? mMessageSet.size() : 0;
        }

        /**
         * Set the <code>message_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setMessageMap(java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> value) {
            if (value == null) {
                return clearMessageMap();
            }

            optionals.set(29);
            modified.set(29);
            mMessageMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>message_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInMessageMap(String key, net.morimekta.test.providence.core.OptionalFields value) {
            optionals.set(29);
            modified.set(29);
            mutableMessageMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>message_map</code> field.
         *
         * @return True if message_map has been set.
         */
        public boolean isSetMessageMap() {
            return optionals.get(29);
        }

        /**
         * Checks for presence of the <code>message_map</code> field.
         *
         * @return True if message_map is present.
         */
        public boolean hasMessageMap() {
            return optionals.get(29);
        }

        /**
         * Checks if the <code>message_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if message_map has been modified.
         */
        public boolean isModifiedMessageMap() {
            return modified.get(29);
        }

        /**
         * Clear the <code>message_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearMessageMap() {
            optionals.clear(29);
            modified.set(29);
            mMessageMap = null;
            return this;
        }

        /**
         * @return The mutable <code>message_map</code> container
         */
        public java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> mutableMessageMap() {
            optionals.set(29);
            modified.set(29);

            if (mMessageMap == null) {
                mMessageMap = new java.util.HashMap<>();
            } else if (!(mMessageMap instanceof java.util.HashMap)) {
                mMessageMap = new java.util.HashMap<>(mMessageMap);
            }
            return mMessageMap;
        }

        /**
         * @return The <code>message_map</code> field value
         */
        public java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> getMessageMap() {
            return mMessageMap;
        }

        /**
         * @return Optional <code>message_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<String,net.morimekta.test.providence.core.OptionalFields>> optionalMessageMap() {
            return java.util.Optional.ofNullable(mMessageMap);
        }

        /**
         * @return Number of entries in <code>message_map</code>.
         */
        public int numMessageMap() {
            return mMessageMap != null ? mMessageMap.size() : 0;
        }

        /**
         * Set the <code>message_key_map</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setMessageKeyMap(java.util.Map<net.morimekta.test.providence.core.CompactFields,String> value) {
            if (value == null) {
                return clearMessageKeyMap();
            }

            optionals.set(30);
            modified.set(30);
            mMessageKeyMap = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>message_key_map</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInMessageKeyMap(net.morimekta.test.providence.core.CompactFields key, String value) {
            optionals.set(30);
            modified.set(30);
            mutableMessageKeyMap().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>message_key_map</code> field.
         *
         * @return True if message_key_map has been set.
         */
        public boolean isSetMessageKeyMap() {
            return optionals.get(30);
        }

        /**
         * Checks for presence of the <code>message_key_map</code> field.
         *
         * @return True if message_key_map is present.
         */
        public boolean hasMessageKeyMap() {
            return optionals.get(30);
        }

        /**
         * Checks if the <code>message_key_map</code> field has been modified since the
         * builder was created.
         *
         * @return True if message_key_map has been modified.
         */
        public boolean isModifiedMessageKeyMap() {
            return modified.get(30);
        }

        /**
         * Clear the <code>message_key_map</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearMessageKeyMap() {
            optionals.clear(30);
            modified.set(30);
            mMessageKeyMap = null;
            return this;
        }

        /**
         * @return The mutable <code>message_key_map</code> container
         */
        public java.util.Map<net.morimekta.test.providence.core.CompactFields,String> mutableMessageKeyMap() {
            optionals.set(30);
            modified.set(30);

            if (mMessageKeyMap == null) {
                mMessageKeyMap = new java.util.HashMap<>();
            } else if (!(mMessageKeyMap instanceof java.util.HashMap)) {
                mMessageKeyMap = new java.util.HashMap<>(mMessageKeyMap);
            }
            return mMessageKeyMap;
        }

        /**
         * @return The <code>message_key_map</code> field value
         */
        public java.util.Map<net.morimekta.test.providence.core.CompactFields,String> getMessageKeyMap() {
            return mMessageKeyMap;
        }

        /**
         * @return Optional <code>message_key_map</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<net.morimekta.test.providence.core.CompactFields,String>> optionalMessageKeyMap() {
            return java.util.Optional.ofNullable(mMessageKeyMap);
        }

        /**
         * @return Number of entries in <code>message_key_map</code>.
         */
        public int numMessageKeyMap() {
            return mMessageKeyMap != null ? mMessageKeyMap.size() : 0;
        }

        /**
         * Set the <code>required_fields</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setRequiredFields(net.morimekta.test.providence.core.RequiredFields_OrBuilder value) {
            if (value == null) {
                return clearRequiredFields();
            }

            optionals.set(31);
            modified.set(31);
            if (value instanceof net.morimekta.test.providence.core.RequiredFields._Builder) {
                value = ((net.morimekta.test.providence.core.RequiredFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.RequiredFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.RequiredFields: " + value.getClass().getName());
            }
            mRequiredFields = (net.morimekta.test.providence.core.RequiredFields) value;
            mRequiredFields_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>required_fields</code> field.
         *
         * @return True if required_fields has been set.
         */
        public boolean isSetRequiredFields() {
            return optionals.get(31);
        }

        /**
         * Checks for presence of the <code>required_fields</code> field.
         *
         * @return True if required_fields is present.
         */
        public boolean hasRequiredFields() {
            return optionals.get(31);
        }

        /**
         * Checks if the <code>required_fields</code> field has been modified since the
         * builder was created.
         *
         * @return True if required_fields has been modified.
         */
        public boolean isModifiedRequiredFields() {
            return modified.get(31);
        }

        /**
         * Clear the <code>required_fields</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearRequiredFields() {
            optionals.clear(31);
            modified.set(31);
            mRequiredFields = null;
            mRequiredFields_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>required_fields</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.RequiredFields._Builder mutableRequiredFields() {
            optionals.set(31);
            modified.set(31);

            if (mRequiredFields != null) {
                mRequiredFields_builder = mRequiredFields.mutate();
                mRequiredFields = null;
            } else if (mRequiredFields_builder == null) {
                mRequiredFields_builder = net.morimekta.test.providence.core.RequiredFields.builder();
            }
            return mRequiredFields_builder;
        }

        /**
         * @return The <code>required_fields</code> field value
         */
        public net.morimekta.test.providence.core.RequiredFields getRequiredFields() {
            return mRequiredFields_builder != null ? mRequiredFields_builder.build() : mRequiredFields;
        }

        /**
         * @return Optional <code>required_fields</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.RequiredFields> optionalRequiredFields() {
            return java.util.Optional.ofNullable(mRequiredFields_builder != null ? mRequiredFields_builder.build() : mRequiredFields);
        }

        /**
         * Set the <code>default_fields</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setDefaultFields(net.morimekta.test.providence.core.DefaultFields_OrBuilder value) {
            if (value == null) {
                return clearDefaultFields();
            }

            optionals.set(32);
            modified.set(32);
            if (value instanceof net.morimekta.test.providence.core.DefaultFields._Builder) {
                value = ((net.morimekta.test.providence.core.DefaultFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.DefaultFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.DefaultFields: " + value.getClass().getName());
            }
            mDefaultFields = (net.morimekta.test.providence.core.DefaultFields) value;
            mDefaultFields_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>default_fields</code> field.
         *
         * @return True if default_fields has been set.
         */
        public boolean isSetDefaultFields() {
            return optionals.get(32);
        }

        /**
         * Checks for presence of the <code>default_fields</code> field.
         *
         * @return True if default_fields is present.
         */
        public boolean hasDefaultFields() {
            return optionals.get(32);
        }

        /**
         * Checks if the <code>default_fields</code> field has been modified since the
         * builder was created.
         *
         * @return True if default_fields has been modified.
         */
        public boolean isModifiedDefaultFields() {
            return modified.get(32);
        }

        /**
         * Clear the <code>default_fields</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearDefaultFields() {
            optionals.clear(32);
            modified.set(32);
            mDefaultFields = null;
            mDefaultFields_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>default_fields</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.DefaultFields._Builder mutableDefaultFields() {
            optionals.set(32);
            modified.set(32);

            if (mDefaultFields != null) {
                mDefaultFields_builder = mDefaultFields.mutate();
                mDefaultFields = null;
            } else if (mDefaultFields_builder == null) {
                mDefaultFields_builder = net.morimekta.test.providence.core.DefaultFields.builder();
            }
            return mDefaultFields_builder;
        }

        /**
         * @return The <code>default_fields</code> field value
         */
        public net.morimekta.test.providence.core.DefaultFields getDefaultFields() {
            return mDefaultFields_builder != null ? mDefaultFields_builder.build() : mDefaultFields;
        }

        /**
         * @return Optional <code>default_fields</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.DefaultFields> optionalDefaultFields() {
            return java.util.Optional.ofNullable(mDefaultFields_builder != null ? mDefaultFields_builder.build() : mDefaultFields);
        }

        /**
         * Set the <code>optional_fields</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setOptionalFields(net.morimekta.test.providence.core.OptionalFields_OrBuilder value) {
            if (value == null) {
                return clearOptionalFields();
            }

            optionals.set(33);
            modified.set(33);
            if (value instanceof net.morimekta.test.providence.core.OptionalFields._Builder) {
                value = ((net.morimekta.test.providence.core.OptionalFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.OptionalFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.OptionalFields: " + value.getClass().getName());
            }
            mOptionalFields = (net.morimekta.test.providence.core.OptionalFields) value;
            mOptionalFields_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>optional_fields</code> field.
         *
         * @return True if optional_fields has been set.
         */
        public boolean isSetOptionalFields() {
            return optionals.get(33);
        }

        /**
         * Checks for presence of the <code>optional_fields</code> field.
         *
         * @return True if optional_fields is present.
         */
        public boolean hasOptionalFields() {
            return optionals.get(33);
        }

        /**
         * Checks if the <code>optional_fields</code> field has been modified since the
         * builder was created.
         *
         * @return True if optional_fields has been modified.
         */
        public boolean isModifiedOptionalFields() {
            return modified.get(33);
        }

        /**
         * Clear the <code>optional_fields</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearOptionalFields() {
            optionals.clear(33);
            modified.set(33);
            mOptionalFields = null;
            mOptionalFields_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>optional_fields</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.OptionalFields._Builder mutableOptionalFields() {
            optionals.set(33);
            modified.set(33);

            if (mOptionalFields != null) {
                mOptionalFields_builder = mOptionalFields.mutate();
                mOptionalFields = null;
            } else if (mOptionalFields_builder == null) {
                mOptionalFields_builder = net.morimekta.test.providence.core.OptionalFields.builder();
            }
            return mOptionalFields_builder;
        }

        /**
         * @return The <code>optional_fields</code> field value
         */
        public net.morimekta.test.providence.core.OptionalFields getOptionalFields() {
            return mOptionalFields_builder != null ? mOptionalFields_builder.build() : mOptionalFields;
        }

        /**
         * @return Optional <code>optional_fields</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOptionalFields() {
            return java.util.Optional.ofNullable(mOptionalFields_builder != null ? mOptionalFields_builder.build() : mOptionalFields);
        }

        /**
         * Set the <code>union_fields</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setUnionFields(net.morimekta.test.providence.core.UnionFields_OrBuilder value) {
            if (value == null) {
                return clearUnionFields();
            }

            optionals.set(34);
            modified.set(34);
            if (value instanceof net.morimekta.test.providence.core.UnionFields._Builder) {
                value = ((net.morimekta.test.providence.core.UnionFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.UnionFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.UnionFields: " + value.getClass().getName());
            }
            mUnionFields = (net.morimekta.test.providence.core.UnionFields) value;
            mUnionFields_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>union_fields</code> field.
         *
         * @return True if union_fields has been set.
         */
        public boolean isSetUnionFields() {
            return optionals.get(34);
        }

        /**
         * Checks for presence of the <code>union_fields</code> field.
         *
         * @return True if union_fields is present.
         */
        public boolean hasUnionFields() {
            return optionals.get(34);
        }

        /**
         * Checks if the <code>union_fields</code> field has been modified since the
         * builder was created.
         *
         * @return True if union_fields has been modified.
         */
        public boolean isModifiedUnionFields() {
            return modified.get(34);
        }

        /**
         * Clear the <code>union_fields</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearUnionFields() {
            optionals.clear(34);
            modified.set(34);
            mUnionFields = null;
            mUnionFields_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>union_fields</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.UnionFields._Builder mutableUnionFields() {
            optionals.set(34);
            modified.set(34);

            if (mUnionFields != null) {
                mUnionFields_builder = mUnionFields.mutate();
                mUnionFields = null;
            } else if (mUnionFields_builder == null) {
                mUnionFields_builder = net.morimekta.test.providence.core.UnionFields.builder();
            }
            return mUnionFields_builder;
        }

        /**
         * @return The <code>union_fields</code> field value
         */
        public net.morimekta.test.providence.core.UnionFields getUnionFields() {
            return mUnionFields_builder != null ? mUnionFields_builder.build() : mUnionFields;
        }

        /**
         * @return Optional <code>union_fields</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.UnionFields> optionalUnionFields() {
            return java.util.Optional.ofNullable(mUnionFields_builder != null ? mUnionFields_builder.build() : mUnionFields);
        }

        /**
         * Set the <code>exception_fields</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setExceptionFields(net.morimekta.test.providence.core.ExceptionFields_OrBuilder value) {
            if (value == null) {
                return clearExceptionFields();
            }

            optionals.set(35);
            modified.set(35);
            if (value instanceof net.morimekta.test.providence.core.ExceptionFields._Builder) {
                value = ((net.morimekta.test.providence.core.ExceptionFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.ExceptionFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.ExceptionFields: " + value.getClass().getName());
            }
            mExceptionFields = (net.morimekta.test.providence.core.ExceptionFields) value;
            mExceptionFields_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>exception_fields</code> field.
         *
         * @return True if exception_fields has been set.
         */
        public boolean isSetExceptionFields() {
            return optionals.get(35);
        }

        /**
         * Checks for presence of the <code>exception_fields</code> field.
         *
         * @return True if exception_fields is present.
         */
        public boolean hasExceptionFields() {
            return optionals.get(35);
        }

        /**
         * Checks if the <code>exception_fields</code> field has been modified since the
         * builder was created.
         *
         * @return True if exception_fields has been modified.
         */
        public boolean isModifiedExceptionFields() {
            return modified.get(35);
        }

        /**
         * Clear the <code>exception_fields</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearExceptionFields() {
            optionals.clear(35);
            modified.set(35);
            mExceptionFields = null;
            mExceptionFields_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>exception_fields</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.ExceptionFields._Builder mutableExceptionFields() {
            optionals.set(35);
            modified.set(35);

            if (mExceptionFields != null) {
                mExceptionFields_builder = mExceptionFields.mutate();
                mExceptionFields = null;
            } else if (mExceptionFields_builder == null) {
                mExceptionFields_builder = net.morimekta.test.providence.core.ExceptionFields.builder();
            }
            return mExceptionFields_builder;
        }

        /**
         * @return The <code>exception_fields</code> field value
         */
        public net.morimekta.test.providence.core.ExceptionFields getExceptionFields() {
            return mExceptionFields_builder != null ? mExceptionFields_builder.build() : mExceptionFields;
        }

        /**
         * @return Optional <code>exception_fields</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.ExceptionFields> optionalExceptionFields() {
            return java.util.Optional.ofNullable(mExceptionFields_builder != null ? mExceptionFields_builder.build() : mExceptionFields);
        }

        /**
         * Set the <code>default_values</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setDefaultValues(net.morimekta.test.providence.core.DefaultValues_OrBuilder value) {
            if (value == null) {
                return clearDefaultValues();
            }

            optionals.set(36);
            modified.set(36);
            if (value instanceof net.morimekta.test.providence.core.DefaultValues._Builder) {
                value = ((net.morimekta.test.providence.core.DefaultValues._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.DefaultValues)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.DefaultValues: " + value.getClass().getName());
            }
            mDefaultValues = (net.morimekta.test.providence.core.DefaultValues) value;
            mDefaultValues_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>default_values</code> field.
         *
         * @return True if default_values has been set.
         */
        public boolean isSetDefaultValues() {
            return optionals.get(36);
        }

        /**
         * Checks for presence of the <code>default_values</code> field.
         *
         * @return True if default_values is present.
         */
        public boolean hasDefaultValues() {
            return optionals.get(36);
        }

        /**
         * Checks if the <code>default_values</code> field has been modified since the
         * builder was created.
         *
         * @return True if default_values has been modified.
         */
        public boolean isModifiedDefaultValues() {
            return modified.get(36);
        }

        /**
         * Clear the <code>default_values</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearDefaultValues() {
            optionals.clear(36);
            modified.set(36);
            mDefaultValues = null;
            mDefaultValues_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>default_values</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.DefaultValues._Builder mutableDefaultValues() {
            optionals.set(36);
            modified.set(36);

            if (mDefaultValues != null) {
                mDefaultValues_builder = mDefaultValues.mutate();
                mDefaultValues = null;
            } else if (mDefaultValues_builder == null) {
                mDefaultValues_builder = net.morimekta.test.providence.core.DefaultValues.builder();
            }
            return mDefaultValues_builder;
        }

        /**
         * @return The <code>default_values</code> field value
         */
        public net.morimekta.test.providence.core.DefaultValues getDefaultValues() {
            return mDefaultValues_builder != null ? mDefaultValues_builder.build() : mDefaultValues;
        }

        /**
         * @return Optional <code>default_values</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.DefaultValues> optionalDefaultValues() {
            return java.util.Optional.ofNullable(mDefaultValues_builder != null ? mDefaultValues_builder.build() : mDefaultValues);
        }

        /**
         * Set the <code>map_list_compact</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setMapListCompact(java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> value) {
            if (value == null) {
                return clearMapListCompact();
            }

            optionals.set(37);
            modified.set(37);
            mMapListCompact = net.morimekta.util.collect.UnmodifiableMap.copyOf(value);
            return this;
        }

        /**
         * Adds a mapping to the <code>map_list_compact</code> map.
         *
         * @param key The inserted key
         * @param value The inserted value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder putInMapListCompact(int key, java.util.List<net.morimekta.test.providence.core.CompactFields> value) {
            optionals.set(37);
            modified.set(37);
            mutableMapListCompact().put(key, value);
            return this;
        }

        /**
         * Checks for explicit presence of the <code>map_list_compact</code> field.
         *
         * @return True if map_list_compact has been set.
         */
        public boolean isSetMapListCompact() {
            return optionals.get(37);
        }

        /**
         * Checks for presence of the <code>map_list_compact</code> field.
         *
         * @return True if map_list_compact is present.
         */
        public boolean hasMapListCompact() {
            return optionals.get(37);
        }

        /**
         * Checks if the <code>map_list_compact</code> field has been modified since the
         * builder was created.
         *
         * @return True if map_list_compact has been modified.
         */
        public boolean isModifiedMapListCompact() {
            return modified.get(37);
        }

        /**
         * Clear the <code>map_list_compact</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearMapListCompact() {
            optionals.clear(37);
            modified.set(37);
            mMapListCompact = null;
            return this;
        }

        /**
         * @return The mutable <code>map_list_compact</code> container
         */
        public java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> mutableMapListCompact() {
            optionals.set(37);
            modified.set(37);

            if (mMapListCompact == null) {
                mMapListCompact = new java.util.HashMap<>();
            } else if (!(mMapListCompact instanceof java.util.HashMap)) {
                mMapListCompact = new java.util.HashMap<>(mMapListCompact);
            }
            return mMapListCompact;
        }

        /**
         * @return The <code>map_list_compact</code> field value
         */
        public java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> getMapListCompact() {
            return mMapListCompact;
        }

        /**
         * @return Optional <code>map_list_compact</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>>> optionalMapListCompact() {
            return java.util.Optional.ofNullable(mMapListCompact);
        }

        /**
         * @return Number of entries in <code>map_list_compact</code>.
         */
        public int numMapListCompact() {
            return mMapListCompact != null ? mMapListCompact.size() : 0;
        }

        /**
         * Set the <code>list_map_numbers</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setListMapNumbers(java.util.Collection<java.util.Map<Integer,Integer>> value) {
            if (value == null) {
                return clearListMapNumbers();
            }

            optionals.set(38);
            modified.set(38);
            mListMapNumbers = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>list_map_numbers</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        @SuppressWarnings("unchecked")
        @SafeVarargs
        public final Containers._Builder addToListMapNumbers(java.util.Map<Integer,Integer>... values) {
            optionals.set(38);
            modified.set(38);
            java.util.List<java.util.Map<Integer,Integer>> _container = mutableListMapNumbers();
            for (java.util.Map<Integer,Integer> item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>list_map_numbers</code> field.
         *
         * @return True if list_map_numbers has been set.
         */
        public boolean isSetListMapNumbers() {
            return optionals.get(38);
        }

        /**
         * Checks for presence of the <code>list_map_numbers</code> field.
         *
         * @return True if list_map_numbers is present.
         */
        public boolean hasListMapNumbers() {
            return optionals.get(38);
        }

        /**
         * Checks if the <code>list_map_numbers</code> field has been modified since the
         * builder was created.
         *
         * @return True if list_map_numbers has been modified.
         */
        public boolean isModifiedListMapNumbers() {
            return modified.get(38);
        }

        /**
         * Clear the <code>list_map_numbers</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearListMapNumbers() {
            optionals.clear(38);
            modified.set(38);
            mListMapNumbers = null;
            return this;
        }

        /**
         * @return The mutable <code>list_map_numbers</code> container
         */
        public java.util.List<java.util.Map<Integer,Integer>> mutableListMapNumbers() {
            optionals.set(38);
            modified.set(38);

            if (mListMapNumbers == null) {
                mListMapNumbers = new java.util.ArrayList<>();
            } else if (!(mListMapNumbers instanceof java.util.ArrayList)) {
                mListMapNumbers = new java.util.ArrayList<>(mListMapNumbers);
            }
            return mListMapNumbers;
        }

        /**
         * @return The <code>list_map_numbers</code> field value
         */
        public java.util.List<java.util.Map<Integer,Integer>> getListMapNumbers() {
            return mListMapNumbers;
        }

        /**
         * @return Optional <code>list_map_numbers</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<java.util.Map<Integer,Integer>>> optionalListMapNumbers() {
            return java.util.Optional.ofNullable(mListMapNumbers);
        }

        /**
         * @return Number of entries in <code>list_map_numbers</code>.
         */
        public int numListMapNumbers() {
            return mListMapNumbers != null ? mListMapNumbers.size() : 0;
        }

        /**
         * Set the <code>set_list_numbers</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setSetListNumbers(java.util.Collection<java.util.List<Integer>> value) {
            if (value == null) {
                return clearSetListNumbers();
            }

            optionals.set(39);
            modified.set(39);
            mSetListNumbers = net.morimekta.util.collect.UnmodifiableSet.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>set_list_numbers</code> set.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        @SuppressWarnings("unchecked")
        @SafeVarargs
        public final Containers._Builder addToSetListNumbers(java.util.List<Integer>... values) {
            optionals.set(39);
            modified.set(39);
            java.util.Set<java.util.List<Integer>> _container = mutableSetListNumbers();
            for (java.util.List<Integer> item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>set_list_numbers</code> field.
         *
         * @return True if set_list_numbers has been set.
         */
        public boolean isSetSetListNumbers() {
            return optionals.get(39);
        }

        /**
         * Checks for presence of the <code>set_list_numbers</code> field.
         *
         * @return True if set_list_numbers is present.
         */
        public boolean hasSetListNumbers() {
            return optionals.get(39);
        }

        /**
         * Checks if the <code>set_list_numbers</code> field has been modified since the
         * builder was created.
         *
         * @return True if set_list_numbers has been modified.
         */
        public boolean isModifiedSetListNumbers() {
            return modified.get(39);
        }

        /**
         * Clear the <code>set_list_numbers</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearSetListNumbers() {
            optionals.clear(39);
            modified.set(39);
            mSetListNumbers = null;
            return this;
        }

        /**
         * @return The mutable <code>set_list_numbers</code> container
         */
        public java.util.Set<java.util.List<Integer>> mutableSetListNumbers() {
            optionals.set(39);
            modified.set(39);

            if (mSetListNumbers == null) {
                mSetListNumbers = new java.util.HashSet<>();
            } else if (!(mSetListNumbers instanceof java.util.HashSet)) {
                mSetListNumbers = new java.util.HashSet<>(mSetListNumbers);
            }
            return mSetListNumbers;
        }

        /**
         * @return The <code>set_list_numbers</code> field value
         */
        public java.util.Set<java.util.List<Integer>> getSetListNumbers() {
            return mSetListNumbers;
        }

        /**
         * @return Optional <code>set_list_numbers</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.Set<java.util.List<Integer>>> optionalSetListNumbers() {
            return java.util.Optional.ofNullable(mSetListNumbers);
        }

        /**
         * @return Number of entries in <code>set_list_numbers</code>.
         */
        public int numSetListNumbers() {
            return mSetListNumbers != null ? mSetListNumbers.size() : 0;
        }

        /**
         * Set the <code>list_list_numbers</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder setListListNumbers(java.util.Collection<java.util.List<Integer>> value) {
            if (value == null) {
                return clearListListNumbers();
            }

            optionals.set(40);
            modified.set(40);
            mListListNumbers = net.morimekta.util.collect.UnmodifiableList.copyOf(value);
            return this;
        }

        /**
         * Adds entries to the <code>list_list_numbers</code> list.
         *
         * @param values The added value
         * @return The builder
         */
        @javax.annotation.Nonnull
        @SuppressWarnings("unchecked")
        @SafeVarargs
        public final Containers._Builder addToListListNumbers(java.util.List<Integer>... values) {
            optionals.set(40);
            modified.set(40);
            java.util.List<java.util.List<Integer>> _container = mutableListListNumbers();
            for (java.util.List<Integer> item : values) {
                _container.add(item);
            }
            return this;
        }

        /**
         * Checks for explicit presence of the <code>list_list_numbers</code> field.
         *
         * @return True if list_list_numbers has been set.
         */
        public boolean isSetListListNumbers() {
            return optionals.get(40);
        }

        /**
         * Checks for presence of the <code>list_list_numbers</code> field.
         *
         * @return True if list_list_numbers is present.
         */
        public boolean hasListListNumbers() {
            return optionals.get(40);
        }

        /**
         * Checks if the <code>list_list_numbers</code> field has been modified since the
         * builder was created.
         *
         * @return True if list_list_numbers has been modified.
         */
        public boolean isModifiedListListNumbers() {
            return modified.get(40);
        }

        /**
         * Clear the <code>list_list_numbers</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public Containers._Builder clearListListNumbers() {
            optionals.clear(40);
            modified.set(40);
            mListListNumbers = null;
            return this;
        }

        /**
         * @return The mutable <code>list_list_numbers</code> container
         */
        public java.util.List<java.util.List<Integer>> mutableListListNumbers() {
            optionals.set(40);
            modified.set(40);

            if (mListListNumbers == null) {
                mListListNumbers = new java.util.ArrayList<>();
            } else if (!(mListListNumbers instanceof java.util.ArrayList)) {
                mListListNumbers = new java.util.ArrayList<>(mListListNumbers);
            }
            return mListListNumbers;
        }

        /**
         * @return The <code>list_list_numbers</code> field value
         */
        public java.util.List<java.util.List<Integer>> getListListNumbers() {
            return mListListNumbers;
        }

        /**
         * @return Optional <code>list_list_numbers</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<java.util.List<java.util.List<Integer>>> optionalListListNumbers() {
            return java.util.Optional.ofNullable(mListListNumbers);
        }

        /**
         * @return Number of entries in <code>list_list_numbers</code>.
         */
        public int numListListNumbers() {
            return mListListNumbers != null ? mListListNumbers.size() : 0;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            Containers._Builder other = (Containers._Builder) o;
            return java.util.Objects.equals(optionals, other.optionals) &&
                   java.util.Objects.equals(mBooleanList, other.mBooleanList) &&
                   java.util.Objects.equals(mByteList, other.mByteList) &&
                   java.util.Objects.equals(mShortList, other.mShortList) &&
                   java.util.Objects.equals(mIntegerList, other.mIntegerList) &&
                   java.util.Objects.equals(mLongList, other.mLongList) &&
                   java.util.Objects.equals(mDoubleList, other.mDoubleList) &&
                   java.util.Objects.equals(mStringList, other.mStringList) &&
                   java.util.Objects.equals(mBinaryList, other.mBinaryList) &&
                   java.util.Objects.equals(mBooleanSet, other.mBooleanSet) &&
                   java.util.Objects.equals(mByteSet, other.mByteSet) &&
                   java.util.Objects.equals(mShortSet, other.mShortSet) &&
                   java.util.Objects.equals(mIntegerSet, other.mIntegerSet) &&
                   java.util.Objects.equals(mLongSet, other.mLongSet) &&
                   java.util.Objects.equals(mDoubleSet, other.mDoubleSet) &&
                   java.util.Objects.equals(mStringSet, other.mStringSet) &&
                   java.util.Objects.equals(mBinarySet, other.mBinarySet) &&
                   java.util.Objects.equals(mBooleanMap, other.mBooleanMap) &&
                   java.util.Objects.equals(mByteMap, other.mByteMap) &&
                   java.util.Objects.equals(mShortMap, other.mShortMap) &&
                   java.util.Objects.equals(mIntegerMap, other.mIntegerMap) &&
                   java.util.Objects.equals(mLongMap, other.mLongMap) &&
                   java.util.Objects.equals(mDoubleMap, other.mDoubleMap) &&
                   java.util.Objects.equals(mStringMap, other.mStringMap) &&
                   java.util.Objects.equals(mBinaryMap, other.mBinaryMap) &&
                   java.util.Objects.equals(mEnumList, other.mEnumList) &&
                   java.util.Objects.equals(mEnumSet, other.mEnumSet) &&
                   java.util.Objects.equals(mEnumMap, other.mEnumMap) &&
                   java.util.Objects.equals(mMessageList, other.mMessageList) &&
                   java.util.Objects.equals(mMessageSet, other.mMessageSet) &&
                   java.util.Objects.equals(mMessageMap, other.mMessageMap) &&
                   java.util.Objects.equals(mMessageKeyMap, other.mMessageKeyMap) &&
                   java.util.Objects.equals(getRequiredFields(), other.getRequiredFields()) &&
                   java.util.Objects.equals(getDefaultFields(), other.getDefaultFields()) &&
                   java.util.Objects.equals(getOptionalFields(), other.getOptionalFields()) &&
                   java.util.Objects.equals(getUnionFields(), other.getUnionFields()) &&
                   java.util.Objects.equals(getExceptionFields(), other.getExceptionFields()) &&
                   java.util.Objects.equals(getDefaultValues(), other.getDefaultValues()) &&
                   java.util.Objects.equals(mMapListCompact, other.mMapListCompact) &&
                   java.util.Objects.equals(mListMapNumbers, other.mListMapNumbers) &&
                   java.util.Objects.equals(mSetListNumbers, other.mSetListNumbers) &&
                   java.util.Objects.equals(mListListNumbers, other.mListListNumbers);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    Containers.class, optionals,
                    Containers._Field.BOOLEAN_LIST, mBooleanList,
                    Containers._Field.BYTE_LIST, mByteList,
                    Containers._Field.SHORT_LIST, mShortList,
                    Containers._Field.INTEGER_LIST, mIntegerList,
                    Containers._Field.LONG_LIST, mLongList,
                    Containers._Field.DOUBLE_LIST, mDoubleList,
                    Containers._Field.STRING_LIST, mStringList,
                    Containers._Field.BINARY_LIST, mBinaryList,
                    Containers._Field.BOOLEAN_SET, mBooleanSet,
                    Containers._Field.BYTE_SET, mByteSet,
                    Containers._Field.SHORT_SET, mShortSet,
                    Containers._Field.INTEGER_SET, mIntegerSet,
                    Containers._Field.LONG_SET, mLongSet,
                    Containers._Field.DOUBLE_SET, mDoubleSet,
                    Containers._Field.STRING_SET, mStringSet,
                    Containers._Field.BINARY_SET, mBinarySet,
                    Containers._Field.BOOLEAN_MAP, mBooleanMap,
                    Containers._Field.BYTE_MAP, mByteMap,
                    Containers._Field.SHORT_MAP, mShortMap,
                    Containers._Field.INTEGER_MAP, mIntegerMap,
                    Containers._Field.LONG_MAP, mLongMap,
                    Containers._Field.DOUBLE_MAP, mDoubleMap,
                    Containers._Field.STRING_MAP, mStringMap,
                    Containers._Field.BINARY_MAP, mBinaryMap,
                    Containers._Field.ENUM_LIST, mEnumList,
                    Containers._Field.ENUM_SET, mEnumSet,
                    Containers._Field.ENUM_MAP, mEnumMap,
                    Containers._Field.MESSAGE_LIST, mMessageList,
                    Containers._Field.MESSAGE_SET, mMessageSet,
                    Containers._Field.MESSAGE_MAP, mMessageMap,
                    Containers._Field.MESSAGE_KEY_MAP, mMessageKeyMap,
                    Containers._Field.REQUIRED_FIELDS, getRequiredFields(),
                    Containers._Field.DEFAULT_FIELDS, getDefaultFields(),
                    Containers._Field.OPTIONAL_FIELDS, getOptionalFields(),
                    Containers._Field.UNION_FIELDS, getUnionFields(),
                    Containers._Field.EXCEPTION_FIELDS, getExceptionFields(),
                    Containers._Field.DEFAULT_VALUES, getDefaultValues(),
                    Containers._Field.MAP_LIST_COMPACT, mMapListCompact,
                    Containers._Field.LIST_MAP_NUMBERS, mListMapNumbers,
                    Containers._Field.SET_LIST_NUMBERS, mSetListNumbers,
                    Containers._Field.LIST_LIST_NUMBERS, mListListNumbers);
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                case 51: return mutableRequiredFields();
                case 52: return mutableDefaultFields();
                case 53: return mutableOptionalFields();
                case 54: return mutableUnionFields();
                case 55: return mutableExceptionFields();
                case 56: return mutableDefaultValues();
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public Containers._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                case 1: setBooleanList((java.util.List<Boolean>) value); break;
                case 2: setByteList((java.util.List<Byte>) value); break;
                case 3: setShortList((java.util.List<Short>) value); break;
                case 4: setIntegerList((java.util.List<Integer>) value); break;
                case 5: setLongList((java.util.List<Long>) value); break;
                case 6: setDoubleList((java.util.List<Double>) value); break;
                case 7: setStringList((java.util.List<String>) value); break;
                case 8: setBinaryList((java.util.List<net.morimekta.util.Binary>) value); break;
                case 11: setBooleanSet((java.util.Set<Boolean>) value); break;
                case 12: setByteSet((java.util.Set<Byte>) value); break;
                case 13: setShortSet((java.util.Set<Short>) value); break;
                case 14: setIntegerSet((java.util.Set<Integer>) value); break;
                case 15: setLongSet((java.util.Set<Long>) value); break;
                case 16: setDoubleSet((java.util.Set<Double>) value); break;
                case 17: setStringSet((java.util.Set<String>) value); break;
                case 18: setBinarySet((java.util.Set<net.morimekta.util.Binary>) value); break;
                case 21: setBooleanMap((java.util.Map<Boolean,Boolean>) value); break;
                case 22: setByteMap((java.util.Map<Byte,Byte>) value); break;
                case 23: setShortMap((java.util.Map<Short,Short>) value); break;
                case 24: setIntegerMap((java.util.Map<Integer,Integer>) value); break;
                case 25: setLongMap((java.util.Map<Long,Long>) value); break;
                case 26: setDoubleMap((java.util.Map<Double,Double>) value); break;
                case 27: setStringMap((java.util.Map<String,String>) value); break;
                case 28: setBinaryMap((java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary>) value); break;
                case 31: setEnumList((java.util.List<net.morimekta.test.providence.core.Value>) value); break;
                case 32: setEnumSet((java.util.Set<net.morimekta.test.providence.core.Value>) value); break;
                case 33: setEnumMap((java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value>) value); break;
                case 41: setMessageList((java.util.List<net.morimekta.test.providence.core.OptionalFields>) value); break;
                case 42: setMessageSet((java.util.Set<net.morimekta.test.providence.core.OptionalFields>) value); break;
                case 43: setMessageMap((java.util.Map<String,net.morimekta.test.providence.core.OptionalFields>) value); break;
                case 44: setMessageKeyMap((java.util.Map<net.morimekta.test.providence.core.CompactFields,String>) value); break;
                case 51: setRequiredFields((net.morimekta.test.providence.core.RequiredFields) value); break;
                case 52: setDefaultFields((net.morimekta.test.providence.core.DefaultFields) value); break;
                case 53: setOptionalFields((net.morimekta.test.providence.core.OptionalFields) value); break;
                case 54: setUnionFields((net.morimekta.test.providence.core.UnionFields) value); break;
                case 55: setExceptionFields((net.morimekta.test.providence.core.ExceptionFields) value); break;
                case 56: setDefaultValues((net.morimekta.test.providence.core.DefaultValues) value); break;
                case 61: setMapListCompact((java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>>) value); break;
                case 62: setListMapNumbers((java.util.List<java.util.Map<Integer,Integer>>) value); break;
                case 63: setSetListNumbers((java.util.Set<java.util.List<Integer>>) value); break;
                case 64: setListListNumbers((java.util.List<java.util.List<Integer>>) value); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                case 1: return optionals.get(0);
                case 2: return optionals.get(1);
                case 3: return optionals.get(2);
                case 4: return optionals.get(3);
                case 5: return optionals.get(4);
                case 6: return optionals.get(5);
                case 7: return optionals.get(6);
                case 8: return optionals.get(7);
                case 11: return optionals.get(8);
                case 12: return optionals.get(9);
                case 13: return optionals.get(10);
                case 14: return optionals.get(11);
                case 15: return optionals.get(12);
                case 16: return optionals.get(13);
                case 17: return optionals.get(14);
                case 18: return optionals.get(15);
                case 21: return optionals.get(16);
                case 22: return optionals.get(17);
                case 23: return optionals.get(18);
                case 24: return optionals.get(19);
                case 25: return optionals.get(20);
                case 26: return optionals.get(21);
                case 27: return optionals.get(22);
                case 28: return optionals.get(23);
                case 31: return optionals.get(24);
                case 32: return optionals.get(25);
                case 33: return optionals.get(26);
                case 41: return optionals.get(27);
                case 42: return optionals.get(28);
                case 43: return optionals.get(29);
                case 44: return optionals.get(30);
                case 51: return optionals.get(31);
                case 52: return optionals.get(32);
                case 53: return optionals.get(33);
                case 54: return optionals.get(34);
                case 55: return optionals.get(35);
                case 56: return optionals.get(36);
                case 61: return optionals.get(37);
                case 62: return optionals.get(38);
                case 63: return optionals.get(39);
                case 64: return optionals.get(40);
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            switch (key) {
                case 1: return modified.get(0);
                case 2: return modified.get(1);
                case 3: return modified.get(2);
                case 4: return modified.get(3);
                case 5: return modified.get(4);
                case 6: return modified.get(5);
                case 7: return modified.get(6);
                case 8: return modified.get(7);
                case 11: return modified.get(8);
                case 12: return modified.get(9);
                case 13: return modified.get(10);
                case 14: return modified.get(11);
                case 15: return modified.get(12);
                case 16: return modified.get(13);
                case 17: return modified.get(14);
                case 18: return modified.get(15);
                case 21: return modified.get(16);
                case 22: return modified.get(17);
                case 23: return modified.get(18);
                case 24: return modified.get(19);
                case 25: return modified.get(20);
                case 26: return modified.get(21);
                case 27: return modified.get(22);
                case 28: return modified.get(23);
                case 31: return modified.get(24);
                case 32: return modified.get(25);
                case 33: return modified.get(26);
                case 41: return modified.get(27);
                case 42: return modified.get(28);
                case 43: return modified.get(29);
                case 44: return modified.get(30);
                case 51: return modified.get(31);
                case 52: return modified.get(32);
                case 53: return modified.get(33);
                case 54: return modified.get(34);
                case 55: return modified.get(35);
                case 56: return modified.get(36);
                case 61: return modified.get(37);
                case 62: return modified.get(38);
                case 63: return modified.get(39);
                case 64: return modified.get(40);
                default: break;
            }
            return false;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) getBooleanList();
                case 2: return (T) getByteList();
                case 3: return (T) getShortList();
                case 4: return (T) getIntegerList();
                case 5: return (T) getLongList();
                case 6: return (T) getDoubleList();
                case 7: return (T) getStringList();
                case 8: return (T) getBinaryList();
                case 11: return (T) getBooleanSet();
                case 12: return (T) getByteSet();
                case 13: return (T) getShortSet();
                case 14: return (T) getIntegerSet();
                case 15: return (T) getLongSet();
                case 16: return (T) getDoubleSet();
                case 17: return (T) getStringSet();
                case 18: return (T) getBinarySet();
                case 21: return (T) getBooleanMap();
                case 22: return (T) getByteMap();
                case 23: return (T) getShortMap();
                case 24: return (T) getIntegerMap();
                case 25: return (T) getLongMap();
                case 26: return (T) getDoubleMap();
                case 27: return (T) getStringMap();
                case 28: return (T) getBinaryMap();
                case 31: return (T) getEnumList();
                case 32: return (T) getEnumSet();
                case 33: return (T) getEnumMap();
                case 41: return (T) getMessageList();
                case 42: return (T) getMessageSet();
                case 43: return (T) getMessageMap();
                case 44: return (T) getMessageKeyMap();
                case 51: return (T) getRequiredFields();
                case 52: return (T) getDefaultFields();
                case 53: return (T) getOptionalFields();
                case 54: return (T) getUnionFields();
                case 55: return (T) getExceptionFields();
                case 56: return (T) getDefaultValues();
                case 61: return (T) getMapListCompact();
                case 62: return (T) getListMapNumbers();
                case 63: return (T) getSetListNumbers();
                case 64: return (T) getListListNumbers();
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return mBooleanList != null;
                case 2: return mByteList != null;
                case 3: return mShortList != null;
                case 4: return mIntegerList != null;
                case 5: return mLongList != null;
                case 6: return mDoubleList != null;
                case 7: return mStringList != null;
                case 8: return mBinaryList != null;
                case 11: return mBooleanSet != null;
                case 12: return mByteSet != null;
                case 13: return mShortSet != null;
                case 14: return mIntegerSet != null;
                case 15: return mLongSet != null;
                case 16: return mDoubleSet != null;
                case 17: return mStringSet != null;
                case 18: return mBinarySet != null;
                case 21: return mBooleanMap != null;
                case 22: return mByteMap != null;
                case 23: return mShortMap != null;
                case 24: return mIntegerMap != null;
                case 25: return mLongMap != null;
                case 26: return mDoubleMap != null;
                case 27: return mStringMap != null;
                case 28: return mBinaryMap != null;
                case 31: return mEnumList != null;
                case 32: return mEnumSet != null;
                case 33: return mEnumMap != null;
                case 41: return mMessageList != null;
                case 42: return mMessageSet != null;
                case 43: return mMessageMap != null;
                case 44: return mMessageKeyMap != null;
                case 51: return mRequiredFields != null || mRequiredFields_builder != null;
                case 52: return mDefaultFields != null || mDefaultFields_builder != null;
                case 53: return mOptionalFields != null || mOptionalFields_builder != null;
                case 54: return mUnionFields != null || mUnionFields_builder != null;
                case 55: return mExceptionFields != null || mExceptionFields_builder != null;
                case 56: return mDefaultValues != null || mDefaultValues_builder != null;
                case 61: return mMapListCompact != null;
                case 62: return mListMapNumbers != null;
                case 63: return mSetListNumbers != null;
                case 64: return mListListNumbers != null;
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public Containers._Builder addTo(int key, Object value) {
            switch (key) {
                case 1: addToBooleanList((boolean) value); break;
                case 2: addToByteList((byte) value); break;
                case 3: addToShortList((short) value); break;
                case 4: addToIntegerList((int) value); break;
                case 5: addToLongList((long) value); break;
                case 6: addToDoubleList((double) value); break;
                case 7: addToStringList((String) value); break;
                case 8: addToBinaryList((net.morimekta.util.Binary) value); break;
                case 11: addToBooleanSet((boolean) value); break;
                case 12: addToByteSet((byte) value); break;
                case 13: addToShortSet((short) value); break;
                case 14: addToIntegerSet((int) value); break;
                case 15: addToLongSet((long) value); break;
                case 16: addToDoubleSet((double) value); break;
                case 17: addToStringSet((String) value); break;
                case 18: addToBinarySet((net.morimekta.util.Binary) value); break;
                case 31: addToEnumList((net.morimekta.test.providence.core.Value) value); break;
                case 32: addToEnumSet((net.morimekta.test.providence.core.Value) value); break;
                case 41: addToMessageList((net.morimekta.test.providence.core.OptionalFields) value); break;
                case 42: addToMessageSet((net.morimekta.test.providence.core.OptionalFields) value); break;
                case 62: addToListMapNumbers((java.util.Map<Integer,Integer>) value); break;
                case 63: addToSetListNumbers((java.util.List<Integer>) value); break;
                case 64: addToListListNumbers((java.util.List<Integer>) value); break;
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public Containers._Builder clear(int key) {
            switch (key) {
                case 1: clearBooleanList(); break;
                case 2: clearByteList(); break;
                case 3: clearShortList(); break;
                case 4: clearIntegerList(); break;
                case 5: clearLongList(); break;
                case 6: clearDoubleList(); break;
                case 7: clearStringList(); break;
                case 8: clearBinaryList(); break;
                case 11: clearBooleanSet(); break;
                case 12: clearByteSet(); break;
                case 13: clearShortSet(); break;
                case 14: clearIntegerSet(); break;
                case 15: clearLongSet(); break;
                case 16: clearDoubleSet(); break;
                case 17: clearStringSet(); break;
                case 18: clearBinarySet(); break;
                case 21: clearBooleanMap(); break;
                case 22: clearByteMap(); break;
                case 23: clearShortMap(); break;
                case 24: clearIntegerMap(); break;
                case 25: clearLongMap(); break;
                case 26: clearDoubleMap(); break;
                case 27: clearStringMap(); break;
                case 28: clearBinaryMap(); break;
                case 31: clearEnumList(); break;
                case 32: clearEnumSet(); break;
                case 33: clearEnumMap(); break;
                case 41: clearMessageList(); break;
                case 42: clearMessageSet(); break;
                case 43: clearMessageMap(); break;
                case 44: clearMessageKeyMap(); break;
                case 51: clearRequiredFields(); break;
                case 52: clearDefaultFields(); break;
                case 53: clearOptionalFields(); break;
                case 54: clearUnionFields(); break;
                case 55: clearExceptionFields(); break;
                case 56: clearDefaultValues(); break;
                case 61: clearMapListCompact(); break;
                case 62: clearListMapNumbers(); break;
                case 63: clearSetListNumbers(); break;
                case 64: clearListListNumbers(); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public Containers._Builder validate() {
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor<Containers> descriptor() {
            return Containers.kDescriptor;
        }

        @Override
        public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
            byte type = reader.expectByte();
            while (type != 0) {
                int field = reader.expectShort();
                switch (field) {
                    case 1: {
                        if (type == 15) {
                            byte t_3 = reader.expectByte();
                            if (t_3 == 2) {
                                final int len_2 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Boolean> b_1 = net.morimekta.util.collect.UnmodifiableList.builder(len_2);
                                for (int i_4 = 0; i_4 < len_2; ++i_4) {
                                    boolean key_5 = reader.expectUInt8() == 1;
                                    b_1.add(key_5);
                                }
                                mBooleanList = b_1.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_3) + " for providence.Containers.boolean_list, should be bool(2)");
                            }
                            optionals.set(0);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.boolean_list, should be struct(12)");
                        }
                        break;
                    }
                    case 2: {
                        if (type == 15) {
                            byte t_8 = reader.expectByte();
                            if (t_8 == 3) {
                                final int len_7 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Byte> b_6 = net.morimekta.util.collect.UnmodifiableList.builder(len_7);
                                for (int i_9 = 0; i_9 < len_7; ++i_9) {
                                    byte key_10 = reader.expectByte();
                                    b_6.add(key_10);
                                }
                                mByteList = b_6.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_8) + " for providence.Containers.byte_list, should be byte(3)");
                            }
                            optionals.set(1);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.byte_list, should be struct(12)");
                        }
                        break;
                    }
                    case 3: {
                        if (type == 15) {
                            byte t_13 = reader.expectByte();
                            if (t_13 == 6) {
                                final int len_12 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Short> b_11 = net.morimekta.util.collect.UnmodifiableList.builder(len_12);
                                for (int i_14 = 0; i_14 < len_12; ++i_14) {
                                    short key_15 = reader.expectShort();
                                    b_11.add(key_15);
                                }
                                mShortList = b_11.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_13) + " for providence.Containers.short_list, should be i16(6)");
                            }
                            optionals.set(2);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.short_list, should be struct(12)");
                        }
                        break;
                    }
                    case 4: {
                        if (type == 15) {
                            byte t_18 = reader.expectByte();
                            if (t_18 == 8) {
                                final int len_17 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Integer> b_16 = net.morimekta.util.collect.UnmodifiableList.builder(len_17);
                                for (int i_19 = 0; i_19 < len_17; ++i_19) {
                                    int key_20 = reader.expectInt();
                                    b_16.add(key_20);
                                }
                                mIntegerList = b_16.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_18) + " for providence.Containers.integer_list, should be i32(8)");
                            }
                            optionals.set(3);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.integer_list, should be struct(12)");
                        }
                        break;
                    }
                    case 5: {
                        if (type == 15) {
                            byte t_23 = reader.expectByte();
                            if (t_23 == 10) {
                                final int len_22 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Long> b_21 = net.morimekta.util.collect.UnmodifiableList.builder(len_22);
                                for (int i_24 = 0; i_24 < len_22; ++i_24) {
                                    long key_25 = reader.expectLong();
                                    b_21.add(key_25);
                                }
                                mLongList = b_21.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_23) + " for providence.Containers.long_list, should be i64(10)");
                            }
                            optionals.set(4);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.long_list, should be struct(12)");
                        }
                        break;
                    }
                    case 6: {
                        if (type == 15) {
                            byte t_28 = reader.expectByte();
                            if (t_28 == 4) {
                                final int len_27 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<Double> b_26 = net.morimekta.util.collect.UnmodifiableList.builder(len_27);
                                for (int i_29 = 0; i_29 < len_27; ++i_29) {
                                    double key_30 = reader.expectDouble();
                                    b_26.add(key_30);
                                }
                                mDoubleList = b_26.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_28) + " for providence.Containers.double_list, should be double(4)");
                            }
                            optionals.set(5);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.double_list, should be struct(12)");
                        }
                        break;
                    }
                    case 7: {
                        if (type == 15) {
                            byte t_33 = reader.expectByte();
                            if (t_33 == 11) {
                                final int len_32 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<String> b_31 = net.morimekta.util.collect.UnmodifiableList.builder(len_32);
                                for (int i_34 = 0; i_34 < len_32; ++i_34) {
                                    int len_36 = reader.expectUInt32();
                                    String key_35 = new String(reader.expectBytes(len_36), java.nio.charset.StandardCharsets.UTF_8);
                                    b_31.add(key_35);
                                }
                                mStringList = b_31.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_33) + " for providence.Containers.string_list, should be string(11)");
                            }
                            optionals.set(6);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.string_list, should be struct(12)");
                        }
                        break;
                    }
                    case 8: {
                        if (type == 15) {
                            byte t_39 = reader.expectByte();
                            if (t_39 == 11) {
                                final int len_38 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<net.morimekta.util.Binary> b_37 = net.morimekta.util.collect.UnmodifiableList.builder(len_38);
                                for (int i_40 = 0; i_40 < len_38; ++i_40) {
                                    int len_42 = reader.expectUInt32();
                                    net.morimekta.util.Binary key_41 = reader.expectBinary(len_42);
                                    b_37.add(key_41);
                                }
                                mBinaryList = b_37.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_39) + " for providence.Containers.binary_list, should be string(11)");
                            }
                            optionals.set(7);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.binary_list, should be struct(12)");
                        }
                        break;
                    }
                    case 11: {
                        if (type == 14) {
                            byte t_45 = reader.expectByte();
                            if (t_45 == 2) {
                                final int len_44 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<Boolean> b_43 = net.morimekta.util.collect.UnmodifiableSet.builder(len_44);
                                for (int i_46 = 0; i_46 < len_44; ++i_46) {
                                    boolean key_47 = reader.expectUInt8() == 1;
                                    b_43.add(key_47);
                                }
                                mBooleanSet = b_43.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_45) + " for providence.Containers.boolean_set, should be bool(2)");
                            }
                            optionals.set(8);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.boolean_set, should be struct(12)");
                        }
                        break;
                    }
                    case 12: {
                        if (type == 14) {
                            byte t_50 = reader.expectByte();
                            if (t_50 == 3) {
                                final int len_49 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSortedSet.Builder<Byte> b_48 = net.morimekta.util.collect.UnmodifiableSortedSet.builderNaturalOrder(len_49);
                                for (int i_51 = 0; i_51 < len_49; ++i_51) {
                                    byte key_52 = reader.expectByte();
                                    b_48.add(key_52);
                                }
                                mByteSet = b_48.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_50) + " for providence.Containers.byte_set, should be byte(3)");
                            }
                            optionals.set(9);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.byte_set, should be struct(12)");
                        }
                        break;
                    }
                    case 13: {
                        if (type == 14) {
                            byte t_55 = reader.expectByte();
                            if (t_55 == 6) {
                                final int len_54 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<Short> b_53 = net.morimekta.util.collect.UnmodifiableSet.builder(len_54);
                                for (int i_56 = 0; i_56 < len_54; ++i_56) {
                                    short key_57 = reader.expectShort();
                                    b_53.add(key_57);
                                }
                                mShortSet = b_53.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_55) + " for providence.Containers.short_set, should be i16(6)");
                            }
                            optionals.set(10);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.short_set, should be struct(12)");
                        }
                        break;
                    }
                    case 14: {
                        if (type == 14) {
                            byte t_60 = reader.expectByte();
                            if (t_60 == 8) {
                                final int len_59 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<Integer> b_58 = net.morimekta.util.collect.UnmodifiableSet.builder(len_59);
                                for (int i_61 = 0; i_61 < len_59; ++i_61) {
                                    int key_62 = reader.expectInt();
                                    b_58.add(key_62);
                                }
                                mIntegerSet = b_58.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_60) + " for providence.Containers.integer_set, should be i32(8)");
                            }
                            optionals.set(11);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.integer_set, should be struct(12)");
                        }
                        break;
                    }
                    case 15: {
                        if (type == 14) {
                            byte t_65 = reader.expectByte();
                            if (t_65 == 10) {
                                final int len_64 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<Long> b_63 = net.morimekta.util.collect.UnmodifiableSet.builder(len_64);
                                for (int i_66 = 0; i_66 < len_64; ++i_66) {
                                    long key_67 = reader.expectLong();
                                    b_63.add(key_67);
                                }
                                mLongSet = b_63.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_65) + " for providence.Containers.long_set, should be i64(10)");
                            }
                            optionals.set(12);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.long_set, should be struct(12)");
                        }
                        break;
                    }
                    case 16: {
                        if (type == 14) {
                            byte t_70 = reader.expectByte();
                            if (t_70 == 4) {
                                final int len_69 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<Double> b_68 = net.morimekta.util.collect.UnmodifiableSet.builder(len_69);
                                for (int i_71 = 0; i_71 < len_69; ++i_71) {
                                    double key_72 = reader.expectDouble();
                                    b_68.add(key_72);
                                }
                                mDoubleSet = b_68.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_70) + " for providence.Containers.double_set, should be double(4)");
                            }
                            optionals.set(13);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.double_set, should be struct(12)");
                        }
                        break;
                    }
                    case 17: {
                        if (type == 14) {
                            byte t_75 = reader.expectByte();
                            if (t_75 == 11) {
                                final int len_74 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<String> b_73 = net.morimekta.util.collect.UnmodifiableSet.builder(len_74);
                                for (int i_76 = 0; i_76 < len_74; ++i_76) {
                                    int len_78 = reader.expectUInt32();
                                    String key_77 = new String(reader.expectBytes(len_78), java.nio.charset.StandardCharsets.UTF_8);
                                    b_73.add(key_77);
                                }
                                mStringSet = b_73.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_75) + " for providence.Containers.string_set, should be string(11)");
                            }
                            optionals.set(14);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.string_set, should be struct(12)");
                        }
                        break;
                    }
                    case 18: {
                        if (type == 14) {
                            byte t_81 = reader.expectByte();
                            if (t_81 == 11) {
                                final int len_80 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<net.morimekta.util.Binary> b_79 = net.morimekta.util.collect.UnmodifiableSet.builder(len_80);
                                for (int i_82 = 0; i_82 < len_80; ++i_82) {
                                    int len_84 = reader.expectUInt32();
                                    net.morimekta.util.Binary key_83 = reader.expectBinary(len_84);
                                    b_79.add(key_83);
                                }
                                mBinarySet = b_79.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_81) + " for providence.Containers.binary_set, should be string(11)");
                            }
                            optionals.set(15);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.binary_set, should be struct(12)");
                        }
                        break;
                    }
                    case 21: {
                        if (type == 13) {
                            byte t_87 = reader.expectByte();
                            byte t_88 = reader.expectByte();
                            if (t_87 == 2 && t_88 == 2) {
                                final int len_86 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<Boolean,Boolean> b_85 = net.morimekta.util.collect.UnmodifiableMap.builder(len_86);
                                for (int i_89 = 0; i_89 < len_86; ++i_89) {
                                    boolean key_90 = reader.expectUInt8() == 1;
                                    boolean val_91 = reader.expectUInt8() == 1;
                                    b_85.put(key_90, val_91);
                                }
                                mBooleanMap = b_85.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_87) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_88) +
                                        " for providence.Containers.boolean_map, should be bool(2) and bool(2)");
                            }
                            optionals.set(16);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.boolean_map, should be struct(12)");
                        }
                        break;
                    }
                    case 22: {
                        if (type == 13) {
                            byte t_94 = reader.expectByte();
                            byte t_95 = reader.expectByte();
                            if (t_94 == 3 && t_95 == 3) {
                                final int len_93 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSortedMap.Builder<Byte,Byte> b_92 = net.morimekta.util.collect.UnmodifiableSortedMap.builderNaturalOrder(len_93);
                                for (int i_96 = 0; i_96 < len_93; ++i_96) {
                                    byte key_97 = reader.expectByte();
                                    byte val_98 = reader.expectByte();
                                    b_92.put(key_97, val_98);
                                }
                                mByteMap = b_92.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_94) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_95) +
                                        " for providence.Containers.byte_map, should be byte(3) and byte(3)");
                            }
                            optionals.set(17);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.byte_map, should be struct(12)");
                        }
                        break;
                    }
                    case 23: {
                        if (type == 13) {
                            byte t_101 = reader.expectByte();
                            byte t_102 = reader.expectByte();
                            if (t_101 == 6 && t_102 == 6) {
                                final int len_100 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<Short,Short> b_99 = net.morimekta.util.collect.UnmodifiableMap.builder(len_100);
                                for (int i_103 = 0; i_103 < len_100; ++i_103) {
                                    short key_104 = reader.expectShort();
                                    short val_105 = reader.expectShort();
                                    b_99.put(key_104, val_105);
                                }
                                mShortMap = b_99.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_101) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_102) +
                                        " for providence.Containers.short_map, should be i16(6) and i16(6)");
                            }
                            optionals.set(18);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.short_map, should be struct(12)");
                        }
                        break;
                    }
                    case 24: {
                        if (type == 13) {
                            byte t_108 = reader.expectByte();
                            byte t_109 = reader.expectByte();
                            if (t_108 == 8 && t_109 == 8) {
                                final int len_107 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<Integer,Integer> b_106 = net.morimekta.util.collect.UnmodifiableMap.builder(len_107);
                                for (int i_110 = 0; i_110 < len_107; ++i_110) {
                                    int key_111 = reader.expectInt();
                                    int val_112 = reader.expectInt();
                                    b_106.put(key_111, val_112);
                                }
                                mIntegerMap = b_106.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_108) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_109) +
                                        " for providence.Containers.integer_map, should be i32(8) and i32(8)");
                            }
                            optionals.set(19);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.integer_map, should be struct(12)");
                        }
                        break;
                    }
                    case 25: {
                        if (type == 13) {
                            byte t_115 = reader.expectByte();
                            byte t_116 = reader.expectByte();
                            if (t_115 == 10 && t_116 == 10) {
                                final int len_114 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<Long,Long> b_113 = net.morimekta.util.collect.UnmodifiableMap.builder(len_114);
                                for (int i_117 = 0; i_117 < len_114; ++i_117) {
                                    long key_118 = reader.expectLong();
                                    long val_119 = reader.expectLong();
                                    b_113.put(key_118, val_119);
                                }
                                mLongMap = b_113.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_115) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_116) +
                                        " for providence.Containers.long_map, should be i64(10) and i64(10)");
                            }
                            optionals.set(20);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.long_map, should be struct(12)");
                        }
                        break;
                    }
                    case 26: {
                        if (type == 13) {
                            byte t_122 = reader.expectByte();
                            byte t_123 = reader.expectByte();
                            if (t_122 == 4 && t_123 == 4) {
                                final int len_121 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<Double,Double> b_120 = net.morimekta.util.collect.UnmodifiableMap.builder(len_121);
                                for (int i_124 = 0; i_124 < len_121; ++i_124) {
                                    double key_125 = reader.expectDouble();
                                    double val_126 = reader.expectDouble();
                                    b_120.put(key_125, val_126);
                                }
                                mDoubleMap = b_120.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_122) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_123) +
                                        " for providence.Containers.double_map, should be double(4) and double(4)");
                            }
                            optionals.set(21);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.double_map, should be struct(12)");
                        }
                        break;
                    }
                    case 27: {
                        if (type == 13) {
                            byte t_129 = reader.expectByte();
                            byte t_130 = reader.expectByte();
                            if (t_129 == 11 && t_130 == 11) {
                                final int len_128 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<String,String> b_127 = net.morimekta.util.collect.UnmodifiableMap.builder(len_128);
                                for (int i_131 = 0; i_131 < len_128; ++i_131) {
                                    int len_134 = reader.expectUInt32();
                                    String key_132 = new String(reader.expectBytes(len_134), java.nio.charset.StandardCharsets.UTF_8);
                                    int len_135 = reader.expectUInt32();
                                    String val_133 = new String(reader.expectBytes(len_135), java.nio.charset.StandardCharsets.UTF_8);
                                    b_127.put(key_132, val_133);
                                }
                                mStringMap = b_127.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_129) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_130) +
                                        " for providence.Containers.string_map, should be string(11) and string(11)");
                            }
                            optionals.set(22);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.string_map, should be struct(12)");
                        }
                        break;
                    }
                    case 28: {
                        if (type == 13) {
                            byte t_138 = reader.expectByte();
                            byte t_139 = reader.expectByte();
                            if (t_138 == 11 && t_139 == 11) {
                                final int len_137 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<net.morimekta.util.Binary,net.morimekta.util.Binary> b_136 = net.morimekta.util.collect.UnmodifiableMap.builder(len_137);
                                for (int i_140 = 0; i_140 < len_137; ++i_140) {
                                    int len_143 = reader.expectUInt32();
                                    net.morimekta.util.Binary key_141 = reader.expectBinary(len_143);
                                    int len_144 = reader.expectUInt32();
                                    net.morimekta.util.Binary val_142 = reader.expectBinary(len_144);
                                    b_136.put(key_141, val_142);
                                }
                                mBinaryMap = b_136.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_138) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_139) +
                                        " for providence.Containers.binary_map, should be string(11) and string(11)");
                            }
                            optionals.set(23);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.binary_map, should be struct(12)");
                        }
                        break;
                    }
                    case 31: {
                        if (type == 15) {
                            byte t_147 = reader.expectByte();
                            if (t_147 == 8) {
                                final int len_146 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<net.morimekta.test.providence.core.Value> b_145 = net.morimekta.util.collect.UnmodifiableList.builder(len_146);
                                for (int i_148 = 0; i_148 < len_146; ++i_148) {
                                    net.morimekta.test.providence.core.Value key_149 = net.morimekta.test.providence.core.Value.findById(reader.expectInt());
                                    b_145.add(key_149);
                                }
                                mEnumList = b_145.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_147) + " for providence.Containers.enum_list, should be i32(8)");
                            }
                            optionals.set(24);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.enum_list, should be struct(12)");
                        }
                        break;
                    }
                    case 32: {
                        if (type == 14) {
                            byte t_152 = reader.expectByte();
                            if (t_152 == 8) {
                                final int len_151 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<net.morimekta.test.providence.core.Value> b_150 = net.morimekta.util.collect.UnmodifiableSet.builder(len_151);
                                for (int i_153 = 0; i_153 < len_151; ++i_153) {
                                    net.morimekta.test.providence.core.Value key_154 = net.morimekta.test.providence.core.Value.findById(reader.expectInt());
                                    b_150.add(key_154);
                                }
                                mEnumSet = b_150.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_152) + " for providence.Containers.enum_set, should be i32(8)");
                            }
                            optionals.set(25);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.enum_set, should be struct(12)");
                        }
                        break;
                    }
                    case 33: {
                        if (type == 13) {
                            byte t_157 = reader.expectByte();
                            byte t_158 = reader.expectByte();
                            if (t_157 == 8 && t_158 == 8) {
                                final int len_156 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> b_155 = net.morimekta.util.collect.UnmodifiableMap.builder(len_156);
                                for (int i_159 = 0; i_159 < len_156; ++i_159) {
                                    net.morimekta.test.providence.core.Value key_160 = net.morimekta.test.providence.core.Value.findById(reader.expectInt());
                                    net.morimekta.test.providence.core.Value val_161 = net.morimekta.test.providence.core.Value.findById(reader.expectInt());
                                    b_155.put(key_160, val_161);
                                }
                                mEnumMap = b_155.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_157) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_158) +
                                        " for providence.Containers.enum_map, should be i32(8) and i32(8)");
                            }
                            optionals.set(26);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.enum_map, should be struct(12)");
                        }
                        break;
                    }
                    case 41: {
                        if (type == 15) {
                            byte t_164 = reader.expectByte();
                            if (t_164 == 12) {
                                final int len_163 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableList.Builder<net.morimekta.test.providence.core.OptionalFields> b_162 = net.morimekta.util.collect.UnmodifiableList.builder(len_163);
                                for (int i_165 = 0; i_165 < len_163; ++i_165) {
                                    net.morimekta.test.providence.core.OptionalFields key_166 = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.OptionalFields.kDescriptor, strict);
                                    b_162.add(key_166);
                                }
                                mMessageList = b_162.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_164) + " for providence.Containers.message_list, should be struct(12)");
                            }
                            optionals.set(27);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.message_list, should be struct(12)");
                        }
                        break;
                    }
                    case 42: {
                        if (type == 14) {
                            byte t_169 = reader.expectByte();
                            if (t_169 == 12) {
                                final int len_168 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableSet.Builder<net.morimekta.test.providence.core.OptionalFields> b_167 = net.morimekta.util.collect.UnmodifiableSet.builder(len_168);
                                for (int i_170 = 0; i_170 < len_168; ++i_170) {
                                    net.morimekta.test.providence.core.OptionalFields key_171 = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.OptionalFields.kDescriptor, strict);
                                    b_167.add(key_171);
                                }
                                mMessageSet = b_167.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException("Wrong item type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_169) + " for providence.Containers.message_set, should be struct(12)");
                            }
                            optionals.set(28);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.message_set, should be struct(12)");
                        }
                        break;
                    }
                    case 43: {
                        if (type == 13) {
                            byte t_174 = reader.expectByte();
                            byte t_175 = reader.expectByte();
                            if (t_174 == 11 && t_175 == 12) {
                                final int len_173 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<String,net.morimekta.test.providence.core.OptionalFields> b_172 = net.morimekta.util.collect.UnmodifiableMap.builder(len_173);
                                for (int i_176 = 0; i_176 < len_173; ++i_176) {
                                    int len_179 = reader.expectUInt32();
                                    String key_177 = new String(reader.expectBytes(len_179), java.nio.charset.StandardCharsets.UTF_8);
                                    net.morimekta.test.providence.core.OptionalFields val_178 = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.OptionalFields.kDescriptor, strict);
                                    b_172.put(key_177, val_178);
                                }
                                mMessageMap = b_172.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_174) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_175) +
                                        " for providence.Containers.message_map, should be string(11) and struct(12)");
                            }
                            optionals.set(29);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.message_map, should be struct(12)");
                        }
                        break;
                    }
                    case 44: {
                        if (type == 13) {
                            byte t_182 = reader.expectByte();
                            byte t_183 = reader.expectByte();
                            if (t_182 == 12 && t_183 == 11) {
                                final int len_181 = reader.expectUInt32();
                                net.morimekta.util.collect.UnmodifiableMap.Builder<net.morimekta.test.providence.core.CompactFields,String> b_180 = net.morimekta.util.collect.UnmodifiableMap.builder(len_181);
                                for (int i_184 = 0; i_184 < len_181; ++i_184) {
                                    net.morimekta.test.providence.core.CompactFields key_185 = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.CompactFields.kDescriptor, strict);
                                    int len_187 = reader.expectUInt32();
                                    String val_186 = new String(reader.expectBytes(len_187), java.nio.charset.StandardCharsets.UTF_8);
                                    b_180.put(key_185, val_186);
                                }
                                mMessageKeyMap = b_180.build();
                            } else {
                                throw new net.morimekta.providence.serializer.SerializerException(
                                        "Wrong key type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_182) +
                                        " or value type " + net.morimekta.providence.serializer.binary.BinaryType.asString(t_183) +
                                        " for providence.Containers.message_key_map, should be struct(12) and string(11)");
                            }
                            optionals.set(30);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.message_key_map, should be struct(12)");
                        }
                        break;
                    }
                    case 51: {
                        if (type == 12) {
                            mRequiredFields = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.RequiredFields.kDescriptor, strict);
                            optionals.set(31);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.required_fields, should be struct(12)");
                        }
                        break;
                    }
                    case 52: {
                        if (type == 12) {
                            mDefaultFields = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.DefaultFields.kDescriptor, strict);
                            optionals.set(32);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.default_fields, should be struct(12)");
                        }
                        break;
                    }
                    case 53: {
                        if (type == 12) {
                            mOptionalFields = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.OptionalFields.kDescriptor, strict);
                            optionals.set(33);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.optional_fields, should be struct(12)");
                        }
                        break;
                    }
                    case 54: {
                        if (type == 12) {
                            mUnionFields = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.UnionFields.kDescriptor, strict);
                            optionals.set(34);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.union_fields, should be struct(12)");
                        }
                        break;
                    }
                    case 55: {
                        if (type == 12) {
                            mExceptionFields = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.ExceptionFields.kDescriptor, strict);
                            optionals.set(35);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.exception_fields, should be struct(12)");
                        }
                        break;
                    }
                    case 56: {
                        if (type == 12) {
                            mDefaultValues = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.DefaultValues.kDescriptor, strict);
                            optionals.set(36);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.default_values, should be struct(12)");
                        }
                        break;
                    }
                    case 61: {
                        if (type == 13) {
                            mMapListCompact = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), Containers._Field.MAP_LIST_COMPACT.getDescriptor(), strict);
                            optionals.set(37);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.map_list_compact, should be struct(12)");
                        }
                        break;
                    }
                    case 62: {
                        if (type == 15) {
                            mListMapNumbers = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), Containers._Field.LIST_MAP_NUMBERS.getDescriptor(), strict);
                            optionals.set(38);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.list_map_numbers, should be struct(12)");
                        }
                        break;
                    }
                    case 63: {
                        if (type == 14) {
                            mSetListNumbers = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), Containers._Field.SET_LIST_NUMBERS.getDescriptor(), strict);
                            optionals.set(39);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.set_list_numbers, should be struct(12)");
                        }
                        break;
                    }
                    case 64: {
                        if (type == 15) {
                            mListListNumbers = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), Containers._Field.LIST_LIST_NUMBERS.getDescriptor(), strict);
                            optionals.set(40);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.Containers.list_list_numbers, should be struct(12)");
                        }
                        break;
                    }
                    default: {
                        net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                        break;
                    }
                }
                type = reader.expectByte();
            }
        }

        @Override
        @javax.annotation.Nonnull
        public Containers build() {
            return new Containers(this);
        }
    }
}
