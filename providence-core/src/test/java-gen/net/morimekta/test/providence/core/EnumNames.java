package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum EnumNames
        implements net.morimekta.providence.PEnumValue<EnumNames> {
    UM4V(1, "UM4V", "UM4V"),
    UMP3(2, "UMP3", "UMP3"),
    LM4V(3, "lm4v", "lm4v"),
    LMP3(4, "lmp3", "lmp3"),
    UMPEG4(5, "Umpeg4", "Umpeg4"),
    L_MPEG4(6, "lMPEG4", "lMPEG4"),
    L4_U(7, "l4U", "l4U"),
    U4L(8, "U4l", "U4l"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    EnumNames(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static EnumNames findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 1: return EnumNames.UM4V;
            case 2: return EnumNames.UMP3;
            case 3: return EnumNames.LM4V;
            case 4: return EnumNames.LMP3;
            case 5: return EnumNames.UMPEG4;
            case 6: return EnumNames.L_MPEG4;
            case 7: return EnumNames.L4_U;
            case 8: return EnumNames.U4L;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static EnumNames findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "UM4V": return EnumNames.UM4V;
            case "UMP3": return EnumNames.UMP3;
            case "lm4v": return EnumNames.LM4V;
            case "lmp3": return EnumNames.LMP3;
            case "Umpeg4": return EnumNames.UMPEG4;
            case "lMPEG4": return EnumNames.L_MPEG4;
            case "l4U": return EnumNames.L4_U;
            case "U4l": return EnumNames.U4L;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static EnumNames findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "UM4V": return EnumNames.UM4V;
            case "UMP3": return EnumNames.UMP3;
            case "lm4v": return EnumNames.LM4V;
            case "lmp3": return EnumNames.LMP3;
            case "Umpeg4": return EnumNames.UMPEG4;
            case "lMPEG4": return EnumNames.L_MPEG4;
            case "l4U": return EnumNames.L4_U;
            case "U4l": return EnumNames.U4L;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static EnumNames valueForId(int id) {
        EnumNames value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No providence.EnumNames for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static EnumNames valueForName(@javax.annotation.Nonnull String name) {
        EnumNames value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No providence.EnumNames for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static EnumNames valueForPojoName(@javax.annotation.Nonnull String name) {
        EnumNames value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No providence.EnumNames for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<EnumNames> {
        private EnumNames mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = EnumNames.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = EnumNames.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public EnumNames build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<EnumNames> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<EnumNames> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<EnumNames> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<EnumNames>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<EnumNames> {
        public _Descriptor() {
            super("providence", "EnumNames", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public EnumNames[] getValues() {
            return EnumNames.values();
        }

        @Override
        @javax.annotation.Nullable
        public EnumNames findById(int id) {
            return EnumNames.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public EnumNames findByName(String name) {
            return EnumNames.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public EnumNames findByPojoName(String name) {
            return EnumNames.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
