package net.morimekta.test.providence.core.calculator;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface Operand_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Operand> {
    /**
     * @return The operation value.
     */
    net.morimekta.test.providence.core.calculator.Operation getOperation();

    /**
     * @return Optional operation value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.calculator.Operation> optionalOperation();

    /**
     * @return If operation is present.
     */
    boolean hasOperation();

    /**
     * @return The number value.
     */
    double getNumber();

    /**
     * @return Optional number value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Double> optionalNumber();

    /**
     * @return If number is present.
     */
    boolean hasNumber();

    /**
     * @return The imaginary value.
     */
    net.morimekta.test.providence.core.number.Imaginary getImaginary();

    /**
     * @return Optional imaginary value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.number.Imaginary> optionalImaginary();

    /**
     * @return If imaginary is present.
     */
    boolean hasImaginary();

}
