package net.morimekta.test.providence.core.number;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface Imaginary_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Imaginary> {
    /**
     * @return The v value.
     */
    double getV();

    /**
     * @return If v is present.
     */
    boolean hasV();

    /**
     * @return The i value.
     */
    double getI();

    /**
     * @return If i is present.
     */
    boolean hasI();

}
