package net.morimekta.test.providence.core.no_rw_binary;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:no_rw_binary")
@SuppressWarnings("unused")
public interface CompactFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<CompactFields> {
    /**
     * @return The name value.
     */
    @javax.annotation.Nonnull
    String getName();

    /**
     * @return If name is present.
     */
    boolean hasName();

    /**
     * @return The id value.
     */
    int getId();

    /**
     * @return If id is present.
     */
    boolean hasId();

    /**
     * @return The label value.
     */
    String getLabel();

    /**
     * @return Optional label value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalLabel();

    /**
     * @return If label is present.
     */
    boolean hasLabel();

}
