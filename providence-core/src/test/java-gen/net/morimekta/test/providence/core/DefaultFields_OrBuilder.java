package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface DefaultFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<DefaultFields> {
    /**
     * @return The boolean_value value.
     */
    boolean isBooleanValue();

    /**
     * @return If boolean_value is present.
     */
    boolean hasBooleanValue();

    /**
     * @return The byte_value value.
     */
    byte getByteValue();

    /**
     * @return If byte_value is present.
     */
    boolean hasByteValue();

    /**
     * @return The short_value value.
     */
    short getShortValue();

    /**
     * @return If short_value is present.
     */
    boolean hasShortValue();

    /**
     * @return The integer_value value.
     */
    int getIntegerValue();

    /**
     * @return If integer_value is present.
     */
    boolean hasIntegerValue();

    /**
     * @return The long_value value.
     */
    long getLongValue();

    /**
     * @return If long_value is present.
     */
    boolean hasLongValue();

    /**
     * @return The double_value value.
     */
    double getDoubleValue();

    /**
     * @return If double_value is present.
     */
    boolean hasDoubleValue();

    /**
     * @return The string_value value.
     */
    @javax.annotation.Nonnull
    String getStringValue();

    /**
     * @return If string_value is present.
     */
    boolean hasStringValue();

    /**
     * @return The binary_value value.
     */
    @javax.annotation.Nonnull
    net.morimekta.util.Binary getBinaryValue();

    /**
     * @return If binary_value is present.
     */
    boolean hasBinaryValue();

    /**
     * @return The enum_value value.
     */
    net.morimekta.test.providence.core.Value getEnumValue();

    /**
     * @return Optional enum_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.Value> optionalEnumValue();

    /**
     * @return If enum_value is present.
     */
    boolean hasEnumValue();

    /**
     * @return The compact_value value.
     */
    net.morimekta.test.providence.core.CompactFields getCompactValue();

    /**
     * @return Optional compact_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.CompactFields> optionalCompactValue();

    /**
     * @return If compact_value is present.
     */
    boolean hasCompactValue();

}
