package net.morimekta.test.providence.core.no_rw_binary;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:no_rw_binary")
@SuppressWarnings("unused")
public interface Containers_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Containers> {
    /**
     * @return The booleanList value.
     */
    java.util.List<Boolean> getBooleanList();

    /**
     * @return Optional booleanList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Boolean>> optionalBooleanList();

    /**
     * @return If booleanList is present.
     */
    boolean hasBooleanList();

    /**
     * @return Number of entries in booleanList.
     */
    int numBooleanList();

    /**
     * @return The byteList value.
     */
    java.util.List<Byte> getByteList();

    /**
     * @return Optional byteList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Byte>> optionalByteList();

    /**
     * @return If byteList is present.
     */
    boolean hasByteList();

    /**
     * @return Number of entries in byteList.
     */
    int numByteList();

    /**
     * @return The shortList value.
     */
    java.util.List<Short> getShortList();

    /**
     * @return Optional shortList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Short>> optionalShortList();

    /**
     * @return If shortList is present.
     */
    boolean hasShortList();

    /**
     * @return Number of entries in shortList.
     */
    int numShortList();

    /**
     * @return The integerList value.
     */
    java.util.List<Integer> getIntegerList();

    /**
     * @return Optional integerList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Integer>> optionalIntegerList();

    /**
     * @return If integerList is present.
     */
    boolean hasIntegerList();

    /**
     * @return Number of entries in integerList.
     */
    int numIntegerList();

    /**
     * @return The longList value.
     */
    java.util.List<Long> getLongList();

    /**
     * @return Optional longList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Long>> optionalLongList();

    /**
     * @return If longList is present.
     */
    boolean hasLongList();

    /**
     * @return Number of entries in longList.
     */
    int numLongList();

    /**
     * @return The doubleList value.
     */
    java.util.List<Double> getDoubleList();

    /**
     * @return Optional doubleList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Double>> optionalDoubleList();

    /**
     * @return If doubleList is present.
     */
    boolean hasDoubleList();

    /**
     * @return Number of entries in doubleList.
     */
    int numDoubleList();

    /**
     * @return The stringList value.
     */
    java.util.List<String> getStringList();

    /**
     * @return Optional stringList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<String>> optionalStringList();

    /**
     * @return If stringList is present.
     */
    boolean hasStringList();

    /**
     * @return Number of entries in stringList.
     */
    int numStringList();

    /**
     * @return The binaryList value.
     */
    java.util.List<net.morimekta.util.Binary> getBinaryList();

    /**
     * @return Optional binaryList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.util.Binary>> optionalBinaryList();

    /**
     * @return If binaryList is present.
     */
    boolean hasBinaryList();

    /**
     * @return Number of entries in binaryList.
     */
    int numBinaryList();

    /**
     * @return The booleanSet value.
     */
    java.util.Set<Boolean> getBooleanSet();

    /**
     * @return Optional booleanSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Boolean>> optionalBooleanSet();

    /**
     * @return If booleanSet is present.
     */
    boolean hasBooleanSet();

    /**
     * @return Number of entries in booleanSet.
     */
    int numBooleanSet();

    /**
     * @return The byteSet value.
     */
    java.util.Set<Byte> getByteSet();

    /**
     * @return Optional byteSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Byte>> optionalByteSet();

    /**
     * @return If byteSet is present.
     */
    boolean hasByteSet();

    /**
     * @return Number of entries in byteSet.
     */
    int numByteSet();

    /**
     * @return The shortSet value.
     */
    java.util.Set<Short> getShortSet();

    /**
     * @return Optional shortSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Short>> optionalShortSet();

    /**
     * @return If shortSet is present.
     */
    boolean hasShortSet();

    /**
     * @return Number of entries in shortSet.
     */
    int numShortSet();

    /**
     * @return The integerSet value.
     */
    java.util.Set<Integer> getIntegerSet();

    /**
     * @return Optional integerSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Integer>> optionalIntegerSet();

    /**
     * @return If integerSet is present.
     */
    boolean hasIntegerSet();

    /**
     * @return Number of entries in integerSet.
     */
    int numIntegerSet();

    /**
     * @return The longSet value.
     */
    java.util.Set<Long> getLongSet();

    /**
     * @return Optional longSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Long>> optionalLongSet();

    /**
     * @return If longSet is present.
     */
    boolean hasLongSet();

    /**
     * @return Number of entries in longSet.
     */
    int numLongSet();

    /**
     * @return The doubleSet value.
     */
    java.util.Set<Double> getDoubleSet();

    /**
     * @return Optional doubleSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Double>> optionalDoubleSet();

    /**
     * @return If doubleSet is present.
     */
    boolean hasDoubleSet();

    /**
     * @return Number of entries in doubleSet.
     */
    int numDoubleSet();

    /**
     * @return The stringSet value.
     */
    java.util.Set<String> getStringSet();

    /**
     * @return Optional stringSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<String>> optionalStringSet();

    /**
     * @return If stringSet is present.
     */
    boolean hasStringSet();

    /**
     * @return Number of entries in stringSet.
     */
    int numStringSet();

    /**
     * @return The binarySet value.
     */
    java.util.Set<net.morimekta.util.Binary> getBinarySet();

    /**
     * @return Optional binarySet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.util.Binary>> optionalBinarySet();

    /**
     * @return If binarySet is present.
     */
    boolean hasBinarySet();

    /**
     * @return Number of entries in binarySet.
     */
    int numBinarySet();

    /**
     * @return The booleanMap value.
     */
    java.util.Map<Boolean,Boolean> getBooleanMap();

    /**
     * @return Optional booleanMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Boolean,Boolean>> optionalBooleanMap();

    /**
     * @return If booleanMap is present.
     */
    boolean hasBooleanMap();

    /**
     * @return Number of entries in booleanMap.
     */
    int numBooleanMap();

    /**
     * @return The byteMap value.
     */
    java.util.Map<Byte,Byte> getByteMap();

    /**
     * @return Optional byteMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Byte,Byte>> optionalByteMap();

    /**
     * @return If byteMap is present.
     */
    boolean hasByteMap();

    /**
     * @return Number of entries in byteMap.
     */
    int numByteMap();

    /**
     * @return The shortMap value.
     */
    java.util.Map<Short,Short> getShortMap();

    /**
     * @return Optional shortMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Short,Short>> optionalShortMap();

    /**
     * @return If shortMap is present.
     */
    boolean hasShortMap();

    /**
     * @return Number of entries in shortMap.
     */
    int numShortMap();

    /**
     * @return The integerMap value.
     */
    java.util.Map<Integer,Integer> getIntegerMap();

    /**
     * @return Optional integerMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Integer,Integer>> optionalIntegerMap();

    /**
     * @return If integerMap is present.
     */
    boolean hasIntegerMap();

    /**
     * @return Number of entries in integerMap.
     */
    int numIntegerMap();

    /**
     * @return The longMap value.
     */
    java.util.Map<Long,Long> getLongMap();

    /**
     * @return Optional longMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Long,Long>> optionalLongMap();

    /**
     * @return If longMap is present.
     */
    boolean hasLongMap();

    /**
     * @return Number of entries in longMap.
     */
    int numLongMap();

    /**
     * @return The doubleMap value.
     */
    java.util.Map<Double,Double> getDoubleMap();

    /**
     * @return Optional doubleMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Double,Double>> optionalDoubleMap();

    /**
     * @return If doubleMap is present.
     */
    boolean hasDoubleMap();

    /**
     * @return Number of entries in doubleMap.
     */
    int numDoubleMap();

    /**
     * @return The stringMap value.
     */
    java.util.Map<String,String> getStringMap();

    /**
     * @return Optional stringMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalStringMap();

    /**
     * @return If stringMap is present.
     */
    boolean hasStringMap();

    /**
     * @return Number of entries in stringMap.
     */
    int numStringMap();

    /**
     * @return The binaryMap value.
     */
    java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> getBinaryMap();

    /**
     * @return Optional binaryMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary>> optionalBinaryMap();

    /**
     * @return If binaryMap is present.
     */
    boolean hasBinaryMap();

    /**
     * @return Number of entries in binaryMap.
     */
    int numBinaryMap();

    /**
     * @return The enumList value.
     */
    java.util.List<net.morimekta.test.providence.core.no_rw_binary.Value> getEnumList();

    /**
     * @return Optional enumList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.test.providence.core.no_rw_binary.Value>> optionalEnumList();

    /**
     * @return If enumList is present.
     */
    boolean hasEnumList();

    /**
     * @return Number of entries in enumList.
     */
    int numEnumList();

    /**
     * @return The enumSet value.
     */
    java.util.Set<net.morimekta.test.providence.core.no_rw_binary.Value> getEnumSet();

    /**
     * @return Optional enumSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.test.providence.core.no_rw_binary.Value>> optionalEnumSet();

    /**
     * @return If enumSet is present.
     */
    boolean hasEnumSet();

    /**
     * @return Number of entries in enumSet.
     */
    int numEnumSet();

    /**
     * @return The enumMap value.
     */
    java.util.Map<net.morimekta.test.providence.core.no_rw_binary.Value,net.morimekta.test.providence.core.no_rw_binary.Value> getEnumMap();

    /**
     * @return Optional enumMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.test.providence.core.no_rw_binary.Value,net.morimekta.test.providence.core.no_rw_binary.Value>> optionalEnumMap();

    /**
     * @return If enumMap is present.
     */
    boolean hasEnumMap();

    /**
     * @return Number of entries in enumMap.
     */
    int numEnumMap();

    /**
     * @return The messageList value.
     */
    java.util.List<net.morimekta.test.providence.core.no_rw_binary.OptionalFields> getMessageList();

    /**
     * @return Optional messageList value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.test.providence.core.no_rw_binary.OptionalFields>> optionalMessageList();

    /**
     * @return If messageList is present.
     */
    boolean hasMessageList();

    /**
     * @return Number of entries in messageList.
     */
    int numMessageList();

    /**
     * @return The messageSet value.
     */
    java.util.Set<net.morimekta.test.providence.core.no_rw_binary.OptionalFields> getMessageSet();

    /**
     * @return Optional messageSet value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.test.providence.core.no_rw_binary.OptionalFields>> optionalMessageSet();

    /**
     * @return If messageSet is present.
     */
    boolean hasMessageSet();

    /**
     * @return Number of entries in messageSet.
     */
    int numMessageSet();

    /**
     * @return The messageMap value.
     */
    java.util.Map<String,net.morimekta.test.providence.core.no_rw_binary.OptionalFields> getMessageMap();

    /**
     * @return Optional messageMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,net.morimekta.test.providence.core.no_rw_binary.OptionalFields>> optionalMessageMap();

    /**
     * @return If messageMap is present.
     */
    boolean hasMessageMap();

    /**
     * @return Number of entries in messageMap.
     */
    int numMessageMap();

    /**
     * @return The messageKeyMap value.
     */
    java.util.Map<net.morimekta.test.providence.core.no_rw_binary.CompactFields,String> getMessageKeyMap();

    /**
     * @return Optional messageKeyMap value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.test.providence.core.no_rw_binary.CompactFields,String>> optionalMessageKeyMap();

    /**
     * @return If messageKeyMap is present.
     */
    boolean hasMessageKeyMap();

    /**
     * @return Number of entries in messageKeyMap.
     */
    int numMessageKeyMap();

    /**
     * @return The requiredFields value.
     */
    net.morimekta.test.providence.core.no_rw_binary.RequiredFields getRequiredFields();

    /**
     * @return Optional requiredFields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.RequiredFields> optionalRequiredFields();

    /**
     * @return If requiredFields is present.
     */
    boolean hasRequiredFields();

    /**
     * @return The defaultFields value.
     */
    net.morimekta.test.providence.core.no_rw_binary.DefaultFields getDefaultFields();

    /**
     * @return Optional defaultFields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.DefaultFields> optionalDefaultFields();

    /**
     * @return If defaultFields is present.
     */
    boolean hasDefaultFields();

    /**
     * @return The optionalFields value.
     */
    net.morimekta.test.providence.core.no_rw_binary.OptionalFields getOptionalFields();

    /**
     * @return Optional optionalFields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.OptionalFields> optionalOptionalFields();

    /**
     * @return If optionalFields is present.
     */
    boolean hasOptionalFields();

    /**
     * @return The unionFields value.
     */
    net.morimekta.test.providence.core.no_rw_binary.UnionFields getUnionFields();

    /**
     * @return Optional unionFields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.UnionFields> optionalUnionFields();

    /**
     * @return If unionFields is present.
     */
    boolean hasUnionFields();

    /**
     * @return The exceptionFields value.
     */
    net.morimekta.test.providence.core.no_rw_binary.ExceptionFields getExceptionFields();

    /**
     * @return Optional exceptionFields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.ExceptionFields> optionalExceptionFields();

    /**
     * @return If exceptionFields is present.
     */
    boolean hasExceptionFields();

    /**
     * @return The defaultValues value.
     */
    net.morimekta.test.providence.core.no_rw_binary.DefaultValues getDefaultValues();

    /**
     * @return Optional defaultValues value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.DefaultValues> optionalDefaultValues();

    /**
     * @return If defaultValues is present.
     */
    boolean hasDefaultValues();

}
