package net.morimekta.test.providence.core.calculator;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface Operation_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Operation> {
    /**
     * @return The operator value.
     */
    net.morimekta.test.providence.core.calculator.Operator getOperator();

    /**
     * @return Optional operator value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.calculator.Operator> optionalOperator();

    /**
     * @return If operator is present.
     */
    boolean hasOperator();

    /**
     * @return The operands value.
     */
    java.util.List<net.morimekta.test.providence.core.calculator.Operand> getOperands();

    /**
     * @return Optional operands value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.test.providence.core.calculator.Operand>> optionalOperands();

    /**
     * @return If operands is present.
     */
    boolean hasOperands();

    /**
     * @return Number of entries in operands.
     */
    int numOperands();

}
