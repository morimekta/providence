package net.morimekta.test.providence.core;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@javax.annotation.concurrent.Immutable
public class JsonNamedValues
        implements JsonNamedValues_OrBuilder,
                   net.morimekta.providence.PMessage<JsonNamedValues>,
                   Comparable<JsonNamedValues>,
                   java.io.Serializable,
                   net.morimekta.providence.serializer.binary.BinaryWriter {
    private final static long serialVersionUID = -4242406887904525612L;

    private final transient String mStringValue;
    private final transient net.morimekta.test.providence.core.JsonNamedEnum mEnumValue;

    private volatile transient int tHashCode;

    // Transient object used during java deserialization.
    private transient JsonNamedValues tSerializeInstance;

    private JsonNamedValues(_Builder builder) {
        mStringValue = builder.mStringValue;
        mEnumValue = builder.mEnumValue;
    }

    public boolean hasStringValue() {
        return mStringValue != null;
    }

    /**
     * @return The <code>string_value</code> value
     */
    public String getStringValue() {
        return mStringValue;
    }

    /**
     * @return Optional of the <code>string_value</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<String> optionalStringValue() {
        return java.util.Optional.ofNullable(mStringValue);
    }

    public boolean hasEnumValue() {
        return mEnumValue != null;
    }

    /**
     * @return The <code>enum_value</code> value
     */
    public net.morimekta.test.providence.core.JsonNamedEnum getEnumValue() {
        return mEnumValue;
    }

    /**
     * @return Optional of the <code>enum_value</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.JsonNamedEnum> optionalEnumValue() {
        return java.util.Optional.ofNullable(mEnumValue);
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            case 1: return mStringValue != null;
            case 2: return mEnumValue != null;
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            case 1: return (T) mStringValue;
            case 2: return (T) mEnumValue;
            default: return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        JsonNamedValues other = (JsonNamedValues) o;
        return java.util.Objects.equals(mStringValue, other.mStringValue) &&
               java.util.Objects.equals(mEnumValue, other.mEnumValue);
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    JsonNamedValues.class,
                    _Field.STRING_VALUE, mStringValue,
                    _Field.ENUM_VALUE, mEnumValue);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "providence.JsonNamedValues" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        boolean first = true;
        if (hasStringValue()) {
            first = false;
            out.append("string_value:")
               .append('\"')
               .append(net.morimekta.util.Strings.escape(mStringValue))
               .append('\"');
        }
        if (hasEnumValue()) {
            if (!first) out.append(',');
            out.append("enum_value:")
               .append(mEnumValue.asString());
        }
        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(JsonNamedValues other) {
        int c;

        c = Boolean.compare(mStringValue != null, other.mStringValue != null);
        if (c != 0) return c;
        if (mStringValue != null) {
            c = mStringValue.compareTo(other.mStringValue);
            if (c != 0) return c;
        }

        c = Boolean.compare(mEnumValue != null, other.mEnumValue != null);
        if (c != 0) return c;
        if (mEnumValue != null) {
            c = Integer.compare(mEnumValue.ordinal(), mEnumValue.ordinal());
            if (c != 0) return c;
        }

        return 0;
    }

    private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
        oos.defaultWriteObject();
        net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
    }

    private Object readResolve() throws java.io.ObjectStreamException {
        return tSerializeInstance;
    }

    @Override
    public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
        int length = 0;

        if (hasStringValue()) {
            length += writer.writeByte((byte) 11);
            length += writer.writeShort((short) 1);
            net.morimekta.util.Binary tmp_1 = net.morimekta.util.Binary.wrap(mStringValue.getBytes(java.nio.charset.StandardCharsets.UTF_8));
            length += writer.writeUInt32(tmp_1.length());
            length += writer.writeBinary(tmp_1);
        }

        if (hasEnumValue()) {
            length += writer.writeByte((byte) 8);
            length += writer.writeShort((short) 2);
            length += writer.writeInt(mEnumValue.asInteger());
        }

        length += writer.writeByte((byte) 0);
        return length;
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<JsonNamedValues> {
        STRING_VALUE(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "string_value", "string-value", net.morimekta.providence.descriptor.PPrimitive.STRING.provider(), null, null),
        ENUM_VALUE(2, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "enum_value", "enumValue", net.morimekta.test.providence.core.JsonNamedEnum.provider(), null, null),
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<JsonNamedValues> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
                case 1: return _Field.STRING_VALUE;
                case 2: return _Field.ENUM_VALUE;
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "string_value": return _Field.STRING_VALUE;
                case "enum_value": return _Field.ENUM_VALUE;
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
                case "string-value": return _Field.STRING_VALUE;
                case "string_value": return _Field.STRING_VALUE;
                case "enumValue": return _Field.ENUM_VALUE;
                case "enum_value": return _Field.ENUM_VALUE;
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in providence.JsonNamedValues");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.JsonNamedValues");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.JsonNamedValues");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PStructDescriptorProvider<JsonNamedValues> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PStructDescriptor<JsonNamedValues> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PStructDescriptor<JsonNamedValues> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PStructDescriptor<JsonNamedValues> {
        public _Descriptor() {
            super("providence", "JsonNamedValues", _Builder::new, true);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<JsonNamedValues> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<JsonNamedValues> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>providence.JsonNamedValues</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<JsonNamedValues>
            implements JsonNamedValues_OrBuilder,
                       net.morimekta.providence.serializer.binary.BinaryReader {
        private java.util.BitSet optionals;
        private java.util.BitSet modified;

        private String mStringValue;
        private net.morimekta.test.providence.core.JsonNamedEnum mEnumValue;

        /**
         * Make a providence.JsonNamedValues builder instance.
         */
        public _Builder() {
            optionals = new java.util.BitSet(2);
            modified = new java.util.BitSet(2);
        }

        /**
         * Make a mutating builder off a base providence.JsonNamedValues.
         *
         * @param base The base JsonNamedValues
         */
        public _Builder(JsonNamedValues base) {
            this();

            if (base.hasStringValue()) {
                optionals.set(0);
                mStringValue = base.mStringValue;
            }
            if (base.hasEnumValue()) {
                optionals.set(1);
                mEnumValue = base.mEnumValue;
            }
        }

        @javax.annotation.Nonnull
        @Override
        public JsonNamedValues._Builder merge(JsonNamedValues from) {
            if (from.hasStringValue()) {
                optionals.set(0);
                modified.set(0);
                mStringValue = from.getStringValue();
            }

            if (from.hasEnumValue()) {
                optionals.set(1);
                modified.set(1);
                mEnumValue = from.getEnumValue();
            }
            return this;
        }

        /**
         * Set the <code>string_value</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public JsonNamedValues._Builder setStringValue(String value) {
            if (value == null) {
                return clearStringValue();
            }

            optionals.set(0);
            modified.set(0);
            mStringValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>string_value</code> field.
         *
         * @return True if string_value has been set.
         */
        public boolean isSetStringValue() {
            return optionals.get(0);
        }

        /**
         * Checks for presence of the <code>string_value</code> field.
         *
         * @return True if string_value is present.
         */
        public boolean hasStringValue() {
            return optionals.get(0);
        }

        /**
         * Checks if the <code>string_value</code> field has been modified since the
         * builder was created.
         *
         * @return True if string_value has been modified.
         */
        public boolean isModifiedStringValue() {
            return modified.get(0);
        }

        /**
         * Clear the <code>string_value</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public JsonNamedValues._Builder clearStringValue() {
            optionals.clear(0);
            modified.set(0);
            mStringValue = null;
            return this;
        }

        /**
         * @return The <code>string_value</code> field value
         */
        public String getStringValue() {
            return mStringValue;
        }

        /**
         * @return Optional <code>string_value</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<String> optionalStringValue() {
            return java.util.Optional.ofNullable(mStringValue);
        }

        /**
         * Set the <code>enum_value</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public JsonNamedValues._Builder setEnumValue(net.morimekta.test.providence.core.JsonNamedEnum value) {
            if (value == null) {
                return clearEnumValue();
            }

            optionals.set(1);
            modified.set(1);
            mEnumValue = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>enum_value</code> field.
         *
         * @return True if enum_value has been set.
         */
        public boolean isSetEnumValue() {
            return optionals.get(1);
        }

        /**
         * Checks for presence of the <code>enum_value</code> field.
         *
         * @return True if enum_value is present.
         */
        public boolean hasEnumValue() {
            return optionals.get(1);
        }

        /**
         * Checks if the <code>enum_value</code> field has been modified since the
         * builder was created.
         *
         * @return True if enum_value has been modified.
         */
        public boolean isModifiedEnumValue() {
            return modified.get(1);
        }

        /**
         * Clear the <code>enum_value</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public JsonNamedValues._Builder clearEnumValue() {
            optionals.clear(1);
            modified.set(1);
            mEnumValue = null;
            return this;
        }

        /**
         * @return The <code>enum_value</code> field value
         */
        public net.morimekta.test.providence.core.JsonNamedEnum getEnumValue() {
            return mEnumValue;
        }

        /**
         * @return Optional <code>enum_value</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.JsonNamedEnum> optionalEnumValue() {
            return java.util.Optional.ofNullable(mEnumValue);
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            JsonNamedValues._Builder other = (JsonNamedValues._Builder) o;
            return java.util.Objects.equals(optionals, other.optionals) &&
                   java.util.Objects.equals(mStringValue, other.mStringValue) &&
                   java.util.Objects.equals(mEnumValue, other.mEnumValue);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    JsonNamedValues.class, optionals,
                    JsonNamedValues._Field.STRING_VALUE, mStringValue,
                    JsonNamedValues._Field.ENUM_VALUE, mEnumValue);
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public JsonNamedValues._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                case 1: setStringValue((String) value); break;
                case 2: setEnumValue((net.morimekta.test.providence.core.JsonNamedEnum) value); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                case 1: return optionals.get(0);
                case 2: return optionals.get(1);
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            switch (key) {
                case 1: return modified.get(0);
                case 2: return modified.get(1);
                default: break;
            }
            return false;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) getStringValue();
                case 2: return (T) getEnumValue();
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return mStringValue != null;
                case 2: return mEnumValue != null;
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public JsonNamedValues._Builder addTo(int key, Object value) {
            switch (key) {
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public JsonNamedValues._Builder clear(int key) {
            switch (key) {
                case 1: clearStringValue(); break;
                case 2: clearEnumValue(); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public JsonNamedValues._Builder validate() {
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor<JsonNamedValues> descriptor() {
            return JsonNamedValues.kDescriptor;
        }

        @Override
        public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
            byte type = reader.expectByte();
            while (type != 0) {
                int field = reader.expectShort();
                switch (field) {
                    case 1: {
                        if (type == 11) {
                            int len_1 = reader.expectUInt32();
                            mStringValue = new String(reader.expectBytes(len_1), java.nio.charset.StandardCharsets.UTF_8);
                            optionals.set(0);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.JsonNamedValues.string_value, should be struct(12)");
                        }
                        break;
                    }
                    case 2: {
                        if (type == 8) {
                            mEnumValue = net.morimekta.test.providence.core.JsonNamedEnum.findById(reader.expectInt());
                            optionals.set(1);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.JsonNamedValues.enum_value, should be struct(12)");
                        }
                        break;
                    }
                    default: {
                        net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                        break;
                    }
                }
                type = reader.expectByte();
            }
        }

        @Override
        @javax.annotation.Nonnull
        public JsonNamedValues build() {
            return new JsonNamedValues(this);
        }
    }
}
