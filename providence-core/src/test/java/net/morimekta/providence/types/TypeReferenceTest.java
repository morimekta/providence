package net.morimekta.providence.types;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import static net.morimekta.providence.types.TypeReference.parseType;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class TypeReferenceTest {
    @Test
    public void testRef() {

    }

    @Test
    public void testParseType_GlobalTypes() {
        TypeReference ref = parseType("i32");
        assertThat(ref.programName, is(""));
        assertThat(ref.typeName, is("i32"));
        assertThat(ref.keyType, is(nullValue()));
        assertThat(ref.valueType, is(nullValue()));

        ref = parseType("foo.Bar");
        assertThat(ref.programName, is("foo"));
        assertThat(ref.typeName, is("Bar"));
        assertThat(ref.keyType, is(nullValue()));
        assertThat(ref.valueType, is(nullValue()));

        ref = parseType("list<foo.Bar>");
        assertThat(ref.programName, is(""));
        assertThat(ref.typeName, is("list"));
        assertThat(ref.keyType, is(nullValue()));
        assertThat(ref.valueType, is(notNullValue()));
        assertThat(ref.valueType.programName, is("foo"));
        assertThat(ref.valueType.typeName, is("Bar"));

        ref = parseType("set<double>");
        assertThat(ref.programName, is(""));
        assertThat(ref.typeName, is("set"));
        assertThat(ref.keyType, is(nullValue()));
        assertThat(ref.valueType, is(notNullValue()));
        assertThat(ref.valueType.programName, is(""));
        assertThat(ref.valueType.typeName, is("double"));
    }

    @DataProvider
    public static Object[][] parseType_Global() {
        return new Object[][] {
                {"i32"},
                {"list<i32>"},
                {"set<context.real>"},
                {"map<context.real,number.Imaginary>"},
                {"number.Imaginary"},
                {"context.Service.name.request"},
                };
    }

    @Test
    @UseDataProvider("parseType_Global")
    public void testParseType_Global(String typeString) {
        TypeReference ref = parseType(typeString);
        assertThat(ref.toString(), is(typeString));
    }

    @DataProvider
    public static Object[][] parseType_Context() {
        return new Object[][] {
                {"foo", "i32", "i32"},
                {"foo", "list<i32>", "list<i32>"},
                {"foo", "set<context.real>", "set<context.real>"},
                {"foo", "map<context.real,number.Imaginary>", "map<context.real,number.Imaginary>"},
                {"foo", "set<real>", "set<foo.real>"},
                {"foo", "map<real,number.Imaginary>", "map<foo.real,number.Imaginary>"},
                {"foo", "Imaginary", "foo.Imaginary"},
                {"foo", "number.Imaginary", "number.Imaginary"},
                {"foo", "Service.name.request", "foo.Service.name.request"},
                {"foo", "context.Service.name.request", "context.Service.name.request"},
                };
    }

    @Test
    @UseDataProvider("parseType_Context")
    public void testParseType_Context(String context, String typeString, String expected) {
        TypeReference ref = parseType(context, typeString);
        assertThat(ref.toString(), is(expected));
    }

    @DataProvider
    public static Object[][] parseType_GlobalFails() {
        return new Object[][] {
                {"",              "Empty type name"},
                {"42.Imaginary",  "Bad program name '42' in reference '42.Imaginary'"},
                {"number.42",     "Bad type '42' in reference 'number.42'"},
                {"list",          "List without value type in reference 'list'"},
                {"set",           "Set without value type in reference 'set'"},
                {"map",           "Map without key or value type in reference 'map'"},
                {"list<>",        "Empty value type in reference 'list<>'"},
                {"set<>",         "Empty value type in reference 'set<>'"},
                {"map<>",         "Empty key and value type in reference 'map<>'"},
                {"list<foo>",     "No program for type 'foo' in reference 'list<foo>'"},
                {"set<foo>",      "No program for type 'foo' in reference 'set<foo>'"},
                {"map<foo,bar>",  "No program for type 'foo' in reference 'map<foo,bar>'"},
                {"map<,>",        "Empty key type in reference 'map<,>'"},
                {"map<key.Type>", "Map without value type: 'map<key.Type>' in reference 'map<key.Type>'"},
                {"map<,key.Type>","Empty key type in reference 'map<,key.Type>'"},
                {"map<key.Type,>","Empty value type in reference 'map<key.Type,>'"},
                {"Imaginary",     "No program for type 'Imaginary' in reference 'Imaginary'"},
                {"a.b.c",         "No program for type 'a.b.c' in reference 'a.b.c'"},
                {"a.b.c.d.e",     "Bad type reference: 'a.b.c.d.e' in reference 'a.b.c.d.e'"},
                {".b",            "Empty program for name '.b' in reference '.b'"},
                {".b.c.d",        "Empty program for name '.b.c.d' in reference '.b.c.d'"},
        };
    }

    @Test
    @UseDataProvider("parseType_GlobalFails")
    public void testParseType_GlobalFail(String typeString, String message) {
        try {
            parseType(typeString);
            fail("No exception on " + message);
        } catch (IllegalArgumentException e) {
            try {
                assertThat(e.getMessage(), is(message));
            } catch (AssertionError a) {
                e.printStackTrace();
                throw a;
            }
        }
    }
}
