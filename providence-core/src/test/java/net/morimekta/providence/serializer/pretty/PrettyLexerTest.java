package net.morimekta.providence.serializer.pretty;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.util.lexer.LexerException;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class PrettyLexerTest {
    @DataProvider
    public static Object[][] dataGoodTokens() {
        return new Object[][] {
                // comments, special newlines
                {"#ignore \r\n" +
                "{\n" +
                "  # ignore\n" +
                "  boo\r\n" +
                "}\r",
                 listOf("{", "boo", "}")},
                // numbers
                {"12", listOf("12")},
                {"012:", listOf("012", ":")},
                {"0x44,", listOf("0x44", ",")},
                {"0.1235;", listOf("0.1235", ";")},
                {".2345", listOf(".2345")},
                {".2345e-5", listOf(".2345e-5")},
                {"55.2345e+5", listOf("55.2345e+5")},
                // strings
                {"\"foo\"", listOf("\"foo\"")},
                // tokens
                {"true", listOf("true")},
                {"undefined", listOf("undefined")},
                {"@undefined$other", listOf("@", "undefined", "$", "other")},
                {"$undefined@other", listOf("$", "undefined", "@", "other")},
                // symbols
                {"{[]}&", listOf("{", "[", "]", "}", "&")},
        };
    }

    @Test
    @UseDataProvider
    public void testGoodTokens(String text, List<String> tokens) throws IOException {
        ByteArrayInputStream in    = new ByteArrayInputStream(text.getBytes(UTF_8));
        PrettyLexer          lexer = new PrettyLexer(in);

        for (String token : tokens) {
            lexer.expect(token, t -> t.toString().equals(token));
        }
    }

    @DataProvider
    public static Object[][] dataFailAnything() {
        return new Object[][]{
                {"",
                 "Error on line 1 row 1: Expected anything, but got end of file\n" +
                 "\n" +
                 "^"},

                // --- strings
                {"\n\n\"\\\"",
                 "Error on line 3 row 4: Unexpected end of stream in string\n" +
                 "\"\\\"\n" +
                 "---^"},
                {"\n\n  \"\n\"",
                 "Error on line 3 row 4: Unexpected newline in string\n" +
                 "  \"\n" +
                 "---^"},
                {"\n\n  \"\003\"",
                 "Error on line 3 row 4: Unescaped non-printable char in string: '\\003'\n" +
                 "  \"·\"\n" +
                 "---^"},

                // --- numbers
                {"12f",
                 "Error on line 1 row 1-3: Invalid termination of number: '12f'\n" +
                 "12f\n" +
                 "^^^"},
                {"      12f",
                 "Error on line 1 row 7-9: Invalid termination of number: '12f'\n" +
                 "      12f\n" +
                 "------^^^"},
                {"-:",
                 "Error on line 1 row 1: No decimal after negative indicator\n" +
                 "-:\n" +
                 "^"},
                {"-",
                 "Error on line 1 row 1: Negative indicator without number\n" +
                 "-\n" +
                 "^"},
                {".5e:",
                 "Error on line 1 row 1-4: Badly terminated number exponent: '.5e:'\n" +
                 ".5e:\n" +
                 "^^^^"},
                {"\n  .5e",
                 "Error on line 2 row 3-5: Badly terminated number exponent: '.5e'\n" +
                 "  .5e\n" +
                 "--^^^"},

                // --- identifiers
                {"e..b",
                 "Error on line 1 row 1-3: Identifier with double '.'\n" +
                 "e..b\n" +
                 "^^^"},
                {"e.:",
                 "Error on line 1 row 1-2: Identifier with trailing '.'\n" +
                 "e.:\n" +
                 "^^"},
                {"e.7:",
                 "Error on line 1 row 1-3: Identifier part with invalid start '7'\n" +
                 "e.7:\n" +
                 "^^^"},
        };
    }

    @Test
    @UseDataProvider
    public void testFailAnything(String text, String displayString) throws IOException {
        testExpectFails(text, listOf(), displayString);
    }

    public void testExpectSymbolFails() throws IOException {
        try {
            tokenizer("").expectSymbol("id", '&', '%');
            fail();
        } catch (LexerException e) {
            assertThat(e.getMessage(), is("Expected id, but got end of file"));
        }

        try {
            tokenizer("123").expectSymbol("id", '&', '%');
            fail();
        } catch (LexerException e) {
            assertThat(e.getMessage(), is("Expected id, but got '123'"));
        }
    }

    @DataProvider
    public static Object[][] dataExpectFails() {
        return new Object[][] {
                {"",
                 listOf(),
                 "Error on line 1 row 1: Expected anything, but got end of file\n" +
                 "\n" +
                 "^"},
                {"1, false:\"foo\"",
                 listOf("1", ",", "false", ":", "\"foo\""),
                 "Error on line 1 row 15: Expected anything, but got end of file\n" +
                 "1, false:\"foo\"\n" +
                 "--------------^"},
        };
    }

    private PrettyLexer tokenizer(String content) {
        return new PrettyLexer(new ByteArrayInputStream(content.getBytes(UTF_8)));
    }

    @Test
    @UseDataProvider("dataExpectFails")
    public void testExpectFails(String text, List<String> expected, String displayString)
            throws IOException {
        ByteArrayInputStream in        = new ByteArrayInputStream(text.getBytes(UTF_8));
        PrettyLexer          tokenizer = new PrettyLexer(in);

        for (String expect : expected) {
            tokenizer.expect(expect, t -> t.toString().equals(expect));
        }

        try {
            tokenizer.expect("anything");
            fail("no exception");
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        } catch (PrettyException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        }
    }

    @Test
    @UseDataProvider("dataExpectFails")
    public void testPeekFails(String text, List<String> expected, String displayString)
            throws IOException {
        ByteArrayInputStream in        = new ByteArrayInputStream(text.getBytes(UTF_8));
        PrettyLexer          tokenizer = new PrettyLexer(in);

        for (String expect : expected) {
            tokenizer.expect(expect, t -> t.toString().equals(expect));
        }

        try {
            tokenizer.peek("anything");
            fail("no exception");
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        } catch (PrettyException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        }
    }
}
