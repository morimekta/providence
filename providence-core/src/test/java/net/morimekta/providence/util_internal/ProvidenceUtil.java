/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.util_internal;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.streams.MessageStreams;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Convenience methods for handling providence messages.
 */
public class ProvidenceUtil {
    @Nonnull
    public static <Message extends PMessage<Message>>
    Message fromJsonResource(String path,
                             PMessageDescriptor<Message> descriptor)
            throws IOException {
        return fromResource(path, descriptor, new JsonSerializer(true));
    }

    @Nonnull
    public static <Message extends PMessage<Message>>
    ArrayList<Message> arrayListFromJsonResource(String path,
                                                 PMessageDescriptor<Message> descriptor)
            throws IOException {
        return arrayListFromResource(path, descriptor, new JsonSerializer(true));
    }

    @Nonnull
    public static <Message extends PMessage<Message>>
    Message fromResource(String resource,
                         PMessageDescriptor<Message> descriptor,
                         Serializer serializer)
            throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        InputStream in = classLoader.getResourceAsStream(resource);
        if(in == null) {
            in = ProvidenceUtil.class.getResourceAsStream(resource);
            if (in == null) {
                throw new IOException("No such resource " + resource);
            }
        }
        return serializer.deserialize(new BufferedInputStream(in), descriptor);
    }

    @Nonnull
    public static <Message extends PMessage<Message>>
    ArrayList<Message> arrayListFromResource(String path,
                                             PMessageDescriptor<Message> descriptor,
                                             Serializer serializer)
            throws IOException {
        return MessageStreams.resource(path, serializer, descriptor)
                             .collect(Collectors.toCollection(ArrayList::new));
    }

    private ProvidenceUtil() {}
}
