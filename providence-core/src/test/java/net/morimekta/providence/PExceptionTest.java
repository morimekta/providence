/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence;

import net.morimekta.test.providence.core.AutoMessageException;
import net.morimekta.test.providence.core.ExceptionFields;
import net.morimekta.test.providence.core.MessageFieldException;
import net.morimekta.test.providence.core.Value;
import net.morimekta.util.Binary;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Stein Eldar Johnsen
 * @since 15.01.16.
 */
public class PExceptionTest {
    @Test
    public void testGetMessage_default() {
        ExceptionFields ex = ExceptionFields.builder()
                                            .setBooleanValue(true)
                                            .setBinaryValue(Binary.wrap(new byte[]{0, 1, 2, 3, 4, 5}))
                                            .setByteValue((byte) 6)
                                            .setDoubleValue(7.8d)
                                            .setIntegerValue(9)
                                            .setLongValue(10L)
                                            .setStringValue("11")
                                            .setShortValue((short) 12)
                                            .setEnumValue(Value.FIFTEENTH)
                                            .build();

        assertEquals("{" +
                     "boolean_value:true," +
                     "byte_value:6," +
                     "short_value:12," +
                     "integer_value:9," +
                     "long_value:10," +
                     "double_value:7.8," +
                     "string_value:\"11\"," +
                     "binary_value:b64(AAECAwQF)," +
                     "enum_value:FIFTEENTH" +
                     "}", ex.getMessage());
    }

    @Test
    public void testGetMessage_autoMessageField() {
        AutoMessageException ex = AutoMessageException.builder()
                                                 .setBooleanValue(true)
                                                 .setBinaryValue(Binary.wrap(new byte[]{0, 1, 2, 3, 4, 5}))
                                                 .setByteValue((byte) 6)
                                                 .setDoubleValue(7.8d)
                                                 .setIntegerValue(9)
                                                 .setLongValue(10L)
                                                 .setMessage("11")
                                                 .setShortValue((short) 12)
                                                 .setEnumValue(Value.FIFTEENTH)
                                                 .build();

        assertEquals("11", ex.getMessage());
    }

    @Test
    public void testGetMessage_specificField() {
        MessageFieldException ex = MessageFieldException.builder()
                                                  .setBooleanValue(true)
                                                  .setBinaryValue(Binary.wrap(new byte[]{0, 1, 2, 3, 4, 5}))
                                                  .setByteValue((byte) 6)
                                                  .setDoubleValue(7.8d)
                                                  .setIntegerValue(9)
                                                  .setLongValue(10L)
                                                  .setStringValue("11")
                                                  .setShortValue((short) 12)
                                                  .setEnumValue(Value.FIFTEENTH)
                                                  .build();

        assertEquals("11", ex.getMessage());
    }
}
