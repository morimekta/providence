/*
 * Copyright 2015 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.descriptor;

import net.morimekta.providence.PType;
import net.morimekta.util.Binary;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.Type;

/**
 * Descriptors for primitive types.
 * <p>
 * These are the basic types used in the thrift structure except containers.
 */
public enum PPrimitive implements PDescriptor {
    VOID  (PType.VOID,   Void.TYPE,     null, true),
    BOOL  (PType.BOOL,   Boolean.TYPE,  false, true),
    BYTE  (PType.BYTE,   Byte.TYPE,      (byte) 0, true),
    I16   (PType.I16,    Short.TYPE,     (short) 0, true),
    I32   (PType.I32,    Integer.TYPE,   0, true),
    I64   (PType.I64,    Long.TYPE,      (long) 0, true),
    DOUBLE(PType.DOUBLE, Double.TYPE,    0.0, true),
    STRING(PType.STRING, String.class,   "", false),
    BINARY(PType.BINARY, Binary.class,   Binary.empty(), false),
    ;

    private final PPrimitiveProvider provider;
    private final PType              type;
    private final Object             defaultValue;
    private final boolean            nativePrimitive;
    private final Type               nativeType;

    PPrimitive(PType type, Type nativeType, Object defValue, boolean nativePrimitive) {
        this.type = type;
        this.nativeType = nativeType;
        this.provider = new PPrimitiveProvider(this);
        this.defaultValue = defValue;
        this.nativePrimitive = nativePrimitive;
    }

    /**
     * Get the descriptor provider for the primitive.
     *
     * @return The descriptor provider.
     */
    @Nonnull
    public PPrimitiveProvider provider() {
        return provider;
    }

    @Override
    public String getProgramName() {
        return null;
    }

    @Nonnull
    @Override
    public String getName() {
        return type.toString();
    }

    @Nonnull
    @Override
    public String getQualifiedName(String programContext) {
        return type.toString();
    }

    @Override
    public String toString() {
        return type.toString();
    }

    @Nonnull
    @Override
    public PType getType() {
        return type;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * @return Returns false if the primitive type allows null values.
     *         If this method returns true, getter and setter methods
     *         will have parameters and return type with the native
     *         primitive type, not it's boxed variant.
     */
    public boolean isNativePrimitive() {
        return nativePrimitive;
    }

    /**
     * Find primitive by name.
     * @param name The name of the primitive.
     * @return The primitive descriptor.
     */
    public static PPrimitive findByName(@Nonnull String name) {
        switch (name) {
            case "void":
                return VOID;
            case "bool":
                return BOOL;
            case "byte":
            case "i8":
                return BYTE;
            case "i16":
                return I16;
            case "i32":
                return I32;
            case "i64":
                return I64;
            case "double":
                return DOUBLE;
            case "string":
                return STRING;
            case "binary":
                return BINARY;
        }
        return null;
    }

    /**
     * Get the java native type for the given primitive.
     * @return The java type.
     */
    public Type getNativeType() {
        return nativeType;
    }
}
