/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.descriptor;

import net.morimekta.providence.PType;
import net.morimekta.util.collect.UnmodifiableSet;
import net.morimekta.util.collect.UnmodifiableSortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.function.IntFunction;

/**
 * Descriptor for a set with item type.
 */
@Immutable
public class PSet<Item> extends PContainer<Set<Item>> {
    private final IntFunction<Builder<Item>> builderSupplier;

    public PSet(PDescriptorProvider itemType,
                IntFunction<Builder<Item>> builderSupplier) {
        super(itemType);
        this.builderSupplier = builderSupplier;
    }

    @Nonnull
    @Override
    public String getName() {
        return "set<" + itemDescriptor().getName() + ">";
    }

    @Nonnull
    @Override
    public String getQualifiedName(String programContext) {
        return "set<" + itemDescriptor().getQualifiedName(programContext) + ">";
    }

    @Nonnull
    @Override
    public PType getType() {
        return PType.SET;
    }

    @Nullable
    @Override
    public Object getDefaultValue() {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PSet)) {
            return false;
        }
        PSet<?> other = (PSet<?>) o;
        return other.itemDescriptor()
                    .equals(itemDescriptor());
    }

    @Override
    public int hashCode() {
        return PSet.class.hashCode() + itemDescriptor().hashCode();
    }

    @Nonnull
    public Builder<Item> builder(int capacity) {
        return builderSupplier.apply(capacity);
    }

    /**
     * Container builder used in serialization.
     *
     * @param <I> The item type.
     */
    public interface Builder<I> {
        @Nonnull
        Builder<I> add(@Nonnull I value);
        @Nonnull
        Builder<I> addAll(@Nonnull Collection<I> items);

        @Nonnull
        Set<I> build();
    }

    /**
     * Default builder returning an ImmutableSet.
     *
     * @param <I> The item type.
     */
    public static class DefaultBuilder<I> implements Builder<I> {
        private UnmodifiableSet.Builder<I> builder;

        public DefaultBuilder() { this(10); }

        public DefaultBuilder(int capacity) {
            builder = UnmodifiableSet.builder(capacity);
        }

        @Nonnull
        @Override
        public Builder<I> add(@Nonnull I value) {
            builder.add(value);
            return this;
        }

        @Nonnull
        @Override
        public Builder<I> addAll(@Nonnull Collection<I> items) {
            builder.addAll(items);
            return this;
        }

        @Nonnull
        @Override
        public Set<I> build() {
            return builder.build();
        }
    }

    /**
     * Default builder returning an ImmutableSortedSet.
     *
     * @param <I> The item type.
     */
    public static class SortedBuilder<I extends Comparable<I>> implements Builder<I> {
        private UnmodifiableSortedSet.Builder<I> builder;

        public SortedBuilder() {
            this(10);
        }

        public SortedBuilder(int capacity) {
            builder = UnmodifiableSortedSet.builderNaturalOrder(capacity);
        }

        @Nonnull
        @Override
        public Builder<I> add(@Nonnull I value) {
            builder.add(value);
            return this;
        }

        @Nonnull
        @Override
        public Builder<I> addAll(@Nonnull Collection<I> items) {
            builder.addAll(items);
            return this;
        }

        @Nonnull
        @Override
        public Set<I> build() {
            return builder.build();
        }
    }

    @Nonnull
    public static <I> PContainerProvider<Set<I>, PSet<I>> provider(PDescriptorProvider itemDesc) {
        return provider(itemDesc, DefaultBuilder::new);
    }

    @Nonnull
    public static <I extends Comparable<I>> PContainerProvider<Set<I>, PSet<I>> sortedProvider(PDescriptorProvider itemDesc) {
        return provider(itemDesc, SortedBuilder::new);
    }

    @Nonnull
    public static <I extends Comparable<I>> PContainerProvider<Set<I>, PSet<I>> orderedProvider(PDescriptorProvider itemDesc) {
        return provider(itemDesc, DefaultBuilder::new);
    }

    private static <I> PContainerProvider<Set<I>, PSet<I>> provider(PDescriptorProvider itemDesc,
                                                                    IntFunction<Builder<I>> builderFactory) {
        return new PContainerProvider<>(new PSet<>(itemDesc, builderFactory));
    }
}
