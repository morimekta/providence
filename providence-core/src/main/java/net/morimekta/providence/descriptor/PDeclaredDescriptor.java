/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.descriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Type;

/**
 * Descriptor for a declared type. A declared type is a value type that is
 * derived from a thrift definition.
 */
public abstract class PDeclaredDescriptor<T> implements PDescriptor, Type {
    private final String programName;
    private final String name;

    protected PDeclaredDescriptor(@Nonnull String programName, @Nonnull String name) {
        this.programName = programName;
        this.name = name;
    }

    @Nonnull
    @Override
    public String getTypeName() {
        return getQualifiedName();
    }

    @Nonnull
    @Override
    public final String getProgramName() {
        return programName;
    }

    @Nonnull
    @Override
    public final String getName() {
        return name;
    }

    @Nonnull
    @Override
    public final String getQualifiedName(String programContext) {
        if (!this.getProgramName().equals(programContext)) {
            return getProgramName() + "." + getName();
        }
        return getName();
    }

    @Nullable
    @Override
    public T getDefaultValue() {
        // Declared value types currently cannot have default values defined by the descriptor.
        return null;
    }

    @Override
    public String toString() {
        return getQualifiedName();
    }

    /**
     * @return Returns true if the type is an inner type. Meaning it is contained within another type
     *         or service.
     */
    public abstract boolean isInnerType();

    /**
     * @return Returns true if the type is generated automatically by means of something else, e.g.
     *         request and response types for services. Or is a built in type as part of providence
     *         itself.
     */
    public abstract boolean isAutoType();
}
