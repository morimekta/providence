/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.serializer.pretty;

import net.morimekta.util.lexer.Token;

import java.util.regex.Pattern;

/**
 * Pretty token.
 */
public class PrettyToken extends Token<PrettyTokenType> {
    // Various symbols.
    public static final char kMessageStart  = '{';
    public static final char kMessageEnd    = '}';
    public static final char kKeyValueSep   = ':';
    public static final char kFieldValueSep = '=';
    public static final char kParamsStart   = '(';
    public static final char kParamsEnd     = ')';
    public static final char kListStart     = '[';
    public static final char kListEnd       = ']';
    public static final char kListSep       = ',';
    public static final char kFieldSep      = ';';

    // Not really 'symbols'.
    public static final char kIdentifierSep      = '.';
    public static final char kLiteralDoubleQuote = '\"';

    public static final String B64 = "b64";
    public static final String HEX = "hex";

    private static final Pattern RE_IDENTIFIER                  = Pattern.compile("[_a-zA-Z][_a-zA-Z0-9]*");
    private static final Pattern RE_QUALIFIED_IDENTIFIER        = Pattern.compile("[_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*");
    private static final Pattern RE_REFERENCE_IDENTIFIER        = Pattern.compile("[_a-zA-Z][_a-zA-Z0-9]*([.][_a-zA-Z][_a-zA-Z0-9]*)*");
    private static final Pattern RE_INTEGER                     = Pattern.compile("-?(0|[1-9][0-9]*|0[0-7]+|0x[0-9a-fA-F]+)");
    private static final Pattern RE_REAL                        = Pattern.compile("-?(0?\\.[0-9]+|[1-9][0-9]*\\.[0-9]*)([eE][+-]?[0-9][0-9]*)?");

    public PrettyToken(char[] fb, int off, int len, PrettyTokenType type, int lineNo, int linePos) {
        super(fb, off, len, type, lineNo, linePos);
    }

    public boolean isSymbol(char symbol) {
        return len == 1 && fb[off] == symbol;
    }

    public boolean isStringLiteral() {
        return type == PrettyTokenType.STRING;
    }

    public boolean isIdentifier() {
        return RE_IDENTIFIER.matcher(this).matches();
    }

    public boolean isQualifiedIdentifier() {
        return RE_QUALIFIED_IDENTIFIER.matcher(this).matches();
    }

    public boolean isReferenceIdentifier() {
        return RE_REFERENCE_IDENTIFIER.matcher(this).matches();
    }

    public boolean isInteger() {
        return RE_INTEGER.matcher(this)
                         .matches();
    }

    public boolean isReal() {
        return RE_REAL.matcher(this)
                      .matches();
    }
}
