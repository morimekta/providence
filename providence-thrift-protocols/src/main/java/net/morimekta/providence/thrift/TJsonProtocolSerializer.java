/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.thrift;

import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.util.json.JsonTokenizer;
import org.apache.thrift.protocol.TJSONProtocol;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Stein Eldar Johnsen
 * @since 24.10.15.
 */
public class TJsonProtocolSerializer extends TProtocolSerializer {
    public static final String MEDIA_TYPE = "application/vnd.apache.thrift.json";

    public TJsonProtocolSerializer() {
        this(DEFAULT_STRICT);
    }

    public TJsonProtocolSerializer(boolean readStrict) {
        super(readStrict, new TJSONProtocol.Factory(),
              false, MEDIA_TYPE);
    }

    @Override
    public void verifyEndOfContent(@Nonnull InputStream input) throws IOException {
        JsonTokenizer tokenizer = new JsonTokenizer(input);
        try {
            List<String> lines = tokenizer.getRemainingLines(true);
            if (lines.size() > 0) {
                throw new SerializerException("More content after end: " + lines.get(0))
                        .setExceptionType(PApplicationExceptionType.PROTOCOL_ERROR);
            }
        } finally {
            input.close();
        }
    }
}
