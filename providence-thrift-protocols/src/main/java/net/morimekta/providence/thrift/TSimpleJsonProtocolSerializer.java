/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.thrift;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PService;
import org.apache.thrift.protocol.TSimpleJSONProtocol;

import javax.annotation.Nonnull;
import java.io.InputStream;

/**
 * The simple JSON protocol is a write-only protocol, which is mainly
 * used for debugging of apache thrift, as they don't really have any
 * human readable protocols (TJson is a mess, and not even JSON compatible).
 * <p>
 * Serializer is here mostly just for completeness.
 */
public class TSimpleJsonProtocolSerializer extends TProtocolSerializer {
    public static final String MEDIA_TYPE = "application/json";

    public TSimpleJsonProtocolSerializer() {
        super(DEFAULT_STRICT, new TSimpleJSONProtocol.Factory(),
              false, MEDIA_TYPE);
    }

    @Nonnull
    @Override
    public <Message extends PMessage<Message>>
    Message deserialize(@Nonnull InputStream input,
                        @Nonnull PMessageDescriptor<Message> descriptor) {
        throw new UnsupportedOperationException("Deserialize not supported for simple JSON protocol");
    }

    @Nonnull
    @Override
    public <Message extends PMessage<Message>>
    PServiceCall<Message> deserialize(@Nonnull InputStream input,
                                      @Nonnull PService service) {
        throw new UnsupportedOperationException("Deserialize not supported for simple JSON protocol");
    }

    @Override
    public void verifyEndOfContent(@Nonnull InputStream input) {
        throw new UnsupportedOperationException("Deserialize not supported for simple JSON protocol");
    }
}
