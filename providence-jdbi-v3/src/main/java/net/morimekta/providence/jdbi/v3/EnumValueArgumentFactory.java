package net.morimekta.providence.jdbi.v3;

import net.morimekta.providence.PEnumValue;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.argument.ArgumentFactory;
import org.jdbi.v3.core.argument.NullArgument;
import org.jdbi.v3.core.config.ConfigRegistry;

import java.lang.reflect.Type;
import java.sql.Types;
import java.util.Optional;

public class EnumValueArgumentFactory implements ArgumentFactory {
    private final Class<? extends PEnumValue> enumClass;

    @SuppressWarnings("unused")
    public EnumValueArgumentFactory() {
        this(PEnumValue.class);
    }

    public EnumValueArgumentFactory(Class<? extends PEnumValue> enumClass) {
        this.enumClass = enumClass;
    }

    @Override
    public Optional<Argument> build(Type type, Object value, ConfigRegistry config) {
        if (type instanceof Class) {
            Class klass = (Class) type;
            if (enumClass.isAssignableFrom(klass)) {
                if (value == null) {
                    return Optional.of(new NullArgument(Types.INTEGER));
                }
                return Optional.of(new EnumValueArgument((PEnumValue) value));
            }
        }
        return Optional.empty();
    }

}
