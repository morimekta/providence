/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v3.annotations;

import net.morimekta.providence.PEnumValue;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizerFactory;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizingAnnotation;
import org.jdbi.v3.sqlobject.customizer.SqlStatementParameterCustomizer;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.sql.Types;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@SqlStatementCustomizingAnnotation(BindEnumName.Factory.class)
public @interface BindEnumName {
    String value();

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        public SqlStatementParameterCustomizer createForParameter(Annotation annotation,
                                                                  Class<?> sqlObjectType,
                                                                  Method method,
                                                                  Parameter param,
                                                                  int index,
                                                                  Type paramType) {
            if (!(paramType instanceof Class)) {
                throw new IllegalArgumentException(paramType.getTypeName() + " is not a providence enum type");
            }
            Class<?> paramClass = (Class) paramType;
            if (!PEnumValue.class.isAssignableFrom(paramClass)) {
                throw new IllegalArgumentException(paramClass.getName() + " is not a providence enum class");
            }

            BindEnumName bind = (BindEnumName) annotation;
            return (stmt, arg) -> {
                if (arg == null) {
                    stmt.bindNull(bind.value(), Types.VARCHAR);
                } else {
                    stmt.bind(bind.value(), ((PEnumValue) arg).asString());
                }
            };
        }
    }
}
