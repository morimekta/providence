# Providence Utils: Logging

Basic message helpers for creating and managing running structured logs
of providence messages.

Since using the Serializers require knowing more about the source of and how to
handle multiple message in one etc. This interface is mostly a simplification of use
of the Serializers, making specialized `MessageReader` and `MessageWriter`
classes.

- `IOMessageReader` and `IOMessageWriter` are simply wrappers around input stream
  and output stream that handles streaming multiple messages, with optional
  internal separators (e.g. for JSON).
- `FileMessageReader` and `FileMessageWriter` writes to and reads from a single
  file that is appended to and read from as a stream of messages, also with optional
  internal separators.

On the more log-based utilities, we have:

- `QueuedMessageQriter` that uses an in memory message queue and handles
  the actual write operations in a separate thread. This should make using
  the other logging writers more palatable in latency critical operations.
- `RollingFileMessageWriter` writes to file as `FileMessageWriter`, but
  handles rolling policy (when a new log file should be created), and
  cleanup policy (when an old log file should be deleted).

There are also a general utility for creating loggable strings based on
a coded manual setup, `LogFormatter`.