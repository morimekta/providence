# The Providence Project

<div style="float:right; width: 30%;">
<img src="img/providence.svg" width="95%" height="auto" />
<span style="font-size: small;">
The "Eye of Providence" symbol, used among others by the Free-masons,
US Treasury (found on the $1 bill) and attributed to the fabled Illuminati.
</span>
</div>

The `providence` project is a project that aims to handle all manners of
managing structured data. It started as a separate implementation of
Apache Thrift for Java, and has been much expanded since then. It still
uses the `thrift` IDL definition as the base for modelling data, but
has expanded upon that to support features not part of thrift. There are
also a lot of different libraries in providence to solve various tasks
that works upon structured data.

## Features of Providence

All of providence is structures around a set of data / information models, with
a set of `core` features, including:

- Modeling most common structures used in data transfers. Note that some of
  the most "generic" features that required untyped parts of the structure
  are **not** supported.
- Interfacing with service API.
- Structural and information validation. _Note that this is currently defined
  in code, not defined in the schema itself._
- Serialization to a number of different formats. It natively support the
  `thrift-binary` protocol, plain `json`. Each of these are handled via
  a number of `Serializer` implementation, which has full access to the
  model definition, and should thus be able to serialize literally anything.

**Other parts of providence are:**

- Using providence models in GraphQL and OpenAPI interfaces with `providence-graphql`
  and `providence-jax-rs`.
- Using providence models as based for application / service configuration with
  `providence-config`.
- Using providence models as storage structures using `providence-storage` and
  `providence-jdbi-v2` / `providence-jdbi-v3`. The storage module currently has
  a `providence-storage-hazelcast` implementation to facilitate storing providence
  models in hazelcast using it's native storage formats.
- Tools to generate providence models based on your own business rules for
  testing.
- Utilities to use providence in logging.
- Command line tools to parse and convert providence models using known
  model definitions.
- Command line tools to generate code in other languages than java using the
  same java framework for handling the models.

Note that providence 2.x **requires** java >= 8.
