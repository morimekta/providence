# Server side GraphQL (BETA)

This modules handles server side handling for bridging
[graphql](https://graphql.github.io/) and providence. Note
that this is a limited implementation of graphql, and should
not be taken as a reference for anything. There are also some
notable limitations:

- `map` types in providence are not supported.
- Only 1 `interface` per object type.
- `union MyUnion of Interface {}` will generate
  a graphql union, and make appropriate output.
  Note that non-interfaced unions are handled as
  simple types.
- No `subscriptions`. TODO: This should be fairly
  feasible using a WebSocket, thou git needs substantial
  amount of research to do it right. I would want to
  avoid having my own protocol for handling these.
- Always transported over `HTTP`.

### From Providence to GraphQL

To make a graphql service we should first look at how
the providence type structure is mapped to graphql, which
is much more limited:

- All primitive types are mapped to built-in scalars.
    - `string` fields are per default mapped to `String`
      scalar type. In order to use `ID` types, the specific
      `ID` fields needs to be designated to when instantiating
      the `GQLDefinition`.
- `union MyUnion of MyInterface {}` are mapped to graphql
  unions. Each field type representing a possible type.
- A single query service is provided, with an optional mutation
  per GQL service. The definition is built upon whatever these
  two services provide and reference.
- Even if types are spread around multiple thrift and providence
  files, there can be no name conflicts.
- No default types should end name with `Input` or `InputType`,
  as it will mess up the `graphql` distinction between output
  and input object types.
- No directives.
- No default values on variables or arguments.

When it comes to the default handling of the grapgql query to
make a response, these are the current limitations:

- Only arguments to the root function will affect the content
  of the struct per default. The sub-structure of fields etc
  is passed on to the processor (instantiated for the method
  call), which will need to update the return structure with
  all data asked for. The output processor will then trim,
  call field-mutators and otherwise build output according to
  field spec.