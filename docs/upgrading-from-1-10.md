# "Upgrading from 1.10 to 2.0"

There are a lot of changes from `providence-1.10` to `providence-2.0`,
and this page will try to outline the most important parts that you
may need to handle as you do the upgrade. This document will outline
the differences where incompatible, or potentially incompatible.

There is a [separate document for new features](new-features-in-2-0.md)
in providence 2.0.

There are a lot of interface changes shat should be *code* compatible,
but are **not** *binary* compatible, so all code using providence will
have to be re-compiled when doing the upgrade. But first in general:

- All code marked as `@Deprecated` in 1.10 has been removed.

## Generated Code

For the most part, usages of the generated code should be unchanged, with
some few exceptions.

- Getter methods on the builder is now matching those of the message,
  meaning `getBooleanValue` is changed to `isBooleanValue`. And behavior
  around messages and containers are slightly changed, but should mostly
  work as before.
- The optional getters for java primitives now use the boxed version, not
  the optional primitive version. This means the interface around
  `optionalIntValue()` and `optionalLongValue()` are changed to using boxed
  numbers.
- On services methods with non-optional fields have more consistently
  primitives and non-null values as arguments. Primitive response values
  will never use boxed version.
- Service processors will handle *all* thrown `Exception`, but not `Error`.
  This will swallow a lot of failures that previously fell though to the
  handling servlet or server, except things like `AssertError` and
  `OutOfMemoryError`. See change in `providence-core-server` for how to
  handle this.

## Providence - Core

Code in the core part of the library has to some extent been moved around,
but most functionality should still be in place.

- `PMessage` has a single generic for the message type itself. The `F extends PField`
  part is dropped and type-safety is now reversed there. Note that this
  should **only** affect code there messages are handled in general terms.
  Essentially all `<M extends PMessage<M, F>, F extends PField>` should
  be replaced with `<M extends PMessage<M>>`.
- `ProvidenceHelper` is no longer and replaced with a `MessageUtil` class
  that has static utilities for nested field lookup, target modification
  checking field path parsing and conversion. The utilities to serialize
  and deserialize as static methods are moved to it's respective
  serializer and renamed to be more consistent, e.g. `toJson` and `toPrettyJson`.
- `TypeRegistry` and associated classes are moved to package
  `net.morimekta.providence.types`, and some interface methods are changed.
- `MessageReader` and `MessageWriter` and associated classes are moved to
  own `providence-logging` module with matching package.
  
## Providence - Core Client

The client side code will most likely not need update.

- The `ProvidenceHttpContent` class is split in two, one that is buffered
  and one that is not. The buffered content class (same behavior as before)
  is the renamed one, so to keep behavior, the content class must be switched.

## Providence - Core Server

Server side code will need update regarding instrumentation and
special exception handling.

- The whole `ProcessorWrapper` concept is dropped, as exception handling
  on the generated processors are significantly more robust. This means
  the job of the wrapper is now placed in a generic exception transformer
  (`Function<Throwable,Throwable>` that is passed into the generated
  processor, and the `PServiceCallInstrumentation` interface.
- The interface to the `ProvidenceHttpServlet` is changed regarding how
  exceptions are handled, so it uses an external `ExceptionHandler` instead
  of overriding internal method. Generics are also reduced to current
  minimum.
  
## Providence - JDBI

- `MessageInserter` is renamed to `MessageUpserter` to use a more generic
  term for the `insert into * on duplicate key *` behavior. The "upsert"
  concept is taken from common REST and GraphQL system documentation.

## Providence - Testing

- The `Generator` and `MessageGenerator` have been refactored, and uses
  will need some updates mostly related to how the context is used and
  managed. And method name changing, e.g. `generate` -> `nextMessage`
  (naming changed to be consistent with `Random`).
- The `GeneratorWatcher` is moved into a `providence-testing-junit4`
  module, and stripped of many forwarding calls to the generator
  context.
- The matchers, except `EqualToMessage` are removed, as using proper
  chained optional calls and default matchers should do the trick. The
  static call to `new EqualToMessage` is moved to the matcher implementation
  itself.
  
## Providence - Config

- `ConfigSupplier` interface cleanup, with propagation to all the
  various suppliers. All the suppliers are moved to the `impl` package.
- `ConfigLoader` generic cleanup, see `PMessage<M>` change above.
- `ConfigLoader` uses the `Path` class as file interface, not `File`, and
  knows how to differentiate between files and resources.
- Inline anchors (`field % foo = "bar"`) support is entirely removed.
- There is some change in how values are converted to match field types,
  but this should rather remove old error-prone code.

## Providence - Reflect

This module should not be used much outside of providence itself, but
may be indirectly used e.g. in relation to providence config. But this
module is **heavily** rewritten, so most uses may have to be updated.

- The module model is moved to the generator, as it prevented proper
  error handling. A new native java declaration model is used instead
  with links back to the original definition. 
- The `ProgramLoader` replaces the `TypeLoader` and have better handling
  of the differences between Apache Thrift and Providence. This enables
  the loader to have a much richer parsing of `.providence` files.

## CLI Tools

- The CLI tools have been renamed, except for the `pvd` tool. Scripts
  will need to be updated to reflect this change:
    - `pvdgen` -> `pvd-generator`
    - `pvdrpc` -> `pvd-rpc`
    - `pvdcfg` -> `pvd-config`
  The options / flags to each should be unchanged, but some nits in how
  includes and type recognition are handled should make them more
  "stable".
