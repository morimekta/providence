# Providence Tools: Maven Plugin

A maven plugin that compiled *java* sources from thrift files that uses
the `providence-core` libraries.

To use the plugin, add it to the `/project/build/plugins` list of the `pom.xml`
file. E.g.:

```xml
<plugin>
    <groupId>net.morimekta.providence</groupId>
    <artifactId>providence-maven-plugin</artifactId>
    <version>${providence.version}</version>
    <extensions>true</extensions>
</plugin>
```

The `generate` and `testGenerate` goals are declared as default lifecycle goals for
the providence plugin (and executed if run with `extensions`).

```xml
<plugin>
    <groupId>net.morimekta.providence</groupId>
    <artifactId>providence-maven-plugin</artifactId>
    <version>${providence.version}</version>
    <executions>
        <execution>
            <goals>
                <goal>generate</goal>
                <goal>testGenerate</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Then you can trigger providence with the goal shorthands on the command line:

```sh
$ mvn providence:generate
$ mvn providence:testGenerate
```
