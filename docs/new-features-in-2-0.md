# New Features In 2.0

There are a lot of changes from `providence-1.10` to `providence-2.0`,
and this page will try to summarize the most important additions,
see the [upgrading documentation](upgrading-from-1-10.md) to see the
most important breaking changes.

## IDL Changes

The IDL changes only apply to `.providence` files, and none of these
are allowed in `.thrift` files, which still require an Apache Thrift
compatible content.

- `interface`: New struct like definition that will make general interfaces
  that other message types can implement. The interface can not declare
  field IDs, and implementing structs **must** declare all fields from
  the interface. Only `struct` variants may implement an interface directly.
  `union` variants can be a `union of ...`, which will force all
  fields to be structs implementing the interface.
- `arguments.type` New annotation that points to a struct for "arguments"
  for customizing the field content. This is used in `graphql` (see point
  below).

## Generated Code

- All messages have a `MyMessage_OrBuilder` interface class that have
  all the getter methods from the message class and some common message
  generic methods (but not all). Both the message type itself and the
  builder implements this interface.
- Service method request and response message classes are public and
  also included in type registries when services are added. This makes
  it possible to use these messages actively in non-generated code, instead
  of only through generic interface code.

## Providence - Core

- New `MessageOrBuilder` interface, see the point above for context. This
  interface is used instead of `PMessage` places which don't really care
  if it's the builder or the instance, e.g. serialization. It also have
  convenient "cast" like methods for getting the builder or message 
  from it. The old `builder()` and `mutate()` methods are still in place
  as they have different generic handling, and are used to delegate from
  the `toMessage` and `toBuilder` calls. 
- `JsonSerializer` can flatten "union of" messages and un-flatten when
  parsing. This is done by adding a `__typename` field and otherwise
  put the contained union field value in place of the union itself. This
  is similar to how apollo graphql for java does interface and union
  deserialization.

## Providence - Core Server

- New `ProvidenceHttpServletWrapper` that makes a REST like collection
  of endpoint using POST to call service methods. Each servlet will be
  like it was with the `ProvidenceHttpServlet`, but with a service
  processor on the inside.

## Providence - JDBI

- Full set of annotations to directly manage argument binding and
  output mapping. This includes `@BindMessage`, `@BindEnumName` and
  `@RegisterMessageMapper`. See the relevant `providence-jdbi-v*`
  module for details.

## Providence - Config

- The `ConfigLoader` can now load full featured config files with
  includes from jar resources.
- When parsing config you can get callbacks with `ConfigException` for
  anything that can be considered a warning. Unknown field, enum value
  etc. Warnings are anything that will fail when parsing in strict mode,
  but not when non-strict.
- `ConfigSupplier` has utility methods to easy get reference config, 
  overrides etc. And reference config supports referencing a default
  value config instead of requiring the reference to point at an
  explicitly declared message.

## Providence - GraphQL

- Full support for putting a graphql endpoint in front of a pair of
  providence services (one for query and one for mutation) with compatible
  introspection, schema and playground.
  
## Schema CLI Tool

- There is a new `pvd-schema` tool to generate schema output from
  thrift definitions. It currently supports basic `json-schema` and
  `graphql`.