# Providence IntelliJ IDEA Plugin

I have created a plugin for IntelliJ IDEA that handles the syntax and
some some error checking for reserved words, duplicated fields, etc.

Latest release can be downloaded
[here](https://gitlab.com/morimekta/providence-idea-plugin/tags).

#### Source Code

The IntelliJ plugin is placed in [its own](https://gitlab.com/morimekta/providence-idea-plugin)
repository as it itself uses the internal IntelliJ (IDEA) build system (not maven as here),
and may require specific versions of IDEA in order to build.
