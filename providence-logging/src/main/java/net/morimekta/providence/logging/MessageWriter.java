/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.logging;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.PServiceCall;

import java.io.Closeable;
import java.io.IOException;

/**
 * An interface for writing messages and service calls.
 */
public interface MessageWriter extends Closeable {
    /**
     * Write a providence message to the writer.
     *
     * @param message The message to write.
     * @param <Message> The message type.
     * @return The number of bytes written.
     * @throws IOException If write failed.
     */
    <Message extends PMessage<Message>>
    int write(PMessageOrBuilder<Message> message) throws IOException;

    /**
     * Write a providence service call to the writer.
     *
     * @param call The service call to write.
     * @param <Message> The message type embedded in the call.
     * @return The number of bytes written.
     * @throws IOException If write failed.
     */
    <Message extends PMessage<Message>>
    int write(PServiceCall<Message> call) throws IOException;

    /**
     * Write an entry separator to the writer.
     *
     * @return The number of bytes written.
     * @throws IOException If write failed.
     */
    int separator() throws IOException;
}
