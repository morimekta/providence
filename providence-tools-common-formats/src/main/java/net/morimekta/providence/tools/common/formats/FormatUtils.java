package net.morimekta.providence.tools.common.formats;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.logging.IOMessageReader;
import net.morimekta.providence.logging.IOMessageWriter;
import net.morimekta.providence.logging.MessageReader;
import net.morimekta.providence.logging.MessageWriter;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.providence.streams.MessageCollectors;
import net.morimekta.providence.streams.MessageSpliterator;
import net.morimekta.providence.tools.common.ProvidenceTools;
import net.morimekta.providence.types.SimpleTypeRegistry;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static net.morimekta.providence.reflect.util.ReflectionUtils.isThriftBasedFileSyntax;
import static net.morimekta.providence.reflect.util.ReflectionUtils.programNameFromPath;
import static net.morimekta.util.FileUtil.readCanonicalPath;

public class FormatUtils {
    private static void collectIncludes(Path include, Map<String, Path> includes, boolean recursive) throws IOException {
        if (!Files.exists(include)) {
            throw new ArgumentException("No such file or directory: " + include);
        }
        include = readCanonicalPath(include);
        if ("**".equals(include.getFileName().toString())) {
            include = include.getParent();
            recursive = true;
        } else if ("*".equals(include.getFileName().toString())) {
            include = include.getParent();
            recursive = false;
        }

        if (!Files.exists(include)) {
            throw new ArgumentException("Broken link: " + include);
        }

        if (Files.isRegularFile(include)) {
            if (Files.isReadable(include) &&
                isThriftBasedFileSyntax(include)) {
                includes.put(programNameFromPath(include), include);
            }
            // else ignore, yes.
        } else if (Files.isDirectory(include)) {
            try (Stream<Path> stream = Files.list(include)) {
                List<Path> list = stream.collect(Collectors.toList());
                for (Path file : list) {
                    file = include.resolve(file);
                    if (Files.isHidden(file)) {
                        continue;
                    }

                    if (recursive) {
                        collectIncludes(file, includes, true);
                    } else {
                        Path f2 = readCanonicalPath(file);
                        if (!Files.exists(f2)) {
                            throw new ArgumentException("Broken link: " + file);
                        }
                        if (Files.isReadable(f2) &&
                            isThriftBasedFileSyntax(f2)) {
                            includes.put(programNameFromPath(f2), f2);
                        }
                    }
                }
            }
        }
        // Else ignore. It's a special type of file.
    }

    private static void collectConfigIncludes(Path rc, Map<String, Path> includes, boolean verbose) throws IOException {
        if (!Files.exists(rc)) {
            return;
        }

        rc = readCanonicalPath(rc);
        if (!Files.isRegularFile(rc)) {
            throw new ConfigException("RC file is not a file " + rc);
        }

        try {
            SimpleTypeRegistry registry = new SimpleTypeRegistry();
            registry.registerType(ProvidenceTools.kDescriptor);
            ConfigLoader    loader = new ConfigLoader(registry, warning -> {
                if (verbose) {
                    System.err.println(warning.displayString());
                }
            });
            ProvidenceTools config = loader.getConfig(rc);

            if (config.hasIncludes()) {
                Path basePath = rc.getParent();
                if (config.hasIncludesBasePath()) {
                    String base = config.getIncludesBasePath();
                    if (base.charAt(0) == '~') {
                        base = System.getenv("HOME") + base.substring(1);
                    }
                    basePath = Paths.get(base).normalize();
                    if (!Files.exists(basePath)) {
                        throw new ConfigException(
                                "Includes Base path in " + rc + " is not a directory: " + basePath);
                    }
                }
                for (String path : config.getIncludes()) {
                    collectIncludes(basePath.resolve(path), includes, false);
                }
            }
        } catch (SerializerException e) {
            System.err.println("Config error: " + e.getMessage());
            System.err.println(e.displayString());
            System.err.println();
            throw new ArgumentException(e, "Exception when parsing " + rc);
        }
    }

    public static Map<String, Path> getIncludeMap(Path rc, List<Path> includes, boolean verbose) throws IOException {
        Map<String, Path> includeMap = new HashMap<>();
        if (includes.isEmpty()) {
            collectConfigIncludes(rc, includeMap, verbose);

            if (includeMap.isEmpty()) {
                throw new ArgumentException("No includes, use --include/-I or update ~/.pvdrc");
            }
        } else {
            for (Path file : includes) {
                collectIncludes(file, includeMap, false);
            }
        }
        return includeMap;
    }

    public static <Message extends PMessage<Message>>
    Stream<Message> getInput(@Nonnull PMessageDescriptor<Message> descriptor,
                             @Nonnull ConvertStream in,
                             boolean strict) throws IOException {
        Serializer serializer = in.format.createSerializer(strict);
        InputStream is = getInputStream(in);
        // Ensures it's closed at the end.
        return StreamSupport.stream(new MessageSpliterator<>(is, serializer, descriptor, is),
                                    false);
    }

    public static MessageReader getServiceInput(ConvertStream in, boolean strict) throws IOException {
        Serializer serializer = in.format.createSerializer(strict);
        return new IOMessageReader(getInputStream(in), serializer);
    }

    private static InputStream getInputStream(ConvertStream in) throws IOException {
        InputStream is;
        if (in.file != null) {
            File file = in.file.getCanonicalFile();
            if (!file.exists()) {
                throw new ArgumentException("%s does not exists", file.getAbsolutePath());
            }
            if (!file.isFile()) {
                throw new ArgumentException("%s is not a file", file.getAbsolutePath());
            }

            is = new FileInputStream(file);
        } else {
            is = new BufferedInputStream(System.in) {
                @Override
                public void close() {
                    // ignore
                }
            };
        }

        if (in.base64mime) {
            is = Base64.getMimeDecoder().wrap(is);
        } else if (in.base64) {
            is = Base64.getDecoder().wrap(is);
        }
        return new BufferedInputStream(is);
    }

    public static <Message extends PMessage<Message>>
    Collector<Message, ?, Integer> getOutput(ConvertStream out,
                                             boolean strict)
            throws IOException {
        Serializer serializer = out.format.createSerializer(strict);
        return MessageCollectors.toStream(getOutputStream(out), serializer, true);
    }

    public static MessageWriter getServiceOutput(ConvertStream out, boolean strict)
            throws IOException {
        Serializer serializer = out.format.createSerializer(strict);
        return new IOMessageWriter(getOutputStream(out), serializer);
    }

    private static OutputStream getOutputStream(ConvertStream out) throws IOException {
        OutputStream os;
        if (out.file != null) {
            File file = out.file.getCanonicalFile();
            if (file.exists() && !file.isFile()) {
                throw new ArgumentException("%s exists and is not a file.", file.getAbsolutePath());
            }
            Files.createDirectories(file.getParentFile().toPath());
            os = new BufferedOutputStream(new FileOutputStream(file));
        } else {
            os = new BufferedOutputStream(System.out) {
                @Override
                public void close() throws IOException {
                    flush();
                }
            };
        }

        if (out.base64mime) {
            os = Base64.getMimeEncoder()
                       .wrap(os);
        } else if (out.base64) {
            os = Base64.getEncoder()
                       .withoutPadding()
                       .wrap(os);
        }
        return os;
    }
}
