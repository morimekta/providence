package net.morimekta.providence.tools.rpc;

import net.morimekta.providence.serializer.DefaultSerializerProvider;
import net.morimekta.providence.server.ProvidenceServlet;
import net.morimekta.providence.tools.rpc.internal.NoLogging;
import net.morimekta.test.providence.Failure;
import net.morimekta.test.providence.MyService;
import net.morimekta.test.providence.Request;
import net.morimekta.test.providence.Response;
import net.morimekta.testing.IntegrationExecutor;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.providence.tools.rpc.internal.TestNetUtil.getExposedPort;
import static net.morimekta.testing.ExtraMatchers.equalToLines;
import static net.morimekta.testing.ResourceUtils.copyResourceTo;
import static net.morimekta.testing.ResourceUtils.getResourceAsStream;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * Test that we can connect to a thrift servlet and get reasonable input and output.
 */
public class RPCProvidenceHttpIT {
    private static final String ENDPOINT = "test";

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    private File                rc;
    private int                 port;
    private Server              server;
    private MyService.Iface     impl;
    private IntegrationExecutor rpc;
    private int                 exitCode;

    private String endpoint() {
        return "http://localhost:" + port + "/" + ENDPOINT;
    }

    @BeforeClass
    public static void setUpProperties() {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "off");
    }

    @Before
    public void setUp() throws Exception {
        Log.setLog(new NoLogging());

        rc = copyResourceTo("/pvdrc", temp.getRoot());
        copyResourceTo("/test.thrift", temp.getRoot());

        impl = mock(MyService.Iface.class);

        server = new Server(0);
        DefaultSerializerProvider provider = new DefaultSerializerProvider();

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new ProvidenceServlet(new MyService.Processor(impl), provider)),
                           "/" + ENDPOINT);

        server.setHandler(handler);
        server.start();
        port = getExposedPort(server);

        Thread.sleep(5);

        exitCode = 0;
        rpc = new IntegrationExecutor("providence-tools-rpc", "providence-tools-rpc.jar");
    }

    @After
    public void tearDown() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleRequest() throws IOException, Failure {
        rpc.setInput(getResourceAsStream("/req1.json"));

        when(impl.test(any(Request.class))).thenReturn(new Response("response"));

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-o", "pretty_json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                "--verbose",
                endpoint());

        assertThat(rpc.getError(), is(""));
        assertThat(rpc.getOutput(), is(
                "[\n" +
                "    \"test\",\n" +
                "    \"reply\",\n" +
                "    44,\n" +
                "    {\n" +
                "        \"success\": {\n" +
                "            \"text\": \"response\"\n" +
                "        }\n" +
                "    }\n" +
                "]\n"));
        assertThat(exitCode, is(0));
    }

    @Test
    public void testSimpleRequest_FileIO() throws IOException, Failure {
        File inFile = copyResourceTo("/req1.json", temp.getRoot());
        File outFile = temp.newFile();

        when(impl.test(any(Request.class))).thenReturn(new Response("response"));

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                "-i", "file:" + inFile.getAbsolutePath(),
                "-o", "json,file:" + outFile.getAbsolutePath(),
                "--verbose",
                endpoint());

        assertThat(rpc.getOutput(), is(""));
        assertThat(rpc.getError(), is(equalToLines("")));
        assertThat(exitCode, is(0));

        verify(impl).test(any(Request.class));

        String out = new String(Files.readAllBytes(outFile.toPath()), UTF_8);
        assertThat(out, is("[\"test\",2,44,{\"0\":{\"1\":\"response\"}}]\n"));
    }

    @Test
    public void testSimpleRequest_exception() throws IOException, Failure {
        rpc.setInput(getResourceAsStream("/req1.json"));

        when(impl.test(any(Request.class))).thenThrow(Failure.builder().setText("failure").build());

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-o", "pretty_json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                endpoint());

        verify(impl).test(any(Request.class));

        assertThat(rpc.getError(), is(""));
        assertThat(rpc.getOutput(), is(
                "[\n" +
                "    \"test\",\n" +
                "    \"reply\",\n" +
                "    44,\n" +
                "    {\n" +
                "        \"f\": {\n" +
                "            \"text\": \"failure\"\n" +
                "        }\n" +
                "    }\n" +
                "]\n"));
        assertThat(exitCode, is(0));
    }

    @Test
    public void testSimpleRequest_404() throws IOException, Failure {
        rpc.setInput(getResourceAsStream("/req1.json"));

        when(impl.test(any(Request.class))).thenThrow(Failure.builder().setText("failure").build());

        rpc.setDeadlineMs(3000);
        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                endpoint() + "_does_not_exsist");

        assertThat(rpc.getOutput(), is(""));
        assertThat(rpc.getError(), is(
                "Received 405 Method Not Allowed\n" +
                " - from: http://localhost:" + port + "/test_does_not_exsist\n"));
        assertThat(exitCode, is(not(0)));
    }

    @Test
    public void testSimpleRequest_wrongMethod() throws IOException, Failure {
        byte[] tmp = ("[\n" +
                      "    \"testing\",\n" +
                      "    \"call\",\n" +
                      "    44,\n" +
                      "    {\n" +
                      "        \"request\": {\n" +
                      "            \"text\": \"request\"\n" +
                      "        }\n" +
                      "    }\n" +
                      "]").getBytes(UTF_8);
        rpc.setInput(new ByteArrayInputStream(tmp));

        when(impl.test(any(Request.class))).thenThrow(new Failure("failure"));

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-o", "pretty_json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService2",
                endpoint());

        verifyNoInteractions(impl);

        assertThat(rpc.getError(), is(""));
        assertThat(rpc.getOutput(), is(
                "[\n" +
                "    \"testing\",\n" +
                "    \"exception\",\n" +
                "    44,\n" +
                "    {\n" +
                "        \"message\": \"No such method testing on test.MyService\",\n" +
                "        \"type\": \"UNKNOWN_METHOD\"\n" +
                "    }\n" +
                "]\n"));
        assertThat(exitCode, is(0));
    }

    @Test
    public void testSimpleRequest_cannotConnect() throws IOException, Failure {
        rpc.setInput(getResourceAsStream("/req1.json"));

        when(impl.test(any(Request.class))).thenReturn(new Response("failure"));

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                "http://localhost:834");

        verifyNoInteractions(impl);

        assertThat(rpc.getOutput(), is(""));
        assertThat(rpc.getError(),
                   allOf(startsWith("Connect to localhost:834 "),
                         containsString(" failed: ")));
        assertThat(exitCode, is(1));
    }

    @Test
    public void testSimpleRequest_unknownHost() throws IOException {
        rpc.setInput(getResourceAsStream("/req1.json"));

        exitCode = rpc.run("--rc", rc.getAbsolutePath(),
                "-i", "json",
                "-I", temp.getRoot().getAbsolutePath(),
                "-s", "test.MyService",
                "http://this-is-a-fake-host-so-skip-it.trust-me:834");

        verifyNoInteractions(impl);

        assertThat(rpc.getOutput(), is(""));
        assertThat(rpc.getError(),
                   startsWith("Unable to resolve host this-is-a-fake-host-so-skip-it.trust-me"));
        assertThat(exitCode, is(1));
    }
}
