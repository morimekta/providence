/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.utils;

import net.morimekta.providence.descriptor.PContainer;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PList;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PSet;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.reflect.contained.CAnnotatedDescriptor;

/**
 *
 */
public class JConst {
    private final PDescriptor type;
    private final JHelper helper;

    public JConst(PDescriptor type, JHelper helper) {
        this.type = type;
        this.helper = helper;
    }

    private PContainer.Type containerType() {
        return (type instanceof CAnnotatedDescriptor) ?
               JAnnotation.containerType((CAnnotatedDescriptor) type) :
               null;
    }

    public String valueType() throws GeneratorException {
        return helper.getValueType(type);
    }

    private String fieldType() throws GeneratorException {
        return valueType();
    }

    public String wrappedBuilderInstanceType() throws GeneratorException  {
        switch (type.getType()) {
            case MAP:
                if (containerType() == PContainer.Type.SORTED) {
                    return PMap.SortedBuilder.class.getName().replace('$', '.');
                }
                return PMap.DefaultBuilder.class.getName().replace('$', '.');
            case SET:
                if (containerType() == PContainer.Type.SORTED) {
                    return PSet.SortedBuilder.class.getName().replace('$', '.');
                }
                return PSet.DefaultBuilder.class.getName().replace('$', '.');
            case LIST:
                return PList.DefaultBuilder.class.getName().replace('$', '.');
            default:
                return fieldType();
        }
    }
}
