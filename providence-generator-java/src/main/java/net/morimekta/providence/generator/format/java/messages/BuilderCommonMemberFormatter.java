/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.messages;

import net.morimekta.providence.PType;
import net.morimekta.providence.descriptor.PAnnotation;
import net.morimekta.providence.descriptor.PContainer;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PEnumDescriptor;
import net.morimekta.providence.descriptor.PInterfaceDescriptor;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.format.java.shared.MessageMemberFormatter;
import net.morimekta.providence.generator.format.java.utils.BlockCommentBuilder;
import net.morimekta.providence.generator.format.java.utils.JAnnotation;
import net.morimekta.providence.generator.format.java.utils.JField;
import net.morimekta.providence.generator.format.java.utils.JHelper;
import net.morimekta.providence.generator.format.java.utils.JMessage;
import net.morimekta.providence.generator.format.java.utils.JUtils;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.io.IndentedPrintWriter;

import java.util.BitSet;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static net.morimekta.providence.PType.I32;
import static net.morimekta.providence.PType.LIST;
import static net.morimekta.providence.PType.MAP;
import static net.morimekta.providence.PType.MESSAGE;
import static net.morimekta.providence.PType.SET;
import static net.morimekta.providence.PType.STRING;
import static net.morimekta.providence.generator.format.java.messages.CoreOverridesFormatter.UNION_FIELD;

/**
 * @author Stein Eldar Johnsen
 * @since 08.01.16.
 */
public class BuilderCommonMemberFormatter implements MessageMemberFormatter {
    protected final IndentedPrintWriter        writer;
    protected final JHelper                    helper;

    public BuilderCommonMemberFormatter(IndentedPrintWriter writer,
                                        JHelper helper) {
        this.writer = writer;
        this.helper = helper;
    }

    @Override
    public Collection<String> getExtraImplements(JMessage<?> message) throws GeneratorException {
        PInterfaceDescriptor implementing = message.descriptor().getImplementing();
        if (implementing != null && !message.isUnion()) {
            return UnmodifiableList.listOf(
                    helper.getJavaPackage(implementing) + "." + JUtils.getInterfaceName(implementing) + "._Builder",
                    JUtils.getInterfaceName(message.descriptor()));
        }
        return UnmodifiableList.listOf(JUtils.getInterfaceName(message.descriptor()));
    }

    @Override
    public void appendClassAnnotations(JMessage<?> message) throws GeneratorException {
        if (JAnnotation.isDeprecated(message.descriptor())) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
    }

    @Override
    public void appendConstructors(JMessage<?> message) throws GeneratorException {
        appendDefaultConstructor(message);
        appendMutateConstructor(message);
    }

    @Override
    public void appendFields(JMessage<?> message) throws GeneratorException {
        if (message.isUnion()) {
            appendUnionFields(message);
        } else {
            if (message.isException()) {
                appendExceptionFields(message);
            }
            appendStructFields(message);
        }
        appendModifiedFields(message);

        for (JField field : message.declaredOrderFields()) {
            // Void fields have no value.
            if (field.isVoid()) {
                continue;
            }
            writer.formatln("private %s %s;", field.fieldType(), field.member());
            if (field.type() == MESSAGE) {
                writer.formatln("private %s._Builder %s_builder;", field.builderFieldType(), field.member());
            }
        }
        if (message.declaredOrderFields()
                   .size() > 0) {
            writer.newline();
        }
    }

    @Override
    public void appendMethods(JMessage<?> message) throws GeneratorException {
        for (JField field : message.declaredOrderFields()) {
            appendSetter(message, field);
            if (field.container()) {
                appendAdder(message, field);
            }
            appendIsSet(message, field);
            if( !message.isUnion() ) {
                appendIsModified(message, field);
            }
            appendResetter(message, field);
            appendMutableGetters(message, field);
            appendGetter(message, field);
        }
        if( message.isUnion() ) {
            appendIsUnionModified(message);
        }
        if (message.isException()) {
            appendInitCause(message);
        }
        appendEquals(message);
        appendHashCode(message);
    }

    @Override
    public void appendExtraProperties(JMessage<?> message) throws GeneratorException {
        appendBuilderBuild(message);
    }

    private void appendBuilderBuild(JMessage<?> message) {
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s build() {", message.instanceType())
              .begin();
        if (message.isException()) {
            writer.formatln("%s e = new %s(this);", message.instanceType(), message.instanceType())
                  .newline();

            writer.appendln("try {")
                  .appendln("    StackTraceElement[] stackTrace = e.getStackTrace();")
                  .appendln("    StackTraceElement[] subTrace = new StackTraceElement[stackTrace.length - 1];")
                  .appendln("    System.arraycopy(stackTrace, 1, subTrace, 0, subTrace.length);")
                  .appendln("    e.setStackTrace(subTrace);")
                  .appendln("} catch (Throwable ignored) {")
                  .appendln("}")
                  .newline();

            writer.appendln("if (cause != null) {")
                  .appendln("    e.initCause(cause);")
                  .appendln("}")
                  .newline()
                  .appendln("return e;");
        } else {
            writer.formatln("return new %s(this);", message.instanceType());
        }
        writer.end()
              .appendln('}');
    }

    private void appendEquals(JMessage<?> message) {
        writer.appendln("@Override")
              .appendln("public boolean equals(Object o) {")
              .begin()
              .appendln("if (o == this) return true;")
              .appendln("if (o == null || !o.getClass().equals(getClass())) return false;");
        if (message.numericalOrderFields()
                   .size() > 0) {
            writer.formatln("%s._Builder other = (%s._Builder) o;", message.instanceType(), message.instanceType())
                  .appendln("return ");
            if (message.isUnion()) {
                writer.format("%s.equals(%s, other.%s)",
                              Objects.class.getName(),
                              UNION_FIELD,
                              UNION_FIELD);
            } else {
                writer.format("%s.equals(optionals, other.optionals)",
                              Objects.class.getName());
            }

            for (JField field : message.declaredOrderFields()) {
                if (field.isVoid()) continue;

                writer.append(" &&")
                      .appendln("       ");
                if (field.type() == MESSAGE) {
                    writer.format("%s.equals(%s(), other.%s())",
                                  Objects.class.getName(),
                                  field.getter(),
                                  field.getter());
                } else {
                    writer.format("%s.equals(%s, other.%s)",
                                  Objects.class.getName(),
                                  field.member(),
                                  field.member());
                }
            }
            writer.append(';');
        } else {
            writer.appendln("return true;");
        }
        writer.end()
              .appendln("}")
              .newline();
    }

    private void appendHashCode(JMessage<?> message) {
        writer.appendln("@Override")
              .appendln("public int hashCode() {")
              .begin()
              .formatln("return %s.hash(",
                        Objects.class.getName())
              .begin("        ")
              .formatln("%s.class", message.instanceType());
        if (!message.isUnion()) {
            writer.append(", optionals");
        }
        message.numericalOrderFields()
               .stream()
               .filter(field -> !field.isVoid())
               .forEach(field -> {
                   if (field.type() == MESSAGE) {
                       writer.append(",");
                       writer.formatln("%s._Field.%s, %s()", message.instanceType(), field.fieldEnum(), field.getter());
                   } else {
                       writer.append(",");
                       writer.formatln("%s._Field.%s, %s", message.instanceType(), field.fieldEnum(), field.member());
                   }
               });

        writer.end()
              .append(");")
              .end()
              .appendln("}")
              .newline();
    }

    private void appendUnionFields(JMessage<?> message) {
        writer.formatln("private %s._Field %s;", message.instanceType(), UNION_FIELD)
              .newline();
    }

    private void appendExceptionFields(JMessage<?> message) {
        writer.formatln("private Throwable cause;");
    }

    private void appendStructFields(JMessage<?> message) {
        writer.formatln("private %s optionals;",
                        BitSet.class.getName());
    }

    private void appendModifiedFields(JMessage<?> message) {
        if (!message.isUnion()) {
            writer.formatln("private %s modified;", BitSet.class.getName())
                  .newline();
        } else {
            writer.formatln("private %s modified;", boolean.class.getName())
                  .newline();
        }
    }

    private void appendDefaultConstructor(JMessage<?> message) throws GeneratorException {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Make a " + message.descriptor().getQualifiedName() + " builder instance.")
               .finish();
        writer.appendln("public _Builder() {")
              .begin();
        if (!message.isUnion()) {
            writer.formatln("optionals = new %s(%d);",
                            BitSet.class.getName(),
                            message.declaredOrderFields()
                                   .size());
            writer.formatln("modified = new %s(%d);",
                            BitSet.class.getName(),
                            message.declaredOrderFields()
                                   .size());
        } else {
            writer.appendln("modified = false;");
        }

        for (JField field : message.declaredOrderFields()) {
            if (field.alwaysPresent()) {
                writer.formatln("%s = %s;", field.member(), field.kDefault());
            }
        }
        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendMutateConstructor(JMessage<?> message) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Make a mutating builder off a base " + message.descriptor().getQualifiedName() + ".")
               .newline()
               .param_("base", "The base " + message.descriptor().getName())
               .finish();
        writer.formatln("public _Builder(%s base) {", message.instanceType())
              .begin()
              .appendln("this();")
              .newline();
        if (message.isUnion()) {
            writer.formatln("%s = base.%s;", UNION_FIELD, UNION_FIELD)
                  .newline();
        }
        for (JField field : message.declaredOrderFields()) {
            boolean checkPresence = message.isUnion() ? field.container() : !field.alwaysPresent();
            if (checkPresence) {
                writer.formatln("if (base.%s()) {", field.presence())
                      .begin();
            }
            if (!message.isUnion()) {
                writer.formatln("optionals.set(%d);", field.index());
            }
            if (field.type() != PType.VOID) {
                writer.formatln("%s = base.%s;", field.member(), field.member());
            }
            if (checkPresence) {
                writer.end()
                      .appendln('}');
            }
        }

        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendSetter(JMessage message, JField field) throws GeneratorException {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Set the <code>" + field.name() + "</code> field value.");
        if (field.hasComment()) {
            comment.paragraph()
                   .comment(field.comment());
        }
        comment.newline();
        if (!field.isVoid()) {
            comment.param_("value", "The new value");
        }
        comment.return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();
        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.appendln(JAnnotation.NON_NULL);
        if (field.isVoid()) {
            // Void fields have no value.
            writer.formatln("public %s._Builder %s() {", message.instanceType(), field.setter())
                  .begin();
        } else {
            writer.formatln("public %s._Builder %s(%s value) {",
                            message.instanceType(), field.setter(), helper.getSetterParamType(field.field().getDescriptor()));
            writer.begin()
                  .formatln("if (value == null) {")
                  .formatln("    return %s();", field.resetter())
                  .appendln('}')
                  .newline();
        }

        if (message.isUnion()) {
            writer.formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
            writer.appendln("modified = true;");
        } else {
            writer.formatln("optionals.set(%d);", field.index());
            writer.formatln("modified.set(%d);", field.index());
        }

        switch (field.type()) {
            case VOID:
                // Void fields have no value.
                break;
            case SET:
            case LIST:
            case MAP:
                writer.formatln("%s = %s;", field.member(), field.fieldInstanceCopy("value"));
                break;
            case MESSAGE:
                writer.formatln("if (value instanceof %s._Builder) {",
                                field.instanceType());
                writer.formatln("    value = ((%s._Builder) value).build();",
                                field.instanceType());
                writer.formatln("} else if (!(value instanceof %s)) {", field.instanceType());
                writer.formatln("    throw new %s(\"Invalid type for %s: \" + value.getClass().getName());",
                                IllegalArgumentException.class.getName(), field.field().getDescriptor().getQualifiedName());
                writer.appendln("}");
                writer.formatln("%s = (%s) value;", field.member(), field.instanceType());
                writer.formatln("%s_builder = null;", field.member());
                break;
            default:
                writer.formatln("%s = value;", field.member());
                break;
        }

        writer.appendln("return this;")
              .end()
              .appendln('}')
              .newline();

        if (field.isPrimitiveJavaValue() && !field.isVoid()) {
            comment = new BlockCommentBuilder(writer);
            comment.commentRaw("Set the <code>" + field.name() + "</code> field value.");
            if (field.hasComment()) {
                comment.paragraph()
                       .comment(field.comment());
            }
            comment.newline();
            if (!field.isVoid()) {
                comment.param_("value", "The new value");
            }
            comment.return_("The builder");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();
            if (JAnnotation.isDeprecated(field)) {
                writer.appendln(JAnnotation.DEPRECATED);
            }
            writer.appendln(JAnnotation.NON_NULL);
            writer.formatln("public %s._Builder %s(%s value) {", message.instanceType(), field.setter(), field.valueType());
            writer.begin();
            if (message.isUnion()) {
                writer.formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
                writer.appendln("modified = true;");
            } else {
                writer.formatln("optionals.set(%d);", field.index());
                writer.formatln("modified.set(%d);", field.index());
            }
            writer.formatln("%s = value;", field.member())
                  .appendln("return this;")
                  .end()
                  .appendln("}")
                  .newline();
        }

        // -------- ENUM REFERENCE --------
        // i32 -> ref.enum

        PDescriptor enumType = field.refEnum(message.descriptor(), helper);
        if (enumType != null) {
            comment = new BlockCommentBuilder(writer);
            comment.commentRaw("Set the value of the <code>" + field.name() + "</code> field.");
            if (field.hasComment()) {
                comment.paragraph()
                       .comment(field.comment());
            }
            comment.newline();
            if (!field.isVoid()) {
                comment.param_("value", "The new value ref");
            }
            comment.return_("The builder");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();
            writer.appendln(JAnnotation.NON_NULL);
            writer.formatln("public %s._Builder %s(%s value) {", message.instanceType(), field.setter(), helper.getValueType(enumType))
                  .begin()
                  .formatln("if (value == null) return %s();", field.resetter());
            if (field.type() == STRING) {
                writer.formatln("return %s(value.asString());", field.setter());
            } else {
                String cast = "";
                if (field.isPrimitiveJavaValue() && field.type() != I32) {
                    PPrimitive primitive = (PPrimitive) field.field().getDescriptor();
                    cast = "(" + primitive.getNativeType().getTypeName() + ") ";
                }

                writer.formatln("return %s(%svalue.asInteger());", field.setter(), cast);
            }
            writer.end()
                  .appendln('}')
                  .newline();
        }

    }

    private void appendAdder(JMessage message, JField field) throws GeneratorException {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);

        if (field.type() == PType.MAP) {
            comment.commentRaw("Adds a mapping to the <code>" + field.name() + "</code> map.");
        } else if (field.type() == SET) {
            comment.commentRaw("Adds entries to the <code>" + field.name() + "</code> set.");
        } else {
            comment.commentRaw("Adds entries to the <code>" + field.name() + "</code> list.");
        }
        if (field.hasComment()) {
            comment.paragraph()
                   .comment(field.comment());
        }
        comment.newline();

        if (field.type() == PType.MAP) {
            comment.param_("key", "The inserted key")
                   .param_("value", "The inserted value");
        } else {
            comment.param_("values", "The added value");
        }
        comment.return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();

        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.appendln(JAnnotation.NON_NULL);

        switch (field.type()) {
            case MAP: {
                PMap<?, ?> mType = (PMap<?, ?>) field.field()
                                                     .getDescriptor();
                String mkType = helper.getValueType(mType.keyDescriptor());
                String miType = helper.getValueType(mType.itemDescriptor());

                writer.formatln("public %s._Builder %s(%s key, %s value) {", message.instanceType(), field.adder(), mkType, miType)
                      .begin();
                if (message.isUnion()) {
                    writer.formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
                    writer.appendln("modified = true;");
                } else {
                    writer.formatln("optionals.set(%d);", field.index());
                    writer.formatln("modified.set(%d);", field.index());
                }
                writer.formatln("%s().put(key, value);", field.mutable())
                      .appendln("return this;")
                      .end()
                      .appendln('}')
                      .newline();

                break;
            }
            case SET:
            case LIST: {
                PContainer<?> lType = (PContainer<?>) field.field()
                                                                 .getDescriptor();
                String liType = helper.getValueType(lType.itemDescriptor());
                if (lType.itemDescriptor().getType() == LIST ||
                    lType.itemDescriptor().getType() == SET ||
                    lType.itemDescriptor().getType() == MAP) {
                    writer.appendln("@SuppressWarnings(\"unchecked\")");
                    writer.appendln("@SafeVarargs");
                    writer.formatln("public final %s._Builder %s(%s... values) {", message.instanceType(), field.adder(), liType)
                          .begin();
                } else {
                    writer.formatln("public %s._Builder %s(%s... values) {", message.instanceType(), field.adder(), liType)
                          .begin();
                }

                if (message.isUnion()) {
                    writer.formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
                    writer.appendln("modified = true;");
                } else {
                    writer.formatln("optionals.set(%d);", field.index());
                    writer.formatln("modified.set(%d);", field.index());
                }
                writer.formatln("%s _container = %s();", field.fieldType(), field.mutable())
                      .formatln("for (%s item : values) {", liType)
                      .begin()
                      .appendln("_container.add(item);")
                      .end()
                      .appendln('}')
                      .appendln("return this;")
                      .end()
                      .appendln('}')
                      .newline();

                break;
            }
            default: {
                throw new GeneratorException("Unexpected field type: " + field.type());
            }
        }
    }

    private void appendIsSet(JMessage message, JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Checks for explicit presence of the <code>" + field.name() + "</code> field.");
        comment.newline()
               .return_(String.format(Locale.US, "True if %s has been set.", field.name()))
               .finish();
        writer.formatln("public boolean %s() {", field.isSet())
              .begin();

        if (message.isUnion()) {
            writer.formatln("return %s == %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
        } else {
            writer.formatln("return optionals.get(%d);", field.index());
        }
        writer.end()
              .appendln('}')
              .newline();

        comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Checks for presence of the <code>" + field.name() + "</code> field.");
        comment.newline()
               .return_(String.format(Locale.US, "True if %s is present.", field.name()))
               .finish();
        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.formatln("public boolean %s() {", field.presence())
              .begin();

        if (message.isUnion()) {
            writer.formatln("return %s == %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
        } else if (field.alwaysPresent()) {
            writer.formatln("return true;");
        } else {
            writer.formatln("return optionals.get(%d);", field.index());
        }
        writer.end()
              .appendln('}')
              .newline();

    }

    private void appendIsModified(JMessage message, JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Checks if the <code>" + field.name() + "</code> field has been modified since the \n" +
                           "builder was created.");

        comment.newline()
               .return_(String.format(Locale.US, "True if %s has been modified.", field.name()))
               .finish();
        writer.formatln("public boolean %s() {", field.isModified())
              .begin();
        writer.formatln("return modified.get(%d);", field.index());
        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendIsUnionModified(JMessage message) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Checks if the <code>" + message.descriptor().getName() + "</code> union has been modified since the \n" +
                           "builder was created.");

        comment.newline()
               .return_(String.format(Locale.US, "True if %s has been modified.", message.descriptor().getName()))
               .finish();
        writer.formatln("public boolean %s() {", "isUnionModified")
              .begin();
        writer.appendln("return modified;");
        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendResetter(JMessage message, JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Clear the <code>" + field.name() + "</code> field.");
        comment.newline()
               .return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();
        writer.appendln(JAnnotation.NON_NULL);
        writer.formatln("public %s._Builder %s() {", message.instanceType(), field.resetter())
              .begin();

        if (message.isUnion()) {
            writer.formatln("if (%s == %s._Field.%s) %s = null;", UNION_FIELD, message.instanceType(), field.fieldEnum(), UNION_FIELD);
            writer.appendln("modified = true;");
        } else {
            writer.formatln("optionals.clear(%d);", field.index());
            writer.formatln("modified.set(%d);", field.index());
        }

        // Void fields have no value.
        if (!field.isVoid()) {
            if (field.alwaysPresent()) {
                writer.formatln("%s = %s;", field.member(), field.kDefault());
            } else {
                writer.formatln("%s = null;", field.member());
                if (field.type() == MESSAGE) {
                    writer.formatln("%s_builder = null;", field.member());
                }
            }
        }

        writer.appendln("return this;")
              .end()
              .appendln('}')
              .newline();
    }

    /**
     * Get mutable values. This returns message builders for messages, collection
     * builders for collections, and the normal immutable value for everything
     * else.
     *
     * @param message The message to get mutable getters for.
     * @param field The field to generate getter for.
     */
    private void appendMutableGetters(JMessage message, JField field) throws GeneratorException {
        switch (field.type()) {
            case MESSAGE: {
                BlockCommentBuilder comment = new BlockCommentBuilder(writer);
                comment.commentRaw("Get the builder for the contained <code>" + field.name() + "</code> message field.");
                if (field.hasComment()) {
                    comment.paragraph()
                           .comment(field.comment());
                }
                comment.newline()
                       .return_("The field message builder");
                if (JAnnotation.isDeprecated(field)) {
                    String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                    if (reason != null && reason.trim().length() > 0) {
                        comment.deprecated_(reason);
                    }
                }
                comment.finish();
                if (JAnnotation.isDeprecated(field)) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                writer.appendln(JAnnotation.NON_NULL);
                writer.formatln("public %s._Builder %s() {", field.instanceType(), field.mutable())
                      .begin();

                if (message.isUnion()) {
                    writer.formatln("if (%s != %s._Field.%s) {", UNION_FIELD, message.instanceType(), field.fieldEnum())
                          .formatln("    %s();", field.resetter())
                          .appendln('}')
                          .formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
                    writer.appendln("modified = true;");
                } else {
                    writer.formatln("optionals.set(%d);", field.index());
                    writer.formatln("modified.set(%d);", field.index());
                }

                writer.newline()
                      .formatln("if (%s != null) {", field.member())
                      .formatln("    %s_builder = %s.mutate();", field.member(), field.member())
                      .formatln("    %s = null;", field.member())
                      .formatln("} else if (%s_builder == null) {", field.member())
                      .formatln("    %s_builder = %s.builder();", field.member(), field.instanceType())
                      .appendln('}')
                      .formatln("return %s_builder;", field.member());

                writer.end()
                      .appendln('}')
                      .newline();
                break;
            }
            case SET:
            case LIST:
            case MAP: {
                BlockCommentBuilder comment = new BlockCommentBuilder(writer);
                if (field.hasComment()) {
                    comment.comment(field.comment())
                           .newline();
                }
                comment.return_("The mutable <code>" + field.name() + "</code> container");
                if (JAnnotation.isDeprecated(field)) {
                    String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                    if (reason != null && reason.trim().length() > 0) {
                        comment.deprecated_(reason);
                    }
                }
                comment.finish();
                if (JAnnotation.isDeprecated(field)) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                writer.formatln("public %s %s() {", field.fieldType(), field.mutable())
                      .begin();

                if (message.isUnion()) {
                    writer.formatln("if (%s != %s._Field.%s) {", UNION_FIELD, message.instanceType(), field.fieldEnum())
                          .formatln("    %s();", field.resetter())
                          .appendln('}')
                          .formatln("%s = %s._Field.%s;", UNION_FIELD, message.instanceType(), field.fieldEnum());
                    writer.appendln("modified = true;");
                } else {
                    writer.formatln("optionals.set(%d);", field.index());
                    writer.formatln("modified.set(%d);", field.index());
                }
                writer.newline()
                      .formatln("if (%s == null) {", field.member())
                      .formatln("    %s = new %s<>();", field.member(), field.builderMutableType())
                      .formatln("} else if (!(%s instanceof %s)) {", field.member(), field.builderMutableType())
                      .formatln("    %s = new %s<>(%s);", field.member(), field.builderMutableType(), field.member())
                      .appendln("}");

                writer.formatln("return %s;", field.member());

                writer.end()
                      .appendln('}')
                      .newline();
                break;
            }
            default: {
                break;
            }
        }
    }

    private void appendGetter(JMessage message, JField field) {
        if (field.isVoid()) {
            return;
        }

        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        if (field.hasComment()) {
            comment.comment(field.comment())
                   .newline();
        }
        comment.return_("The <code>" + field.name() + "</code> field value");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();
        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }

        writer.formatln("public %s %s() {",
                        field.valueType(), field.getter())
              .begin();

        if (field.type() == MESSAGE) {
            if (field.hasDefaultConstant()) {
                writer.formatln("return %s_builder != null ? %s_builder.build() : %s != null ? %s : %s;",
                                field.member(),
                                field.member(),
                                field.member(),
                                field.member(),
                                field.kDefault());
            } else {
                writer.formatln("return %s_builder != null ? %s_builder.build() : %s;",
                                field.member(),
                                field.member(),
                                field.member());
            }
        } else if (field.hasDefaultConstant()) {
            writer.formatln("return %s() ? %s : %s;",
                            field.isSet(),
                            field.member(),
                            field.kDefault());
        } else {
            writer.formatln("return %s;", field.member());
        }

        writer.end()
              .appendln('}')
              .newline();

        // -------- ENUM REFERENCE --------
        // i32 -> ref.enum

        PEnumDescriptor enumType = field.refEnum(message.descriptor(), helper);
        if (enumType != null) {
            comment = new BlockCommentBuilder(writer);
            if (field.hasComment()) {
                comment.comment(field.comment())
                       .newline();
            }
            comment.return_("The <code>" + enumType.getName() + "</code> ref for the " +
                            "<code>" + field.name() + "</code> field, if it has a known value.");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();

            if (JAnnotation.isDeprecated(message.descriptor())) {
                writer.appendln(JAnnotation.DEPRECATED);
            }
            writer.appendln(JAnnotation.NULLABLE);
            writer.formatln("public %s %s() {", helper.getValueType(enumType), field.ref())
                  .begin();

            if (field.type() == PType.STRING) {
                writer.formatln("return %s != null ? %s.findByName(%s) : null;",
                                field.member(), helper.getValueType(enumType), field.member());
            } else {
                if (field.alwaysPresent()) {
                    String cast = "";
                    if (field.type() != PType.I32) {
                        cast = "(int) ";
                    }
                    writer.formatln("return %s.findById(%s%s);",
                                    helper.getValueType(enumType), cast, field.member());
                } else {
                    writer.formatln("return %s != null ? %s.findById(%s.intValue()) : null;",
                                    field.member(), helper.getValueType(enumType), field.member());
                }
            }
            writer.end()
                  .appendln('}')
                  .newline();
        }

        if (!field.alwaysPresent()) {
            comment = new BlockCommentBuilder(writer);
            if (field.hasComment()) {
                comment.comment(field.comment())
                       .newline();
            }
            comment.return_("Optional <code>" + field.name() + "</code> field value");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();
            if (JAnnotation.isDeprecated(field)) {
                writer.appendln(JAnnotation.DEPRECATED);
            }

            writer.appendln(JAnnotation.NON_NULL);
            writer.formatln("public %s<%s> %s() {",
                            Optional.class.getName(),
                            field.fieldType(), field.optional())
                  .begin();

            if (field.type() == MESSAGE) {
                writer.formatln("return %s.ofNullable(%s_builder != null ? %s_builder.build() : %s);",
                                Optional.class.getName(),
                                field.member(),
                                field.member(),
                                field.member());
            } else {
                writer.formatln("return %s.ofNullable(%s);",
                                Optional.class.getName(),
                                field.member());
            }

            writer.end()
                  .appendln('}')
                  .newline();
        }

        if (field.container()) {
            comment = new BlockCommentBuilder(writer);
            if (field.hasComment()) {
                comment.comment(field.comment())
                       .newline();
            }
            comment.return_("Number of entries in <code>" + field.name() + "</code>.");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();
            if (JAnnotation.isDeprecated(field)) {
                writer.appendln(JAnnotation.DEPRECATED);
            }

            writer.formatln("public int %s() {",
                            field.counter())
                  .begin();

            writer.formatln("return %s != null ? %s.size() : 0;",
                            field.member(),
                            field.member());

            writer.end()
                  .appendln('}')
                  .newline();

        }
    }

    private void appendInitCause(JMessage<?> message) {
        new BlockCommentBuilder(writer)
                .commentRaw("Initializes the cause of the " + message.descriptor().getQualifiedName())
                .newline()
                .param_("cause", "The cause")
                .return_("Builder instance")
                .finish();
        writer.appendln(JAnnotation.NON_NULL);
        writer.formatln("public %s._Builder initCause(Throwable cause) {", message.instanceType())
              .appendln("    this.cause = cause;")
              .appendln("    return this;")
              .appendln("}")
              .newline();
    }
}
