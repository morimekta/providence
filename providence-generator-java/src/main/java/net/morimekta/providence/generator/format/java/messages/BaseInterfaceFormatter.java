/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.messages;

import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.PMessageVariant;
import net.morimekta.providence.PType;
import net.morimekta.providence.descriptor.PAnnotation;
import net.morimekta.providence.descriptor.PContainer;
import net.morimekta.providence.descriptor.PDefaultValueProvider;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PDescriptorProvider;
import net.morimekta.providence.descriptor.PEnumDescriptor;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PInterfaceDescriptor;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PRequirement;
import net.morimekta.providence.descriptor.PStructDescriptor;
import net.morimekta.providence.descriptor.PStructDescriptorProvider;
import net.morimekta.providence.descriptor.PValueProvider;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.GeneratorOptions;
import net.morimekta.providence.generator.format.java.JavaOptions;
import net.morimekta.providence.generator.format.java.utils.BlockCommentBuilder;
import net.morimekta.providence.generator.format.java.utils.JAnnotation;
import net.morimekta.providence.generator.format.java.utils.JField;
import net.morimekta.providence.generator.format.java.utils.JHelper;
import net.morimekta.providence.generator.format.java.utils.JMessage;
import net.morimekta.providence.generator.format.java.utils.JUtils;
import net.morimekta.providence.generator.format.java.utils.ValueBuilder;
import net.morimekta.providence.reflect.contained.CAnnotatedDescriptor;
import net.morimekta.util.io.IndentedPrintWriter;

import javax.annotation.Generated;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static net.morimekta.providence.PType.LIST;
import static net.morimekta.providence.PType.MAP;
import static net.morimekta.providence.PType.SET;
import static net.morimekta.providence.generator.format.java.utils.JUtils.getInterfaceName;
import static net.morimekta.providence.types.TypeReference.parseType;

/**
 * Base formatter for messages
 */
public class BaseInterfaceFormatter {
    protected final IndentedPrintWriter writer;

    private final JHelper helper;
    private final JavaOptions javaOptions;
    private final GeneratorOptions generatorOptions;

    public BaseInterfaceFormatter(IndentedPrintWriter writer,
                                  JHelper helper,
                                  JavaOptions javaOptions,
                                  GeneratorOptions generatorOptions) {
        this.writer = writer;
        this.helper = helper;
        this.javaOptions = javaOptions;
        this.generatorOptions = generatorOptions;
    }

    public void appendInterface(PMessageDescriptor<?> descriptor) throws GeneratorException {
        @SuppressWarnings("unchecked")
        JMessage<?> message = new JMessage(descriptor, helper);

        {
            BlockCommentBuilder ifComment = null;
            if (message.descriptor() instanceof CAnnotatedDescriptor) {
                CAnnotatedDescriptor annotatedDescriptor = (CAnnotatedDescriptor) message.descriptor();
                if (annotatedDescriptor.getDocumentation() != null) {
                    ifComment = new BlockCommentBuilder(writer);
                    ifComment.comment(annotatedDescriptor.getDocumentation());
                }
                String deprecatedReason = annotatedDescriptor.getAnnotationValue(PAnnotation.DEPRECATED);
                if (deprecatedReason != null && deprecatedReason.trim().length() > 0) {
                    if (ifComment == null) {
                        ifComment = new BlockCommentBuilder(writer);
                    } else {
                        ifComment.newline();
                    }
                    ifComment.deprecated_(deprecatedReason);
                }
            }
            if (ifComment != null) {
                ifComment.finish();
            }
        }

        if (message.hasAnnotation(PAnnotation.DEPRECATED)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }

        if (!message.descriptor().getName().contains(".")) {
            writer.formatln("@%s(", Generated.class.getName())
                  .begin("        ")
                  .formatln("value = \"net.morimekta.providence:providence-generator-java%s\",",
                            javaOptions.generated_annotation_version ? ":" + generatorOptions.program_version : "");
            if (javaOptions.generated_annotation_date) {
                writer.formatln("date = \"%s\",",
                                DateTimeFormatter.ISO_DATE_TIME.format(ZonedDateTime.now(Clock.systemUTC())
                                                                                    .withNano(0)));
            }
            writer.formatln("comments = \"%s\")",
                            javaOptions.toString())
                  .end();
        }

        String ext = "";
        Set<String> skipFields = new HashSet<>();
        if (descriptor.getImplementing() != null && !message.isUnion()) {
            ext = "extends " +
                  helper.getJavaPackage(descriptor.getImplementing()) + "." +
                  JUtils.getInterfaceName(descriptor.getImplementing()) + " ";
            for (PField field : descriptor.getImplementing().getFields()) {
                skipFields.add(field.getName());
            }
        }

        if (descriptor.getVariant() != PMessageVariant.INTERFACE) {
            if (ext.isEmpty()) {
                ext = "extends ";
            } else {
                ext += ", ";
            }
            ext += PMessageOrBuilder.class.getName() + "<" + JUtils.getClassName(descriptor) + "> ";
        }

        writer.appendln(JAnnotation.SUPPRESS_WARNINGS_UNUSED);
        writer.formatln("public interface %s %s{",
                        getInterfaceName(descriptor), ext)
              .begin();


        for (JField field : message.declaredOrderFields()) {
            if (skipFields.contains(field.name())) {
                continue;
            }

            boolean isDeprecated = false;
            String deprecatedReason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (deprecatedReason != null) {
                if (deprecatedReason.trim().isEmpty()) {
                    deprecatedReason = null;
                }
                isDeprecated = true;
            }

            if (!field.isVoid()) {
                {
                    BlockCommentBuilder getComment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        getComment.comment(field.comment());
                        getComment.newline();
                    }

                    getComment.return_("The " + field.name() + " value.");
                    if (deprecatedReason != null) {
                        getComment.deprecated_(deprecatedReason);
                    }
                    getComment.finish();
                    if (isDeprecated) {
                        writer.appendln(JAnnotation.DEPRECATED);
                    }
                    if (field.alwaysPresent() && !field.isPrimitiveJavaValue()) {
                        writer.appendln(JAnnotation.NON_NULL);
                    }
                    writer.formatln("%s %s();",
                                    field.valueType(),
                                    field.getter());
                    writer.newline();
                }

                PEnumDescriptor enumType = field.refEnum(message.descriptor(), helper);
                if (enumType != null) {
                    BlockCommentBuilder comment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        comment.comment(field.comment())
                               .newline();
                    }
                    comment.return_("The <code>" + enumType.getName() + "</code> ref for the " +
                                    "<code>" + field.name() + "</code> field, if it has a known value.");
                    if (JAnnotation.isDeprecated(field)) {
                        String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                        if (reason != null && reason.trim().length() > 0) {
                            comment.deprecated_(reason);
                        }
                    }
                    comment.finish();

                    if (JAnnotation.isDeprecated(message.descriptor())) {
                        writer.appendln(JAnnotation.DEPRECATED);
                    }
                    writer.appendln(JAnnotation.NULLABLE);
                    writer.formatln("%s %s();", helper.getValueType(enumType), field.ref())
                          .newline();
                }

                if (!field.alwaysPresent()) {
                    BlockCommentBuilder optionalComment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        optionalComment.comment(field.comment());
                        optionalComment.newline();
                    }

                    optionalComment.return_("Optional " + field.name() + " value.");
                    if (deprecatedReason != null) {
                        optionalComment.deprecated_(deprecatedReason);
                    }
                    optionalComment.finish();
                    if (isDeprecated) {
                        writer.appendln(JAnnotation.DEPRECATED);
                    }
                    writer.appendln(JAnnotation.NON_NULL);
                    writer.formatln("%s<%s> %s();",
                                    Optional.class.getName(),
                                    field.fieldType(),
                                    field.optional());
                    writer.newline();
                }
            }

            {
                BlockCommentBuilder hasComment = new BlockCommentBuilder(writer);
                hasComment.return_("If " + field.name() + " is present.");
                if (deprecatedReason != null) {
                    hasComment.deprecated_(deprecatedReason);
                }
                hasComment.finish();
                if (isDeprecated) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                writer.formatln("boolean %s();",
                                field.presence());
            }

            if (field.container()) {
                writer.newline();

                {
                    BlockCommentBuilder numComment = new BlockCommentBuilder(writer);
                    numComment.return_("Number of entries in " + field.name() + ".");
                    if (deprecatedReason != null) {
                        numComment.deprecated_(deprecatedReason);
                    }
                    numComment.finish();
                }
                if (isDeprecated) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                writer.formatln("int %s();",
                                field.counter());

            }
            writer.newline();
        }

        if (message.variant() == PMessageVariant.INTERFACE) {
            appendFieldEnum(message);
            appendDescriptor(message);
            appendBuilder(message);
        }

        writer.end()
              .appendln('}').newline();
    }

    private void appendBuilder(JMessage<?> message) {
        writer.newline()
              .formatln("interface _Builder extends %s {", message.instanceType())
              .begin();

        for (JField field : message.declaredOrderFields()) {
            appendBuilderSetter(field);

            if (field.container()) {
                appendBuilderAdder(field);
            }

            appendBuilderResetter(field);
        }

        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.return_("The built instance");
        comment.finish();

        writer.appendln(JAnnotation.NON_NULL);
        writer.formatln("%s build();", message.instanceType());

        writer.end()
              .appendln("}");
    }

    private void appendBuilderAdder(JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);

        if (field.type() == PType.MAP) {
            comment.commentRaw("Adds a mapping to the <code>" + field.name() + "</code> map.");
        } else if (field.type() == SET) {
            comment.commentRaw("Adds entries to the <code>" + field.name() + "</code> set.");
        } else {
            comment.commentRaw("Adds entries to the <code>" + field.name() + "</code> list.");
        }
        if (field.hasComment()) {
            comment.paragraph()
                   .comment(field.comment());
        }
        comment.newline();

        if (field.type() == PType.MAP) {
            comment.param_("key", "The inserted key")
                   .param_("value", "The inserted value");
        } else {
            comment.param_("values", "The added value");
        }
        comment.return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();

        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.appendln(JAnnotation.NON_NULL);

        switch (field.type()) {
            case MAP: {
                PMap<?, ?> mType = (PMap<?, ?>) field.field()
                                                     .getDescriptor();
                String mkType = helper.getValueType(mType.keyDescriptor());
                String miType = helper.getValueType(mType.itemDescriptor());

                writer.formatln("public _Builder %s(%s key, %s value);", field.adder(), mkType, miType);
                writer.newline();
                break;
            }
            case SET:
            case LIST: {
                PContainer<?> lType = (PContainer<?>) field.field()
                                                           .getDescriptor();
                String liType = helper.getValueType(lType.itemDescriptor());
                if (lType.itemDescriptor().getType() == LIST ||
                    lType.itemDescriptor().getType() == SET ||
                    lType.itemDescriptor().getType() == MAP) {
                    writer.formatln("public _Builder %s(%s... values);", field.adder(), liType);
                } else {
                    writer.formatln("public _Builder %s(%s... values);", field.adder(), liType);
                }
                writer.newline();
                break;
            }
            default: {
                throw new GeneratorException("Unexpected field type: " + field.type());
            }
        }
    }

    private void appendBuilderSetter(JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Set the <code>" + field.name() + "</code> field value.");
        if (field.hasComment()) {
            comment.paragraph()
                   .comment(field.comment());
        }
        comment.newline();
        if (!field.isVoid()) {
            comment.param_("value", "The new value");
        }
        comment.return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();
        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.appendln(JAnnotation.NON_NULL);
        if (field.isVoid()) {
            // Void fields have no value.
            writer.formatln("public _Builder %s();", field.setter());
        } else {
            writer.formatln("public _Builder %s(%s value);",
                            field.setter(),
                            helper.getSetterParamType(field.field().getDescriptor()));
        }
        writer.newline();

        if (field.isPrimitiveJavaValue() && !field.isVoid()) {
            comment = new BlockCommentBuilder(writer);
            comment.commentRaw("Set the <code>" + field.name() + "</code> field value.");
            if (field.hasComment()) {
                comment.paragraph()
                       .comment(field.comment());
            }
            comment.newline();
            if (!field.isVoid()) {
                comment.param_("value", "The new value");
            }
            comment.return_("The builder");
            if (JAnnotation.isDeprecated(field)) {
                String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                if (reason != null && reason.trim().length() > 0) {
                    comment.deprecated_(reason);
                }
            }
            comment.finish();
            if (JAnnotation.isDeprecated(field)) {
                writer.appendln(JAnnotation.DEPRECATED);
            }
            writer.appendln(JAnnotation.NON_NULL);
            writer.formatln("public _Builder %s(%s value);", field.setter(), field.valueType())
                  .newline();
        }

    }

    private void appendBuilderResetter(JField field) {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        comment.commentRaw("Clear the <code>" + field.name() + "</code> field value.");
        if (field.hasComment()) {
            comment.paragraph()
                   .comment(field.comment());
        }
        comment.newline();
        comment.return_("The builder");
        if (JAnnotation.isDeprecated(field)) {
            String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
            if (reason != null && reason.trim().length() > 0) {
                comment.deprecated_(reason);
            }
        }
        comment.finish();
        if (JAnnotation.isDeprecated(field)) {
            writer.appendln(JAnnotation.DEPRECATED);
        }
        writer.appendln(JAnnotation.NON_NULL);
        writer.formatln("public _Builder %s();", field.resetter());
        writer.newline();
    }

    private void appendDescriptor(JMessage<?> message) {
        writer.appendln(JAnnotation.SUPPRESS_WARNINGS_UNCHECKED)
              .formatln("%s<?> kDescriptor =",
                        PInterfaceDescriptor.class.getName())
              .formatln("        new %s<>(\"%s\", \"%s\", _Field.values()",
                        PInterfaceDescriptor.class.getName(),
                        message.descriptor().getProgramName(),
                        message.descriptor().getName());
        writer.begin("                ");

        for (PMessageDescriptor<?> possibleType : ((PInterfaceDescriptor<?>) message.descriptor()).getPossibleTypes()) {
            writer.append(",").appendln();
            writer.append(helper.getProviderName(possibleType));
        }

        writer.end()
              .append(");");
    }

    private void appendFieldEnum(JMessage<?> message) {
        writer.formatln("enum _Field implements %s {", PField.class.getName())
              .begin();

        for (JField field : message.declaredOrderFields()) {
            String arguments = "null";
            String argsType = field.field().getAnnotationValue(PAnnotation.ARGUMENTS_TYPE);
            if (argsType != null) {
                try {
                    PMessageDescriptor descriptor = helper.getRegistry().requireMessageType(parseType(
                            message.descriptor().getProgramName(), argsType));
                    if (descriptor.getVariant() != PMessageVariant.STRUCT) {
                        throw new GeneratorException("Bad arguments type " + descriptor.getQualifiedName() +
                                                     " for field " + field.name() + " in " + message.descriptor().getQualifiedName() +
                                                     ", not a struct.");

                    }
                    arguments = helper.getProviderName(descriptor);
                } catch (IllegalArgumentException e) {
                    throw new GeneratorException("No such arguments type available " +
                                                 field.field().getAnnotationValue(PAnnotation.ARGUMENTS_TYPE) +
                                                 " for field " + field.name() + " in " + message.descriptor().getQualifiedName());
                }
            }

            writer.formatln("%s(%d, %s.%s, \"%s\", \"%s\", %s, %s, ",
                            field.fieldEnum(),
                            field.id(),
                            PRequirement.class.getName(),
                            field.field().getRequirement().toString(),
                            field.name(),
                            field.field().getPojoName(),
                            field.getProvider(),
                            arguments);

            if (field.field().hasDefaultValue()) {
                writer.format("new %s<>(", PDefaultValueProvider.class.getName());
                ValueBuilder values = new ValueBuilder(writer, helper);
                values.appendTypedValue(field.field().getDefaultValue(), field.field().getDescriptor());
                writer.append(")");
            } else {
                writer.append("null");
            }

            writer.append("),");
        }

        writer.appendln(";")
              .newline();

        writer.appendln("private final int mId;")
              .formatln("private final %s mRequired;", PRequirement.class.getName())
              .appendln("private final String mName;")
              .appendln("private final String mPojoName;")
              .formatln("private final %s mTypeProvider;", PDescriptorProvider.class.getName())
              .formatln("private final %s mArgumentsProvider;", PStructDescriptorProvider.class.getName())
              .formatln("private final %s<?> mDefaultValue;", PValueProvider.class.getName())
              .newline();

        writer.formatln("_Field(int id, %s required, String name, String pojoName, %s typeProvider, %s argumentsProvider, %s<?> defaultValue) {",
                        PRequirement.class.getName(),
                        PDescriptorProvider.class.getName(),
                        PStructDescriptorProvider.class.getName(),
                        PValueProvider.class.getName())
              .begin()
              .appendln("mId = id;")
              .appendln("mRequired = required;")
              .appendln("mName = name;")
              .appendln("mPojoName = pojoName;")
              .appendln("mTypeProvider = typeProvider;")
              .appendln("mArgumentsProvider = argumentsProvider;")
              .appendln("mDefaultValue = defaultValue;")
              .end()
              .appendln('}')
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public int getId() { return mId; }")
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getRequirement() { return mRequired; }",
                        PRequirement.class.getName())
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getDescriptor() { return mTypeProvider.descriptor(); }",
                        PDescriptor.class.getName())
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .formatln("public %s getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }",
                        PStructDescriptor.class.getName())
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .appendln("public String getName() { return mName; }")
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .appendln("public String getPojoName() { return mPojoName; }")
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public boolean hasDefaultValue() { return mDefaultValue != null; }")
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .appendln("public Object getDefaultValue() {")
              .appendln("    return hasDefaultValue() ? mDefaultValue.get() : null;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.SUPPRESS_WARNINGS_UNCHECKED)
              .formatln("public %s onMessageType() {",
                        PMessageDescriptor.class.getName())
              .appendln("    return kDescriptor;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public String toString() {")
              .formatln("    return %s.asString(this);", PField.class.getName())
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("id", "Field name")
                .return_("The identified field or null")
                .finish();
        writer.appendln("public static _Field findById(int id) {")
              .begin()
              .appendln("switch (id) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            writer.formatln("case %d: return _Field.%s;", field.id(), field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field name")
                .return_("The named field or null")
                .finish();
        writer.appendln("public static _Field findByName(String name) {")
              .begin()
              .appendln("if (name == null) return null;")
              .appendln("switch (name) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            writer.formatln("case \"%s\": return _Field.%s;", field.name(), field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field POJO name")
                .return_("The named field or null")
                .finish();
        writer.appendln("public static _Field findByPojoName(String name) {")
              .begin()
              .appendln("if (name == null) return null;")
              .appendln("switch (name) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            writer.formatln("case \"%s\": return _Field.%s;", field.field().getPojoName(), field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("id", "Field name")
                .return_("The identified field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForId(int id) {")
              .begin()
              .appendln("_Field field = findById(id);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field id \" + id + \" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field name")
                .return_("The named field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForName(String name) {")
              .begin()
              .appendln("if (name == null) {")
              .appendln("    throw new IllegalArgumentException(\"Null name argument\");")
              .appendln("}")
              .appendln("_Field field = findByName(name);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field \\\"\" + name + \"\\\" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field POJO name")
                .return_("The named field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForPojoName(String name) {")
              .begin()
              .appendln("if (name == null) {")
              .appendln("    throw new IllegalArgumentException(\"Null name argument\");")
              .appendln("}")
              .appendln("_Field field = findByPojoName(name);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field \\\"\" + name + \"\\\" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}');

        writer.end()
              .appendln("}")
              .newline();
    }
}
