package net.morimekta.providence.graphql.utils;

import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.graphql.GQLDefinition;
import net.morimekta.providence.graphql.GQLFieldProvider;
import net.morimekta.providence.graphql.gql.GQLField;
import net.morimekta.providence.graphql.introspection.InputValue;
import net.morimekta.providence.graphql.introspection.Type;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import java.util.Collection;

public class InputValueFieldProvider extends BaseTypeProvider implements GQLFieldProvider<InputValue> {
    public InputValueFieldProvider(@Nonnull GQLDefinition definition) {
        super(definition);
    }

    @Override
    public PMessageDescriptor<InputValue> getDescriptor() {
        return InputValue.kDescriptor;
    }

    @Nonnull
    @Override
    public Collection<PField<InputValue>> getFields() {
        return UnmodifiableList.listOf(
                InputValue._Field.TYPE);
    }

    @Override
    public Type provide(@Nonnull InputValue message, @Nonnull PField<InputValue> field, @Nonnull GQLField selection) {
        return resolveType(message.getType());
    }
}
