package net.morimekta.providence.it.serialization;

import io.codearte.jfairy.Fairy;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.serializer.BinarySerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.streams.MessageStreams;
import net.morimekta.providence.testing.junit4.GeneratorWatcher;
import net.morimekta.providence.testing.junit4.SimpleGeneratorWatcher;
import net.morimekta.util.collect.UnmodifiableList;
import org.apache.thrift.TBase;
import org.apache.thrift.TException;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Data generator for running tests for a given object type.
 */
public class ITGenerator<PM extends PMessage<PM>,
                         TM extends TBase<TM, TF>, TF extends TFieldIdEnum> {
    private final PMessageDescriptor<PM> descriptor;
    private final Supplier<TM>           supplier;

    private final UnmodifiableList.Builder<PM>                     pvdListBuilder;
    private final UnmodifiableList.Builder<TM>                     thrListBuilder;
    private final GeneratorWatcher<SimpleGeneratorWatcher.Context> generator;

    private static final Fairy[]    LOCAL_FAIRIES = new Fairy[]{
            Fairy.create(Locale.ENGLISH),
            Fairy.create(Locale.FRENCH),
            Fairy.create(Locale.GERMAN),
            Fairy.create(Locale.ITALIAN),
            Fairy.create(Locale.SIMPLIFIED_CHINESE),
            Fairy.create(new Locale("es")),
            Fairy.create(new Locale("pl")),
            Fairy.create(new Locale("sv")),
    };
    private static final Serializer BINARY        = new BinarySerializer(true);

    public ITGenerator(PMessageDescriptor<PM> descriptor,
                       Supplier<TM> supplier,
                       SimpleGeneratorWatcher generator) {
        this.descriptor = descriptor;
        this.supplier = supplier;
        this.thrListBuilder = UnmodifiableList.builder();
        this.pvdListBuilder = UnmodifiableList.builder();
        this.generator = generator.apply(ctx -> {
            ctx.withGenerator(PPrimitive.STRING, ctx1 -> ctx1.getFairy().textProducer().sentence());
        });
    }

    public void generate(final int n) throws IOException, TException {
        for (int i = 0; i < n; ++i) {
            Fairy fairy = LOCAL_FAIRIES[new Random().nextInt(LOCAL_FAIRIES.length)];
            generator.apply(ctx -> ctx.setFairy(fairy));

            PM providence = generator.generate(descriptor);
            TM thrift = convert(providence);

            pvdListBuilder.add(providence);
            thrListBuilder.add(thrift);
        }
    }

    public void load(File file) throws IOException {
        MessageStreams.file(file, BINARY, descriptor).forEach(providence -> {
            try {
                TM thrift = convert(providence);

                pvdListBuilder.add(providence);
                thrListBuilder.add(thrift);
            } catch (TException e) {
                throw new RuntimeException(e.getMessage(), e);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
    }

    private TM convert(PM message) throws IOException, TException {
        TM tBase = supplier.get();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BINARY.serialize(baos, message);

        TIOStreamTransport transport = new TIOStreamTransport(new ByteArrayInputStream(baos.toByteArray()));
        TProtocol protocol = new TBinaryProtocol(transport);
        tBase.read(protocol);

        return tBase;
    }

    public List<PM> getProvidence() {
        return pvdListBuilder.build();
    }

    public List<TM> getThrift() {
        return thrListBuilder.build();
    }
}
