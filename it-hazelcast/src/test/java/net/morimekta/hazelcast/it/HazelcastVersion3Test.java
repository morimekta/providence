package net.morimekta.hazelcast.it;

import net.morimekta.test.hazelcast.v3.PortableFields;
import net.morimekta.test.hazelcast.v3.PortableListFields;
import net.morimekta.test.hazelcast.v3.PortableMapFields;
import net.morimekta.test.hazelcast.v3.PortableMapListFields;
import net.morimekta.test.hazelcast.v3.PortableMapSetFields;
import net.morimekta.test.hazelcast.v3.PortableSetFields;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * TBD
 */
public class HazelcastVersion3Test extends GenericMethods {
    private static HazelcastInstance instance1;
    private static HazelcastInstance instance2;

    @BeforeClass
    public static void setUpHazelcast() {
        instance1 = Hazelcast.newHazelcastInstance(getV3Config());
        instance2 = Hazelcast.newHazelcastInstance(getV3Config());
    }

    @AfterClass
    public static void tearDownHazelcast() {
        instance1.shutdown();
        instance2.shutdown();
    }

    @Test
    public void testMapIntegrityAll() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_list() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_set() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_map() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableMapFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_maplist() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableMapListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_mapset() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableMapSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_list() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_set() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_map() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_maplist() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_mapset() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapSetFields.kDescriptor);
    }
}
