package net.morimekta.providence.testing.junit4;

import io.codearte.jfairy.Fairy;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.PrettySerializer;
import net.morimekta.test.providence.testing.CompactFields;
import net.morimekta.testing.rules.ConsoleWatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Description;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class GeneratorWatcherTest {
    @Rule
    public ConsoleWatcher console = new ConsoleWatcher();

    @Test
    public void testRandom_defaultDump() {
        GeneratorWatcher<SimpleGeneratorWatcher.Context> generator =
                new SimpleGeneratorWatcher().dumpOnFailure();
        generator.starting(Description.EMPTY);

        CompactFields compact = generator.generate(CompactFields.kDescriptor);

        assertThat(compact.getLabel(), is(notNullValue()));
        assertThat(compact.getName(), is(notNullValue()));
        assertThat(compact.hasId(), is(true));

        assertThat(generator.context().getGeneratedMessages(), hasItem(compact));

        generator.failed(new Throwable(), Description.EMPTY);

        assertThat(console.output(), is(""));
        assertThat(console.error(), is(pretty(compact) + "\n"));
    }

    @Test
    public void testRandom_multipleMessages() {
        GeneratorWatcher<SimpleGeneratorWatcher.Context> generator =
                new SimpleGeneratorWatcher()
                        .apply(ctx -> ctx.setFillRate(0.667))
                        .dumpOnFailure();
        generator.starting(Description.EMPTY);

        ArrayList<CompactFields> list = new ArrayList<>(100);
        IntStream.range(0, 100).forEach(i -> list.add(generator.generate(CompactFields.kDescriptor)));

        assertThat(list.size(), is(100));
        for (CompactFields compact : list) {
            assertThat(generator.context().getGeneratedMessages(), hasItem(compact));
        }

        generator.failed(new Throwable(), Description.EMPTY);

        assertThat(console.output(), is(""));
        for (CompactFields compact : list) {
            assertThat(console.error(), containsString(pretty(compact) + "\n"));
        }
    }

    @Test
    public void testRandom_customSerializer() throws IOException {
        Random random = new Random();
        Fairy  fairy  = Fairy.create(Locale.ENGLISH);
        GeneratorWatcher<SimpleGeneratorWatcher.Context> watcher = GeneratorWatcher
                .create()
                .apply(context -> context.setFairy(fairy)
                                         .setMaxCollectionSize(2)
                                         .setRandom(random)
                                         .withMessageGenerator(CompactFields.kDescriptor,
                                                               gen -> gen.setAlwaysPresent(CompactFields._Field.NAME)
                                                                         .setAlwaysAbsent(CompactFields._Field.LABEL)
                                                                         .setValueGenerator(CompactFields._Field.NAME,
                                                                                            ctx -> fairy.textProducer()
                                                                                                        .word(1))))
                .setOutputSerializer(new JsonSerializer())
                .dumpOnFailure();
        watcher.starting(Description.EMPTY);

        CompactFields compact = watcher.generate(CompactFields.kDescriptor);

        assertThat(compact.getLabel(), is(nullValue()));
        assertThat(compact.getName(), is(notNullValue()));
        assertThat(compact.getName(), not(containsString(" ")));
        assertThat(compact.hasId(), is(true));

        assertThat(watcher.context().getGeneratedMessages(), hasItem(compact));

        watcher.failed(new Throwable(), Description.EMPTY);

        assertThat(console.output(), is(""));
        assertThat(console.error(), is(json(compact) + "\n"));
    }

    @Test
    public void testRandom_noDump() {
        GeneratorWatcher<SimpleGeneratorWatcher.Context> generator =
                GeneratorWatcher.create();
        generator.starting(Description.EMPTY);

        CompactFields compact = generator.generate(CompactFields.kDescriptor);

        assertThat(compact.getLabel(), is(notNullValue()));
        assertThat(compact.getName(), is(notNullValue()));
        assertThat(compact.hasId(), is(true));
        assertThat(generator.context().getGeneratedMessages(), hasItem(compact));

        generator.failed(new Throwable(), Description.EMPTY);

        assertThat(console.output(), is(""));
        assertThat(console.error(), is(""));
    }

    private <M extends PMessage<M>> String json(M message) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new JsonSerializer().serialize(baos, message);
        return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }

    private <M extends PMessage<M>> String pretty(M message) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new PrettySerializer().config().serialize(baos, message);
        return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }
}
