Providence Serialization 
========================

[![Pipeline Status](https://gitlab.com/morimekta/providence/badges/master/pipeline.svg)](https://gitlab.com/morimekta/providence/commits/master)
[![Code Coverage](https://img.shields.io/codecov/c/gl/morimekta/providence/master.svg)](https://codecov.io/gl/morimekta/providence/branch/master)
[![Javadocs](https://www.javadoc.io/badge/net.morimekta.providence/providence.svg)](https://www.javadoc.io/doc/net.morimekta.providence/providence)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)

The `providence` project was made in order to make an immutable model java
library for thrift. It is mostly separate from the thrift library, but can use
the standard thrift protocols to serialize and deserialize messages. It is mainly
based on the Facebook / Apache [thrift](https://thrift.apache.org/) library,
but with some differences and limitations.

See the project site at [morimekta.net/providence](https://morimekta.net/providence/)
for setup and usage instructions.

### Also See

Projects that are strongly related to this:

- [Providence Gradle Plugin](https://gitlab.com/morimekta/providence-gradle-plugin)
- [Providence IntelliJ Plugin](https://gitlab.com/morimekta/providence-idea-plugin)
- [Morimekta IO-Util](https://gitlab.com/morimekta/io-util)
- [Morimekta Console-Util](https://gitlab.com/morimekta/console-util)
- [Morimekta Testing-Util](https://gitlab.com/morimekta/testing-util)
- [JavaScript Code Generator](https://gitlab.com/morimekta/providence-js)