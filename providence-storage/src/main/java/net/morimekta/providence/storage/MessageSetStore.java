package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import java.util.Collection;

import static net.morimekta.providence.util.MessageUtil.toMessageAll;

/**
 * Interface for storing messages of a single type as if it's a set of
 * distinct messages. Note that the distinctness is based on some arbitrary
 * <code>key</code> definition, not on the message as a whole. Each
 * implementation will need to handle the message to key mapping itself.
 *
 * @param <K> The key type.
 * @param <M> The stored message type.
 */
public interface MessageSetStore<K, M extends PMessage<M>>
        extends MessageReadOnlyStore<K, M>, ReadWriteSetStore<K, M> {
    /**
     * Put the message represented by the builder into the store on the given key.
     * Any further modifications to the builder will not be reflected on the store.
     *
     * @param builder The builder to store.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putBuilder(@Nonnull B builder) {
        put(builder.build());
    }

    /**
     * Put a collection of key and builder pairs onto the store. Any further modifications
     * to the builders will not be reflected onto the store.
     *
     * @param builders Collection of builders to put into the store.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putAllBuilders(@Nonnull Collection<B> builders) {
        putAll(toMessageAll(builders));
    }
}
