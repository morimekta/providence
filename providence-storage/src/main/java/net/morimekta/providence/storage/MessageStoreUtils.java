package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.util.MessageUtil;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Utilities for message store implementations and interfaces.
 *
 * @deprecated Use {@link MessageUtil} methods instead. This class will be removed in
 *             a future major release.
 */
@Deprecated
@SuppressWarnings("unused")
public class MessageStoreUtils {
    /**
     * Build all items of the collection containing builders. The list must not
     * contain any null items.
     *
     * @param builders List of builders.
     * @param <M> The message type.
     * @param <V> The actual value type.
     * @return List of messages or null if null input.
     */
    public static <M extends PMessage<M>, V extends PMessageOrBuilder<M>>
    List<M> toMessageAll(Collection<V> builders) {
        return MessageUtil.toMessageAll(builders);
    }

    /**
     * Mutate all items of the collection containing messages. The list must not
     * contain any null items.
     *
     * @param messages List of messages
     * @param <M> The message type.
     * @param <V> The actual value type.
     * @param <B> The builder type.
     * @return List of builders or null if null input.
     */
    public static <M extends PMessage<M>, V extends PMessageOrBuilder<M>, B extends PMessageBuilder<M>>
    List<B> toBuilderAll(Collection<V> messages) {
        return MessageUtil.toBuilderAll(messages);
    }

    /**
     * Mutate all items of the collection containing messages. The list must not
     * contain any null items.
     *
     * @param messages List of messages
     * @param <K> The map key type.
     * @param <M> The message type.
     * @param <V> The actual value type.
     * @param <B> The builder type.
     * @return List of builders or null if null input.
     */
    public static <K, M extends PMessage<M>, V extends PMessageOrBuilder<M>, B extends PMessageBuilder<M>>
    Map<K, B> toBuilderValues(Map<K, V> messages) {
        return MessageUtil.toBuilderValues(messages);
    }

    /**
     * Mutate all items of the collection containing messages. The list must not
     * contain any null items.
     *
     * @param messages List of messages
     * @param <K> The map key type.
     * @param <M> The message type.
     * @param <V> The actual value type.
     * @return List of builders or null if null input.
     */
    public static <K, M extends PMessage<M>, V extends PMessageOrBuilder<M>>
    Map<K, M> toMessageValues(Map<K, V> messages) {
        return MessageUtil.toMessageValues(messages);
    }

    /**
     * Build the message from builder if it is not null.
     *
     * @param mob The builder to build.
     * @param <M> The message type.
     * @return The message or null if null input.
     */
    public static <M extends PMessage<M>>
    M toMessageIfNotNull(PMessageOrBuilder<M> mob) {
        return MessageUtil.toMessageIfNotNull(mob);
    }

    /**
     * Mutate the message if it is not null.
     *
     * @param mob Message or builder to mutate.
     * @param <M> The message type.
     * @param <B> The builder type.
     * @return The builder or null if null input.
     */
    public static <M extends PMessage<M>, B extends PMessageBuilder<M>>
    B toBuilderIfNonNull(PMessageOrBuilder<M> mob) {
        return MessageUtil.toBuilderIfNonNull(mob);
    }
}
