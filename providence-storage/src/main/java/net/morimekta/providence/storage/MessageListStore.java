package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.morimekta.providence.util.MessageUtil.toMessageAll;

/**
 * Interface for storing messages of a single type.
 *
 * @param <K> The key type.
 * @param <M> The stored message type.
 */
public interface MessageListStore<K, M extends PMessage<M>>
        extends MessageListReadOnlyStore<K, M>, ReadWriteStore<K, List<M>> {
    /**
     * Put messages into the map represented by their builders. Further
     * modifications to the builders will not be reflected onto the contents
     * of the store.
     *
     * @param key The key to put builders to.
     * @param builders The list of builders to put.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putBuilders(@Nonnull K key, @Nonnull List<B> builders) {
        put(key, toMessageAll(builders));
    }

    /**
     * Put messages into the map represented by their builders. Further
     * modifications to the builders will not be reflected onto the contents
     * of the store.
     *
     * @param builders Map of key to list of builders.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putAllBuilders(@Nonnull Map<K, List<B>> builders) {
        Map<K,List<M>> instances = new HashMap<>();
        builders.forEach((k,bList) -> instances.put(k, toMessageAll(bList)));
        putAll(instances);
    }
}
