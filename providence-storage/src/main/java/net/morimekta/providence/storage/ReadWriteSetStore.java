package net.morimekta.providence.storage;

import net.morimekta.util.collect.UnmodifiableSet;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Interface for a store that acts as a set of unique values with a natural key
 * per item. Note that it is only the setter methods that differs from a normal
 * read-write map store.
 *
 * @param <K> Key to use for gathering information.
 * @param <V> Value to fetch, that is a generic method.
 */
public interface ReadWriteSetStore<K, V> extends ReadOnlyStore<K, V> {
    /**
     * @param values Put all key value pairs form this map into the storage.
     */
    void putAll(@Nonnull Collection<V> values);

    /**
     * Remove the values for the given keys.
     * @param keys Map of removed key value pairs.
     */
    void removeAll(Collection<K> keys);

    /**
     * @param value The value to add.
     */
    default void put(@Nonnull V value) {
        putAll(UnmodifiableSet.setOf(value));
    }

    /**
     * Remove the key value pair from the store.
     *
     * @param key The key to remove.
     */
    default void remove(@Nonnull K key) {
        removeAll(UnmodifiableSet.setOf(key));
    }
}
