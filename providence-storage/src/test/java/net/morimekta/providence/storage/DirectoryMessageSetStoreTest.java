package net.morimekta.providence.storage;

import net.morimekta.providence.serializer.PrettySerializer;
import net.morimekta.providence.storage.dir.StringKeyFileManager;
import net.morimekta.test.providence.storage.OptionalFields;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class DirectoryMessageSetStoreTest extends TestBase {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testConformity() {
        try (DirectoryMessageSetStore<String, OptionalFields> store = new DirectoryMessageSetStore<>(
                new StringKeyFileManager(tmp.getRoot().toPath()),
                OptionalFields::getStringValue,
                OptionalFields.kDescriptor,
                new PrettySerializer().config())) {
            assertConformity(OptionalFields::getStringValue,
                             this::modifyNotKey,
                             store);
        }
    }

    private OptionalFields modifyNotKey(OptionalFields opt) {
        return opt.mutate()
                  .setLongValue(generator.context().getRandom().nextLong())
                  .setIntegerValue(generator.context().getRandom().nextInt())
                  .setByteValue((byte) generator.context().getRandom().nextInt())
                  .setShortValue((short) generator.context().getRandom().nextInt())
                  .build();
    }


    @Test
    public void testDirectoryStore() throws InterruptedException {
        List<OptionalFields> source = new ArrayList<>();
        Set<String>         keySet = new HashSet<>();
        for (int i = 0; i < 100; ++i) {
            OptionalFields of = generator.generate(OptionalFields.kDescriptor);
            source.add(of);
            keySet.add(of.getStringValue());
        }

        try (DirectoryMessageSetStore<String, OptionalFields> store = new DirectoryMessageSetStore<>(
                new StringKeyFileManager(tmp.getRoot().toPath()),
                OptionalFields::getStringValue,
                OptionalFields.kDescriptor,
                new PrettySerializer().config())) {
            store.putAll(source);
        }

        Thread.sleep(1);

        try (DirectoryMessageSetStore<String, OptionalFields> store = new DirectoryMessageSetStore<>(
                new StringKeyFileManager(tmp.getRoot().toPath()),
                OptionalFields::getStringValue,
                OptionalFields.kDescriptor,
                new PrettySerializer().config())) {
            assertThat(store.keys(), is(keySet));

            Set<String> random5 = new HashSet<>();
            source.forEach((v) -> {
                if (random5.size() < 5) {
                    random5.add(v.getStringValue());
                }
                assertThat(store.containsKey(v.getStringValue()), is(true));
                assertThat(store.get(v.getStringValue()), is(equalToMessage(v)));
            });
            for (int i = 0; i < 100; ++i) {
                assertThat(store.containsKey(UUID.randomUUID().toString()), is(false));
            }

            store.removeAll(random5);

            for (String k : random5) {
                assertThat(store.containsKey(k), is(false));
                assertThat(store.get(k), is(nullValue()));
            }
        }
    }
}
