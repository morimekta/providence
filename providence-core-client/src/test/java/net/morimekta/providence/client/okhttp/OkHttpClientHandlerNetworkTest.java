package net.morimekta.providence.client.okhttp;

import net.morimekta.providence.client.HttpClientConnectException;
import net.morimekta.providence.client.HttpResponseException;
import net.morimekta.providence.serializer.DefaultSerializerProvider;
import net.morimekta.providence.serializer.SerializerProvider;
import net.morimekta.test.providence.client.Failure;
import net.morimekta.test.providence.client.Request;
import net.morimekta.test.providence.client.TestService;
import net.morimekta.util.Strings;
import okhttp3.OkHttpClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * Test that we can connect to a thrift servlet and get reasonable input and output.
 */
@SuppressWarnings("deprecation")
public class OkHttpClientHandlerNetworkTest {
    private static final String ENDPOINT = "test";

    private int                port;
    private ExecutorService    executorService;
    private SerializerProvider provider;
    private ServerSocket       serverSocket;

    private AtomicBoolean flag;

    private URL endpoint() {
        try {
            return new URL("http://localhost:" + port + "/" + ENDPOINT);
        } catch (MalformedURLException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }

    @Before
    public void setUp() throws Exception {
        executorService = Executors.newSingleThreadExecutor();
        provider = new DefaultSerializerProvider();

        serverSocket = new ServerSocket();
        serverSocket.bind(new InetSocketAddress("localhost", 0));
        port = serverSocket.getLocalPort();

        serverSocket.setSoTimeout(200);
        flag = new AtomicBoolean(true);

        executorService.submit(() -> {
            while (flag.get()) {
                try {
                    Socket socket = serverSocket.accept();
                    Thread.sleep(1L);

                    // Do not read request.
                    socket.getOutputStream().write(
                            ("HTTP/1.1 403 Unauthorized\r\n" +
                             "\r\n" +
                             "\r\n").getBytes(StandardCharsets.UTF_8));
                    socket.close();
                } catch (SocketTimeoutException e) {
                    // ignore
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @After
    public void tearDown() {
        flag.set(false);

        try {
            executorService.shutdown();
            executorService.awaitTermination(1000L, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBrokenPipe() throws Failure, IOException {
        OkHttpClient factory = new OkHttpClient.Builder().build();
        TestService.Iface client = new TestService.Client(new OkHttpClientHandler(
                this::endpoint, factory, provider));

        try {
            // The request must be larger than the socket read buffer, to force it to fail the write to socket.
            client.test(new Request(Strings.times("request ", 1024 * 1024)));
            fail("No exception");
        } catch (HttpResponseException e) {
            assertThat(e.getStatusCode(), is(403));
            assertThat(e.getStatusMessage(), is("Unauthorized"));
        } catch (SocketException ex) {
            System.err.println("OkHttpClient does NOT handle broken pipe on request.");
            // ex.printStackTrace();
        }
    }

    @Test
    public void testSimpleRequest_unknownHost() throws IOException, Failure {
        OkHttpClient factory = new OkHttpClient.Builder().build();

        URL url = new URL("http://this-is-not-a-valid-domain-so-skip-it:" + port + "/" + ENDPOINT);
        TestService.Iface client = new TestService.Client(new OkHttpClientHandler(
                () -> url, factory, provider));

        try {
            client.test(new Request("request"));
            fail("No exception");
        } catch (UnknownHostException ex) {
            assertThat(ex.getMessage(), containsString("this-is-not-a-valid-domain-so-skip-it"));
        }
    }

    @Test
    public void testSimpleRequest_connectionRefused() throws IOException, Failure {
        OkHttpClient factory = new OkHttpClient.Builder().build();

        URL url = new URL("http://localhost:834/" + ENDPOINT);
        TestService.Iface client = new TestService.Client(new OkHttpClientHandler(
                () -> url, factory, provider));

        try {
            client.test(new Request("request"));
            fail("No exception");
        } catch (HttpClientConnectException ex) {
            // ex.printStackTrace();
            assertThat(ex.getMessage(), startsWith("Connect to localhost:834 [localhost/127.0.0.1"));
            assertThat(ex.getCause(), is(instanceOf(ConnectException.class)));
        }
    }
}
