package net.morimekta.providence.client;

import net.morimekta.providence.serializer.Serializer;

/**
 * Object parser for providence messages.
 *
 * @deprecated Use {@link net.morimekta.providence.client.google.ProvidenceObjectParser}.
 */
@Deprecated
@SuppressWarnings("unused")
public class ProvidenceObjectParser extends net.morimekta.providence.client.google.ProvidenceObjectParser {
    public ProvidenceObjectParser(Serializer serializer) {
        super(serializer);
    }
}
