/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */
package net.morimekta.providence.client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;

/**
 * An IO exception for HTTP response exceptions. Normalization between
 * client library exceptions, or lack thereof.
 *
 * @since 2.6
 */
public class HttpResponseException extends IOException {
    private static final long serialVersionUID = 0xa013ef97976d90b7L;
    @Nonnull
    private final URL    url;
    private final int    statusCode;
    @Nonnull
    private final String statusMessage;
    @Nullable
    private final String content;

    /**
     * Creates a HttpClientConnectException based on original {@link ConnectException}.
     *
     * @param url The URL tried to connect to.
     * @param statusCode The response code.
     * @param statusMessage The response status message.
     * @param content The response body, if any.
     * @param cause The cause of the exception, if any.
     */
    public HttpResponseException(
            @Nonnull URL url,
            int statusCode,
            @Nonnull String statusMessage,
            @Nullable String content,
            @Nullable IOException cause) {
        super("Http response not OK: " + statusCode + ": " + statusMessage, cause);
        this.url = url;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.content = content;
    }

    @Nonnull
    public URL getUrl() {
        return url;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nonnull
    public String getStatusMessage() {
        return statusMessage;
    }

    @Nullable
    public String getContent() {
        return content;
    }
}
