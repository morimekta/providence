package net.morimekta.providence.jax.rs.test_web_app;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import net.morimekta.providence.PApplicationException;
import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.test.providence.jax.rs.calculator.CalculateException;
import net.morimekta.test.providence.jax.rs.calculator.Calculator;
import net.morimekta.test.providence.jax.rs.calculator.Operand;
import net.morimekta.test.providence.jax.rs.calculator.Operation;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Test service implementation for jersey test app.
 */
@Path("/calculator")
public class TestCalculatorResource {
    private final Calculator.Iface impl;

    public TestCalculatorResource() {
        this.impl = new TestCalculator();
    }

    @GET
    public Response root() {
        return Response.noContent().build();
    }

    @POST
    @Path("/calculate")
    @ApiResponse(
            responseCode = "200",
            description = "Calculation result",
            content = @Content(
                    mediaType = APPLICATION_JSON,
                    schema = @Schema(
                            implementation = Operand.class)))
    @ApiResponse(responseCode = "400", description = "On bad requests")
    @ApiResponse(responseCode = "500", description = "On Internal errors")
    public Response postCalculate(
            @RequestBody(content = @Content(mediaType = APPLICATION_JSON)) Operation pOp) {
        try {
            return Response.ok()
                           .entity(impl.calculate(pOp))
                           .build();
        } catch (IOException e) {
            return Response.serverError()
                           .entity(new PApplicationException(
                                   e.getMessage(),
                                   PApplicationExceptionType.INTERNAL_ERROR))
                           .build();
        } catch (CalculateException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                           .entity(e)
                           .build();
        }
    }

    @GET
    @Path("/iamalive")
    public Response getIAmAlive() {
        try {
            impl.iamalive();
            // Do nothing.
            return Response.noContent()
                           .build();
        } catch (IOException e) {
            return Response.serverError()
                           .entity(new PApplicationException(
                                   e.getMessage(),
                                   PApplicationExceptionType.INTERNAL_ERROR))
                           .build();
        }
    }

    @POST
    @Path("/file")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @io.swagger.v3.oas.annotations.Operation(
            description = "Creates an application for tax form for review",
            requestBody = @RequestBody(
                    content = @Content(
                            mediaType = "multipart/form-data",
                            schema = @Schema(implementation = Operation.class),
                            encoding = {
                                    @Encoding(name = "file", contentType = "application/pdf", style = "", allowReserved = false, explode = false, extensions = {})
                            }
                    )
            )
    )
    @ApiResponse(
            responseCode = "200",
            description = "A tax form application",
            content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Operand.class))
            }
    )
    public Response postFile() {
        return null;
    }
}
