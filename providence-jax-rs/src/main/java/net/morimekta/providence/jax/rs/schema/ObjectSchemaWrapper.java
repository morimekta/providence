package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjectSchemaWrapper extends ObjectSchema {
    @JsonProperty("additionalProperties")
    public void setAdditionalProperties(Schema additionalProperties) {
        setAdditionalProperties((Object) additionalProperties);
    }

    @Override
    @JsonIgnore
    public void setAdditionalProperties(Object additionalProperties) {
        super.setAdditionalProperties(additionalProperties);
    }
}
