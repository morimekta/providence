package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllOfSchema extends Schema<AllOfSchema> {

    private List<Schema> allOf = new ArrayList<>();

    public AllOfSchema() {
        super(null, null);
    }

    public List<Schema> getAllOf() {
        return allOf;
    }

    public void setAllOf(List<Schema> allOf) {
        this.allOf = allOf;
    }

    public AllOfSchema allOf(List<Schema> allOf) {
        setAllOf(allOf);
        return this;
    }
}
