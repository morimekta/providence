/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v2.annotations;

import net.morimekta.providence.PEnumValue;
import net.morimekta.providence.descriptor.PEnumDescriptor;
import net.morimekta.providence.jdbi.v2.EnumValueMapper;
import org.skife.jdbi.v2.Query;
import org.skife.jdbi.v2.ResultColumnMapperFactory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizer;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizerFactory;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizingAnnotation;
import org.skife.jdbi.v2.tweak.ResultColumnMapper;

import javax.annotation.Nonnull;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@SqlStatementCustomizingAnnotation(RegisterEnumValueMapper.Factory.class)
@Repeatable(RegisterEnumValueMappers.class)
public @interface RegisterEnumValueMapper {
    Class<? extends PEnumValue<?>>[] value();
    boolean acceptUnknown() default false;

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        public SqlStatementCustomizer createForMethod(Annotation annotation, Class sqlObjectType, Method method) {
            return createForType(annotation, sqlObjectType);
        }

        @Override
        public SqlStatementCustomizer createForType(Annotation annotation, Class sqlObjectType) {
            RegisterEnumValueMapper        register  = (RegisterEnumValueMapper) annotation;
            PEnumDescriptor<?>             descriptor;
            List<ResultColumnMapperFactory>          factories   = new ArrayList<>();
            for (Class<? extends PEnumValue<?>> klass : register.value()) {
                descriptor = getDescriptor(klass);
                EnumValueMapper<?> mapper = new EnumValueMapper<>(register.acceptUnknown(), descriptor);
                factories.add(new ResultColumnMapperFactory() {
                    @Override
                    public boolean accepts(Class type, StatementContext ctx) {
                        return mapper.getType().equals(type);
                    }

                    @Override
                    public ResultColumnMapper columnMapperFor(Class type, StatementContext ctx) {
                        return mapper;
                    }
                });
            }
            return stmt -> {
                if (stmt instanceof Query) {
                    Query q = (Query) stmt;
                    for (ResultColumnMapperFactory factory : factories) {
                        q.registerColumnMapper(factory);
                    }
                }
            };
        }

        @Override
        public SqlStatementCustomizer createForParameter(Annotation annotation,
                                                         Class sqlObjectType,
                                                         Method method,
                                                         Object arg) {
            return stmt -> {};
        }

        @Nonnull
        static PEnumDescriptor getDescriptor(Class<?> register) {
            try {
                return Objects.requireNonNull(
                        (PEnumDescriptor) register.getDeclaredField("kDescriptor")
                                                  .get(null));
            } catch (IllegalAccessException | NoSuchFieldException | NullPointerException e) {
                throw new IllegalArgumentException(
                        "Not a valid providence enum class " + register.getName());
            }
        }
    }
}
