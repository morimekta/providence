/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v2.annotations;

import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.jdbi.v2.MessageNamedArgumentFinder;
import net.morimekta.providence.jdbi.v2.util.NullArgument;
import net.morimekta.util.collect.UnmodifiableMap;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.sql.Types;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Annotation to bind providence message objects as
 * bean-like arguments, but using the declared field names instead
 * of magic java bean names.
 */
@Target(value = ElementType.PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
@BindingAnnotation(BindMessage.Factory.class)
public @interface BindMessage {
    String value() default "";
    BindType[] types() default {};

    class Factory implements BinderFactory<BindMessage> {
        private AtomicReference<Map<PField, Integer>> fieldTypesInstance = new AtomicReference<>();
        private Map<PField, Integer> getFieldTypes(BindMessage annotation, PMessageDescriptor descriptor) {
            return fieldTypesInstance.updateAndGet(map -> {
                if (map != null) return map;
                map = new LinkedHashMap<>();

                for (BindType field : annotation.types()) {
                    PField<?> pf = descriptor.fieldForName(field.name());
                    map.put(pf, field.type());
                }

                return UnmodifiableMap.copyOf(map);
            });
        }

        @Override
        @SuppressWarnings("unchecked")
        public Binder<BindMessage, Object> build(BindMessage bind) {
            return (sqlStatement, annotation, o) -> {
                if (o instanceof PMessageOrBuilder) {
                    PMessageOrBuilder message = (PMessageOrBuilder) o;
                    Map<PField, Integer> ft = getFieldTypes(annotation, message.descriptor());
                    sqlStatement.bindNamedArgumentFinder(
                            new MessageNamedArgumentFinder(
                                    annotation.value(), message, ft));
                } else if (o == null){
                    sqlStatement.bind(annotation.value(), new NullArgument(Types.OTHER));
                } else {
                    throw new IllegalArgumentException("Not a message: " + o.getClass().toString());
                }
            };
        }
    }
}
