/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v2.annotations;

import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.jdbi.v2.MessageRowMapper;
import org.skife.jdbi.v2.Query;
import org.skife.jdbi.v2.ResultSetMapperFactory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizer;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizerFactory;
import org.skife.jdbi.v2.sqlobject.SqlStatementCustomizingAnnotation;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import static net.morimekta.providence.jdbi.v2.annotations.RegisterMessageMapper.Factory.getDescriptor;
import static net.morimekta.providence.jdbi.v2.annotations.RegisterMessageMapper.Factory.makeFieldMap;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@SqlStatementCustomizingAnnotation(RegisterMessageMappers.Factory.class)
public @interface RegisterMessageMappers {
    RegisterMessageMapper[] value();

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        @SuppressWarnings("unchecked")
        public SqlStatementCustomizer createForType(Annotation annotation, Class sqlObjectType) {
            RegisterMessageMappers      registers = (RegisterMessageMappers) annotation;
            Map<Type, MessageRowMapper> mappers   = new HashMap<>();
            for (RegisterMessageMapper register : registers.value()) {
                if (register.value().isInterface()) {
                    throw new IllegalArgumentException("Interfaces cannot be used as message return types");
                }
                PMessageDescriptor          descriptor = getDescriptor(register);
                Map<String, PField>         fieldMap   = makeFieldMap(descriptor, register);
                MessageRowMapper            mapper     = new MessageRowMapper(descriptor, fieldMap);
                mappers.put(register.value(), mapper);
            }
            return stmt -> {
                if (stmt instanceof Query) {
                    Query q = (Query) stmt;
                    q.registerMapper(new ResultSetMapperFactory() {
                        @Override
                        public boolean accepts(Class type, StatementContext ctx) {
                            return mappers.containsKey(type);
                        }

                        @Override
                        public ResultSetMapper mapperFor(Class type, StatementContext ctx) {
                            return mappers.get(type);
                        }
                    });
                }
            };
        }

        @Override
        public SqlStatementCustomizer createForMethod(Annotation annotation, Class sqlObjectType, Method method) {
            return createForType(annotation, sqlObjectType);
        }

        @Override
        public SqlStatementCustomizer createForParameter(Annotation annotation,
                                                         Class sqlObjectType,
                                                         Method method,
                                                         Object arg) {
            return stmt -> {};
        }
    }
}
