package net.morimekta.providence.jdbi.v2;

import net.morimekta.providence.PEnumValue;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.Argument;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class EnumValueArgument implements Argument {
    private final PEnumValue value;

    public EnumValueArgument(PEnumValue value) {
        this.value = value;
    }

    @Override
    public void apply(int position, PreparedStatement statement, StatementContext ctx) throws SQLException {
        if (value == null) {
            statement.setNull(position, Types.INTEGER);
        } else {
            statement.setInt(position, value.asInteger());
        }
    }
}
