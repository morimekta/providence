package net.morimekta.providence.tools.schema;

import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Option;
import net.morimekta.console.args.SubCommand;
import net.morimekta.console.args.SubCommandSet;
import net.morimekta.console.util.STTY;
import net.morimekta.providence.tools.common.CommonOptions;
import net.morimekta.providence.tools.schema.cmd.Command;
import net.morimekta.providence.tools.schema.cmd.GraphQL;
import net.morimekta.providence.tools.schema.cmd.Help;
import net.morimekta.providence.tools.schema.cmd.JsonSchema;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static net.morimekta.console.util.Parser.outputFile;
import static net.morimekta.console.util.Parser.path;
import static net.morimekta.providence.tools.common.formats.FormatUtils.getIncludeMap;

public class SchemaOptions extends CommonOptions {
    private SubCommandSet<Command> subCommandSet;

    public SchemaOptions(STTY tty) {
        super(tty);
    }

    @Override
    public ArgumentParser getArgumentParser(String prog, String description) throws IOException {
        ArgumentParser parser = super.getArgumentParser(prog, description);

        parser.add(new Option("--include", "I", "dir", "Include services from files in directory",
                              path(this::addInclude), null, true, false, false));
        parser.add(new Option("--out", "o", "spec", "Output Specification",
                              outputFile(this::setOut)));

        subCommandSet = new SubCommandSet<>("language", "Language to generate schema for", this::setCommand, "help");
        subCommandSet.add(new SubCommand<>("help", "Show help", () -> new Help(subCommandSet, parser), cmd -> cmd.parser(parser)));
        subCommandSet.add(new SubCommand<>("graphql", "Generate GraphQL schema", GraphQL::new, cmd -> cmd.parser(parser), "gql"));
        subCommandSet.add(new SubCommand<>("json-schema", "Generate json-schema", JsonSchema::new, cms -> cms.parser(parser), "json"));

        parser.add(subCommandSet);
        return parser;
    }

    public SubCommandSet<Command> getSubCommandSet() {
        return subCommandSet;
    }

    private void setOut(File out) {
        this.out = out.toPath();
    }

    private void addInclude(Path include) {
        this.includes.add(include);
    }

    private void setCommand(Command command) {
        this.command = command;
    }

    public Map<String, Path> getIncludes() throws IOException {
        return getIncludeMap(getRc(), includes, verbose());
    }

    public Path getOutput() {
        return out;
    }


    public Command getCommand() {
        return command;
    }

    private Path         out;
    private List<Path>   includes = new ArrayList<>();
    private Command command;
}
