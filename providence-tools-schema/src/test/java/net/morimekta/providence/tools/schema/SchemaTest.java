package net.morimekta.providence.tools.schema;

import net.morimekta.providence.tools.common.Utils;
import net.morimekta.testing.ResourceUtils;
import net.morimekta.testing.rules.ConsoleWatcher;
import net.morimekta.util.FileUtil;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SchemaTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Rule
    public ConsoleWatcher console = new ConsoleWatcher();

    public Schema schema = new Schema(console.tty()) {
        @Override
        protected void exit(int i) {
        }
    };

    @Test
    public void testHelp() {
        schema.run("--help");
        assertThat(console.error(), is(""));
        assertThat(console.output(),
                   is("Providence Schema Tool - " + Utils.getVersionString() + "\n" +
                      "Usage: pvd-schema [-hVv] [--rc FILE] [-I dir] [-o spec] [[help | graphql | json-schema] [...]]\n" +
                      "\n" +
                      "Example code to run:\n" +
                      "$ pvd-schema gql -I thrift/ -o calculator.graphqls -q cal.Calculator\n" +
                      "\n" +
                      " --help (-h, -?)    : This help listing.\n" +
                      " --verbose (-V)     : Show verbose output and error messages.\n" +
                      " --version (-v)     : Show program version.\n" +
                      " --rc FILE          : Providence RC to use (default: ~/.pvdrc)\n" +
                      " --include (-I) dir : Include services from files in directory\n" +
                      " --out (-o) spec    : Output Specification\n" +
                      " language           : Language to generate schema for (default: help)\n" +
                      "\n" +
                      "Available Commands:\n" +
                      "\n" +
                      " help        : Show help\n" +
                      " graphql     : Generate GraphQL schema\n" +
                      " json-schema : Generate json-schema\n"));
    }

    @Test
    public void testHelp_arg() {
        schema.run("help");
        assertThat(console.error(), is(""));
        assertThat(console.output(),
                   is("Providence Schema Tool - " + Utils.getVersionString() + "\n" +
                      "Usage: pvd-schema [-hVv] [--rc FILE] [-I dir] [-o spec] [[help | graphql | json-schema] [...]]\n" +
                      "\n" +
                      "Example code to run:\n" +
                      "$ pvd-schema gql -I thrift/ -o calculator.graphqls -q cal.Calculator\n" +
                      "\n" +
                      " --help (-h, -?)    : This help listing.\n" +
                      " --verbose (-V)     : Show verbose output and error messages.\n" +
                      " --version (-v)     : Show program version.\n" +
                      " --rc FILE          : Providence RC to use (default: ~/.pvdrc)\n" +
                      " --include (-I) dir : Include services from files in directory\n" +
                      " --out (-o) spec    : Output Specification\n" +
                      " language           : Language to generate schema for (default: help)\n" +
                      "\n" +
                      "Available Commands:\n" +
                      "\n" +
                      " help        : Show help\n" +
                      " graphql     : Generate GraphQL schema\n" +
                      " json-schema : Generate json-schema\n"));
    }

    @Test
    public void testGraphql() throws IOException {
        ResourceUtils.copyResourceTo("/graphql.providence", tmp.getRoot());
        schema.run("-I", tmp.getRoot().toString(), "graphql",
                   "-q", "graphql.MyQuery",
                   "-m", "graphql.MyMutation",
                   "-i", "graphql.Human:id,graphql.Droid:id,graphql.Character:id,graphql.MyQuery.heroIds.response:success");

        assertThat(console.error(), is(""));
        if ("true".equals(System.getenv("UPDATE_OPENAPI"))) {
            writeResource(console.output(), "/schema.graphql");
        } else {
            String resource = ResourceUtils.getResourceAsString("/schema.graphql");
            assertThat(console.output(), is(equalTo(resource)));
        }
    }

    @Test
    public void testJsonSchema() throws IOException {
        ResourceUtils.copyResourceTo("/graphql.providence", tmp.getRoot());
        schema.run("-I", tmp.getRoot().toString(), "json",
                   "-p", "graphql");
        assertThat(console.error(), is(""));

        if ("true".equals(System.getenv("UPDATE_OPENAPI"))) {
            writeResource(console.output(), "/default/json-schema.json");
        } else {
            String resource = ResourceUtils.getResourceAsString("/default/json-schema.json");
            assertThat(console.output(), is(equalTo(resource)));
        }
    }

    @Test
    public void testJsonSchema_withCompact() throws IOException {
        ResourceUtils.copyResourceTo("/graphql.providence", tmp.getRoot());
        schema.run("-I", tmp.getRoot().toString(), "json",
                   "-p", "graphql", "--allow-compact", "--allow-enum-id", "--complex-unions", "--verbose");
        assertThat(console.error(), is(""));

        if ("true".equals(System.getenv("UPDATE_OPENAPI")))
            writeResource(console.output(), "/with-compact/json-schema.json");
        else {
            String resource = ResourceUtils.getResourceAsString("/with-compact/json-schema.json");
            assertThat(console.output(), is(equalTo(resource)));
        }
    }

    private void writeResource(String content, String name) throws IOException {
        if (Files.isDirectory(Paths.get("providence-tools-schema"))) {
            Path path = FileUtil.readCanonicalPath(Paths.get(".")
                                                        .toAbsolutePath()
                                                        .resolve("providence-tools-schema/src/test/resources/" + name));
            Files.createDirectories(path.getParent());
            Files.write(path, content.getBytes(StandardCharsets.UTF_8), CREATE, TRUNCATE_EXISTING);
        } else {
            Path path = FileUtil.readCanonicalPath(Paths.get(".")
                                                        .toAbsolutePath()
                                                        .resolve("src/test/resources/" + name));
            Files.createDirectories(path.getParent());
            Files.write(path, content.getBytes(StandardCharsets.UTF_8), CREATE, TRUNCATE_EXISTING);
        }
    }
}
